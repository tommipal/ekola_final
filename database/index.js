const Database = require('./Database');
const MongoDatabase = require('./MongoDatabase');
const MariaDatabase = require('./MariaDatabase');

const mongo = new MongoDatabase(encodeURI(`mongodb://${process.env['MONGO_USER']}:${process.env['MONGO_PASSWORD']}@${process.env['MONGO_HOST']}/ekola`));
const maria = new MariaDatabase(encodeURI(`mysql://${process.env['MARIA_USER']}:${process.env['MARIA_PASSWORD']}@${process.env['MARIA_HOST']}/ekola`));

MongoDatabase.default = mongo;
MariaDatabase.default = maria;

exports.MongoDatabase = MongoDatabase;
exports.MariaDatabase = MariaDatabase;