const Database = require('./Database');
const mysql = require('mysql2/promise');
const { Utils, Logger } = require('common');

const CONNECTIONS = {};
const queryOptions = {
  //nestTables: true
}

//Wrapper around the query construction string, that does some validations and provides a consistent interface for simple queries
//if more advanced functionality is needed, I recommend using something like Knex
class SQLQuery {
  constructor(table, pk){
    if(!table) throw new Error('Table must be given to query constructor');
    this.table = table;
    this.pk = pk || 'id';
    this.params = [];
    this.selectStatement = `SELECT * FROM ${this.table}`;

    this._start = [];
    this._where = [];
    this._join = [];
    this._count = [];
    this._delete = []
    this._update = [];
    this._set = [];
    this._insert = [];
    this._modifiers = [];
  }

  get expectedParams(){
    let matches = this.toString().match(/\?{1,2}/gmi);
    return matches ? matches.length : 0;
  }

  static _objectToClause(obj){
    const compound = { clause: [], values: [] };
    for(let [key, val] of Utils.iterateObject(obj)){
      if(typeof val !== 'undefined') {
        compound.clause.push(`${key} = ?`);
        compound.values.push(val);
      }
    }
    return compound;
  }

  and(){
    this._whereword = 'AND';
    return this;
  }

  or() {
    this._whereword = 'OR';
    return this;
  }

  /**
   * Overrides current select statement and start statement if there is one.
   * @param {string} selectStatement
   */
  select(selectStatement){
    this.selectStatement = selectStatement;
    if(!!this._start.length) this._start = [this.selectStatement];
    return this;
  }

  where(clause){
    if(this._start.length === 0) this._start.push(this.selectStatement);
    if(!clause) return this;

    if(this._where.length > 0) this._where.push(`${this._whereword || 'AND'}(`)
    else this._where.push('WHERE(');

    if(Utils.legitObject(clause)){
      let compound = SQLQuery._objectToClause(clause);
      this._where.push(compound.clause.join(' AND '));
      this.values(...compound.values);
    } else if(Array.isArray(clause) && clause.every(sub=>Utils.legitObject(sub))) {
      const allCriteria = clause.map(sub => {
        let compound = SQLQuery._objectToClause(sub);
        return { clause: `(${compound.clause.join(' AND ')})`, values: compound.values };
      })
      this._where.push(allCriteria.map(cc=>cc.clause).join(' OR '));
      this.values(...allCriteria.reduce((acc, curr) => acc.concat(curr.values), []));
    } else if(typeof clause === 'string' || Number.isInteger(clause)) {
      //supports here "numbers" like "1", "2" and so on i.e. from requests because bodyparser can't cast those apparently
      if(typeof clause === 'string' && isNaN(clause)){
        this._where.push(clause);
      } else {
        this._where.push(`${this.pk} = ${clause}`);
      }
    } else {
      throw new Error('Clause of where statement must be an object, an array of objects, as string or an integer row id')
    }
    this._where.push(')');
    return this;
  }

  insert(clause){
    if(this._start.length === 0) this._start.push(`INSERT INTO ${this.table}`);
    this._insert.push(clause);
    return this;
  }

  /**
   * Add a join clause to the query. If this function is called at least once, results of the query will be nested by table name.
   * If only the first 3 parameters are specified, then table is treated as foriegn table and tableKey as a full join clause.
   * @param {String} joinType the type of this join (INNER JOIN, OUTER JOIN, etc.)
   * @param {String} table a table that is already in this query (either one the query was started with, or one that has already been joined before)
   * @param {String} tableKey the key on the existing table by which to join
   * @param {String} foreignTable the table which to join into the query
   * @param {String} foreignTableKey the key in the foright table by which to join
   * @returns {SQLQuery} this object for chaining
   */
  join(joinType, table, tableKey, foreignTable, foreignTableKey){
    const fkTable = foreignTableKey || this.table;
    if(this._start.length === 0) this._start.push(this.selectStatement);
    if(typeof tableKey === 'string' && typeof foreignTable === 'undefined') {
      //if the function is called with full join clause: .join('LEFT JOIN', 'join_other_stuff', 'join_order_stuff.stuff_id > memes.okidoki ...')
      this._join.push(`${joinType} ${table} ON ${tableKey}`);
    } else {
      //full function signature
      this._join.push(`${joinType} ${foreignTable} ON ${foreignTable}.${foreignTableKey} = ${table}.${tableKey}`);
    }

    return this;
  }

  update(clause){
    if(this._start.length === 0) this._start.push(`UPDATE ${this.table}`);
    this._update.push(clause);
    return this;
  }
  set(clause){
    if(this._set.length === 0) this._set.push('SET')
    this._set.push(clause);
    return this;
  }

  delete(){
    if(this._start.length === 0) this._start.push(`DELETE FROM ${this.table}`);
    else throw new Error('Did not start a delete query with a delete() call');
    return this;
  }

  modifiers(clause){
    if(typeof clause !== 'string') throw new Error('Modifiers must be a string');
    this._modifiers.push(clause);
    return this;
  }

  count(clause = `SELECT COUNT(*) as count FROM ${this.table}`){
    if(typeof clause !== 'string') throw new Error('Modifiers must be a string');
    this._start = [clause];
    return this;
  }

  values(...values){
    for(let value of values){
      if(typeof value === 'undefined') throw new Error('Prepared statement values can not be undefined');
      if(Utils.legitObject(value)){
        for(let key of Object.keys(value)){
          if(key.startsWith('_')) {
            delete value[key];
            continue;
          }
          if(typeof value[key] === 'undefined') throw new Error(`Prepared statement object had key ${key} which was undefined and this is not allowed`);
        }
        //expand object values into key = ? pairs for inserts and updates. This was not needed before because
        //using mysql.query expansion rules does the same thing, but with binary protocole, there is no automatic expansion
        if( ( this._insert.length > 0 && this._insert.some(ins=>ins.match(/SET/gmi)) ) ||
            ( this._update.length > 0 && this._update.some(upd=>upd.match(/SET/gmi)) ) ){
          let set = [];
          for(let [key, val] of Utils.iterateObject(value)){
            set.push(`${key} = ?`);
            if(Utils.legitObject(val)) this.params.push(JSON.stringify(val));
            else this.params.push(val);
          }
          if(this._insert.length>0) this._insert.push(set.join(', '));
          else this._update.push(set.join(', '));
          continue;
        }
      }
      if(Array.isArray(value) && value.length === 0){
        throw new Error('Can not pass empty arrays as query values, please an array value must have at least one element');
      }
      this.params.push(value);
    }
    return this;
  }

  toString(){
    if (this._start.length === 0) this._start.push(this.selectStatement);
    let query = [];
    query.push(this._start.join(' '));
    switch(query[0].split(' ')[0]){ //first word in query
      case 'SELECT':
      case 'DELETE':
      query.push(this._join.join(' '));
      query.push(this._where.join(' '));
      break;
      case 'UPDATE':
      query.push(this._update.join(' '));
      query.push(this._join.join(' '));
      query.push(this._set.join(' '));
      query.push(this._where.join(' '));
      break;
      case 'INSERT':
      query.push(this._insert.join(' '));
      break;
    }
    query.push(this._modifiers.join(' '));
    query = query.filter(clause => clause.length);

    return query.join('\n');
  }

  prepare(options){
    if(this.expectedParams !== this.params.length) throw new Error(`Query does not support this number of parameters:\nexpected: ${this.expectedParams}\ngot: ${this.params.length}\nquery so far:\n${this.toString()}`);
    if(!options) options = {};
    const final = this.toString();
    //nest tables on joined queries, since they have high chance at field collision, at least for IDs
    if(typeof options.nestTables === 'undefined' && this._join.length > 0) {
      options.nestTables = true;
    }
    if(this.expectedParams !== this.params.length) throw new Error(`Number of parameters(${this.params.length}) did not match the number of placeholders in the prepared query(${placeholders.length})!`);
    const query = Object.assign({ sql: final }, options);
    if(this.params.length === 0) return [ query ];
    else {
      const actualParams = Utils.flatten(this.params);
      if(this.params.some(pp=>Array.isArray(pp))){
        const r = /\(\s*\?{1}\s*\)/gmi;
        for(let i = 0; i<this.expectedParams; i++){
          if(Array.isArray(this.params[i])){
            query.sql = Utils.replaceNthOccurence(query.sql, r, i+1, `(${this.params[i].map(pp=>'?').join(', ')})`);
          }
        }
      }
      return [ query, actualParams ];
    }
  }
}

module.exports =
class MariaDatabase extends Database {
  constructor(url, inTransaction) {
    super(url, mysql);
    Logger.info(`Created a MariaDatabase instance ${inTransaction ? 'for a transaction' : `with a url ${url}`}`);
  }

  static get SQLQuery() {
    return SQLQuery;
  }

  //TODO:
  //strict mode - operate exactly with data provided, let mysql throw on schema errors. Also throw instead of just printing an error message in each method when result count is unexpected
  //relaxed mode - simply ignore (filter out) all fields that the table doesn't have and operate with reduced valid dataset(requires 1 extra query to fetch columns)
  async execute(...queries) {
    if(!queries.every(q=>q instanceof SQLQuery)) throw new Error('All queries must be instances of SQLQuery object');
    const autoRelease = !this.connection;
    const connection = await this.connect();

    const executing = [];
    for(let query of queries){
      //Logger.verbose(`Executing\n${query.toString()}\nwith ${query.params.length} parameters`);
      //testing this, if too many problems start switch back to connection.query
      //this makes MariaDB use LRU cache for frequent queries, which is a very useful optimization for our Permission system,
      //because this uses the binary protocole, it has some different behaviours. If it proves to be more trouble than it's worth
      //just switch back to old query.
      executing.push(connection.execute(...query.prepare()));
    }
    try {
      let result = await Promise.all(executing);
      if (autoRelease) connection.release();
      return result;
    } catch(err){
      Logger.error('Error performing a query. Queries(only first 10 will be shown):')
      Logger.error(queries.slice(0,10).map(qq=>qq.toString()).join('\n'));
      throw err;
    }
  }

  /**
   * Select data from a single table
   * @param {String} table table to select data from
   * @param {String} where this is really just the part of the query after "SELECT * FROM table WHERE" and can include things like ORDER BY and LIMIT
   * @param {*} params parameters if the query should be sent as a prepared statement
   * @returns {Array<Array<RowDataPacket|BinaryRow>>} result of the query, where each element corresponds to a row
   */
  async select(table, where, ...params) {
    const query = new SQLQuery(table).where(where).values(...params);
    const [results] = await this.execute(query);
    return results[0];
  }

  /**
   * Inserts data objects into the table. Each data object should represent a new row in the table
   * @param {String} table table to insert to
   * @param {Object[]|String} data data to insert
   * @returns {Array<Number>} An array of inserted IDs, where each id corresponds to a data element passed to the function.
   *                          If the primary key is an auto increment integer, otherwise a null would take it's place
   */
  async insert(table, ...data) {
    if(data.length === 0) throw new Error('Tried to insert empty dataset (no second argument on insert)');
    data = Utils.flatten(data);
    let queries = [];
    if(typeof data[0] === 'string') {
      let clause = data[0];
      data = data.slice(1);
    }
    for(let row of data){
      if(!Utils.legitObject(row) && !Array.isArray(row)) throw new TypeError(`One of objects to insert passed to db.insert method was not a serializable data object: ${row}`);
      queries.push(new SQLQuery(table).insert('SET').values(row));
    }
    const results = await this.execute(...queries);
    return results.map(rr=>rr[0].insertId);
  }

  /**
   * Updates the table's rows matching the where clause with the given data
   * @param {String} table table to update
   * @param {Object} data data to set row's values to
   * @param {String|Object|Number} where where clause @see MariaDatabase.select
   * @param {*} params parameters for prepared query
   * @returns {Number} number of affected rows
   */
  async update(table, data, where, ...params) {
    const query = new SQLQuery(table).update('SET').values(data).where(where);
    if(params) query.values(...params);
    const [results] = await this.execute(query);
    return results[0].affectedRows;
  }

  /**
   * Used to construct a separate where clause for multiple data objects to be used for updating different rows in the table
   * @callback WhereConstructor
   * @param {MariaDatabase.SQLQuery} query initial query
   * @param {Object} data row data
   * @param {Number} index the index of the row data in the given dataset
   */

  /**
   * Update using multiple conditions for each passed data object
   * @param {String} table table to update
   * @param {Array<Object>} data array of data objects each of which will be used with a separate query to update one or more rows
   * @param {WhereConstructor} whereFn the callback receiving SQLQuery object, data and index and which must return the query object that will be used to update with the particular data object passed to it
   * @returns {Number} number of affected rows
   */
  async updateMultiple(table, data, whereFn){
    if(!Array.isArray(data)) throw new Error('updateMultiple can only accept arrays of objects to update');
    if(typeof whereFn !== 'function') throw new Error('whereFn must be a function that sets the where condition for each record');
    const queries = data
      .map((dd, index)=>{
        let finalQuery = whereFn(new SQLQuery(table).update('SET ?').values(dd), dd, index);
        if(!(finalQuery instanceof SQLQuery)) throw new Error(`whereFn must set the where clause of the query object and return it. Returned instead ${finalQuery}`);
        return finalQuery;
      });
    const results = await this.execute(...queries);
    if(results.length !== data.length) Logger.error(`Results length on updateEach did not match the object count of ${data.length}, likely an error has occurred!`);
    return results.map(rr=>rr[0].affectedRows).reduce((t,c)=>t+c, 0);
  }

  /**
   * Deletes one or more records matching the provided where clause
   * @param {String} table table from which to delete
   * @param {String|Number|Object} where the where clause
   * @param {*} params parameters for prepared query
   * @returns {Number} number of deleted rows
   */
  async delete(table, where, ...params) {
    if(!where) throw new Error('Can not execute a delete statement without where clause, please use raw TRUNCATE query to empty tables.');
    const query = new SQLQuery(table).delete().where(where).values(...params);
    const [results] = await this.execute(query);
    return results[0].affectedRows;
  }

  /**
   * Returns the count of rows matching provided where clause or overall number of rows if the where clause is not provided
   * @param {String} table table from which to count
   * @param {String|Object|Number} where where clause
   * @param {*} params parameters for prepared query
   * @returns {Number} the number of rows matching provided where clause
   */
  async count(table, where, ...params) {
    const query = new SQLQuery(table).count();
    if(where) query.where(where);
    if(params) query.values(...params);
    const [results] = await this.execute(query);
    return results[0][0].count;
  }

  async connect() {
    if(this.dead) throw new Error(`This instance of MariaDatabase was cloned for a transaction and is now dead, please use original`);
    if(this.connection) return this.connection;
    if (!CONNECTIONS[this.url]) {
      let pool = mysql.createPool(this.url+'?multipleStatements=true&connectionLimit=100'); //to debug add &debug=[\'ComQueryPacket\'] to the url
      //let pool = mysql.createPool(this.url+'?multipleStatements=true&connectionLimit=100&debug=[\'ComQueryPacket\']'); //to debug add &debug=[\'ComQueryPacket\'] to the url
      CONNECTIONS[this.url] = pool;
    }
    return await CONNECTIONS[this.url].getConnection();
  }

  async disconnect() {
    if(!CONNECTIONS[this.url]) return;
    Logger.info(`Closing ${CONNECTIONS[this.url]}`);
    await CONNECTIONS[this.url].end();
    delete CONNECTIONS[this.url];
  }

  static async disconnect(){
    Logger.info('Closing all MariaDatabase connections for all instances');
    for (let [url, pool] of Utils.iterateObject(CONNECTIONS)) {
      Logger.info(`Closing ${url}`);
      await pool.end();
      delete CONNECTIONS[url];
    }
  }

  /**
   * Runs operations defined in the provided function callback in a transaction and commits it automatically, or rolls it back if any uncaught error is thrown
   * If an error occurs, it will be re-thrown after transaction is rolled back.
   * @param {MariaDatabase~transactionCallback} transactionFn function that runs operations in transaction and returns a promise that resolves/rejects when the transaction is complete/failed
   * @returns {*} return value of the transaction callback
   */
  transaction(transactionFn){
    if(typeof transactionFn !== 'function') return Promise.reject(new TypeError('Transaction takes a promise-returning function.'));
    //if already a transaction instance, just execute the function and don't do anything else
    //rethrow this, because its nested, and we must crash the outer transaction in order to trigger automatic rollback
    if(this.connection) return transactionFn(this, this.connection).catch(err=>{throw err});
    const transactionInstance = new MariaDatabase(this.url, true);
    return transactionInstance.connect()
      .then((connection)=>{
        return connection.beginTransaction().then(()=>{
          Logger.verbose('Starting new transaction');
          transactionInstance.connection = connection;
          return transactionFn(transactionInstance, connection)
              //propagate the result of user's callback to the end of chain
            .then((result)=>{
              Logger.verbose('Commiting transaction')
              return connection.commit().then(()=>[result, null])
            })
            .catch((err)=>{
              connection.rollback();
              Logger.error(`Rolled back transaction due to error:\n${err.stack}`);
              return [null, err];
            })
            //finally - release connection and mark the cloned instance used
            .then((result)=>{
              transactionInstance.connection.release();
              transactionInstance.connection = null;
              transactionInstance.dead = true;
              Logger.info('Destroyed a temporary transaction MariaDatabase instance');
              process.removeListener('exit', this.disconnect);
              process.removeListener('SIGINT', this.disconnect);
              process.removeListener('SIGUSR1', this.disconnect);
              process.removeListener('SIGUSR2', this.disconnect);
              if(!result[0] && result[1] instanceof Error) throw result[1];
              else return result[0];
            });
        })
      });
  }

  static get JOIN_EVALUATIONS_USERS(){
    return 'join_evaluations_users';
  }

  static get JOIN_EVALUATIONS_USERGROUPS(){
    return 'join_evaluations_usergroups';
  }
}