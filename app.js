const Koa = require('koa');
const CSRF = require('koa-csrf');
const bodyparser = require('koa-body');
const jwt = require('koa-jwt');
const serve = require('koa-static');
const path = require('path');
const webpack = require('koa-webpack');
const Router = require('koa-router');
const session = require('koa-session');

const common = require('common');
const {MariaDatabase, MongoDatabase} = require('database');
const models = require('models');
const controllers = require('controllers');
const middleware = require('middleware');

const { config, Logger, Utils, webpackConfig } = common;
const { Responder } = middleware;

const app = new Koa();
const router = new Router();
//app.proxy = true; //this is needed if running from behind a reverse proxy

//more info https://github.com/shellscape/koa-webpack
if(config.env !== 'production'){
  app.use(webpack({ config: webpackConfig }));
  if(!process.env['NO_STATIC'])
    app.use(serve(path.join(config.appRoot, 'client', 'dist')));
} else if(process.env['DISABLE_AUTHENTICATOR']) {
  throw new Error('Authenticator must be enabled in production please remove DISABLE_AUTHENTICATOR environment variable');
}

app.keys = [ 'todo', 'from-env' ];

app.use(session(config.session, app));
app.use(Responder.middleware);
app.use(bodyparser({ multipart: true }));

/**
 * Because koa middleware has no established convention of where to store request parameters,
 * this function uses Javascript getters and setters to aggregate all parameters parsed by the router,
 * koa body middleware and so on behind ctx.params. If other body parser is installed it's parameters
 * should be aggregated from their respective fields using this function.
 * TODO: move this to a separate middleware, isntead of inlining it here
 */
app.use(async function(ctx, next){
  Object.defineProperties(ctx,{
    _params: {
      value: {},
      writable: false,
      configurable: false,
      enumerable: false
    },
    params: {
      configurable: false,
      enumerable: true,
      get(){
        let params;
        if(ctx.request.type === 'application/json' || !Object.keys(this.request.body).every(key => key === 'fields' || key === 'files')){
          //poor man's json type request detection - if the bodyparser didn't create a standard structure or the type is explicitly
          //application/json, then aggregate from ctx.request.body
          params = Object.assign({}, this._params, this.request.query, this.request.body);
        } else {
          //for everything else aggregate from bodyparser's ctx.request.body.fields
          params = Object.assign({}, this._params, this.request.query, this.request.body.fields);
        }
        const entries = Object.entries(params);
        if(entries.length === 0) return {};
        const parsed = {};
        for (let [path, value] of entries) {
          if (!path.includes('.')) {
            Object.assign(parsed, {
              [path]: value
            });
            continue;
          }
          const obj = {};
          const segments = path.split('.');
          segments.reduce((final, current, index) => {
            let segment = {[current]: (index === segments.length-1 ? value : {})};
            Object.assign(final, segment);
            return segment[current];
          }, obj);
          Utils.mergeDeep(parsed, obj);
        }
        return parsed;
      },
      set(param){
        if(Utils.legitObject(param)) Object.assign(this._params, param);
        else this._params = param;
        return true;
      }
    },
    getParams: {
      configurable: false,
      enumerable: false,
      writable: false,
      value: function(){
        return this.params;
      }
    }
  });
  await next();
});

if (config.env === 'production') app.use(new CSRF(config.csrf));

controllers.load(app);

async function bootstrap(){
  Logger.info('Running bootstrap');
  try {
    const result = await MariaDatabase.default.transaction(async function(db, connection){
      //create public userGroup;
      const publicgroup = await UserGroup.find({ name: 'public', description: 'none' });
      if(!publicgroup){
        const group = new UserGroup({name: 'public', description: 'none'}).lock();
        await group.save();
      }
    });
  } catch (error){
    Logger.error(error);
    process.exit(1);
  }
  app.listen(3000);
  Logger.info('Application running on port 3000');
}

bootstrap();