"use strict";

//growl is a plugin that outputs notifications in Windows 10 about some task being complete. Useless in this case - it is mainly for when you have some really long tasks and you go do something while they are running
//At some point here we might have to actually do data optimizations, such as database restructure, backup, scaling, etc. It might be useful then, but not now.
const enableGrowl = false;
const gulp = require('gulp');
const rimraf = require('rimraf');
const path = require('path');
//Gulp is based on plugins so instead of writing 20 require statements here we include them dynamically using the gulp plugin naming convention (I hope no dependencies break it)
//Remember to ALWAYS check your dependencies (whether they are maintained, when was last commit, supported platforms, etc.)
const plugins = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'merge-*', 'run-*', 'main-*'], // the glob to search for
  replaceString: /\bgulp[\-.]|run[\-.]|merge[\-.]|main[\-.]/,
  // what to remove from the name of the module when adding it to the context
  camelizePluginName: true, // if true, transforms hyphenated plugins names to camel case
  lazy: true // whether the plugins should be lazy loaded on demand
});
//pipeline contains lists of javascript/jst(templates)/css dependencies. It is based on COMPILED dependencies (i.e. AFTER they have gone through sass/babel/concat and so on)
const pipeline = require('./tasks/pipeline');
//automatic dependency tagger config is bulky and ugly due to support for multiple templating languages, so I put it in a separate file. Everything else is inlined below
const linker = require('./tasks/sails-linker-config');
const _ = require('lodash');

//some hook is holding the process, need to explicitly exit here,
//I can't figure out which is it (suspect either include-all or gulp-sequence)
gulp.doneCallback = function (err) {
  process.exit(err ? 1 : 0);
};

function inProduction() { return process.env['NODE_ENV'] === 'production'; }

function babel(){
  let baseDir = inProduction() ? 'www/js/' : 'assets/js/'; //where to take the source from
  let sourceGlob = [baseDir+'**/*.js', '!'+baseDir+'dependencies/**/*.js'];
  return gulp.src(sourceGlob)
    .pipe(plugins.babel({presets: ['es2015']}))
    .pipe(gulp.dest(inProduction() ? (file)=>file.base : '.tmp/public/js')) //in production modify file in place, otherwise output to .tmp/public
    .pipe(plugins.livereload())
    .pipe(plugins.if(enableGrowl, plugins.notify({ message: 'babel compilation complete' })));
}

function clean(cb) {
  return rimraf(inProduction() ? 'www' : '.tmp/public', cb);
}

function scripts(cb) {
  //process our scripts here. This also runs babel if in production, so keep that in mind (i.e. DONT include babel function in production task)
  //If you want to use TypeScript, add the task here also, so production gets your compiled TS files.
  let dest = inProduction() ? 'www/js' : '.tmp/public/js';
  let tasksToRun = [];

  //concatenate all module dependencies into a separate file to avoid namespace collision (copy task will skip this for production)
  let taskId = _.uniqueId('vendor-js');
  let src = [];
  pipeline.modulesToCopy.forEach((module)=>{
    if(!module.dest.includes('styles/')) src.push(module.src);
  });
  src = _.flatten(src)
  gulp.task(taskId, function(){
    return gulp.src(src).pipe(plugins.concat('vendor.js')).pipe(gulp.dest(dest+'/vendor'));
  });
  tasksToRun.push(taskId);

  //concatenate all manual dependencies under assets/js/dependencies folder
  taskId = _.uniqueId('dep-js');
  gulp.task(taskId, function(){
    return gulp.src('assets/js/dependencies/**/*.js')
      .pipe(plugins.babel({presets:['es2015'], plugins: ["transform-regenerator"]}))
      .pipe(plugins.concat('dependencies.js'))
      .pipe(gulp.dest(dest+'/dependencies'));
  });
  tasksToRun.push(taskId);
  //finally process our own javascript, and concatenate if we are in production
  taskId = _.uniqueId(('js-global'));
  gulp.task(taskId, function(){
    return gulp.src(['assets/js/**/*.js', '!assets/js/dependencies/**/*.js', '!assets/js/pages/**/*.js'])
      .pipe(plugins.babel({presets:['es2015'], plugins: ["transform-regenerator"]}))
      .pipe(plugins.if(inProduction(), plugins.concat('app.js')))
      .pipe(gulp.dest(dest));
  });
  tasksToRun.push(taskId);
  //process page-specific javascript
  taskId = _.uniqueId(('js-pages'));
  gulp.task(taskId, function(){
    return gulp.src(['assets/js/pages/**/*.js'], {base:'assets/js'})
      .pipe(plugins.babel({presets:['es2015'], plugins: ["transform-regenerator"]}))
      .pipe(gulp.dest(dest));
  });
  tasksToRun.push(taskId);

  //run them in series for injector to work correctly
  return gulp.series(tasksToRun)(cb);
}

//copies static assets from asset folder to corresponding static serve folder (e.g. favicon)
//but NOT javascript, source scss and images
function copy() {
  let destGlob = inProduction() ? 'www' : '.tmp/public';
  let src = pipeline.modulesToCopy
    .filter(m=>m.dest.includes('styles/'))
    .concat(['assets/**/*.!(scss)', '!assets/images{,/**}', '!assets/js/**/*.js', '!assets/templates/**/*'])
    .map(m=>typeof m === 'string' ? m : m.src);
  return gulp.src(src, {base: 'assets'})
    .pipe(gulp.dest(destGlob))
    .pipe(plugins.if(enableGrowl, plugins.notify({message: 'Copy task complete'})));
}

function images(){
  let destGlob = inProduction() ? 'www/images' : '.tmp/public/images';
  return gulp.src('assets/images/**/*')
  //pipe(plugins.someImageOptimizationPluginIDunnoWhatevs()) <-- I dont know if we need this, but if we ever do, this is where to put it
    .pipe(gulp.dest(destGlob))
    .pipe(plugins.if(enableGrowl, plugins.notify({ message: 'Images task complete' })));
}

function templates(){
  let destGlob = inProduction() ? 'www' : '.tmp/public';
  return gulp.src(pipeline.templateFilesToInject)
    .pipe(plugins.ejs({},{
        client: true,
        strict: true,
        root: process.cwd()+'/'+destGlob+'/',
        compileDebug: !inProduction()
      },{ ext: 'js', suffix: 'Template', name: function(file){
        return file.basename.substring(0, file.basename.length - 4)
          .replace(/\s(.)/g, function($1) { return $1.toUpperCase(); })
          .replace(/-(.)/g, function($1) { return $1.toUpperCase(); })
          .replace(/_(.)/g, function($1) { return $1.toUpperCase(); })
          .replace(/\.(.)/g, function($1) { return $1.toUpperCase(); })
          .replace(/[\s-_.]/g, '')
          .replace(/^(.)/, function($1) { return $1.toLowerCase(); });
      } }))
    .pipe(plugins.concat('templates.js'))
    .pipe(plugins.babel({presets:['es2015']}))
    .pipe(gulp.dest(destGlob))
    .pipe(plugins.if(enableGrowl, plugins.notify({ message: 'templates task complete' })));
}

function reload(){
  return plugins.livereload.reload();
}

function sass(){
  let destGlob = inProduction() ? 'www/styles/' : '.tmp/public/styles/';
  return gulp.src('assets/styles/index.scss')
    .pipe(plugins.if(inProduction(), plugins.rename({basename: 'app'})))
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.sass({
      outputStyle: 'compressed',
      ext: 'css',
      includePaths: ['node_modules/foundation-sites/scss', 'node_modules/motion-ui/src']
    }))
    .pipe(plugins.autoprefixer('last 2 version', 'safari 5', 'ie 10', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(plugins.sourcemaps.write())
    .pipe(gulp.dest(destGlob))
    .pipe(plugins.livereload())
    .pipe(plugins.if(enableGrowl, plugins.notify({ message: 'sass task complete' })));
}

function sync(){
  let destGlob = inProduction() ? 'www' : '.tmp/public';
  return gulp.src(['assets/**/*.!(scss)', '!assets/images{,/**}', '!assets/js/**/*.js'])
    .pipe(plugins.changed(destGlob))
    .pipe(gulp.dest(destGlob))
    .pipe(plugins.if(enableGrowl, plugins.notify({ message: 'Sync task complete' })));
}

function watch(){
  plugins.livereload.listen();

  // Watch API files
  // NOTE This watcher is set-up by the sails-hook-autoreload NPM package

  gulp
    .watch(['assets/**/*', 'tasks/pipeline.js', 'views/**/templates/**'], gulp.series(gulp.parallel(scripts, sass, images, templates), linker, sync))
    .on('error', function (err) {
      console.error(err.toString());
      this.emit('end');
    });
  gulp.watch(['views/*', 'views/**/*'], reload).on('error', function (err) {
    console.error(err.toString());
    this.emit('end');
  });
}
//register the watch task globally for the sails hook to use it
gulp.task('watch', gulp.series(watch));

//Default task will compile scss and babel, copy dependencies and other static files, then run the images through compression and finally injections. Then it will watch for changes
gulp.task('default', gulp.series(clean, gulp.parallel(scripts, templates, sass), copy, gulp.parallel(images, linker), watch));
//Production task will do the same as default, but before it will clean the output directory and process js files (concatenate, transpile and all that crap).
gulp.task('prod', gulp.series(clean, gulp.parallel(scripts, templates, sass), copy, gulp.parallel(images, linker)));
