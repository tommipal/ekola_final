const jwt = require('jsonwebtoken');
const moment = require('moment');
const { config, Logger, Utils } = require("common");
const { ManagedModel } = require("models");

/**
 * This class can contain your authentication mechanisms
 * In controllers, when declaring routes simply use the notation
 * router.get('/mycontroller/action', Authenticator.login, MyController.action);
 */
class Authenticator {
  constructor() {
    throw new Error('Authenticator is a static abstract class'); //haters gonna hate
  }

  /**
   * Try to check if the requested object exists with permissions.
   */
  static async _exists(ctx) {
    const permissions = await Permission.where('targetId = ? AND targetModel = ?', ctx.params.id, Authenticator._getTargetFromPrefix(ctx.path));
    if (!permissions.length) throw new NotFoundError(`Requested ${Authenticator._getTargetFromPrefix(ctx.path)} with id ${ctx.params.id} doesn't exist!`);
  }

  static async _getPermission(ctx){
    await Authenticator.login(ctx);
    const permission = await Permission.find('user = ? AND targetId = ? AND targetModel = ?', ctx.session.user.id, ctx.params.id, Authenticator._getTargetFromPrefix(ctx.path));
    if(!permission) throw new AuthorizationError(`No permission to ${Authenticator._getTargetFromPrefix(ctx.path)}: ${ctx.params.id} found for ${ctx.session.user.username}: ${ctx.session.user.id}`);
    return permission;
  }

  static _getTargetFromPrefix(path){
    const match = path.match(/^(?:\/?)\w+(?:\/?)/);
    if(match){
      const [prefix] = match;
      if(!prefix || prefix.length < 3) throw new RequestError(`${prefix} is not a valid prefix`);
      return prefix.replace(/\//g, '').replace(/^[a-z]/, function(char) { return char.toUpperCase(); });
    }
    throw new RequestError(`No valid prefix found in ${path}`);
  }

  static async canAccessAnswers(ctx, next){
    if(process.env['DISABLE_AUTHENTICATOR']) return await next();
    Logger.verbose('Authenticator: canAccessAnswers');

    if(isNaN(ctx.params.sessionId)) throw new RequestError(`Expected an id, got '${ctx.params.sessionId}'`);
    const session = await SurveySession.find(ctx.params.sessionId);
    if(!session) throw new NotFoundError(`Requested session with id ${ctx.params.id} doesn't exist!`);

    if(ctx.session.user && ctx.session.user.id === session.user) return await next();
    if(ctx.session.surveySession && ctx.session.surveySession == session.id) return await next();

    throw new AuthorizationError(`Current user ${ctx.session.user ? ctx.session.user.id : ''} doesn't have permission to access answers of session ${session.id}`);
  }

  static async canAnswerSurvey(ctx, next){
    if(process.env['DISABLE_AUTHENTICATOR']) return await next();
    Logger.verbose('Authenticator: canAnswerSurvey');

    if(!ctx.params.link){
      await Authenticator.login(ctx);
      if(isNaN(ctx.params.id)) throw new RequestError(`Expected an id, got '${ctx.params.id}'`);
      const survey = await Survey.find({id: ctx.params.id});
      if(!survey) throw new NotFoundError(`Requested survey with id ${ctx.params.id} doesn't exist`);
      if(!survey.open) throw new SurveyNotActiveError(`Survey ${survey.id} is closed`, survey);
      if(!survey.evaluation) throw new RequestError('Survey has no evaluation selected!');
      const user = await User.find(ctx.session.user.id);
      const allowedGroups = await new Evaluation(survey.evaluation).myUserGroups;
      const userEvaluations = await user.myEvaluations;

      //Allow answering if user already has a valid session with the survey, but is no longer part if the target group or evaluation
      const sessions = await user.mySessions;
      if(sessions.some(session => session.open && session.survey === survey.id)) return await next();

      if(!userEvaluations.some((e)=>e.id===survey.evaluation)) throw new AuthorizationError('You are not participating in this evaluation');
      if(allowedGroups.some(group => group.id === user.userGroup)) return await next();
      throw new RequestError(`User's active group is not part of survey's evaluation.`);
    } else {
      const link = await Link.find({key: ctx.params.link });
      if(link === null) throw new NotFoundError('Invitation link is invalid');
      const survey = await Survey.find(link.survey);
      //TODO: make the session here right away or leave it to controller???
      if(!survey) throw new NotFoundError('The invitation link points to a survey that does not exist!');
      if(!survey.open) throw new SurveyNotActiveError(`Survey ${survey.id} is closed`, survey);
      return await next();
    }
  }

  static async canDelete(ctx, next){
    if(process.env['DISABLE_AUTHENTICATOR']) return await next();
    Logger.verbose('Authenticator: canDelete');
    await Authenticator._exists(ctx);
    const permission = await Authenticator._getPermission(ctx);
    if(!permission.permissions.delete) throw new AuthorizationError(`${ctx.session.user.name} does not have delete permission on ${Authenticator._getTargetFromPrefix(ctx.path)} with id ${ctx.params.id}`);
    return await next();
  }

  static async canSaveAsTemplate(ctx, next) {
    if (process.env['DISABLE_AUTHENTICATOR']) return await next();
    Logger.verbose('Authenticator: canSaveAsTemplate');
    await Authenticator.canEdit(ctx);
    const survey = await Survey.find(ctx.params.id);
    const template = await SurveyTemplate.initialize(survey.serialize());
    if (!template.id) return await next();

    const permissions = await template.myPermissions;
    if (permissions.some(permission => permission.user.id === ctx.session.user.id && permission.permissions.edit)) return await next();
    throw new ValidationError(`User ${ctx.session.user.id} doesn't have permission to edit template ${template.id}`);
  }

  static async canEdit(ctx, next){
    if(process.env['DISABLE_AUTHENTICATOR']) return await next();
    Logger.verbose('Authenticator: canEdit');
    await Authenticator._exists(ctx);
    const permission = await Authenticator._getPermission(ctx);
    if(!permission.permissions.edit) throw new AuthorizationError(`${ctx.session.user.username} does not have edit permission on ${Authenticator._getTargetFromPrefix(ctx.path)} with id ${ctx.params.id}`);

    if (typeof next === 'function') return await next();
  }

  static async canView(ctx, next) {
    if(process.env['DISABLE_AUTHENTICATOR']) return await next();
    await Authenticator._exists(ctx);
    const permission = await Authenticator._getPermission(ctx);
    if(!permission.permissions.view) throw new AuthorizationError(`User ${ctx.session.user.id} does not have view permission on ${Authenticator._getTargetFromPrefix(ctx.path)} with id ${ctx.params.id}`);
    Logger.verbose('Authenticator: canView');
    return await next();
  }

  static async canSaveAnswers(ctx, next) {
    if (process.env['DISABLE_AUTHENTICATOR']) return await next();
    Logger.verbose('Authenticator: canSaveAnswers');
    const session = await SurveySession.find(ctx.params.sessionId);
    if (!session) throw new NotFoundError(`Requested session with id ${ctx.params.sessionId} doesn't exist!`);
    if (!session.open) throw new RequestError(`Requested session with id ${sessionId} is closed. Can't save answers to a closed session!`);

    const survey = await Survey.find(session.survey);
    if (!survey) throw new NotFoundError(`Requested survey with id ${session.survey} doesn't exist!`);

    const endDate = moment(survey.endDate).utc(true);
    const startDate = moment(survey.startDate).utc(true);
    const now = moment().utc();
    if (now.isBefore(startDate)) throw new RequestError(`Can't save answers to a closed survey! Survey with id ${survey.id} is opens at ${startDate.format('YYYY-MM-DD HH:mm')}. It's now ${now.format('YYYY-MM-DD HH:mm')}`);
    if (now.isAfter(endDate)) throw new RequestError(`Can't save answers to a closed survey! Survey with id ${survey.id} has already closed at ${endDate.format('YYYY-MM-DD HH:mm')}. It's now ${now.format('YYYY-MM-DD HH:mm')}`);

    if (ctx.session.user && ctx.session.user.id === session.user) return await next();
    // If user is using quick access req.session holds surveySession (session.id)
    if (ctx.session.surveySession && ctx.session.surveySession == session.id && !session.user) return await next();
    throw new Error(`No permission to save answers to session ${session.id} for user ${ctx.session.user}..`);
  }

  static async canViewResults(ctx,next){
    if(process.env['DISABLE_AUTHENTICATOR']) return await next();
    Logger.verbose('Authenticator: canViewResults');

    const survey = await Survey.find(ctx.params.surveyId);
    if(!survey) throw new NotFoundError(`Requested survey with id ${session.survey} doesn't exist!`);
    const session = await SurveySession.find({survey: survey.id, user: ctx.session.user.id});
    //survey.openResults is true and the user participated in it (has a SurveySession)
    if(session && survey.openResults) return await next();

    //has view or surveyViewResults (merge these two?) permission
    const permissions = await survey.myPermissions;
    if (permissions.find(permission => permission.user.id === ctx.session.user.id && permission.permissions.view)) return await next();
    throw new AuthorizationError(`User ${ctx.session.user.id} has no permission to view results of survey ${survey.id}`);
  }

  static async login(ctx,next){
    if((!ctx.session.user || !ctx.session.user.id) && process.env['DISABLE_AUTHENTICATOR']){
      Logger.warn('DEVELOPMENT: SESSION MISSING - FILLING IN AS RANDOM USER; DISABLE IN PRODUCTION!');
      ctx.session.user = Utils.selectRandom(await User.where());
      Logger.warn(`LOGGED IN AS USER ${ctx.session.user.username}`);
      ctx.session.save();
    } else {
      if(!ctx.session.user) throw new AuthorizationError('This action requires user to be logged in');
      if(!ctx.session.user.id) throw new Error('Session corrupted, please relogin');
    }
    if(typeof next === 'function') return await next();
  }
}

if(process.env['DISABLE_AUTHENTICATOR']) Logger.warn(`AUTHENTICATOR IS IN PASSTHROUGH MODE`);

module.exports = Authenticator;
