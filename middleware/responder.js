const pug = require('pug');
const path = require('path');
const ejs = require('ejs');
const fs = require('fs');
const { Model } = require('models');
const { Readable } = require('stream');
const LRU = require('lru-cache');

const { config, Logger, Utils } = require('common');
const Translator = require('../client/scripts/translator').Translator;
const moment = require('moment');

ejs.cache = LRU(100);

/**
 * This place is the end of all things. And the beginning.
 * Well actually, it just sets up every new request context with helpers,
 * does request analysis, catches all thrown errors and renders the response.
 *
 * No big deal really...
 */
class Responder {
  constructor(ctx){
    Object.defineProperties(ctx,{
      view: {
        value: this.view.bind(this),
        writable: false,
        configurable: false,
        enumerable: false
      },
      json: {
        value: this.json.bind(this),
        writable: false,
        configurable: false,
        enumerable: false
      },
      raw: {
        value: this.raw.bind(this),
        writable: false,
        configurable: false,
        enumerable: false
      },
      websocket: {
        value: this.websocket.bind(this),
        writable: false,
        configurable: false,
        enumerable: false
      },
      flash: {
        value: this.flash.bind(this),
        writable: false,
        configurable: false,
        enumerable: false
      },
      wantsJSON: {
        writable: false,
        configurable: false,
        enumerable: false,
        value: function(){
          //TODO: this reliable?
          if(this.headers['X-Requested-With'] === 'XMLHttpRequest') return true;
          if(this.headers['content-type'] === 'application/json') return true;
          if(this.is('application/json')) return true;
          if(this.accepts('json') && !this.accepts('html')) return true;
          return false;
        }
      }
    });
    this.context = ctx;
  }

  get allViews(){
    if(!this._allViews){
      this._allViews = Utils.buldFileList(this.viewsFolder);
    }
    return this._allViews;
  }

  get viewsFolder(){
    return path.join(config.appRoot, 'views');
  }

  get pugOptions(){
    const options = {
      Utils, //expose util functions to the templates
      ctx: this.context //expose context to views
    }
    if(this.viewToRender && config.env === 'production') options.cache = true;
    if(options.cache) options.filename = path.basename(this.viewToRender);
    return options;
  }

  get ejsOptions(){
    const options = {
      locals: {
        Utils, //expose util functions to the templates
        ctx: this.context
      },
      _with: true,
      views: [path.join(config.appRoot, 'views')]
    }
    if(this.viewToRender && config.env === 'production') options.cache = true;
    if(options.cache) options.filename = path.basename(this.viewToRender);
    return options;
  }

  sanitize(object){
    if(Utils.isBasicType(object)) return object;
    const safe = {};
    for(let [name, val] of Utils.iterateObject(object)){
      if(name.startsWith('_')) continue;
      switch(name){
        case 'password':
        //don't send
        //TODO: any other unsafe fields here
        break;
        default:
        safe[name] = val;
        break;
      }
    }
    return safe;
  }

  get data(){
    if(this.unsafe) return this._data;
    else if(Array.isArray(this._data)) return this._data.map(dd=>this.sanitize(dd));
    else return this.sanitize(this._data);
  }
  set data(data) {
    if(Array.isArray(data)) {
      this._data = data.map(dd=>Utils.convertRecursive(dd, (val, key, parent)=>(val instanceof Model ? val.serialize(true, true) : val)));
    } else {
      this._data = Utils.convertRecursive(data, (val, key, parent)=>(val instanceof Model ? val.serialize(true, true) : val));
    }
  }

  view(view, data){
    if(view.includes('/views'))
      this.viewToRender = path.relative(this.viewsFolder, view);
    else
      this.viewToRender = view;
    this.data = Object.assign({}, this.data, data);
  }

  json(data){
    this.data = data;
  }

  raw(stringBufferOrStream){
    if(stringBufferOrStream instanceof Buffer || stringBufferOrStream instanceof Readable || typeof stringBufferOrStream === 'string')
      this.data = stringBufferOrStream;
    else Logger.error('Can not send raw response - not a buffer, string or readabale stream');
  }

  websocket(){
    throw new Error('Not implemented');
  }

  flash(flash){
    if(typeof flash !== 'string') throw new Error(`Flash message must be a string, was ${typeof flash}`);
    if(!this.data) this.data = {flash};
    this.data.flash = flash;
  }

  render(ctx){
    if(!this.data) this.data = {};
    if(ctx.csrf) this.data.csrf = ctx.csrf;
    if(this.viewToRender){
      ctx.body = this.renderFile(this.viewToRender, this.data);
      return;
    }
    if(this.data instanceof Buffer || this.data instanceof Readable || typeof this.data === 'string'){
      if(this.data instanceof Readable) ctx.req.pipe(this.data);
      else ctx.body = this.data;
      return;
    }
    ctx.body = JSON.stringify(this.data);
  }

  static renderEJSLayout(locals, options){
    if(!Responder.ejsLayout){
      //statically cache layout. In develoment this can be commented out so you don't have to restart the server every time the layout is changed
      Responder.ejsLayout = ejs.compile(fs.readFileSync(path.join(config.appRoot, 'views', 'layout.ejs'), 'utf-8'), options);
    }
    return Responder.ejsLayout(locals);
  }

  renderFile(pathtofile, data){
    const ext = path.extname(pathtofile);
    const view = path.join(this.viewsFolder, pathtofile);
    const translator = new Translator(this.context.session.locale || 'en');
    switch(ext){
      case '.pug':
      return pug.renderFile(view, Object.assign({},data,this.pugOptions))
      case '.ejs':
      data = Object.assign({}, data, this.ejsOptions.locals, {translator});
      const viewHTML = ejs.render(fs.readFileSync(view, 'utf-8'), data, this.ejsOptions);
      const layoutLocals = Object.assign({}, {body: viewHTML, title: data.title, translator, flash: data.flash}, this.ejsOptions.locals);
      return Responder.renderEJSLayout(layoutLocals, this.ejsOptions);
      case '.html':
      Logger.warn(`Outputting raw HTML file, ${view}, please consider serving static files using a separate server`);
      return fs.readFileSync(view, 'utf8');
    }
    return 'Unsupported view file extension';
  }

  viewExists(viewpath){
    const absolutePath = path.join(this.viewsFolder, viewpath);
    const file = this.allViews.find(vv=>path.basename(vv.path, vv.path.split('.').pop()) === absolutePath);
    return file && file.isFile();
  }

  static async middleware(ctx, next){
    const responder = new Responder(ctx);
    try {
      //topmost try-catch block, this will catch ALL errors

      ctx.responder = responder;
      //TODO: is this safe? Maybe only match static views in certain folder (e.g. "static")
      if(ctx.method === 'GET'){
        if(responder.viewExists(ctx.path)){
          //attempt to match a view
          Logger.info('Matching static view '+ctx.path);
          const path = ctx.path;
          responder.view(path, ctx.query);
          return responder.render(ctx);
        } else if(ctx.path === '/' || ctx.path.length === 0){
          if(ctx.session.user){
            return ctx.redirect('/user/profile');
            // responder.view('user/profile.ejs', {user: ctx.session.user});
          } else {
            // responder.view('user/login.ejs');
            return ctx.redirect('/user/login');
          }

          // await next();
          return responder.render(ctx);
        }
      }

      //*anticipation intensifies*
      await next();

      if (responder.viewToRender || responder.data) responder.render(ctx);
      else if (!ctx._matchedRoute) throw new NotFoundError(`Requested resource ${ctx.path} is not available`);

    } catch(err){
      Logger.error(`An error occurred processing request: ${err.message}`);
      Logger.error(err.stack);
      if (err.status) ctx.status = err.status;
      else ctx.status = 500;
      //TODO: don't render a view, instead use flash, so we keep the same view, but just display a message with the error
      //Or maybe we can be fancy, connect a socket and put all errors there, then have a clientside script that displays them whenever they arrive
      //this would make it easier in the long run, but might take a while to setup

      //Handle any special error cases here
      if (err instanceof SurveyNotActiveError) {
        ctx.view('error-surveynotactive.ejs', { survey: err.survey, moment });
      }
      else responder.view(config.env === 'production' ? 'error-production.ejs' : 'error.ejs', { error: err });

      responder.render(ctx);
    }
  }
}

module.exports = Responder;
