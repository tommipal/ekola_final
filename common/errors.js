class ValidationError extends Error {
  constructor(msg, record, fields){
    super(msg);
    this.record = record;
    this.fields = fields;
    this.status = 400;
  }
}

class SurveyNotActiveError extends Error {
  constructor(msg, survey) {
    super(msg);
    this.survey = survey;
    this.status = 400;
  }
}

class ArgumentError extends Error {
  constructor(msg, expected, received){
    super(msg);
    this.expected = expected;
    this.received = received;
  }
}

class RequestError extends Error {
   constructor(msg, request){
     super(msg);
     this.request = request;
     this.status = 400;
   }
}

class AuthorizationError extends Error {
  constructor(msg, user){
    super(msg);
    this.user = user;
    this.status = 403;
  }
}

class NotFoundError extends Error {
  constructor(msg, model, query){
    super(msg);
    this.model = model;
    this.query = query;
    this.status = 404;
  }
}

module.exports = { ValidationError, SurveyNotActiveError, ArgumentError, RequestError, AuthorizationError, NotFoundError };