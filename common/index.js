const logger = require('winston');
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, { level: 'debug', colorize:true });
//maintain this order to avoid any circular dependency BS
const { ArgumentError, AuthorizationError, RequestError, ValidationError, SurveyNotActiveError, NotFoundError } = require('./errors');
exports.Logger = logger;
exports.config = require('./config');
exports.Utils = require('./utils');
exports.webpackConfig = require('./webpack.config');
exports.Email = require('./email');

Object.assign(global, { ArgumentError, AuthorizationError, RequestError, ValidationError, SurveyNotActiveError, NotFoundError });

exports.ArgumentError = ArgumentError;
exports.AuthorizationError = AuthorizationError;
exports.RequestError = RequestError;
exports.ValidationError = ValidationError;
exports.SurveyNotActiveError = SurveyNotActiveError
exports.NotFoundError = NotFoundError;

