const path = require('path');
const fs = require('fs');
const config = require('./config');
const { Logger } = require('common');
const { ArgumentError, AuthorizationError, NotFoundError, RequestError, ValidationError } = require('./errors');
const moment = require('moment');

class Utils {
  static requireFolder(folderPath, ignoreRegex) {
    if (ignoreRegex && !(ignoreRegex instanceof RegExp)) throw new Error(`$Argument ${ignoreRegex} was not a regular expression! Must be a regular expression that matches filenames to be ignored`);

    const modules = {};

    const directory = path.join(config.appRoot, folderPath);

    const files = require('fs').readdirSync(directory)
    if (files.includes('index.js')) {
      if (ignoreRegex && !ignoreRegex.test('index.js'))
        Loggerwarn(`${directory} includes an index.js file that the passed ignoreRegex ${ignoreRegex} does not match. \nThis means it will be required along with other files. That may not be what you want.`);
      else return require(directory);
    }

    if (!ignoreRegex) ignoreRegex = /(index)(\.js)/;

    const dirStats = fs.statSync(folderPath); //throws if the directory doesnt exist or permission is denied

    if (!dirStats.isDirectory()) throw new Error(`${folderPath} is not a directory!`);

    function* walk(directory) {
      const files = fs.readdirSync(directory);
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        if (file.match(ignoreRegex)) continue;
        const stat = fs.statSync(path.join(directory, file));
        if (stat.isDirectory()) yield* walk(path.join(directory, file));
        else yield path.join(directory, file);
      }
    }

    const iterator = walk(directory);
    let result = iterator.next();
    while (!result.done) {
      /* Store module with its name (from filename) */
      modules[path.basename(result.value, '.js')] = require(result.value);
      result = iterator.next();
    }
    return modules;
  }

  static requireNamespace(folderPath, namespace) {
    const modules = {};

    const directory = path.join(config.appRoot, folderPath);

    const files = require('fs').readdirSync(directory)

    let ignoreRegex = /(index)(\.js)/;

    const dirStats = fs.statSync(folderPath); //throws if the directory doesnt exist or permission is denied

    if (!dirStats.isDirectory()) throw new Error(`${folderPath} is not a directory!`);

    function* walk(directory) {
      const files = fs.readdirSync(directory);
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        if (file.match(ignoreRegex)) continue;
        const stat = fs.statSync(path.join(directory, file));
        if (stat.isDirectory()) yield* walk(path.join(directory, file));
        else yield path.join(directory, file);
      }
    }

    const iterator = walk(directory);
    let result = iterator.next();
    while (!result.done) {
      if(result.value.endsWith('.ts')) {
        result = iterator.next();
        continue;
      }
      let m = require(result.value);
      if (m[namespace]) {
        //store potentially incomplete reference containing the namespace here
        modules[path.basename(result.value, '.js')] = m;
      }
      result = iterator.next();
    }

    //Now bring the namespaced modules to top after all have been required
    //this should resolve circular dependency problems if any
    const moduleNames = Object.keys(modules);
    for (let moduleName of moduleNames) {
      modules[moduleName] = modules[moduleName][namespace];
    }

    return modules;
  }

  static get DEF_GLOBAL_PROXY_HANDLER() {
    return {
      get(target, name) {
        if (name in target) return target[name];
        else throw new ReferenceError(`This global object has no enumerable property "${name}"`);
      }
    }
  }

  static installGlobal(object, name) {
    if (Object.keys(global).includes(name)) throw new Error(`A variable by "${name}" already exists at global scope, choose a different name!`);
    //create a proxy to watch changes and prevent pollution
    const proxy = new Proxy(object, Utils.DEF_GLOBAL_PROXY_HANDLER);
    global[name] = proxy;
  }

  static requireModels() {
    let models = Utils.requireNamespace('data', 'model');
    return models;
  }

  static getObjectClassName(object) {
    if(object.constructor.name === 'Function' && object.name)
      return object.name;
    if(object.constructor.name !== 'Object')
      return object.constructor.name;
    else
      return object.toString().split('(' || /s+/)[0].split(' ' || /s+/)[1];
  }

  static flatten(array) {
    return array.reduce((a, b) => a.concat(b), []);
  }

  static isBasicType(value) {
    return !value || Object.keys(value) === 0 || typeof value === 'string';
  }

  /**
   * Merge two arrays, updating entries in first with corresponding entries from second array when comparatr returns true
   * and appending the rest of the entries from the second array to end of the first, returning array1, modified in palce
   * @param {Array} array1 the array to merge with
   * @param {Any} array2 the array or value to merge. If it's a value, it will be converted to a single element array containing it, and the comparator will receive it entirely
   * @param {Function} comparator function taking two parameters, the first is value from array1, second value from array2, that will determine whether to merge values or append
   * @returns {Array} array1, modified
   */
  static arrayMerge(array1, array2, comparator){
    if(typeof comparator !== 'function') throw new Error('merger must be a function');
    if(!Array.isArray(array2)) array2 = [array2];
    for(let i=0; i<array2.length; i++){
      let found = array1.findIndex(val=>comparator(val, array2[i]));
      if(found>-1) {
        array1[found] = array2[i];
      } else {
        array1.push(array2[i]);
      }
    }
    return array1;
  }

  /**
   * Returns a generator that will iterate an object's enumerable properties, compatible with for..of loops
   * @param {*} object whose enumerable properties will be iterated
   * @returns {Generator} a generator object that conforms to the iterator protocol
   */
  static *iterateObject(object){
    for(let key in object){
      if(object.hasOwnProperty(key)){
        yield [ key, object[key] ];
      }
    }
  }

  /**
   * Checks if the passed object is a JSON-serializable data object. 100% legit no fake (2017)
   * @param {*} object to test for legitimacy
   * @returns {Boolean} true if legit, false if busted
   */
  static legitObject(object){
    //seems legit
    if(object && object.constructor === Object) return true;
    return  typeof object === 'object' &&
            object !== null &&
            {}.constructor === object.constructor;
  }

  /**
   * The true God.
   * @param {Array} from the universe
   * @param {Boolean} remove humbly request thy object be freed of the physical
   * @returns {*} the chosen one to echo through the ages
   */
  static selectRandom(from, remove){
    if (!Array.isArray(from)) throw new Error("from must be an array of choices! Was " + from);
    if (from.length === 0) throw new Error("Can't select from an empty array");
    let rand = Utils.random(0, from.length - 1);
    let value = from[rand];
    if (remove === true) {
      from.splice(rand, 1);
    }
    return value;
  }

  /**
   * return random integer between 2 numbers (inclusive)
   * @param {number} min minimum limit, inclusive
   * @param {number} max maximum limit, inclusive
   * @returns {number} integer between min and max inclusively
   */
  static random(min, max){
    return Math.round(min + ((max-min) * Math.random()));
  }
  static randomBoolean(trueProbability = 0.5) {
    return Math.random() < trueProbability;
  }

  static randomDateFuture(minDays, maxDays) {
    const minMinutes = 1440 * minDays;
    const maxMinutes = 1440 * maxDays;

    return moment().add(Utils.random(minMinutes, maxMinutes), 'minutes');
  }
  static randomDatePast(minDays, maxDays) {
    const minMinutes = 1440 * minDays;
    const maxMinutes = 1440 * maxDays;

    return moment().subtract(Utils.random(minMinutes, maxMinutes), 'minutes');
  }

  static generateArray(times, generator, ...args){
    const result = [];
    for(let i = 0; i<times; i++){
      result.push(generator(i, ...args));
    }
    return result;
  }

  static generate(generator){
    return function () {
      let iterator = generator.apply(this, arguments);

      function handle(result){
        // result => { done: [Boolean], value: [Object] }
        if (result.done) return Promise.resolve(result.value);

        return Promise.resolve(result.value).then(function (res){
          return handle(iterator.next(res));
        }, function (err){
          return handle(iterator.throw(err));
        });
      }

      try {
        return handle(iterator.next());
      } catch (ex) {
        return Promise.reject(ex);
      }
    }
  }

  /**
   * Generate a random string from the pool of characters. Length of the string can be defined with length parameter
   * @param {Number} length how long string should be
   * @returns {String} randomString
   */
  static randomString(length){
    const pool  = '12345678901234567890' +
      'QWERTYUIOPASDFGHJKLZXCVBNM' +
      'qwertyuiopasdfghjklzxcvbnm';
    let randomString = '';
    for (let i = 0; i<length;i++) {
      const num = Math.floor(Math.random() * (pool.length));
      randomString += pool[num];
    }
    return randomString;
  }

  /**
   * Order an array of objects based on the key of your choosing
   * @param {Array} array Array of objects you want sorted
   * @param {String} key Name of the key that sorting will be based on
   * @param {String} dir Direction of the sort; ASC = ascending, DESC = descending
   */
  static sortArrayOfObjects(array, key, dir = 'ASC') {
    if(dir !== 'ASC' || dir !== 'DESC') dir = 'ASC';
    if(typeof array[0] === 'undefined' || typeof array[0][key] === 'undefined') return array;
    return array.sort((a,b) => {
      const aa = typeof a[key] === 'string' ? a[key].toLowerCase() : a[key];
      const bb = typeof b[key] === 'string' ? b[key].toLowerCase() : b[key];
      let comparison = 0;
      if (aa > bb) comparison = dir === 'ASC' ? 1 : -1;
      else if (aa < bb) comparison = dir === 'ASC' ? -1 : 1;
      return comparison;
    });
  }

  /**
   * Performs a recursive deep merge of two objects. This should crash on circular references, but be careful anyway
   * @param {Object} target plain data object that will be mutated
   * @param {...Object} sources one or more objects from which properties will be recursively assigned
   * @returns {Object} target, although it will be mutated in place
   */
  static mergeDeep(target, ...sources) {
    if (!sources.length) return target;
    const source = sources.shift();

    if (Utils.legitObject(target) && Utils.legitObject(source)) {
      for (let key in source) {
        if (Utils.legitObject(source[key])) {
          if (!target[key]) Object.assign(target, { [key]: {} });
          Utils.mergeDeep(target[key], source[key]);
        } else {
          Object.assign(target, { [key]: source[key] });
        }
      }
    }

    return Utils.mergeDeep(target, ...sources);
  }

  static get RANDOM_STRING_CHAR_POOL(){
    return [
      '12345678901234567890',
      'QWERTYUIOPASDFGHJKLZXCVBNM',
      'qwertyuiopasdfghjklzxcvbnm'
    ].join('');
  }

  /**
   * Generate a random string from the pool of characters. Length of the string can be defined with length parameter
   * @param {Number} length of the string
   * @returns {String} randomString
   */
  static randomString(length){
    let randomString = '';
    for (let i = 0; i<length;i++) {
      const num = Math.floor(Math.random() * (Utils.RANDOM_STRING_CHAR_POOL.length));
      randomString += Utils.RANDOM_STRING_CHAR_POOL[num];
    }
    return randomString;
  }

  /**
   * Returns a promise that resolves to true when the given amount of milliseconds has passed
   * @param {Number} howMuch milliseconds before return
   * @returns {Promise<Boolean>} promise that resolves to true when the time has passed
   */
  static delay(howMuch){
    return new Promise(function(yes, no){
      setTimeout(()=>yes(true), howMuch);
    });
  }

  /**
   * Recursively convert an object by replacing every value within it with the return value of converter.
   * If Utils.legitObject(target) returns false, this function will call converter on the target and return immediately.
   * @param {Object} target the target object that will be modified IN PLACE!
   * @param {Function} converter the function that does the conversion. Will receive every element in target, it's key and parent, and must return whatever to replace that element with
   * @returns {Object} target, modified in place
   */
  static convertRecursive(target, converter){
    if(!Utils.legitObject(target)) return converter(target);
    target[Symbol.for('converted')] = true;
    for(let [name, val] of Utils.iterateObject(target)){
      if(Utils.legitObject(val) && !val[Symbol.for('converted')]) {
        Utils.convertRecursive(val, converter);
        continue;
      }
      target[name] = converter(val, name, target);
    }
    return target;
  }

  /**
   * From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
   * @param {Object} target where to copy
   * @param {Enumerable<Object>} sources from what to copy
   * @returns {Object} target
   */
  static completeAssign(target, ...sources) {
    sources.forEach(source => {
      let descriptors = Object.keys(source).reduce((descriptors, key) => {
        descriptors[key] = Object.getOwnPropertyDescriptor(source, key);
        return descriptors;
      }, {});
      // by default, Object.assign copies enumerable Symbols too
      Object.getOwnPropertySymbols(source).forEach(sym => {
        let descriptor = Object.getOwnPropertyDescriptor(source, sym);
        if (descriptor.enumerable) {
          descriptors[sym] = descriptor;
        }
      });
      Object.defineProperties(target, descriptors);
    });
    return target;
  }

  /**
   * Returns a clone of the given object without the keys specified by second parameter
   * @param {Object} object the object which to filter
   * @param {Function|Enumerable<Object>} filter either keys that need to be a removed, or a function that returns true for each key: value pair in the object that needs to be removed
   * @returns {Object} filtered clone of the first parameter
   */
  static filter(object, ...filter){
    if(!Utils.legitObject(object)) throw new Error('First argument must be a data object');
    if(filter.length === 1 && typeof filter[0] === 'function'){
      const fn = filter[0];
      filter = [];
      for(let [key, val] of Utils.iterateObject(object)){
        if(fn(key, val)) filter.push(key);
      }
    }
    const clone = Utils.completeAssign({}, object);
    filter.forEach(key=>delete clone[key]);
    return clone;
  }

  /**
   * Tests if an object is empty (i.e. has no enumerable keys). If the object
   * is not legit, will throw a NotLegitObjectException (not really, will just throw...)
   * This will return false if object's only enumerable properties are functions and will
   * will return true if object is undefined
   * @param {Object} object a data object
   * @returns {Boolean} whether the object has enumerable keys or not
   */
  static isEmpty(object){
    if(typeof object === 'undefined') return true;
    if(!Utils.legitObject(object)) throw new Error(`${object} must be a data object`);
    let i = 0;
    for(let key in Object.keys(object)){
      if(typeof object[key] !== 'function') i++;
    }
    return i === 0;
  }

  /**
   * Tests if given string contains valid JSON and can be parsed
   * @param {string} str
   * @returns {boolean}
   */
  static containsValidJSON(str) {
    if(typeof str !== 'string') return false;
    try {
      JSON.parse(str);
      return true;
    } catch(err) {
      return false;
    }
  }

  /**
   * Replace nth occurence of regex in the string with provided value
   * This is NOT 0-based, n must be 1 or more
   * @param {String} string string to modify
   * @param {RegExp} regex what to use for searching
   * @param {Number} n which occurence to replace
   * @param {String} replace what to replace with
   * @returns {String} the string with values replaced
   */
  static replaceNthOccurence(string, regex, n, replace){
    let index = 0;
    return string.replace(regex, function(match, i) {
        index++;
        if( index === n ) {
            return replace;
        }
        return match;
    });
  }

  static buldFileList(pathname, recursive){
    const dirinfo = fs.statSync(pathname);
    if(!dirinfo.isDirectory()) throw new Error(`${pathname} is not a directory`);
    const fileList = [];
    let fileinfo;
    const allFiles = fs.readdirSync(pathname).map(file=>path.join(pathname, file));
    for(let filepath of allFiles){
      fileinfo = fs.statSync(filepath);
      if(fileinfo.isFile()){
        fileinfo.path = filepath;
        fileList.push(fileinfo);
      } else if(fileinfo.isDirectory()){
        if(recursive){
          fileList.push(...Utils.buldFileList(filepath, true));
        } else {
          fileinfo.path = filepath;
          fileList.push(fileinfo);
        }
      }
    }
    return fileList;
  }
}

module.exports = Utils;