const webpack = require('webpack');
const path = require('path');
const babelMinify = require('babel-minify-webpack-plugin');
//const ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = require('./config');

if(config.env === 'production') console.info('Loading wepback in production mode. Is this desired?');

const productionPlugins = [
  new webpack
    .optimize
    .DedupePlugin(),
  new webpack
    .optimize
    .UglifyJsPlugin(),
    //dont enable this shit, it breaks the app completely, need to figure out what to do about it
    //new ExtractTextPlugin(),
  new babelMinify()
];

const developmentPlugins = [
  //new webpack.optimize.OccurrenceOrderPlugin(),
  new webpack.NoEmitOnErrorsPlugin(),
  new webpack.HotModuleReplacementPlugin()
];

const webpackConfig = {
  entry: {
    'js/vendors':          path.join(config.appRoot, 'client', 'scripts', 'vendors.js'),
    'js/utils':            path.join(config.appRoot, 'client', 'scripts', 'utils.js'),
    'js/surveyedit':       path.join(config.appRoot, 'client', 'scripts', 'SurveyEdit.jsx'),
    'js/surveylist':       path.join(config.appRoot, 'client', 'scripts', 'SurveyList.jsx'),
    'js/usergroupedit':    path.join(config.appRoot, 'client', 'scripts', 'UserGroupEdit.jsx'),
    'js/results':          path.join(config.appRoot, 'client', 'scripts', 'pages', 'results.js'),
    'js/answer':           path.join(config.appRoot, 'client', 'scripts', 'pages', 'answer.js'),
    'js/evaluation-new':   path.join(config.appRoot, 'client', 'scripts', 'pages', 'evaluation-new.js'),
    'js/evaluation-edit':  path.join(config.appRoot, 'client', 'scripts', 'pages', 'evaluation-edit.js'),
    'js/profile':          path.join(config.appRoot, 'client', 'scripts', 'pages', 'profile.js'),
    'css/app':             path.join(config.appRoot, 'client', 'styles',  'index.scss')
  },
  context: path.join(config.appRoot, 'client'),
  output: {
    path: path.join(config.appRoot, 'client', 'dist'),
    publicPath: '/',
    filename: '[name].js'
  },
  plugins: config.env === 'production' ? productionPlugins : developmentPlugins,
  resolve: {
    extensions: ['.js', '.jsx', '.css', '.scss'],
    modules: [
      path.resolve(config.appRoot, 'client', 'scripts'),
      path.resolve(config.appRoot, 'node_modules'),
    ]
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/, //.js, .jsx
        loader: 'babel-loader',
        options: {
          presets: ['env', 'react'],
          plugins: [ 'transform-async-to-generator', 'transform-regenerator' ],
          cacheDirectory: true
        },
        include: [
          path.resolve(config.appRoot, 'client', 'scripts')
        ]
      }, {
        test: /\.s?css$/, //.css, .scss
        use: ["style-loader", "css-loader",
          {
            loader: "sass-loader", // compiles Sass to CSS
            options: {
              outputStyle: 'compressed',
              ext: 'css',
              includePaths: [
                path.join(config.appRoot, 'node_modules', 'foundation-sites', 'scss'),
                path.join(config.appRoot, 'node_modules', 'motion-ui',        'src'),
                path.join(config.appRoot, 'node_modules', 'selectize',        'dist',    'css'),
                path.join(config.appRoot, 'node_modules', 'flatpickr',        'dist'),
              ]
            }
          }
        ]
      }
    ]
  },
  node: {
    console: true
  }
}

if(config.env !== 'production') webpackConfig.devtool = 'cheap-module-source-map';

module.exports = webpackConfig;