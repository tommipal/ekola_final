const nodemailer = require('nodemailer');
const { Logger } = require('common');

class Email {
  /**
   * Send email ONLY if NODE_ENV is set to 'production'.
	 * @param {Array} to Array of email addresses
	 * @param {string} subject subject of email
	 * @param {string} text text content
	 * @param {string} html html content
   * @returns {Promise} a promise chain of nodemailer transporter.verify()=>sendMail()
	 */
  static sendEmail(to, subject, text, html) {
    if (to instanceof Array == false) throw new Error(`Expected an array of email addresses. Found ${to}`);

    const params = {
      from: '"Ekola" <ekola.no-reply@metropolia.fi>',
      to: to.join(', '),
      subject: subject,
      text: text,
      html: html
    }
    if (process.env.NODE_ENV !== 'production') return Logger.verbose(`EmailService: no sending in dev environment. ${JSON.stringify(params)}`);

    /**
     * This is now set to use the sendmail binary.
     * TODO use a smtp server.
     */
    let transporter = nodemailer.createTransport({
      //pool: true,
			/* Use defaults for now
			host: 'smtp.example.com',
			port: 465,
			secure: true, // use TLS
			auth: {
					user: 'username',
					pass: 'password'
			}
			*/
      sendmail: true,
      newline: 'unix',
      path: '/usr/sbin/sendmail'
    });
    // return transporter.verify().then(()=>sendMail(params));

    transporter.sendMail(params, (err, info) => {
      if (err) Logger.error(err);
      if (info) Logger.verbose(info);
    });
  }
}
module.exports = Email;