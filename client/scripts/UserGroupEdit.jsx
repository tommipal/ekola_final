import React, { Component } from 'react';
import ReactDOM             from 'react-dom';
import {UserGroupTableContainer}      from './UserGroup/edit/UserGroupTableContainer';
import UserInfo             from './User/UserInfo';
import Loading              from './Loading';
import Modal                from 'react-modal';
import {Translator}         from 'translator';
import { Utils }            from 'utils';

const translator = new Translator(Utils.getLocale());

const root = document.getElementById('react-root');
const groupId = Number(root.dataset['groupId']);

Modal.setAppElement(root);

/**
 * Parent component for the UserGroup Edit react page
 */
class App extends Component {
  constructor() {
    super();
    this.state = {
      showModal: false,
      modalData: {},
      fetchedResults: false,
      fetchFailed: false,
    }
  }

  // Close modal and empty the modal data
  handleCloseModal() {this.setState({ modalData: {}, showModal: false, fetchedResults: false });}

  // Get user info data when user info button is clicked
  handleUserInfoClick(id) {
    this.setState({ showModal: true });
    fetch(`/user/${id}/info`, { credentials: 'same-origin' })
    .then((response) => {
      if(!response.ok) throw Error('Network request failed');
      return response.json();
    })
    .then((data) => {
      this.setState({modalData:data, fetchedResults:true});
    }, (error) => {
      this.setState({fetchFailed:true});
    });

  }
  // Render user info modal if this.state.modalData contains data.
  userInfoModal() {
    if(this.state.fetchFailed) return <h4>{translator.trans('Fetch failed! Try again later.')}</h4>;
    if(!this.state.fetchedResults) return <Loading/>;
    if(!!Object.keys(this.state.modalData).length) return <UserInfo data={this.state.modalData}/>;
    else return (<h4>{translator.trans('No data found. Try again later.')}</h4>);
  }

  render() {
    const modalContent = this.userInfoModal(); // Set up modal
    return (<div className="grid-x grid-margin-x">
        <UserGroupTableContainer groupId={groupId} userInfoClick={this.handleUserInfoClick.bind(this)}/>
        <Modal
          isOpen={this.state.showModal}
          onRequestClose={this.handleCloseModal.bind(this)}
          className={'react-reveal'}
          overlayClassName={'react-reveal-overlay'}
          >
          <button type="button" className="close-button" aria-label="Close reveal" onClick={this.handleCloseModal.bind(this)}>
            <span aria-hidden="true">×</span>
          </button>
          {modalContent}
        </Modal>
      </div>);
  }
}

import registerServiceWorker from './registerServiceWorker';
ReactDOM.render(<App />, root);
registerServiceWorker();
