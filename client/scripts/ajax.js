import { Utils } from 'utils';
export class CookieMonster {
  static getItem(sKey) {
    if (!sKey) {
      return null;
    }
    return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-.+*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
  }

  /**
   * Set cookie
   * @param {String} sKey key name
   * @param {String} sValue key value (will be escaped and converted with JSON.stringify, if an object or an array is passed)
   * @param {String} vEnd duration of the cookie in seconds
   * @param {String} sPath set the path, this will dictate which urls the cookie applies. Setting this to / makes it apply to whole app. Defaults to current url
   * @param {String} sDomain set the domain to which the cookie applies
   * @param {Boolean} bSecure if true the cookie will not be changeable by the server
   * @return {Boolean} true if set successfully
   */
  static setItem(sKey, sValue, vEnd, sPath, sDomain, bSecure) {
    if (!sKey || /^(?:expires|max-age|path|domain|secure)$/i.test(sKey)) {
      return false;
    }
    let sExpires = "";
    if (vEnd) {
      switch (vEnd.constructor) {
        case Number:
          sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
          /*
           Note: Despite officially defined in RFC 6265, the use of `max-age` is not compatible with any
           version of Internet Explorer, Edge and some mobile browsers. Therefore passing a number to
           the end parameter might not work as expected. A possible solution might be to convert the the
           relative time to an absolute time. For instance, replacing the previous line with:
           */
          /*
           sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; expires=" + (new Date(vEnd * 1e3 + Date.now())).toUTCString();
           */
          break;
        case String:
          sExpires = "; expires=" + vEnd;
          break;
        case Date:
          sExpires = "; expires=" + vEnd.toUTCString();
          break;
      }
    }
    if(typeof sValue === 'object') {
      try {
        sValue = JSON.stringify(sValue);
      } catch (err) {
        console.error(`${sValue} was an object, but could not convert it using JSON.stringify. Cookies only accept strings and numbers as values.`);
        return false;
      }
    }
    document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + "; path=" + (sPath ? sPath : "/") + (bSecure ? "; secure" : "");
    return true;
  }

  /**
   * Merge an object to an existing JSON string inside a cookie
   * @param {String} key key at which JSON is stored
   * @param {*} value object to merge (will override existing properties under same keys)
   * @return {Boolean} whether the key => value pair has been succsessfully merged
   */
  static mergeItem(key, value){
    let json = CookieMonster.getItem(key);
    try {
      let obj = JSON.parse(json);
      obj = Object.assign(obj, value);
      return CookieMonster.setItem(key, obj);
    } catch (err){
      console.error(`Failed to merge object - the value of the cookie at key ${key} was either not a JSON or didn't exist.`);
      return false;
    }
  }

  static removeItem(sKey, sPath, sDomain) {
    if (!CookieMonster.hasItem(sKey)) {
      return false;
    }
    document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + "; path=" + (sPath ? sPath : "/");
    return true;
  }

  static hasItem(sKey) {
    if (!sKey || /^(?:expires|max-age|path|domain|secure)$/i.test(sKey)) {
      return false;
    }
    return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-.+*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
  }

  static keys() {
    const aKeys = document.cookie.replace(/((?:^|\s*;)[^=]+)(?=;|$)|^\s*|\s*(?:=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:=[^;]*)?;\s*/);
    for (let nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) {
      aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]);
    }
    return aKeys;
  }
}

export class Ajax {
  constructor(method, data, route, preProcessData) {
    this.method = method;
    this.type = (data instanceof FormData) ? 'formdata' : 'json';
    this.data = {};
    this.addData(data);

    if(typeof preProcessData === 'function'){
      this.preProcessData = preProcessData;
    }

    //strip route of parameters and add them to data instead
    if(route.match(/.+\?\w*=?.+/)){
      //route contains parameters
      let paramString = route.split('?')[1];
      let params = paramString.split('&');
      let paramMap = params.map(p=>p.split('='));
      paramMap = paramMap.map(p=>{
        if(p.length === 1) return { [p]: true };
        else return { [p[0]]: p[1] }
      });
      this.addData(paramMap);
      this.route = route.split('?')[0];
    } else {
      this.route = route;
    }

    this.addHeaders({
      'X-Requested-With': 'XMLHttpRequest'
    })
  }

  addData(data) {
    if(!data || Object.keys(data).length === 0) return;
    switch (this.type) {
      case 'formdata':
        for (let el in data) {
          if (data.hasOwnProperty(el)) {
            this.data.append(el, data[el]);
          }
        }
        break;
      case 'json':
        if(this.method === 'GET'){
          for(let key in data){
            if(data.hasOwnProperty(key)){
              if(typeof data[key] === 'object') {
                console.warn(`Non-simple data type of parameter ${key} in GET request, consider using POST`);
                this.addHeaders({
                  'Content-Type': 'application/json'
                });
                break;
              }
            }
          }
        }
        Object.assign(this.data, data);
        if(this.method !== 'GET'){
          this.addHeaders({
            'Content-Type': 'application/json'
          });
        }
        break;
    }
    return this;
  }

  /**
   * @param {Function} [cb=null] that will be called onreadystatechange, you can omit this if you use promises
   * @returns {Promise} that resolves to responseText of xhr onload or rejext onerror
   */
  send(cb) {
    this._appendCsrf();
    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();

      if (cb){
        xhr.onreadystatechange = () => {
          cb(xhr.responseText, xhr.readyState);
        }
      }

      xhr.onload = () => resolve(xhr.responseText);
      xhr.onerror = () => reject(xhr.responseText);

      let route = this.route;
      let data = this.data;

      if(this.method === 'GET'){
        //for GET methods, encode everything in URL and set data to nothing
        let params = [];
        for(let key in data){
          if(data.hasOwnProperty(key)){
            params.push(`${key}=${data[key]}`);
          }
        }
        //should we encodeURIComponent???
        route += `?${params.join('&')}`;
        data = '';
      }

      if(this.type === 'json') data = JSON.stringify(data);

      xhr.open(this.method, route);
      xhr.setRequestHeader('csrf-token', Utils.getCsrfToken());
      this._setHeaders(xhr);
      xhr.send(data);
    });
  }

  addHeaders(headers){
    if(!this.headers) this.headers = {};
    for(let header in headers){
      if(headers.hasOwnProperty(header)){
        if(!Utils.checkTypeOk(headers[header], 'string', 'number')) {
          console.error(`${header} header must be a string or a number, was ${typeof headers[header]}`);
          return this;
        }
        this.headers[header] = headers[header];
      }
    }
    return this;
  }

  preProcessData(){
    //any extra processing that must be done to data goes here
    //default implementation checks the startDate and endDate and adds timezone info if neccessary
    for (let key in this.data) {
      if(this.data.hasOwnProperty(key)){
        switch (key){
          case 'startDate':
          case 'endDate':
            if(!this.data[key].match(/[\d\-T:.]+\+(\d{2}:\d{2})+/) && moment.parseZone(this.data[key]).utcOffset() === 0){
              //missing offset, need to add
              console.warn(`date ${this.data[key]} does not include timezone offset. Will assume it's UTC and add timezone before sending`);
              this.data[key] = moment.utc(this.data[key]).local().format();
            }
            break;
        }
      }
    }
  }

  _setHeaders(xhr){
    for(let header in this.headers){
      if(this.headers.hasOwnProperty(header)){
        xhr.setRequestHeader(header, this.headers[header]);
      }
    }
  }

  _appendCsrf() {
    switch (this.type) {
      case 'formdata':
        this.data.append("_csrf", Utils.getCsrfToken());
        break;
      case 'json':
        this.data._csrf = Utils.getCsrfToken();
        break;
    }
    return this;
  }
}