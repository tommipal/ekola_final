import React, { Component }   from 'react';
import {UserGroupTableRow} from './UserGroupTableRow';
import {Utils}                from 'utils';
import {Translator}           from 'translator';

const translator = new Translator(Utils.getLocale());

/**
 * User groub table element
 */
export class UserGroupTable extends Component {
  showSelectAll() {
    if(!this.props.canEdit) return;
    return (
      <tfoot>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td>
            <div className="table-buttons">
            {this.props.selected.length === this.props.users.length &&
              <button className="button success" title={translator.trans('Unselect all')} onClick={this.handleSelectAll.bind(this)}><i className="fi-check"></i><i className="fi-check"></i></button>
            }
            {this.props.selected.length !== this.props.users.length &&
              <button className="button hollow" title={translator.trans('Select all')} onClick={this.handleSelectAll.bind(this)}><i className="fi-check"></i><i className="fi-check"></i></button>
            }
            </div>
          </td>
        </tr>
      </tfoot>
    );
  }

  handleUserClick({target}) {
    const id = Number(Utils.traverseUp(target, '[data-user]').dataset['user']);
    let selected = this.props.selected.slice();
    if(selected.includes(id)) {
      selected = selected.filter((value)=>{
        return value !== id;
      })
    } else {
      selected.push(id);
    }
    this.setState({selected});
    this.props.selectUser(selected);
  }

  handleSelectAll({target}) {
    const ids = this.props.users.map((user)=>{
      return user.id;
    });
    if(this.props.selected.length === this.props.users.length) {
      this.setState({selected:[]});
      this.props.selectUser([]);
    } else {
      this.setState({selected:ids});
      this.props.selectUser(ids);
    }
  }

  render() {
    const rows = this.props.users.map((user,index) => {
      return (<UserGroupTableRow key={`user-${index}`} index={index}
        user          = {user}
        selected      = {this.props.selected.includes(user.id)}
        onClick       = {this.handleUserClick.bind(this)}
        userInfoClick = {this.props.userInfoClick}
        canEdit       = {this.props.canEdit}
        />);
    });
    const selectAllButtons = this.showSelectAll();
    return (
      <table id="group-user-table">
        <thead>
          <tr>
            <th>{translator.trans('Full name')}</th>
            <th>{translator.trans('Email')}</th>
            <th>{translator.trans('Role')}</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
        {selectAllButtons}
      </table>
    );
  }
}