import React, { Component } from 'react';
import ReactDOM             from 'react-dom';
import Modal                from 'react-modal';
import { Utils }            from 'utils';
import {Translator}         from 'translator';

const translator = new Translator(Utils.getLocale());

/**
 * Single user group table row
 */
export class UserGroupTableRow extends Component {
  showSelectButton() {
    if(!this.props.canEdit) return;
    else if(this.props.selected) {
      return <button className="button success" title={translator.trans('Unselect')} onClick={this.props.onClick}><i className="fi-check"></i></button>
    }
    else if(!this.props.selected) {
      return <button className="button hollow" title={translator.trans('Select')} onClick={this.props.onClick}><i className="fi-check"></i></button>
    }
  }
  // Send clicked users id to the parent
  showUserInfo() {this.props.userInfoClick(this.props.user.id);}
  render() {
    const selectButton = this.showSelectButton();
    return (
      <tr className="group-user" data-user={this.props.user.id}>
        <td>{`${this.props.user.givenName} ${this.props.user.familyName}`}</td>
        <td>{this.props.user.email}</td>
        <td>{translator.trans(this.props.user.role)}</td>
        <td>
          <div className="table-buttons">
            {selectButton}
            <button className="button" title={translator.trans('Show user information')} onClick={this.showUserInfo.bind(this)}><i className="fi-torso"></i></button>
          </div>
        </td>
      </tr>
    );
  }
}