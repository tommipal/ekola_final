import React, { Component } from 'react';
import {UserGroupTable}     from './UserGroupTable';
import Loading              from 'Loading';
import {Utils}              from 'utils';
import Modal                from 'react-modal';
import Pagination           from 'react-paginate';
import {Translator}         from 'translator';
import {ReactSelectize, SimpleSelect, MultiSelect}      from 'react-selectize';
import 'react-selectize/dist/index.min.css';

const translator = new Translator(Utils.getLocale());

Modal.setAppElement('#react-root');

/**
 * Container for the user group table, user info modal and pagination buttons
 */
export class UserGroupTableContainer extends Component {
  constructor() {
    super();
    this.state = {
      selectedAction: '',
      userSearch:     '',
      evaluations:    [],
      users:          [],
      selectedUsers:  [],
      usersNotInGroup:[],
      usersToBeAdded: [],

      page:           1,
      pageCount:      1,
      pageLimit:      10,

      showModal:      false,
      fetchedResults: false,
      fetchFailed:    false,
      newSearch:      true,
    }
  }

  // Perform fetch after component has loaded
  componentDidMount() {
    this.fetchData(this.state.page);
  }

  fetchUsers() {
    if(this.state.newSearch) {
      fetch(`/user/find?query=usersNotInGroup&userGroup=${this.props.groupId}`, {credentials: 'same-origin'})
      .then((response)=>{
        if(!response.ok) throw Error('Network request failed');
        return response.json();
      }).then((data)=>{
        data = data.map((item)=>{
          return {
            id: item.id,
            username: item.username,
            fullName: item.givenName + ' ' + item.familyName,
          }
        });
        this.setState({usersNotInGroup: data, newSearch: false});
      },(error)=>Utils.showMessage(translator.trans('Something went wrong. Try again later.'), 'error'));
    }
  }

  // Fetch data that is needed for the table
  fetchData(page) {
      fetch(`/userGroup/${this.props.groupId}/edit?page=${page}&pageLimit=${this.state.pageLimit}`, {
        headers: {'Content-Type': 'application/json'},
        credentials: 'same-origin',
      }).then((response) => {
        if(!response.ok) throw Error('Network request failed');
        return response.json();
      }).then((data) => {
        this.setState({
          users:          data.users,
          evaluations:    data.evaluations,
          fetchedResults: true,
          canEdit:        data.canEdit,
          pageCount:      data.pageCount,
          page,
        });
      },(error) => {this.setState({fetchFailed: true})});
  }

  // Perform the action that the user has confirmed by checking 'this.state.selectedAction'.
  modalConfirm({target}) {
    const selectElem = Utils.findSibling(target, 'select', 0) || '';
    const selectedEvaluationId = selectElem ? selectElem.value : '';
    const selectedEvaluation = this.state.evaluations.find((evaluation)=>evaluation.id == selectedEvaluationId) || '';
    const users = {};
    switch(this.state.selectedAction) {
      case 'assignEval':
        if(selectedEvaluationId){
          users.add = this.state.selectedUsers;
          this.editEvaluation(selectedEvaluationId, users)
          .then((data) => {
            const translation = translator.trans(`%value% users added to evaluation '%value%'`, this.state.selectedUsers.length, selectedEvaluation.name);
            Utils.showMessage(translation, 'success');
            this.setState({showModal:false, selectedUsers:[]});
          },(error) => Utils.showMessage(translator.trans('Something went wrong. Try again later.'), 'error'));
        }
        break;
      case 'removeEval':
        if(selectedEvaluationId) {
          users.remove = this.state.selectedUsers;
          this.editEvaluation(selectedEvaluationId, users)
          .then((data) => {
            const translation = translator.trans(`%value% users removed from evaluation '%value%'`, this.state.selectedUsers.length, selectedEvaluation.name);
            Utils.showMessage(translation, 'success');

            this.setState({showModal:false, selectedUsers:[]});
          },(error) => Utils.showMessage(translator.trans('Something went wrong. Try again later.'), 'error'));
        }
        break;
      case 'removeGroup':
        users.remove = this.state.selectedUsers;
        this.editUserGroup(users)
        .then((data) => {
          const translation = translator.trans(`%value% users removed from this group`, this.state.selectedUsers.length);
          Utils.showMessage(translation, 'success');
          this.setState({showModal:false, selectedUsers:[], usersNotInGroup:[], fetchedResults: false, newSearch: true});
          if (data.redirect) window.location.replace(data.redirect);
          this.fetchData(this.state.page);
        },(error) => Utils.showMessage(translator.trans('Something went wrong. Try again later.'), 'error'));
        break;
      case 'admin':
        users.admin = this.state.selectedUsers;
        this.editUserGroup(users)
        .then((data) => {
          const translation = translator.trans(`%value% users promoted to administrators`, this.state.selectedUsers.length);
          this.setState({showModal:false, selectedUsers:[]});
          Utils.showMessage(translation, 'success');
        },(error) => Utils.showMessage(translator.trans('Something went wrong. Try again later.'), 'error'));
        break;
    }
  }

  addUsers() {
    const users = {
      add: this.state.usersToBeAdded.map((u)=>u.id)
    };
    this.editUserGroup(users).then((data) => {
      Utils.showMessage(translator.trans('%value% users added to the group', this.state.usersToBeAdded.length), 'success')
      this.setState({fetchedResults: false, usersToBeAdded: [], usersNotInGroup:[], newSearch: true})
      this.fetchData(this.state.page);
    },(error) => Utils.showMessage(translator.trans('Something went wrong. Try again later.'), 'error'));
  }

  // add or remove users from userGroup or promote to admin
  editUserGroup(users) {
    return new Promise((resolve, reject) => {
      fetch(`/userGroup/${this.props.groupId}/save`, {
        method: 'POST',
        body: JSON.stringify({users}),
        headers: {'Content-Type': 'application/json', 'csrf-token': Utils.getCsrfToken()},
        credentials: 'same-origin',
      }).then((response) => {
        if(!response.ok) throw Error('Network request failed');
        return response.json();
      }).then((data) => resolve(data), (error) => reject(error));
    });
  }

  // Either remove or add users to evaluations depending on the users object.
  // if it contains an 'add'-array, users will be added. if it contains a 'remove'-array users will be removed from the evaluation
  editEvaluation(evaluationId, users) {
    return new Promise((resolve, reject) => {
      fetch(`/evaluation/${evaluationId}/save`, {
        method: 'POST',
        body: JSON.stringify({users, userGroups:{}}),
        headers: {'Content-Type': 'application/json', 'csrf-token': Utils.getCsrfToken()},
        credentials: 'same-origin',
      }).then((response) => {
        if(!response.ok) throw Error('Network request failed');
        return response.json();
      }).then((data) => resolve(data), (error) => reject(error));
    });
  }

  // Listener for the button that says 'Do it!'
  doIt() {
    if(!this.state.selectedUsers.length) {
      Utils.showMessage('At least 1 user must be selected to perform this action.', 'error');
    } else if (!this.state.selectedAction) {
      Utils.showMessage('Select an action to perform first.', 'error');
    } else {
      this.setState({showModal:true});
    }
  }

  // Render modal based on which action is selected in 'this.state.selectedAction'.
  // Only render if 'showModal'-boolean is true and 'selectedUsers'-array contains items
  setUpModal() {
    if(this.state.showModal && !!this.state.selectedUsers.length) {
      switch(this.state.selectedAction) {
        case 'assignEval':
          return this.assignEval();
          break;
        case 'removeEval':
          return this.removeEval();
          break;
        case 'removeGroup':
          return this.removeGroup();
          break;
        case 'admin':
          return this.admin();
          break;
      }
    }
  }

  // Render modal for assign to an evaluation option
  assignEval() {
    const evaluations = this.state.evaluations.map((evaluation, index) => {
      return <option key={'evaluation-'+index} value={evaluation.id}>{evaluation.name}</option>;
    });
    return (<section className="modal-content">
      <b>{translator.trans('Select an evaluation to assign %value% users to:', this.state.selectedUsers.length)}</b>
      <select>
        <option value="">{translator.trans('Choose...')}</option>
        {evaluations}
      </select>
      <button type="button" className="button" onClick={this.modalConfirm.bind(this)}>OK</button>
    </section>);
  }

  // Render modal for remove from evaluation option
  removeEval() {
    const evaluations = this.state.evaluations.map((evaluation, index) => {
      return <option key={'evaluation-'+index} value={evaluation.id}>{evaluation.name}</option>;
    });
    return (<section className="modal-content">
      <b>{translator.trans('Select an evaluation where %value% users are removed from:', this.state.selectedUsers.length)}</b>
      <select>
        <option value="">{translator.trans('Choose...')}</option>
        {evaluations}
      </select>
      <button type="button" className="alert button" onClick={this.modalConfirm.bind(this)}>OK</button>
    </section>);
  }

  // Render modal for remove from userGroup option
  removeGroup() {
    return (<section className="modal-content grid-x grid-padding-x align-center text-center">
        <h4>{translator.trans('Are you sure you want to remove %value% users?', this.state.selectedUsers.length)}</h4>
        <div className="button-group">
          <button type="button" className="button" onClick={this.modalConfirm.bind(this)}>{translator.trans(`I'm sure`)}</button>
          <button type="button" className="alert button" onClick={() => this.setState({showModal:false})}>{translator.trans('I changed my mind')}</button>
        </div>
      </section>);
  }
  // Render modal for give admin permissions option
  admin() {
    return (<section className="modal-content grid-x grid-padding-x align-center text-center">
      <h4>{translator.trans('Are you sure you want to give %value% users admin permissions to this group', this.state.selectedUsers.length)}</h4>
      <div className="button-group">
        <button type="button" className="button" onClick={this.modalConfirm.bind(this)}>{translator.trans(`I'm sure`)}</button>
        <button type="button" className="alert button" onClick={() => this.setState({showModal:false})}>{translator.trans('I changed my mind')}</button>
      </div>
    </section>);
  }

  showEditControls() {
    if(!this.state.canEdit) return;
    return (
      <div className="grid-x grid-margin-x">
          <div className="small-12 large-6 cell">
            <label>{translator.trans('Add users to the group')}</label>
            <div className="input-group">
              <MultiSelect
                placeholder = {translator.trans('Search')}
                options = {this.state.usersNotInGroup}
                search = {this.state.userSearch}
                values = {this.state.usersToBeAdded}

                onValuesChange = {(selected) => {
                  this.setState({
                    usersToBeAdded: selected
                  });
                }}
                onSearchChange = {(userSearch) => {
                    this.setState({userSearch});
                    if (userSearch.length > 0) {
                      this.fetchUsers();
                    }
                }}
                filterOptions = {(options, values, userSearch) => {
                  options = options.filter((o)=>values.map((v)=>v.id).indexOf(o.id) === -1);
                  const fullNameOptions = options.map((o)=>o.fullName.toLowerCase()).filter((item)=>{
                    return item.indexOf(userSearch.toLowerCase()) !== -1;
                  });
                  const usernameOptions = options.map((o)=>o.username.toLowerCase()).filter((item)=>{
                    return item.indexOf(userSearch.toLowerCase()) !== -1
                  });
                  let filteredOptions = [];
                  options.filter((o)=>{
                    for(const no of fullNameOptions) {
                      if(no.toLowerCase() === o.fullName.toLowerCase())
                      filteredOptions.push(o);
                    }
                    for(const uo of usernameOptions) {
                      if(uo.toLowerCase() === o.username.toLowerCase())
                      filteredOptions.push(o);
                    }
                  });
                  filteredOptions = Utils.removeDuplicates(filteredOptions, 'id');
                  return filteredOptions;
                }}

                uid = {(item) => item.id}

                renderOption = {(item) => {
                  return <div className="simple-option" style={{fontSize: 12}}>
                    <div>
                      <span style={{fontWeight: "bold"}}>{`${item.fullName} `}</span>
                      <span>({item.username})</span>
                    </div>
                  </div>
                }}

                renderValue = {(item) => {
                  return <div className="simple-value" style={{fontSize: 14}}>
                    <span>{item.fullName}</span>
                  </div>
                }}

                renderNoResultsFound = {(value, userSearch) => {
                  return <div className="no-results-found" style={{fontSize: 12}}>
                    {!this.state.userSearch.length ?
                    translator.trans('Type a name of username to search users') : translator.trans('No results found')}
                  </div>
                }}/>
              <div className="input-group-button">
                <button className="button" disabled={!this.state.usersToBeAdded.length} onClick={this.addUsers.bind(this)}>{translator.trans('Add')}</button>
              </div>
            </div>
          </div>
          <div className="small-12 large-6 cell">
            <label htmlFor="user-action">{translator.trans('Selected users:')}</label>
            <div className="input-group">
              <select id="user-action" value={this.state.selectedAction} className="input-group-field" onChange={this.handleSelectChange.bind(this)}>
                <option value="">{translator.trans('Choose...')}</option>
                <option value="assignEval">{translator.trans('Assign to an evaluation')}</option>
                <option value="removeEval">{translator.trans('Remove from evaluation')}</option>
                <option disabled={!this.state.canEdit} value="removeGroup">{translator.trans('Remove from group')}</option>
                <option disabled={!this.state.canEdit} value="admin">{translator.trans('Give admin permissions')}</option>
              </select>
              <div className="input-group-button">
                <button type="button" className="button" disabled={!this.state.selectedUsers.length || !this.state.selectedAction} onClick={this.doIt.bind(this)}>OK</button>
              </div>
            </div>
          </div>
        </div>
    );
  }

  handleOpenModal() {this.setState({ showModal: true });}
  handleCloseModal() {this.setState({ showModal: false });}
  handleUserSelect(selectedUsers) {this.setState({selectedUsers})}
  handleSelectChange({target}) {this.setState({selectedAction: target.value});}
  handlePageChange({selected}) {
    this.setState({users: [], fetchFailed: false, fetchedResults: false});
    this.fetchData(selected + 1);
  }
  render() {
    if(this.state.fetchFailed) return <b>{translator.trans("Fetch failed! Try again later.")}</b>;
    if(!this.state.fetchedResults) return <Loading/>;
    const modalContent = this.setUpModal(this);
    const editControls = this.showEditControls();
    return (
      <div className="small-12 cell">
        {editControls}
        <h4>{translator.trans('Group users')}</h4>
        <section className="table-container">
          <UserGroupTable
            users         = {this.state.users}
            selectUser    = {this.handleUserSelect.bind(this)}
            selected      = {this.state.selectedUsers}
            userInfoClick = {this.props.userInfoClick}
            canEdit       = {this.state.canEdit}
            />
        </section>
        <Pagination
          pageCount           = {this.state.pageCount}
          pageRangeDisplayed  = {5}
          marginPagesDisplayed= {1}
          forcePage           = {this.state.page-1}
          breakLabel          = {<a>...</a>}
          previousLabel       = {translator.trans('‹ Previous')}
          nextLabel           = {translator.trans('Next ›')}
          containerClassName  = {'paginate'}
          disabledClassName   = {'disabled'}
          onPageChange        = {this.handlePageChange.bind(this)}
          />
        <Modal
          isOpen={this.state.showModal}
          onRequestClose={this.handleCloseModal.bind(this)}
          className={'react-reveal'}
          overlayClassName={'react-reveal-overlay'}
          >
          <button type="button" className="close-button" aria-label="Close reveal" onClick={this.handleCloseModal.bind(this)}>
            <span aria-hidden="true">×</span>
          </button>
          {modalContent}
        </Modal>
      </div>
    );
  }
}
