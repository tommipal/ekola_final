import $ from 'jquery';
import Foundation from 'foundation-sites';
import { Utils } from 'utils'

$(document).ready(()=>{
  $('.title-bar').foundation();
  $('.top-bar').foundation();
  Utils.extendDefaultObjects();

});