/* eslint valid-jsdoc: 0 */

import $ from 'jquery';
import selectize from 'selectize';
import Foundation from 'foundation-sites'

import { Ajax } from 'ajax';
import { Utils } from 'utils';
import { Translator } from 'translator';
const translator = new Translator(Utils.getLocale());

'use strict';

class NewEvaluation {
  constructor() {
    this.selectedGroups = []; // array of userGroup IDs in the selectize input
    this.init();
  }

  // First get available userGroups for the selectize dropdown, then initialize selectize with received data
  init() {
    new Ajax('GET', {query:'all'}, '/usergroup/find/')
    .send()
    .then((data) => {
      data = JSON.parse(data);
      this.initSelectize(data, this);
    });

    Utils.byId('create-group-btn').addEventListener('click', (e) => {
      const name = Utils.byId('group-name').value;
      const description = Utils.byId('group-description').value;
      new Ajax('POST', {name: name, description: description}, '/usergroup/new')
      .send()
      .then((response) => {
        const group = JSON.parse(response).userGroup;
        $('#new-group-modal').foundation('close');
        this.selectize.addOption({id:group.id, name:group.name});
        this.selectize.addItem(group.id);
        this.selectize.refreshOptions(false);
        Utils.byId('group-name').value = '';
        Utils.byId('group-description').value = '';
      });
    });
    $('#new-group-modal').on('closed.zf.reveal', () => {
      Utils.byId('group-name').value = '';
      Utils.byId('group-description').value = '';
    });
  }

  /**
   * Initialize selectize on element with id 'userGroups'
   * @param data JSON of userGroups
   * @param owner refers to 'this' because 'this' does not work in the selectize functions
   */
  initSelectize(data, owner) {
    this.select = $('#groups').selectize({
      delimiter: ',',
      persist: false,
      searchField: 'name',
      labelField: 'name',
      valueField: 'id',
      options: data,
      render: {
        item: function (item, escape) {
          return '<div>' +
            '<span class="selected item-label" data-user-group="' + escape(item.id) + '">' + escape(item.name) + '</span>' +
            '</div>';
        },
        option: function (item, escape) {
          return '<div>' +
            '<span class="item-label">' + escape(item.name) + '</span>' +
            '</div>';
        },
        option_create: function (item, escape) {
          return '<div class="create">' + translator.trans('Create') + ' <strong>' + escape(item.input) + '</strong>&hellip;</div>';
        }
      },
      onItemAdd: function (value, item) {
        const name = item[0].childNodes[0].textContent;
        owner.selectedGroups.push({id: value, name: name});
      },
      onItemRemove: function (value, item) {
        const name = item[0].childNodes[0].textContent;
        Utils.removeObjFromArray(owner.selectedGroups, 'id', value);
        this.addOption({id: value, name: name});
      },
      create: function (input) {
        Utils.byId('group-name').value = input;
        $('#new-group-modal').foundation('open');
        return false;
      }
    });
    this.selectize = this.select[0].selectize;
  }
}
window.onload = function () {
  $('#container').foundation();
  new NewEvaluation();
};
