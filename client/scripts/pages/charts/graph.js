/* eslint valid-jsdoc: 0 */

import * as d3 from "d3";
import _ from 'lodash';
import Utils from 'utils';

/**
 * Base class for all the graphs.
 */
export class Graph {
	/**
	 * @param {Array} answers array of answer objects to be displayed
	 * @param {String} containerId id of the html container the graph will be placed
	 * @param {Array} questions questions that will be displayed on the graph
	 */
  constructor(answers, containerId, questions) {
    this.answers = answers;
    this.container = document.getElementById(containerId);
    this.containerId = '#' + containerId;
    this.questions = questions;

    this.upperDomainX;
    this.lowerDomainX;
    this.upperDomainY;
    this.lowerDomainY;

    // https://github.com/wbkd/d3-extended
    d3.selection.prototype.moveToFront = function () {
      return this.each(function () {
        this.parentNode.parentNode.appendChild(this.parentNode);
      });
    };
  }

  drawGraph() {
    throw new Error('This method has not been implemented yet!');
  }

  showResultsByUserGroup(userGroupId) {
    throw new Error('This method has not been implemented yet!');
  }

  /**
   * @param {string} label
   * @param {number} lengthLimit
   */
  static formatLabel(label, lengthLimit = 17) {
    if (label.length < lengthLimit) return label;
    return label.substring(0, lengthLimit - 3).trim() + '...';
  }

	/**
	 * @return {Number} - number of different questions there are answers for. This can sometimes be different from the number of questions in the given survey.
	 */
  getQuestionCount() {
    let questions = [];
    this.answers.forEach(function (answer, i) {
      if (!questions.includes(answer.questionId)) {
        questions.push(answer.questionId);
      }
    });
    return questions.length;
  }

  /**
    * Find the highest value used in the given answers' criteria or answers.
    * @param {Array} criteria optional array of criteria objects. Method will default to criteria found in this.questions
    * @param {Array} answers optional array of answer objects. Method will default to this.answers
    * @return {number} highest value used in criteria definition or existing answer (in case the question got edited) as we don't want to dots to go outside the graph.
    */
  findUpperDomain(criteria = _.flatten(this.questions.map(question => question.criteria)), answers = this.answers) {
    let max;
    let value;
    criteria.forEach(criteria => {
      if (criteria.type === 'scale') {
        value = parseInt(criteria.options.max);
        if (!max) max = value;
        if (value > max) max = value;
      }
      else if (criteria.type === 'select') {
        criteria.options.choices.forEach((choice) => {
          value = parseInt(choice.value);
          if (!max) max = value;
          if (value > max) max = value;
        });
      }
    });
    answers.forEach(answer => {
      value = answer.values[answer.type].value;
      if (value > max) max = value;
    });
    //TODO the grid of graph will bug out if the number of ticks is limited and it cant be divided reasonably into equal parts.
    //for instance we usually limit the ticks to 10, so if D3 can't split it nicely into ~7-13 ticks the grid won't look nice.
    //one solution would be to round the domain up to a reasonable value considering the given scale: 157->200, 556->600 ...
    //but for this you would probably also need to take the lower end into consideration...
    return max;
  }

  /**
   * Find the lowest value used in the given answers' criteria or answers. Used for finding the lower end of a domain for certain graphs.
   * @param {Array} criteria optional array of criteria objects. Method will default to criteria found in this.questions
   * @param {Array} answers optional array of answer objects. Method will default to this.answers
   * @return {number} lowest value used in criteria definition or existing answer (in case the question got edited) as we don't want to dots to go outside the graph.
   */
  findLowerDomain(criteria = _.flatten(this.questions.map(question => question.criteria)), answers = this.answers) {
    let min = 0;
    let value;
    criteria.forEach(criteria => {
      if (criteria.type === 'scale') {
        value = parseInt(criteria.options.min);
        if (!min) min = value;
        if (value < min) min = value;
      }
      else if (criteria.type === 'select') {
        criteria.options.choices.forEach(choice => {
          value = parseInt(choice.value);
          if (!min) min = value;
          if (value < min) min = value;
        });
      }
    });
    answers.forEach(answer => {
      value = answer.values[answer.type].value;
      if (value < min) min = value;
    });
    if (min % 2 != 0) min--;
    return min;
  }

  /**
   * @param {Array} answers Array of answer objects
   * @param {Array} questions OPTIONAL. Questions taken straight from the survey. Provide this if you want to sort the answers to match the survey design.
   * @return {Array} formated answers
   * Static function for formatting answers.
   */
  static formatAnswers(answers, questions) {
    if (!(answers instanceof Array)) answers = [answers];
    answers.forEach(answer => {
      // Round the values to two decimals (needed for average answers)
      answer.values[answer.type].value = Math.round(100 * answer.values[answer.type].value) / 100;

      switch (answer.type) {
        case 'scale':
          break;
        case 'select':
          //Average action doesn't return answers with choice descriptions, so those need to be attached separately.
          //Try to find the choice closest to the average value and use its description.
          if (!answer.values[answer.type].description) {
            let matchingChoice;
            answer.criteria.options.choices.forEach(choice => {
              let diff = choice.value - answer.values[answer.type].value
              diff = diff < 0 ? -diff : diff;
              if (!matchingChoice || diff < matchingChoice.diff) matchingChoice = { diff: diff, choice: choice };
            });
            answer.values[answer.type].description = matchingChoice.choice.description
          }
          break;
        default:
          break;
      }
    });
    //Can't sort so just return answers.
    if (!questions) return answers;

    //Sort answers to match the survey design.
    if (!(questions instanceof Array)) questions = [questions];
    const sortedAnswers = [];
    questions.forEach(question => {
      question.criteria.forEach(criteria => {
        sortedAnswers.push(answers.filter(answer => answer.criteriaTitle === criteria.title && answer.question === question.id));
      });
    });
    return _.flatten(sortedAnswers);
  }

  /**
   * Checks if the default tick count will exceed the allowed limit and if yes, return the limit. Otherwise return default ticks for given the axis.
   * @param {string} axel 'x' or 'y'
   * @param {number} maxTicks maxium ticks allowed. Defaults to 10.
   * @return {number} ticks
   */
  getTickCount(axel = 'x', maxTicks = 10) {
    const ticks = axel == 'x' ? Math.abs(this.upperDomainX) + Math.abs(this.lowerDomainX) : Math.abs(this.upperDomainY) + Math.abs(this.lowerDomainY);
    return ticks < maxTicks ? ticks : maxTicks;
  }

  /**
   * Used in drawing gridlines for x axis. You can also leave out the tickCount and let d3 guess the number of ticks(?)
   * @param {function} x d3 scale
   * @param {number} tickCount number of ticks on the X axis
   */
  makeXGridlines(x, tickCount) {
    return d3.axisBottom(x).ticks(tickCount);
  }
  /**
   * Used in drawing gridlines for x axis.
   * @param {function} y d3 scale
   * @param {number} tickCount number of ticks on the Y axis
   */
  makeYGridlines(y, tickCount) {
    return d3.axisLeft(y).ticks(tickCount);
  }

}
