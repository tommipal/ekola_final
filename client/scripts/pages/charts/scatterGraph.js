/* eslint valid-jsdoc: 0 */
import $ from 'jquery';
import * as d3 from "d3";
import d3Tip from 'd3-tip';
d3.tip = d3Tip;

import { Graph } from "./graph";
import { Utils } from "utils";

import { Translator } from 'translator';
const translator = new Translator(Utils.getLocale());

/**
 * Class for displaying results of a survey with a scatter graph.
 * Each question criteria is represented with a dot. Note that stacking more that three dots will not work as intended so
 * this is only optimized for questions with three or less criteria.
 * Remember to hide all but one userGroup's dots when initially constructing the graph or alternatively only give answers from one userGroup at a time
 */
export class ScatterGraph extends Graph {
  /**
   * @param {Array} answers
   * @param {string} containerId
   * @param {Array} questions
   * @param {boolean} draw
   */
  constructor(answers, containerId, questions, draw = true) {
    super(answers, containerId, questions);
    this.margins = { 'left': 40, 'right': 40, 'top': 100, 'bottom': 100 };
    this.width = this.countWidth(400, 1000, this.getQuestionCount());
    this.height = 400;
    this.upperDomainX = this.getQuestionCount();
    this.upperDomainY = this.findUpperDomain();
    this.lowerDomainY = this.findLowerDomain();
    this.hiddenCriteria = [];
    this.selectedUserGroup = null;

    if (draw) {
      this.drawGraph();
    }
  }

  /**
   * Change the width of the graph based on the count of questions
   * @param {Number} min
   * @param {Number} max
   * @param {Number} count
   * @returns {Number} width
   */
  countWidth(min, max, count) {
    let width = min;
    if(count > 2) {
      width = min + (count * 25);
    } else if(count >= 28) {
      width = max;
    }
    return width;
  }

  /**
   * @param {Number} userGroupId
   * Goes through all the dot objects and removes 'hide' class from dots that are not associated
   * with any hidden criteria type and match the current selection while hiding the rest of the dots.
   */
  showResultsByUserGroup(userGroupId) {
    this.selectedUserGroup = userGroupId;
    const dots = this.container.querySelectorAll(`[data-user-group-id]`);
    for (const dot of dots) {
      const $dot = $(dot);
      if ($dot.data().userGroupId == userGroupId && this.hiddenCriteria.indexOf($dot.data().criteria) === -1 && $dot.hasClass('hide')) {
        $dot.removeClass('hide');
      }
      else if (!$dot.hasClass('hide')) {
        $dot.addClass('hide');
      }
    }
  }

  drawGraph() {
    const thisInstance = this;
    // create tooltip
    const tip = d3.tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(function (d) {
        const header = `<p><span class="d3-tip-value">${thisInstance.questions.find(question => d.question === question.id).title}<span class="d3-tip-value"></p>`;
        const crit = `<p>${translator.trans('Criteria:')} <span class="d3-tip-value">${d.criteriaTitle}</span></p>`;
        const value = `<p>${translator.trans('Value:')} <span class="d3-tip-value">${(Math.round(d.values[d.type].value * 100) / 100)}</span></p>`;
        const desc = `<p>${translator.trans('Description:')} <span class="d3-tip-value">${d.values[d.type].description}</span></p>`;
        if (d.type === 'select' && d.values[d.type].description) {
          return header + crit + value + desc;
        } else {
          return header + crit + value;
        }
      });

    // attach question index to every answer for x-axis labels
    const questionOrder = this.questions.map(question=>question.id);
    this.answers.forEach((answer) => {
      answer.questionIndex = questionOrder.indexOf(answer.question) + 1;
    });

    // Setup color scheme
    const colors = d3.scaleOrdinal(d3.schemeCategory10);

    // Create svg element
    const svg = d3.select(this.containerId).append('svg')
      .attr('width', this.width)
      .attr('height', this.height)
      .append('g')
      .attr('transform', `translate(${this.margins.left},${this.margins.top})`);
    svg.call(tip);
    // X axis range and domain
    const x = d3.scaleBand()
      .padding(1)
      .domain(questionOrder.map((question, index) => { // NOTE: you can map over answers to hide questions without answers
        return (index + 1);
      }))
      .range([0, this.width - this.margins.left - this.margins.right]);
    // Y axis range and domain
    const y = d3.scaleLinear()
      .domain([this.lowerDomainY, this.upperDomainY])
      .range([this.height - this.margins.top - this.margins.bottom, 0]);

    // Append y axis and x axis into svg element
    svg.append('g').attr('class', 'x axis').attr('transform', 'translate(0,' + y.range()[0] + ')');
    svg.append('g').attr('class', 'y axis');

    // Remove decimals from axis numbers
    const formatxAxis = d3.format('.0f');

    // Define how many ticks on each axis
    const xAxis = d3.axisBottom(x).tickFormat(formatxAxis).ticks(this.upperDomainX).tickPadding(2);
    const yAxis = d3.axisLeft(y).ticks(this.upperDomainY > 10 ? 10 : this.upperDomainY).tickPadding(2);

    svg.selectAll('g.y.axis').call(yAxis);
    svg.selectAll('g.x.axis').call(xAxis)
      .selectAll("text")
      .attr("y", 10);

    // Append bottom text
    svg.append('text')
      .attr('fill', '#414241')
      .attr('text-anchor', 'end')
      .attr('x', this.width / 2)
      .attr('y', this.height - this.margins.bottom - 50)
      .text(translator.trans('Question'));

    // X gridlines
    svg.append("g")
      .attr("class", "grid")
      .attr("transform", "translate(0," + (this.height - this.margins.top - this.margins.bottom) + ")") // push up
      .call(this.makeXGridlines(x, this.upperDomainX)
        .tickSize(-(this.height - this.margins.top - this.margins.bottom)) // set line length
        .tickFormat("") // disable second set of labels
      );
    // Y gridlines
    svg.append("g")
      .attr("class", "grid")
      .call(this.makeYGridlines(y, this.upperDomainY > 10 ? 10: this.upperDomainY)
        .tickSize(-(this.width - this.margins.left - this.margins.right))
        .tickFormat("")
      );

    // Unique identifier for each answer
    const answer = svg.selectAll('g.node').data(this.answers, (d) => {
      return d.questionIndex + '-' + d.criteriaTitle;
    });

    // Container for each dot
    const answerGroup = answer.enter().append('g').attr('class', 'node hide')
      .attr('data-user-group-id', (d) => {
        return d.userGroup;
      })
      .attr('data-criteria', (d) => {
        return d.criteriaTitle;
      })
      .attr('transform', (d) => {
        return `translate(${x(d.questionIndex)},${y(d.values[d.type].value)})`;
      });

    // Create a new dot on the scatterplot
    const circle = answerGroup.append('circle');
    circle.attr('r', 6)
      .attr('class', (d) => {
        return 'dot question-' + d.questionIndex + '-' + d.values[d.type].value + '-' + d.userGroup;
      })
      // Try to find dot's that would overlap and slightly adjust their cordinates. Only works with up to three overlaping dots
      .attr('cx', (d) => {
        const sameClasses = this.getOverlappingDots(d);
        if (sameClasses.length === 3) {
          if (sameClasses[0].cx.baseVal.value !== -4) {
            sameClasses[0].cx.baseVal.value = -4;
          }
          if (sameClasses[1].cx.baseVal.value !== 4) {
            sameClasses[1].cx.baseVal.value = 4;
          }
          if (sameClasses[2].cx.baseVal.value !== 0) {
            sameClasses[2].cx.baseVal.value = 0;
          }
        }
        else if (sameClasses.length === 2) {
          if (sameClasses[0].cx.baseVal.value !== 4) {
            sameClasses[0].cx.baseVal.value = 4;
          }
          if (sameClasses[1].cx.baseVal.value !== -4) {
            sameClasses[1].cx.baseVal.value = -4;
          }
        }
        else {
          return 0;
        }
      })
      .attr('cy', (d) => {
        const sameClasses = this.getOverlappingDots(d);

        if (sameClasses.length === 3) {
          if (sameClasses[0].cy.baseVal.value !== 4) {
            sameClasses[0].cy.baseVal.value = 4;
          }
          if (sameClasses[1].cy.baseVal.value !== 4) {
            sameClasses[1].cy.baseVal.value = 4;
          }
          if (sameClasses[2].cy.baseVal.value !== -4) {
            sameClasses[2].cy.baseVal.value = -4;
          }
        }
        else {
          return 0;
        }
      })
      // Add event listener to a dot
      .on('mouseover', function (d) {
        d3.select(this).moveToFront();
        d3.select(this).attr('r', 8);
        tip.show(d);
      })
      .on('mouseout', function (d) {
        tip.hide(d);
        d3.select(this).attr('r', 6);
      })
      .style('fill', (d) => {
        return colors(d.criteriaTitle);
      });

    // Draw legend of criteria types
    let criteriaCount = _.uniq(this.answers.map(answer => answer.criteria.title)).length;
    const legend = svg.selectAll('.legend')
      .data(colors.domain())
      .enter().append('g')
      .attr('class', criteriaCount < 5 ? 'legend' : 'hide')
      .attr('data-legend-criteria', (d) => {
        return d;
      })
      .attr('transform', (d, i) => {
        return `translate(0,${((i * 20) - 100)})`;
      })
      .on('click', function (d) {
        if (thisInstance.hiddenCriteria.indexOf(d) === -1) {
          thisInstance.hiddenCriteria.push(d);
        } else {
          thisInstance.hiddenCriteria.splice(thisInstance.hiddenCriteria.indexOf(d), 1);
        }
        const sameCriteria = document.querySelectorAll(thisInstance.containerId + ' g.node[data-criteria="' + d + '"][data-user-group-id="' + thisInstance.selectedUserGroup + '"]');
        const $legend = $(`[data-legend-criteria="${d}"]`);
        for (const dot of sameCriteria) {
          if (dot.classList.contains('hide')) {
            dot.classList.remove('hide');
            $legend.css('text-decoration', 'none');
          } else {
            dot.classList.add('hide');
            $legend.css('text-decoration', 'line-through');
          }
        }
      });

    legend.append('text')
      .attr('x', 20)
      .attr('y', 9)
      .attr('dy', '.35em')
      .style('text-anchor', 'start')
      .text((d) => {
        return d;
      });

    legend.append('rect')
      .attr('x', 0)
      .attr('width', 18)
      .attr('height', 18)
      .style('fill', colors);
  }

  // gridlines in x axis function
  makeXGridlines(x, tickCount) {
    return d3.axisBottom(x).ticks(tickCount);
  }

  // gridlines in y axis function
  makeYGridlines(y, tickCount) {
    return d3.axisLeft(y).ticks(tickCount);
  }

  /**
   * Find all dots that have the same or similiar value as the given answer and hence it should always return at least one dot.
   * @param {Answer} answer
   * @param {number} similiarity percentage of Y-domain's value used for similiarity comparison. Defaults to 3%.
   * @return {Array} overlappingDots
   */
  getOverlappingDots(answer, similiarity=3) {
    similiarity = 100/similiarity;
    let value = answer.values[answer.type].value;
    let overlappingDots = [];
    let allowedDifferenceInValue = Math.round(100 * (this.upperDomainY / similiarity)) / 100;
    for (let i = value - allowedDifferenceInValue; i <= value + allowedDifferenceInValue; i = Math.round(100 * (i + 0.01)) / 100) {
      overlappingDots = overlappingDots.concat(Utils.toArray(Utils.byClass('question-' + answer.questionIndex + '-' + i + '-' + answer.userGroup)));
    }
    return overlappingDots;
  }

}
