/* eslint valid-jsdoc: 0 */
import $ from 'jquery';
import * as d3 from "d3";
import d3Tip from 'd3-tip';
d3.tip = d3Tip;

import { Graph } from "./graph";
import { Utils } from "utils";
import { Translator } from 'translator';

const translator = new Translator(Utils.getLocale());

/**
 * Class for displaying a question based scatter graph.
 * Needs all the answers to the given question (not the average representing ones).
 * Remember to hide all but one userGroup's bars or the tooltip will not work as intended - or
 * alternatively give only answers from one userGroup at a time.
 */
export class ScatterGraphSmall extends Graph {
	/**
	 * @param {Array} answers
	 * @param {string} containerId
	 * @param {Array} questions
	 * @param {boolean} draw
	 */
	constructor(answers, containerId, questions, draw = true) {
		super(answers, containerId, questions);
		this.margins = { top: 50, right: 60, bottom: 150, left: 40 };
		this.width = 500 - this.margins.left - this.margins.right;
		this.height = 400 - this.margins.top - this.margins.bottom;
		this.upperDomainY = this.findUpperDomain();
		this.lowerDomainY = this.findLowerDomain();
		this.maxCount = this.findLargestStackCount();
		this.maxRadiusForDot = this.maxCount < 5 ? 8 : 16;
		this.minRadiusForDot = 4;

		if (draw) this.drawGraph();
	}

	showResultsByUserGroup(userGroupId) {
		const dots = this.container.querySelectorAll('[data-user-group-id]');
		for (const dot of dots) {
      const $dot = $(dot);
      if ($dot.data().userGroupId == userGroupId) {
        $dot.removeClass('hide');
      }
      else if (!$dot.hasClass('hide')) {
        $dot.addClass('hide');
      }
		}
	}

	drawGraph() {
		// create tooltip
    const tip = d3.tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(function (d) {
        const value = `<p>${translator.trans('Value:')} <span class="d3-tip-value">${(Math.round(d.values[d.type].value * 100) / 100)}</span></p>`;
        const count = `<p>${translator.trans('Answers:')} <span class="d3-tip-value">${d.count}</span></p>`;
        const desc = `<p>${translator.trans('Description:')} <span class="d3-tip-value">${d.values[d.type].description}</span></p>`;
        if (d.type === 'select' && d.values[d.type].description) {
          return value + count + desc;
        } else {
          return value + count;
        }
      });

		const colors = d3.scaleOrdinal(d3.schemeCategory10);
		const thisInstance = this;

		// append svg and g
		let svg = d3.select(this.containerId).append("svg")
			.attr("width", this.width + this.margins.left + this.margins.right)
			.attr("height", this.height + this.margins.top + this.margins.bottom)
			.append("g") // push the graph slightly to right
			.attr("transform", "translate(" + this.margins.left + "," + this.margins.top + ")");
    svg.call(tip);
		// set ranges and domains
		const x = d3.scaleBand().range([0, this.width])
			.padding(1) // with dots insteand of bars this needs to push the dots directly above the labels.
			.domain(this.answers.map(function (d) {
				return Graph.formatLabel(d.criteriaTitle);
			}));
		const y = d3.scaleLinear()
			.domain([this.lowerDomainY, this.upperDomainY])
			.range([this.height, 0]);

		// Append y axis and x axis into svg element
		svg.append('g').attr('class', 'x axis').attr('transform', 'translate(0,' + y.range()[0] + ')');
		svg.append('g').attr('class', 'y axis');

		// Define how many ticks on each axis
		const xAxis = d3.axisBottom(x).ticks(this.getQuestionCount()).tickPadding(2);
		const yAxis = d3.axisLeft(y).ticks(this.upperDomainY > 10 ? 10 : this.upperDomainY).tickPadding(2);
		svg.selectAll('g.y.axis').call(yAxis);
		svg.selectAll('g.x.axis').call(xAxis)
			.selectAll("text") // select and rotate all labels
			.attr("y", 10)
			.attr("x", -9)
			.attr("dy", ".35em")
			.attr("transform", "rotate(-45)")
			.style("text-anchor", "end");

		// X gridlines
		svg.append("g")
			.attr("class", "grid")
			.attr("transform", "translate(0," + (this.height) + ")") // push up
			.call(this.makeXGridlines(x, this.upperDomainX)
				.tickSize(-this.height) // set line length
				.tickFormat("") // disable second set of labels
			);
		// Y gridlines
		svg.append("g")
			.attr("class", "grid")
			.call(this.makeYGridlines(y, this.upperDomainY > 10 ? 10 : this.upperDomainY)
				.tickSize(-this.width)
				.tickFormat("")
			);

		// Add Dots
		svg.selectAll(".dot")
			.data(this.answers)
			.enter().append("circle")
			.attr("class", "dot")
			//set radius so that it gets scaled to the defined boundaries
			.attr('r', (d) => {
				return this.minRadiusForDot + (d.count / this.maxCount * (this.maxRadiusForDot - this.minRadiusForDot));
			})
			.attr('data-user-group-id', (d) => {
				return d.userGroup;
			})
			.attr("cx", (d) => {
				return x(Graph.formatLabel(d.criteriaTitle));
			})
			.attr("cy", function (d) {
				let value = d.values[d.type].value;
				//Try to modify dot's y-coordinate so that it won't sit on top of x-axis.
				//TODO this could really be improved / or if it turns out this doesn't fit the graph in some cases, remove it until a better implementation is created.
				if (value === 0) value = 0.02 * thisInstance.upperDomainY;
				return y(value);
			})
			.style('fill', (d) => {
				return colors(d.criteriaTitle);
			})
			.on('mouseover', tip.show)
			.on('mouseout', tip.hide);
	}

	/**
	 * @return {Number} highest number of stacking dots; in other words answers with the same value for a particular question criteria.
	 * Also sets the count needed in dot label and calculating dot radius for every answer.
	 */
	findLargestStackCount() {
		let max = 0;
		this.answers.forEach(answer => {
			const count = this.answers.filter(a => {
				{
					return answer.criteriaTitle === a.criteriaTitle
						&& answer.values[answer.type].value === a.values[a.type].value
						&& answer.userGroup == a.userGroup;
				}
			}).length;
			answer.count = count;
			if (count > max) max = count;
		});
		return max;
	}
}
