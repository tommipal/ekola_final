/* eslint valid-jsdoc: 0 */
import $ from 'jquery';
import * as d3 from 'd3';
import d3Tip from 'd3-tip';
d3.tip = d3Tip;
import _ from 'lodash';

import { Graph } from './graph';
import { Utils } from 'utils';

import { Translator } from 'translator';
const translator = new Translator(Utils.getLocale());

export class FourWayGraph extends Graph {
  constructor(answers, containerId, questions, draw = true) {
    super(answers, containerId, questions);
    this.margins = { top: 40, right: 180, bottom: 70, left: 70 };
    this.width = 400;
    this.height = 400;
    this.criteriaForXAxis;
    this.criteriaForYAxis;
    let questionIndex = 0;
    this.activeQuestion = this.questions[questionIndex];
    this.hiddenDots = [];
    this.criteria = _.uniq(
      this.answers.filter((answer) => answer.question === this.activeQuestion.id)
        .map((answer) => answer.criteriaTitle)
    );
    this.userGroups = _.uniq(this.answers.map((answer) => answer.userGroup));

    //bind eventlisteners for selecting question
    Utils.byId('next-question').addEventListener('click', () => {
      questionIndex++;
      if (questionIndex >= this.questions.length) { questionIndex = 0; }
      if(this.questions.length > 1) {
        this.changeQuestion(questionIndex);
      }
    });
    Utils.byId('prev-question').addEventListener('click', () => {
      questionIndex--;
      if (questionIndex < 0) { questionIndex = this.questions.length - 1; }
      if(this.questions.length > 1) {
        this.changeQuestion(questionIndex);
      }
    });

    //initialize and draw
    this.setup();
    if (draw) { this.drawGraph(); }
  }

  /**
   * @param {number} i question index
   */
  changeQuestion(i) {
    document.querySelector(this.containerId).innerHTML = '';
    const fourWayTip = document.querySelector('.d3-tip.four-way');
    if(fourWayTip) fourWayTip.parentNode.removeChild(fourWayTip);
    this.activeQuestion = this.questions[i];
    this.criteria = _.uniq(
      this.answers.filter((answer) => answer.question === this.activeQuestion.id)
        .map((answer) => answer.criteriaTitle)
    );
    this.setup();
    this.drawGraph();
  }

  /**
   * Run this ALWAYS before trying to run drawGraph()!
   * Does all the non D3 related initing, like appending x/y-selection, binding eventlistener etc.
   */
  setup() {
    //append two selects to the page for selecting criteria for each axis.
    Utils.byId('active-question').innerHTML = this.activeQuestion.title;
    if (this.criteria.length < 2) return this.container.insertAdjacentHTML('beforeend', `<p>${translator.trans(`This question doesn't have enough criteria to be drawn.`)}</p>`);
    const html = `
    <div class="grid-x">
        <div class="input-group cell small-12 medium-6 large-4">
          <span class="input-group-label">${translator.trans('X-axis')}</span>
          <select id="x">${this.criteria.map((criteria) => `<option value="${criteria}">${criteria}</option>`).join('')}</select>
        </div>
        <div class="input-group cell small-12 medium-6 large-4">
          <span class="input-group-label">${translator.trans('Y-axis')}</span>
          <select id="y">${this.criteria.map((criteria) => `<option value="${criteria}">${criteria}</option>`).join('')}</select>
        </div>
    </div>`;
    this.container.insertAdjacentHTML('beforeend', html);

    //set selects to point to options at index one and two if the selected criteria is not available in the new question.
    //otherwise the selections will stick when changing questions.
    //this.criteria gets updated every time changeQuestion is run.
    const inputs = this.container.querySelectorAll('select');
    if (!this.criteria.includes(this.criteriaForXAxis)) { this.criteriaForXAxis = inputs[0].value; }
    if (!this.criteria.includes(this.criteriaForYAxis)) { this.criteriaForYAxis = inputs[1].value = this.criteria[1]; }
    inputs[0].value = this.criteriaForXAxis;
    inputs[1].value = this.criteriaForYAxis;

    //add eventListeners for the selects
    inputs[0].addEventListener('change', (e) => { // change X
      this.criteriaForXAxis = e.target.value;
      this.container.removeChild(this.container.querySelector('svg'));
      this.drawGraph();
    });
    inputs[1].addEventListener('change', (e) => { // change Y
      this.criteriaForYAxis = e.target.value;
      this.container.removeChild(this.container.querySelector('svg'));
      this.drawGraph();
    });
  }

  /**
   * 1. calculate domains for the selected axis
   * 2. form the dots for the graph by going through the answers
   * 3. do the usual graph building with D3
   */
  drawGraph() {
    if (this.criteria.length < 2) { return; } //selected question isn't feasible.
    //calculate domains for Y axis
    let answers = this.answers.filter((answer) => answer.question === this.activeQuestion.id && answer.criteriaTitle === this.criteriaForYAxis);
    let criteria = this.activeQuestion.criteria.filter(criteria => criteria.title === this.criteriaForYAxis);
    this.upperDomainY = this.findUpperDomain(criteria, answers);
    this.lowerDomainY = this.findLowerDomain(criteria, answers);
    //calculate domains for x axis
    answers = this.answers.filter((answer) => answer.question === this.activeQuestion.id && answer.criteriaTitle === this.criteriaForXAxis);
    criteria = this.activeQuestion.criteria.filter(criteria => criteria.title === this.criteriaForXAxis);
    this.upperDomainX = this.findUpperDomain(criteria, answers);
    this.lowerDomainX = this.findLowerDomain(criteria, answers);

    let dots = [];
    const thisInstance = this;
    const colors = d3.scaleOrdinal(d3.schemeCategory20);

    /**
     * This is designed for one (average value displaying) answer per userGroup, so only use the first answer (we expect there to be only one anyway).
     * Don't add a dot (by returning early) for userGroups that do not have answers to represent both axels.
     */
    this.userGroups.forEach((userGroup) => {
      let answerForXAxis = this.answers.filter((answer) => {
        return answer.userGroup === userGroup && answer.criteriaTitle === this.criteriaForXAxis && answer.question === this.activeQuestion.id;
      });
      if (answerForXAxis.length == 0) return;
      if (answerForXAxis.length > 1) console.warn(`Expected only one answer. Got ${answerForXAxis.length}.`);
      answerForXAxis = answerForXAxis[0];
      let answerForYAxis = this.answers.filter((answer) => {
        return answer.userGroup === userGroup && answer.criteriaTitle === this.criteriaForYAxis && answer.question === this.activeQuestion.id;
      });
      if (answerForYAxis.length == 0) return;
      if (answerForYAxis.length > 1) console.warn(`Expected only one answer. Got ${answerForYAxis.length}.`);
      answerForYAxis = answerForYAxis[0];
      dots.push({ x: answerForXAxis.values[answerForXAxis.type].value, y: answerForYAxis.values[answerForYAxis.type].value, userGroup: userGroup });
    });
    const overlapping = this.getOverlapping(dots);
    //create tooltip
    const tip = d3.tip()
      .attr('class', 'd3-tip four-way')
      .offset([-10, 0])
      .html(function (d) {
        return `<p><span class="d3-tip-value">${overlapping[`x${d.x}y${d.y}`].join(', ')}</span></p>
                <p>${thisInstance.criteriaForXAxis}: <span class="d3-tip-value">${d.x}</span></p>
                <p>${thisInstance.criteriaForYAxis}: <span class="d3-tip-value">${d.y}</span></p>`;
      });

    //append svg and g
    const svg = d3.select(this.containerId).append('svg')
      .attr('width', this.width + this.margins.left + this.margins.right)
      .attr('height', this.height + this.margins.top + this.margins.bottom)
      .append('g')
      .attr('transform', `translate(${this.margins.left}, ${this.margins.top})`);
    svg.call(tip);

    //set ranges and domains
    const x = d3.scaleLinear().range([0, this.width]).domain([this.lowerDomainX, this.upperDomainX]);
    const y = d3.scaleLinear().range([this.height, 0]).domain([this.lowerDomainY, this.upperDomainY]);
    //get tick counts for both axes
    const tickCountX = this.getTickCount('x', 10);
    const tickCountY = this.getTickCount('y', 10);

    //append x axis and define ticks
    svg.append('g')
      .attr('class', 'axis') // A: editing label font
      .attr('transform', `translate(0, ${this.height / 2})`) // push axis down!!!
      .call(d3.axisBottom(x).ticks(tickCountX).tickPadding(2));
    // adding label for Y-axis (bottom) -> add margin to bottom
    svg.append('text')
      .attr('x', (this.width) / 2)
      .attr('y', this.height + this.margins.top)
      .style('text-anchor', 'middle')
      .text(Graph.formatLabel(this.criteriaForYAxis));

    // append y Axis and define ticks
    svg.append('g')
      .attr('transform', `translate(${this.width / 2}, 0 )`) // push y-axis to the right
      .call(d3.axisLeft(y).ticks(tickCountY).tickPadding(2));
    // label for X-axis (left side) -> add margin to left
    svg.append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', 0 - this.margins.left) // ^ due to rotation moves the x position
      .attr('x', 0 - (this.height / 2)) // y position
      .attr('dy', '1em') // shift to the right to move the text 'back' to the edge of the canvas
      .style('text-anchor', 'middle')
      .text(Graph.formatLabel(this.criteriaForXAxis));

    //append X gridlines
    svg.append('g')
      .attr('class', 'grid')
      .attr('transform', `translate(0, ${this.height})`) // adjust position to take into account the coordinates system
      .call(this.makeXGridlines(x, tickCountX)
        .tickSize(-this.height) // ~push it up
        .tickFormat('') // disable second set of labels
      );
    //append Y gridlines
    svg.append('g')
      .attr('class', 'grid')
      .call(this.makeYGridlines(y, tickCountY)
        .tickSize(-this.width)
        .tickFormat('')
      );

    // adding a scatter plot
    svg.selectAll('dot')
      .data(dots).enter() // for each item in data
      .append('circle')
      .attr('r', 6) // radius
      .attr('data-user-group-fw', (d)=>{
        return d.userGroup;
      })
      .attr('cx', (d) => {
        return x(d.x);
      }) // coordinates
      .attr('cy', (d) => {
        return y(d.y);
      })
      .style('fill', (d) => {
        return colors(d.userGroup);
      })
      .on('mouseover', function (d) {
        d3.select(this).attr('r', 8);
        tip.show(d);
      })
      .on('mouseout', function (d) {
        d3.select(this).attr('r', 6);
        tip.hide(d);
      });

    const legend = svg.selectAll('.legend')
      .data(colors.domain())
      .enter().append('g')
      .attr('class', 'legend')
      .attr('data-legend-user-group', (d)=>{
        return d;
      })
      .attr('transform', (d, i) => {
        return `translate(${this.width + 14},${i * 20})`;
      })
      .on('click', (d) => {
        this.hiddenToggle(d);
      });

    legend.append('text')
      .attr('x', 20)
      .attr('y', 9)
      .attr('dy', '.35em')
      .style('text-anchor', 'start')
      .text((d) => {
        const group = groups.find((group) => { return group.id === d; });
        return Graph.formatLabel(group.name);
      });

    legend.append('rect')
      .attr('x', 0)
      .attr('width', 18)
      .attr('height', 18)
      .style('fill', colors);

    this.hiddenDots.forEach((d)=>{
      this.hideHidden(d);
    });
  }

  hideHidden(d) {
    const $legend = $(`[data-legend-user-group="${d}"]`);
    const sameUserGroups = document.querySelectorAll(`[data-user-group-fw="${d}"]`);
    for (const dot of sameUserGroups) {
      if (!dot.classList.contains('hide')) {
        dot.classList.add('hide');
      }
      $legend.css('text-decoration', 'line-through');
    }
  }
  hiddenToggle(d) {
    if (this.hiddenDots.indexOf(d) === -1) {
      this.hiddenDots.push(d);
    } else {
      this.hiddenDots.splice(this.hiddenDots.indexOf(d), 1);
    }
    const $legend = $(`[data-legend-user-group="${d}"]`);
    const sameUserGroups = document.querySelectorAll(`[data-user-group-fw="${d}"]`);
    for (const dot of sameUserGroups) {
      if (dot.classList.contains('hide')) {
        dot.classList.remove('hide');
        $legend.css('text-decoration','none');
      } else {
        dot.classList.add('hide');
        $legend.css('text-decoration', 'line-through');

      }
    }
  }

  /**
   * Create an object that contains all userGroups indexed under their coordinates,
   * so you can easily find overlapping dots with a string like: 'x10y5'
   * @param dots
   * @returns {Object}
   */
  getOverlapping(dots) {
    const overlapping = {};
    dots.forEach((d) => {
      if(!overlapping[`x${d.x}y${d.y}`]) {
        overlapping[`x${d.x}y${d.y}`] = [groups.find((group) => { return group.id === d.userGroup; }).name];
      } else {
        overlapping[`x${d.x}y${d.y}`].push(groups.find((group) => { return group.id === d.userGroup; }).name);
      }
    });
    return overlapping;
  }
}
