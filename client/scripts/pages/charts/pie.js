/* eslint valid-jsdoc: 0 */

/**
 * @param answers - an array of objects
 * @param container - name of id in document
 */
const initPie = (answers, container) => {
  const toSimpleArray = (answers) => {
    let newArr = [];
    for (let i = 0; i<answers.length; i++) {
      newArr.push(answers[i].answer);
    }
    return newArr;
  };
  answers = toSimpleArray(answers);

  const totalData = answers.length;

// Convert array to an object with label and total occurrences of each item
  const red = (arr) => {
    const counts = {};
    arr.forEach((x) => {
      counts[x] = (counts[x] || 0) + 1;
    });
    return counts;
  };

// Convert reduced object to an array of objects
  const getPieData = (obj, length) => {
    const keys = Object.keys(obj);
    const data = [];
    for (let i = 0; i < keys.length; i++) {
      const obj = {'label': keys[i], 'value': counts[keys[i]] / length};
      data.push(obj);
    }
    return data;
  };

  const width = 600;
  const height = 450;
  const radius = Math.min(width, height) / 2;
  const counts = red(answers);
  const data = getPieData(counts, totalData);

  const key = (d) => {
    return d.data.label;
  };
  const midAngle = (d) => {
    return d.startAngle + (d.endAngle - d.startAngle) / 2;
  };

  const createGraph = () => {
    const pie = d3.pie()
      .sort(null)
      .value((d) => {
        return d.value;
      });

    /* Hover tooltip */
    const tooltip = d3.select('#' + container).append('div').attr('class', 'pie-tooltip');
    tooltip.append('div').attr('class', 'pie-label');
    tooltip.append('div').attr('class', 'count');
    tooltip.append('div').attr('class', 'percent');

    const arc = d3.arc()
      .outerRadius(radius * 0.8)
      .innerRadius(0);

    const outerArc = d3.arc()
      .innerRadius(radius * 0.9)
      .outerRadius(radius * 0.5);

    const svg = d3.select('#' + container).append('svg').attr('width', width)
      .attr('height', height).append('g');

    svg.append('g').attr('class', 'slices');
    svg.append('g').attr('class', 'labels');
    svg.append('g').attr('class', 'lines');
    svg.attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');

    const color = d3.scaleOrdinal()
      .range(['#577590', '#F3CA40', '#F2A541', '#F08A4B', '#D78A76', '#DB504A', '#FF6F59', '#4e8985', '#43AA8B', '#B2B09B']);

    drawPie(svg, color, arc, outerArc, pie, tooltip);
  };

  /* ------- PIE SLICES -------*/
  const drawPie = (svg, color, arc, outerArc, pie, tooltip) => {
    const slice = svg.select('.slices').selectAll('path.slice')
      .data(pie(data), key);

    slice.enter()
      .insert('path')
      .attr('class', 'slice')
      .attr('d', arc)
      .attr('fill', (d) => {
        return color(d.data.label);
      })
      .on('mouseover', (d) => {
        const percent = Math.round(100 * d.data.value);
        tooltip.select('.pie-label').html(d.data.label);
        tooltip.select('.count').html('Total: ' + d.data.value * totalData);
        tooltip.select('.percent').html(percent + '%');
        tooltip.style('display', 'block');
      })
      .on('mouseout', () => {
        tooltip.style('display', 'none');
      })
      .on('mousemove', (d) => {
        tooltip.style('top', (d3.event.layerY + 10) + 'px')
          .style('left', (d3.event.layerX + 10) + 'px');
      });

    slice.exit()
      .remove();

    slice.on('mouseover', (d) => {
      const percent = Math.round(100 * d.data.value);
      tooltip.select('.pie-label').html(d.data.label);
      tooltip.select('.count').html(d.data.value * totalData);
      tooltip.select('.percent').html(percent + '%');
      tooltip.style('display', 'block');
    });

    slice.on('mouseout', () => {
      tooltip.style('display', 'none');
    });

    slice.on('mousemove', (d) => {
      tooltip.style('top', (d3.event.layerY + 10) + 'px')
        .style('left', (d3.event.layerX + 10) + 'px');
    });

    drawLabels(svg, pie, outerArc);
    drawLines(svg, pie, outerArc, arc);
  };

  /* ------- TEXT LABELS -------*/
  const drawLabels = (svg, pie, outerArc) => {
    const text = svg.select('.labels').selectAll('text')
      .data(pie(data), key);

    text.enter()
      .append('text')
      .attr('dy', '.35em')
      .text((d) => {
        return d.data.label;
      })
      .attr('transform', (d) => {
        const pos = outerArc.centroid(d);
        pos[0] = radius * (midAngle(d) < Math.PI ? 1 : -1);
        return 'translate(' + pos + ')';
      })
      .style('text-anchor', (d) => {
        return midAngle(d) < Math.PI ? 'start' : 'end';
      });

    text.exit()
      .remove();
  };

  /* ------- POLYLINES -------*/
  const drawLines = (svg, pie, outerArc, arc) => {
    const polyline = svg.select('.lines').selectAll('polyline')
      .data(pie(data), key);

    polyline.enter()
      .append('polyline')
      .attr('points', (d) => {
        const pos = outerArc.centroid(d);
        pos[0] = radius * 0.95 * (midAngle(d) < Math.PI ? 1 : -1);
        return [arc.centroid(d), outerArc.centroid(d), pos];
      });

    polyline.exit()
      .remove();
  };

  createGraph();

};