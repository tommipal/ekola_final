/* eslint valid-jsdoc: 0 */
import $ from 'jquery';
import * as d3 from "d3";
import d3Tip from 'd3-tip';
d3.tip = d3Tip;

import { Graph } from "./graph";
import { Utils } from "utils";
import { Translator } from 'translator';
const translator = new Translator(Utils.getLocale());

/**
 * Class for drawing a bar graph to display the results of a single question.
 * There is one bar representing each of the question criteria per userGroup.
 * Remember to hide all but one userGroup's bars or the tooltip will not work as intended - or
 * alternatively give only answers from one userGroup at a time.
 */
export class BarGraph extends Graph {
  constructor(answers, containerId, questions, draw = true) {
    super(answers, containerId, questions);
    this.margins = { top: 50, right: 60, bottom: 150, left: 40 };
    this.width = 500 - this.margins.left - this.margins.right;
    this.height = 400 - this.margins.top - this.margins.bottom;
    this.upperDomainY = this.findUpperDomain();
    this.lowerDomainY = 0; //Our structure for barGraph doesn't support negative values so always use 0! - this doesn't break the graph.
    this.minimumHeight = 0.02 * this.upperDomainY; //draw bars with height of 0 with this value.
    if (draw) {
      this.drawGraph();
    }
  }

  /**
   * @param {Number} userGroupId
   */
  showResultsByUserGroup(userGroupId) {
    const rectangles = this.container.querySelectorAll('[data-user-group-id]');
    for (const rectangle of rectangles) {
      const $rectangle = $(rectangle);
      if ($rectangle.data().userGroupId == userGroupId && $rectangle.hasClass('hide')) {
        $rectangle.removeClass('hide');
      }
      else if ($rectangle.data().userGroupId != userGroupId && !$rectangle.hasClass('hide')) {
        $rectangle.addClass('hide');
      }
    }
  }

  drawGraph() {
    //create tooltip
    const tip = d3.tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(function (d) {
      return `<p>${translator.trans('Average:')} <span class="d3-tip-value">${(Math.round(d.values[d.type].value * 100) / 100)}</span></p>`;
    });

    const colors = d3.scaleOrdinal(d3.schemeCategory10);
    const thisInstance = this;

    // append svg and g
    let svg = d3.select(this.containerId).append("svg")
      .attr("width", this.width + this.margins.left + this.margins.right)
      .attr("height", this.height + this.margins.top + this.margins.bottom)
      .append("g") // push the graph slightly to right
      .attr("transform", "translate(" + this.margins.left + "," + this.margins.top + ")");
    svg.call(tip);
    // set ranges and domains
    const x = d3.scaleBand().range([0, this.width])
      .padding(0.05)
      .domain(this.answers.map(function (d) {
        return Graph.formatLabel(d.criteriaTitle);
      }));
    const y = d3.scaleLinear()
      .domain([this.lowerDomainY, this.upperDomainY])
      .range([this.height, 0]);

    // Append y axis and x axis into svg element
    svg.append('g').attr('class', 'x axis').attr('transform', 'translate(0,' + y.range()[0] + ')');
    svg.append('g').attr('class', 'y axis');

    // Define how many ticks on each axis
    const xAxis = d3.axisBottom(x).ticks(this.getQuestionCount()).tickPadding(2);
    const yAxis = d3.axisLeft(y).ticks(this.upperDomainY > 10 ? 10 : this.upperDomainY).tickPadding(2);
    svg.selectAll('g.y.axis').call(yAxis);
    svg.selectAll('g.x.axis').call(xAxis)
      .selectAll("text") // select and rotate all labels
      .attr("y", 10)
      .attr("x", -9)
      .attr("dy", ".35em")
      .attr("transform", "rotate(-45)")
      .style("text-anchor", "end");

    // Add rectangles
    svg.selectAll(".bar")
      .data(thisInstance.answers)
      .enter().append("rect")
      .attr("class", "bar")
      .attr('data-user-group-id', (d) => {
        return d.userGroup;
      })
      .attr("x", function (d) {
        return x(Graph.formatLabel(d.criteriaTitle));
      })
      .attr("width", x.bandwidth())
      .attr("y", function (d) {
        let value = d.values[d.type].value > 0 ? d.values[d.type].value : thisInstance.minimumHeight;
        return y(value);
      })
      .attr("height", function (d) {
        let value = d.values[d.type].value > 0 ? d.values[d.type].value : thisInstance.minimumHeight;
        return thisInstance.height - y(value);
      })
      .style('fill', (d) => {
        return colors(d.criteriaTitle);
      })
      .on('mouseover', tip.show)
      .on('mouseout', tip.hide);
  }

  /**
   * @return this
   * Hides all the rectangle elements inside this graph's container.
   */
  hideAll() {
    const rectangles = this.container.querySelectorAll('[data-user-group-id]');
    for (const rectangle of rectangles) {
      if (!rectangle.classList.contains('hide')) {
        rectangle.classList.add('hide');
      }
    }
    return this;
  }
}
