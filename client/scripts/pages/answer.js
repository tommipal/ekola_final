import * as _ from 'lodash';
import { Utils } from 'utils';
import { Ajax } from 'ajax';
import $ from 'jquery';
import Foundation from 'foundation-sites';
import { Translator } from 'translator';

let translator = new Translator(Utils.getLocale());

class Answer {
	/**
	 * @param {Object} element DOM element corresponding to this answer.
	 * @param {Object} criteria
	 * @param {number} question
	 * @param {number} id
	 * @param {AnsweringSession} answeringSession
	 */
	constructor(element, criteria, question, id, answeringSession) {
		this.element = element;
		this.criteria = criteria;
		this.type = criteria.type;
		this.criteriaTitle = criteria.title;
		this.question = question;
		this.id = id;
		this.userGroup = answeringSession.userGroup;
		this.survey = answeringSession.survey;
		this.values = {};
	}

	/**
	 * Returns a complete answer object. Comes with an id if the answer was already created and persisted.
	 * @return {Answer} using the same format as the model OR UNDEFINED if the question is untouched.
	 */
	getData() {
		//attach the value to answer.
		switch (this.type) {
			case 'select':
				// Find the option correspoding to the input the user checked to extract both value and description.
				_.forEach(this.element.getElementsByClassName('input'), (input) => {
					if (input.checked) {
						let selected = _.find(this.criteria.options.choices, (choice) => { return choice.value == input.value; });
						if (selected) this.values.select = { value: selected.value, description: selected.description };
					}
				});
				if (!this.values.select) return;
				break;
			case 'scale':
				let value = this.element.querySelector('input[class="input"]').value;
				if (!value) return;
				this.values.scale = { value: value };
				break;
		}
		//delete any unnecessary information before returning
		delete this.element;
		if (!this.id) delete this.id;
		return this;
	}
}

/**
 * Class for managing the answering page.
 */
class AnsweringSession {
	constructor() {
		this.saveTimeout;
		this.changes = false;
		this.answers = [];
		this.session = document.querySelector('[data-session]').dataset.session;
		this.survey = document.querySelector('[data-survey]').dataset.survey;
		this.userGroup = document.querySelector('[data-user-group]').dataset.userGroup;
		this.saveBtn = Utils.byId('save-answers-button');
		this.cancelBtn = Utils.byId('modal-confirm-btn') ? Utils.byId('modal-confirm-btn') : Utils.byId('cancel-btn');
		this.collapseBtn = Utils.byId('collapse-btn');
		if (Utils.byId('modal-text')) {
			Utils.byId('modal-text').textContent = translator.trans('Are you sure you want to end this answering session? This will delete your current answers. You can take the survey again with the same link.')
		}
		this.saveLoop();
		this.bindActions();
		this.reloadAnswers(() => {
			if ($('.slider').length) $('.slider').foundation('_reflow'); // reflow all sliders to match the new values
		});
	}

	/**
	 * Bind all main actions here. Add reloading of elements if needed.
	 */
	bindActions() {
		$("#answer-form").find(":input").change(() => { this.changes = true; });
		$(".slider").on('moved.zf.slider', () => { this.changes = true; });

		//Save Button: save answers and either reload the answers or redirect to different action.
		this.saveBtn.addEventListener('click', () => {
			if (!this.saveBtn.disabled) {
				this.save((success) => {
					this.reloadAnswers();
					if (success) {
						this.showSavedMessage(translator.trans('Saved'), 'success');
					}
				});
			}
		});
		this.collapseBtn.addEventListener('click', () => {
			// Collapse all questions
			$('.answer-question-content').slideUp('fast');
		});
		this.cancelBtn.addEventListener('click', () => {
			const data = { session: this.session, referrer: document.referrer };
			new Ajax('GET', data, '/answer/cancel').send().then(res => {
				res = JSON.parse(res);
				window.location = res.target;
			}).catch(err => console.error(err));
		});

		document.querySelectorAll('.answer-question-title').forEach(element => element.addEventListener('click', (e) => {
			const parent = e.target.parentNode;
			const id = parent.getElementsByClassName('answer-question-content')[0].id;
			$('#' + id).slideToggle('fast');
		}));
	}

	/**
	 * @param {function} cb Callback to execute if no redirect target is returned with the response.
	 */
	save(cb) {
		Utils.byId('save-answers-button').disabled = true;
		if(!Utils.byId('spinner')) Utils.byId('save-answers-button').innerHTML += '<div id="spinner" class="spinner"></div>';

		let answerData = _.filter(this.answers.map((answer) => answer.getData()), answer => { return !!answer; });
		new Ajax('POST', {
			answers: answerData,
			userInfo: JSON.stringify(this.getUserInfo()),
			evidence: JSON.stringify(this.getEvidence())
		}, `/answer/save/${this.session}`)
			.send()
			.then((res) => {
				res = JSON.parse(res);
				this.changes = false;
				cb(true);
			}).catch((err) => {
				this.changes = false;
				this.showSavedMessage(translator.trans('Something went wrong. Try again later.'), 'error');
				cb(false);
			});
	}

	/**
	 * Set up save loop with a timeout of 30 seconds.
	 * Save and reload answers if there was any changes.
	 */
	saveLoop() {
		if (this.saveTimeout) { clearTimeout(this.saveTimeout); }
		this.saveTimeout = setTimeout(() => {
			// only save if there are changes
			if (this.changes) {
				this.save((success) => {
					this.reloadAnswers();
					if (success) {
						this.showSavedMessage(translator.trans('Saved automatically'), 'success');
					}
				});
			}
			this.saveLoop();
		}, 30000);
	}

	/**
	 * @param {string} message
	 * @param {string} type 'success' or 'error' or nothing
	 */
	showSavedMessage(message, type) {
		Utils.byId('save-answers-button').disabled = false;
		Utils.byId('save-answers-button').removeChild(Utils.byId('spinner'));
		Utils.showMessage(message, type);
	}

	/**
	 * Retrieves all the existing answers and updates the form's initial values based on those.
	 * Also initializes answer objects for every criteria.
	 * NOTE this MUST be called after the initial save because otherwise new records are recreated every time user hits the save!
	 * @param {function} cb optional callback.
	 */
	reloadAnswers(cb) {
		new Ajax('GET', {}, '/answer/' + this.session).send().then((response) => {
			response = JSON.parse(response);
			//Action returning answers will only return 404 if the session has been destroyed so it should be safe to reload the page to generate a new one?
			if (response.status && response.status == 404) return location.reload(true);

			this.answers = []; //clear old answers
			const existingAnswers = response.answers;
			const questions = response.questions;

			//go through all the criteria and initialize answer objects.
			questions.forEach(question => {
				question.criteria = JSON.parse(question.criteria);//...?
				question.criteria.forEach(criteria => {

					const element = document.querySelector(`[data-question="${question.id}"][data-criteria-title="${criteria.title}"]`);
					if (!element) return;
					const existingAnswer = existingAnswers.find(answer => { return answer.question == question.id && answer.criteriaTitle == criteria.title });
					this.answers.push(new Answer(element, criteria, question.id, existingAnswer ? existingAnswer.id : null, this));
					//update the inputs if there was an existing answer
					if (existingAnswer) {
						switch (existingAnswer.type) {
							case 'scale':
								element.querySelector('input').value = existingAnswer.values.scale.value;
								break;
							case 'select':
								// find the input with the correct value and set it checked
								const input = _.find(element.querySelectorAll('input'), input => {
									return (parseInt(input.value) === parseInt(existingAnswer.values.select.value));
								});
								if (!input) break; //survey has been edited while it was still active so no input has the same value as the stored answer.
								input.checked = true;
								break;
						}
						//remove input's default value to force users to select something before the criteria is considered answered. NOTE causes a warning.
					} else if (criteria.type === 'scale') element.querySelector('input').value = null;
				});
			});
			// Update user info values with the data currently stored to survey session
			if (response.session.userInfo) {
				const userInfo = typeof response.session.userInfo === 'object' ? response.session.userInfo : JSON.parse(response.session.userInfo);
				const userInfoInputs = document.querySelectorAll('#user-info input');
				for (let i = 0; i < userInfoInputs.length; i++) {
					const elem = userInfo.find(info => { return info.question === userInfoInputs[i].name });
					if (elem) userInfoInputs[i].value = elem.value;
				}
			}
			// Prefill the evidence inputs
			if (response.session.evidence) {
				const evidence = typeof response.session.evidence === 'object' ? response.session.evidence : JSON.parse(response.session.evidence);
				const evidenceInputs = document.querySelectorAll('.evidence-input');
				for (let i = 0; i < evidenceInputs.length; i++) {
					const elem = evidence.find(e => e.question == evidenceInputs[i].name)
					if (elem) evidenceInputs[i].value = elem.value;
				}
			}
			this.updateProgress();
			if (cb) cb();
		}).catch(err => console.error(err));
	}

	/**
	 * @return {Array} - pairs of user info questions and answers
	 */
	getUserInfo() {
		const userInfo = [];
		const userInfoInputs = document.querySelectorAll('#user-info input');
		for (let i = 0; i < userInfoInputs.length; i++) {
			userInfo.push({ question: userInfoInputs[i].name, value: userInfoInputs[i].value });
		}
		return userInfo.filter(i => { return i.value.length > 0 });
	}

	/**
	 * @return {Array} - pairs of question ids and user filled evidence
	 */
	getEvidence() {
		const evidence = [];
		const evidenceInputs = document.querySelectorAll('.evidence-input');
		for (let i = 0; i < evidenceInputs.length; i++) {
			evidence.push({ question: evidenceInputs[i].name, value: evidenceInputs[i].value });
		}
		return evidence.filter(e => { return e.value.length > 0 });
	}

	/**
	 * Modify question title if the question has been answered (every criteria has a value).
	 */
	updateProgress() {
		let questionElements = document.querySelectorAll('.answer-question');
		questionElements.forEach(question => {
			let answers = this.answers.filter((answer) => {
				return answer.question == question.querySelector('.answer-question-content').id.split('-')[2]
			});

			let answered = answers.every((answer) => {
				if (answer.type === 'scale') {
					return answer.element.querySelector('input').value ? true : false;
				}
				else if (answer.type === 'select') {
					const inputs = answer.element.querySelectorAll('.input');
					for (let i = 0; i < inputs.length; i++) {
						if (inputs[i].checked) return true;
					}
					return false;
				}
			});
			// console.log(answered);
			if (answered) {
				let title = question.querySelector('.answer-question-title');
				if (!title.innerHTML.includes('<i class="fi-check"></i>')) title.innerHTML += ' <i class="fi-check"></i>';
			}
		});
	}
}

window.onload = function() {
	$('#container').foundation();
	new AnsweringSession();
}
