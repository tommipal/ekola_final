/* eslint valid-jsdoc: 0 */
import $ from 'jquery';
import selectize from 'selectize';
import Foundation from 'foundation-sites'

import { Ajax } from "ajax";
import { Utils } from 'utils';
import { Translator } from 'translator';

import React, { Component } from 'react';
import UserInfo from '../User/UserInfo';
import ReactDOM from 'react-dom';

const translator = new Translator(Utils.getLocale());

'use strict';
const removeGroupText = translator.trans('Are you sure you want to remove this group and all of its users from this evaluation?');

class EditEvaluation {
  constructor(root) {
    this.root = root;
    this.evaluationId     = this.root.querySelector('#_evaluationId').value;

    this.addGroupsBtn     = this.root.querySelector('#add-groups');
    this.removeGroupBtns  = this.root.querySelectorAll('[data-remove-group]');
    this.confirmBtn       = Utils.byId('modal-confirm-btn');

    this.userTable        = this.root.querySelector('.group-user-container tbody');
    this.modalTextElem    = Utils.byId('modal-text');

    this.fetchGroupUrl    =  `/usergroup/find/`;  // Get UserGroups
    this.selectedUsers    = [];                   // array of users in the selectize input
    this.selectedGroups   = [];                   // array of userGroups in the selectize input

    this.init(() => {
      this.bindActions();
    });
  }
  init(done) {
    new Ajax('GET', {query:'userGroupsNotInEvaluation', evaluation: this.evaluationId}, this.fetchGroupUrl)
      .send()
      .then(function (res) {
        return JSON.parse(res);
      })
      .then((groups) => {
        // Initialize userGroup selectize
        this.initUserGroupSelectize('#groups', groups, this);
        if (done) return done();
      })
      .catch((err) => console.error(err));
  }

  bindActions(refreshTable = false) {
    if(!refreshTable) {
      Utils.byId('create-group-btn').addEventListener('click', (e) => {
        const name = Utils.byId('group-name').value;
        const description = Utils.byId('group-description').value;
        new Ajax('POST', {name, description}, '/usergroup/new')
          .send()
          .then((response) => {
            const group = JSON.parse(response).userGroup;
            if(group) {
              $('#new-group-modal').foundation('close');
              this.groupSelectize.addOption({id:group.id, name:group.name});
              this.groupSelectize.addItem(group.id);
              this.groupSelectize.refreshOptions(false);
            };
          });
      });
      this.addGroupsBtn.addEventListener('click', () => { this.addUserGroups(); });
      this.removeGroupBtns.forEach((removeBtn) => {
        removeBtn.addEventListener('click', (e) => {
          this.setupModal(e, 'removeGroup', removeGroupText);
        });
      });

      this.root.querySelector('#perform-user-action').addEventListener('click', (e)=>{
        const action = Utils.getSelectElementValue(this.root.querySelector('#user-action'));
        const selectedUserRows = Utils.toArray(this.root.querySelectorAll('.group-user-container .group-user .success'))
        .map((button)=>Utils.traverseUp(button, 'tr'));
        const userIds = selectedUserRows.map((tr)=>tr.dataset.user);
        const data = {userIds, selectedUserRows}
        switch (action){
          case 'remove':
            this.setupModal(data, 'removeUsers', translator.trans('Are you sure you want to remove %value% users from this evaluation?', userIds.length));
          break;
          case 'void':
            this.setupModal(data, 'void', translator.trans('Are you sure you want to void the answering sessions of %value% users?', userIds.length))
          break;
          case 'admin':
            this.setupModal(data, 'admin', translator.trans('Are you sure you want to give %value% users admin permissions to this evaluation?', userIds.length))
          break;
        }
      });
      $('#description-modal').on('closed.zf.reveal', ({target}) => { this.clearModal(target); });
      $('#confirm-modal').on('closed.zf.reveal', ({target}) => { this.clearModal(target); });
      $('#new-group-modal').on('closed.zf.reveal', () => {
        Utils.byId('group-name').value = '';
        Utils.byId('group-description').value = '';
      });
    }

    this.root.querySelectorAll('button.select-user').addEventListener('click', (e)=>Utils.toggleClasses(e.target, 'success', 'hollow'));
    this.root.querySelectorAll('.show-user').addEventListener('click', (e) => {
      const userId = Utils.traverseUp(e.target, 'tr').dataset.user;
      const modal = document.querySelector('#description-modal #content');
      modal.innerHTML = '<div style="text-align:center"><div id="spinner" class="spinner large"></div></div>';
      new Ajax('GET', {}, `/user/${userId}/info`).send().then((response)=>{
        response = JSON.parse(response);
        ReactDOM.render(<UserInfo data={response} />, document.querySelector('#description-modal #content'));
      }).catch((err)=>{
        modal.innerHTML = translator.trans('Something went wrong. Try again later.');
        console.error(err)}
      );
    });
  }

  /**
   * Set up selectize options object for either users or userGroups
   * @param target {String} id of the target input element
   * @param data {[Object]} Array of userGroup objects that will populate selectize suggestions
   * @param owner refers to this EditGroup instance
   */
  initUserGroupSelectize(target, data, owner) {
    const groupSelect = $(target).selectize({
      delimiter: ',',
      persist: false,
      searchField: 'name',
      labelField: 'name',
      valueField: 'id',
      options: data,
      render: {
        item: function (item, escape) {
          return `<div><span class="selected item-label" data-group="${escape(item.id)}">${escape(item.name)}</span></div>`;
        },
        option: function (item, escape) {
          return `<div><span class="item-label"><b>${escape(item.name)}</b></span><br></div>`;
        },
        option_create: function (item, escape) {
          return '<div class="create">' + translator.trans('Create') + ' <strong>' + escape(item.input) + '</strong>&hellip;</div>';
        }
      },
      onItemAdd: function (value, item) {
        const name = item[0].childNodes[0].textContent;
        owner.selectedGroups.push({id: value, name: name});
      },
      onItemRemove: function (value, item) {
        const name = item[0].childNodes[0].textContent;
        Utils.removeObjFromArray(owner.selectedGroups, 'id', value);
        this.addOption({id: value, name: name});
      },
      create: function (input) {
        Utils.byId('group-name').value = input;
        $('#new-group-modal').foundation('open');
        return false;
      }
    });
    this.groupSelectize = groupSelect[0].selectize;
  }

  /**
   * Remove an userGroup from the evaluation and then remove it from the DOM
   * @param params {Object} contains userGroup id to remove
   */
  removeUserGroup(params) {
    $('#confirm-modal').foundation('close');
    const data = {users:{}, userGroups:{remove:[params.userGroupId]}}
    new Ajax('POST', data, `/evaluation/${this.evaluationId}/save`).send().then((res) => {
      res = JSON.parse(res);
      if (res.evaluation) {
        this.refreshTable(res.users);
        const groupRow = this.root.querySelector(`[data-group-id="${params.userGroupId}"`);
        this.groupSelectize.addOption({id:Number(params.userGroupId), name:params.userGroupName});
        this.groupSelectize.refreshOptions(false);
        $(groupRow).fadeOut('fast', ()=>{
          Utils.showMessage(translator.trans('Group removed'), 'success');
          groupRow.parentNode.removeChild(groupRow);
        });
      }
    }).catch((err)=>{
      Utils.showMessage(translator.trans('Something went wrong. Try again later.'), 'error');
      console.error(err);
    });
  }

  removeUsers(params) {
    $('#confirm-modal').foundation('close');
    new Ajax('POST', {users: params.userIds}, `/evaluation/${this.evaluationId}/removeUsers`)
    .send()
    .then((response)=>{
      Utils.toArray(params.selectedUserRows).forEach((tr)=>tr.parentNode.removeChild(tr));
      Utils.showMessage(translator.trans('Users removed'), 'success');
    })
    .catch((err)=>{
      Utils.showMessage(translator.trans('Something went wrong. Try again later.', 'error'), 'error');
      console.error(err);
    });
  }

  voidSessions(params) {
    $('#confirm-modal').foundation('close');
    const data = {users: {void: params.userIds}, userGroups: {}};
    new Ajax('POST', data, `/evaluation/${this.evaluationId}/save`)
      .send()
      .then((response)=>{Utils.showMessage(translator.trans('Sessions successfully voided'), 'success')})
      .catch((err)=>{
        Utils.showMessage(translator.trans('Something went wrong. Try again later.'), 'error');
        console.error(err);
      });
  }

  adminUsers(params) {
    $('#confirm-modal').foundation('close');
    const data = {users: {admin: params.userIds}, userGroups: {}};
    new Ajax('POST', data, `/evaluation/${this.evaluationId}/save`)
      .send()
      .then((response)=>{Utils.showMessage(translator.trans('%value% users promoted to administrators', params.userIds.length), 'success')})
      .catch((err)=>{
        Utils.showMessage(translator.trans('Something went wrong. Try again later.'), 'error');
        console.error(err);
      });
  }

  /**
   * Set up confirmation modal
   * @param d {Any} Event from a click listener that fires this function
   * @param modalText {String} Text that is going to be on the modal
   */
  setupModal(d, action, modalText) {
    this.modalTextElem.textContent = modalText;
    this.confirmBtn = Utils.byId('modal-confirm-btn');
    switch(action) {
      case 'removeUsers':
        this.confirmBtn.addEventListener('click', (e) => {
          this.removeUsers(d);
        });
      break;
      case 'void':
      this.confirmBtn.addEventListener('click', (e) => {
        this.voidSessions(d);
      });
      break;
      case 'admin':
      this.confirmBtn.addEventListener('click', (e) => {
        this.adminUsers(d);
      });
      break;
      case 'removeGroup':
        const data = {
          userGroupId: Utils.traverseUp(d.target, 'tr').dataset.groupId,
          userGroupName: Utils.findSibling(d.target.parentNode, 'td', 0).textContent,
        };
        this.confirmBtn.addEventListener('click', (e) => {
          this.removeUserGroup(data);
        });
      break;
      default:
        console.error('Action is not "removeUsers", "void" or "removeGroup"');
      break;
    }
    $('#confirm-modal').foundation('open');
  }

  /**
   * Empty datasets and textContent and clear listeners from the button
   */
  clearModal(target) {
    switch(target.id) {
      case 'description-modal':
        target.querySelector('#content').innerHTML = '';
      break;
      default:
        this.modalTextElem.textContent = '';
        this.confirmBtn.parentNode.replaceChild(this.confirmBtn.cloneNode(true), this.confirmBtn); // Clear any listeners
        delete this.confirmBtn.dataset['user'];
        delete this.confirmBtn.dataset['group'];
      break;
    }
  }

  refreshTable(users) {
    this.userTable.innerHTML = '';
    let newTable = '';
    users.forEach((user) => {
      newTable += `<tr class="group-user" data-user="${ user.id }">
        <td>${ user.givenName } ${ user.familyName }</td>
        <td>${ user.email }</td>
        <td>${ user.userGroup ? user.userGroup.name : '' }</td>
        <td>
          <div class="table-buttons">
            <button type="button" title="${ translator.trans('Select') }" class="button hollow select-user"><i class="fi-check"></i></button>
            <button type="button" title="${ translator.trans('User info') }" class="button show-user" data-open="description-modal"><i class="fi-torso"></i></button>
          </div>
        </td>
      </tr>`
    });
    this.userTable.innerHTML = newTable;
    this.bindActions(true);
  }

  /**
   * Add one or more userGroups to evaluation
   */
  addUserGroups() {
    const userGroups = this.root.querySelector('#groups').value.split(',');
    if(!userGroups.every(group => group.length > 0)) return;

    const data = {userGroups: { add: userGroups}, users:{}};
    new Ajax('POST', data, `/evaluation/${this.evaluationId}/save`)
      .send()
      .then((res) => {
        res = JSON.parse(res);
        if (res.evaluation) {
          this.refreshTable(res.users);
          this.groupSelectize.clear();
          for(const id of userGroups) this.groupSelectize.removeOption(id);
          const table = document.getElementById('group-table');
          this.selectedGroups.forEach((group) => {
            const node = Utils.tableAppendRow(
              table,
              {groupId: group.id},
              group.name,
              `<button type="button" class="small button alert new-button" data-remove-group>${translator.trans('Remove')}</button>`
            );
            if(!node) {
              const newRow = `<tr class="user-group-container" data-group-id="${group.id}">
                <td>${group.name}</td>
                <td><button type="button" class="small button alert new-button" data-remove-group >${translator.trans('Remove')}</button></td>
              </tr>`;
              table.querySelector('tbody').innerHTML = newRow;
            }
          });
          this.removeGroupBtns = this.root.querySelectorAll('.new-button');
          this.removeGroupBtns.addEventListener('click', (e) => {
            this.setupModal(e, 'removeGroup', removeGroupText);
          });
          this.removeGroupBtns.forEach(button => button.classList.remove('new-button'));
          this.selectedGroups = [];
          Utils.showMessage(translator.trans('Groups added'), 'success');
        }
      })
      .catch((err)=>{
        Utils.showMessage(translator.trans('Something went wrong. Try again later.'), 'error');
        console.error(err)
      });
  }
}
window.onload = function () {
  $('#container').foundation();
  new EditEvaluation(Utils.byId('edit-evaluation'));
};
