import * as _ from 'lodash';
import $ from 'jquery';
import Foundation from 'foundation-sites'

import { Utils } from 'utils';
import { Ajax } from 'ajax';
import { Graph } from './charts/graph';
import { ScatterGraphSmall } from './charts/scatterGraphSmall';
import { BarGraph } from './charts/barGraph';
import { ScatterGraph } from './charts/scatterGraph';
import { FourWayGraph } from './charts/fourWayGraph';
import { Translator } from 'translator';

const translator = new Translator(Utils.getLocale());

const answerCountElem = document.querySelector('#number-of-answers-by-group');
const totalAnswerCountElem = document.querySelector('#total-number-of-answers');
const graphContainer = Utils.byId('small-graphs');
const $orbit = $('.orbit');
let groupDropdown;
let currentSlideIndex;
let init = false;
let sessions;
let questions;
let survey;

// Go to a specific slide in orbit
function goToSlide(slideIndex) {
  $('.orbit-slide.is-active').removeClass('is-active').attr('style', 'position: relative; display: none;');
  $('button.is-active').removeClass('is-active');
  $('[data-slide="' + slideIndex + '"]').addClass('is-active').removeAttr('style');
}

/**
 * Renders evidence inside '#evidence-container'
 * NOTE: this is dependant on currentSlideIndex and thus should be run after orbit related code.
 */
function showEvidence() {
  const groupId = groupDropdown.value;
  let evidence = sessions.map((session) => {
    return session.evidence && session.userGroup == groupId ? session.evidence : [];
  });
  evidence = _.flatten(evidence.map(e => typeof e === 'string' ? JSON.parse(e) : e));
  evidence = evidence.filter((e) => { return e.question == questions[currentSlideIndex].id; });

  let evidenceContainer = document.querySelector('#evidence-container');
  evidenceContainer.innerHTML = '';
  if (evidence.length < 1) return;
  evidenceContainer.insertAdjacentHTML('beforeend', `<fieldset class="fieldset"><legend>${translator.trans('Evidence')}</legend></fieldset>`);
  evidenceContainer = evidenceContainer.querySelector('fieldset');
  evidence.forEach((e) => {
    evidenceContainer.insertAdjacentHTML('beforeend', `<p>${e.value}</p>`);
  });
}

function loadGraphs(scatterGraph, questionSpecificGraphs, sessions) {
  $(window).trigger('resize');
  if (!groupDropdown.value && !init) return;
  else if (!init) {
    scatterGraph.container.classList.remove('hide');
    graphContainer.classList.remove('hide');
  }
  const groupId = groupDropdown.value;

  const option = document.querySelector(`#group-dropdown>[value="${groupId}"]`);
  const groupName = groups.filter((group) => group.id == groupId)[0].name;
  const answerCount = sessions.filter((session) => session.userGroup == groupId).length;

  option.innerHTML = `${groupName} (${translator.trans("%value% answers", answerCount)})`;
  scatterGraph.showResultsByUserGroup(groupId);
  questionSpecificGraphs.forEach((graph) => graph.showResultsByUserGroup(groupId));

  if (!init) {
    init = true;
    currentSlideIndex = $('.orbit-slide.is-active').data('slide');
  } else {
    goToSlide(currentSlideIndex);
  }

  showEvidence(); //changing userGroup
}

/**
 * Initialize the page: Retrieve all answers for this survey, draw graphs, bind event listener etc.
 */
function initializeResults() {
  // reload to dropdown to stop the event listener from breaking after a forward
  Utils.reloadHTML(document.querySelectorAll('#group-dropdown'));
  groupDropdown = Utils.byId('group-dropdown');

  new Ajax('GET', {}, `/answer/averages/${survey}`).send().then((response) => {
    response = JSON.parse(response);
    sessions = response.sessions;
    questions = response.questions.map(question => { question.criteria = JSON.parse(question.criteria); return question; });
    const averageAnswers = Graph.formatAnswers(response.averageAnswers, questions);
    const answers = Graph.formatAnswers(response.answers, questions);

    if (sessions.length === 0 || answers.length === 0) {
      totalAnswerCountElem.innerHTML = translator.trans('There are no answers for this survey.');
      return;
    }
    totalAnswerCountElem.innerHTML = translator.trans('%value% answers', sessions.length);

    //init the main scatter graph
    const scatterGraph = new ScatterGraph(averageAnswers, 'scatter-graph', questions);
    //init question based graphs
    const questionSpecificGraphs = [];
    _.forEach(questions, (question, i) => {
      //find corresponding collective answer and pass it to the new bar graph along with the CONTAINER ID
      questionSpecificGraphs.push(new BarGraph(averageAnswers.filter(answer => answer.question === question.id), 'bar-graph-' + i, [question]));
      //do the same with question based scatter graphs but with ALL the answers
      questionSpecificGraphs.push(new ScatterGraphSmall(_.filter(answers, answer => { return answer.question === question.id; }), 'small-scatter-graph-' + i, [question]));

    });
    //init four way graph
    const fourWayGraph = new FourWayGraph(averageAnswers, 'fourWayGraph', questions); // ...?
    $('.tabs').on('change.zf.tabs', () => {
      const orbitTab = Utils.byId('tab2').classList.contains('is-active');
      if (orbitTab) {
        Foundation.reInit($orbit);
        $orbit.on('slidechange.zf.orbit', () => {
          currentSlideIndex = $('.orbit-slide.is-active').data('slide');
          showEvidence(); //changing slide
        });
      }
    });
    //set up eventlistener to display the correct results and initially reveal the scatter graph
    groupDropdown.addEventListener('change', function () {
      loadGraphs(scatterGraph, questionSpecificGraphs, sessions);
    });
    loadGraphs(scatterGraph, questionSpecificGraphs, sessions);

  });
}

$(document).ready(function () {
  survey = Utils.byId('_surveyId').value;
  $('#container').foundation();

  initializeResults();
});