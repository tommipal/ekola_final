import $ from 'jquery';
import Foundation from 'foundation-sites'
import moment from 'moment';
import { Ajax } from "ajax";
import { Utils } from 'utils';
import { Translator } from 'translator';

/* eslint valid-jsdoc: 0 */
let fp;
const translator = new Translator(Utils.getLocale());

/**
 * Leave selected evaluation
 * @param {HTMLElement} target Leave evaluation button that was clicked
 * @returns {undefined}
 */
function leaveEvaluation(target) {
  const row = Utils.traverseUp(target, '.profile-row');
  const ajax = new Ajax('POST', {users: target.dataset.user}, `/evaluation/${target.dataset.evaluation}/leave/`);
  ajax.send((response, readystate) => {
    if (readystate === 4) {
      $(row).fadeOut('fast', () => {
        row.parentNode.removeChild(row);
        $('#confirm-modal').foundation('close');
      });
    }
  });
}

/**
 * Reopen a closed survey with a new date
 * @param {HTMLElement} target Reopen button that was clicked
 * @param {String} newDate new date to set
 * @returns {undefined}
 */
function openSurvey(target, newDate) {
  const ajax = new Ajax('POST', {
    endDate: newDate,
    startDate: moment().format('YYYY-MM-DD HH:mm'),
  }, `/survey/${target.dataset['openSurvey']}/setdates`);
  ajax.send((response, readystate) => {
    if (readystate === 4) {
      $('#reopen-modal').foundation('close');
      location.reload();
    }
  });
}

/**
 * Closes a survey by setting the closing date to current time and removes it from the DOM
 * @param {HTMLElement} target
 * @param {String} endDate
 * @returns {undefined}
 */
function closeSurvey(target, endDate) {
  const ajax = new Ajax('POST', {
    endDate: endDate,
    close: true
  }, `/survey/${target.dataset['closeSurvey']}/setdates`);
  ajax.send((response, readystate) => {
    if (readystate === 4) {
      $('#confirm-modal').foundation('close');
      location.reload();
    }
  });
}

/**
 * Delete target survey from the DB and the DOM
 * @param target {HTMLElement} Delete button that was clicked
 */
function deleteSurvey(target) {
  const id = target.dataset.id;
  const row = Utils.traverseUp(target, '.profile-row');
  const ajax = new Ajax('POST', {confirm: true}, `/survey/${id}/delete`);
  ajax.send().then((res) => {
    $('#confirm-modal').foundation('close');
    $(row).fadeOut('fast');
  });
}

/**
 * Delete target template from the DB and the DOM
 * @param {HTMLElement} target Delete button that was clicked
 */
function deleteTemplate(target) {
  const id = target.dataset.id;
  const row = Utils.traverseUp(target, '.profile-row');
  const ajax = new Ajax('POST', {}, `/surveytemplate/${id}/delete`).send().then(res=>{
    $('#confirm-modal').foundation('close');
    $(row).fadeOut('fast');
  });
}

/**
 * Set up confirmation modal used with deleting and closing surveys
 * @param modalText {String} Text that is going to be on the modal
 * @param func {Function} function that will be executed when confirm is pressed
 * @param params {Array} parameters for the func parameter
 */
function setUpConfirmModal(modalText, func, ...params) {
  const modal = Utils.byId('confirm-modal');
  modal.querySelector('#modal-text').textContent = modalText;
  modal.querySelector('#modal-confirm-btn').addEventListener('click', () => {
    func.apply(null, params);
  });

}

/**
 * Set up reopening modal
 * @param event {Event} Event from a click listener that fires this function
 */
function setUpOpenModal(target) {
  const modal = Utils.byId('reopen-modal');
  const now = moment().format();
  const dateInput = modal.querySelector('#end-date');
  dateInput.value = now;
  fp.setDate(now);
  Utils.byId('reopen-modal-accept').addEventListener('click', () => {
    const newDate = modal.querySelector('#end-date').value;
    openSurvey(target, newDate);
  });
}

/**
 * Depending on id, clear text content, inputs, listeners,
 * @param modalId {String} either 'reopen-modal' OR 'confirm-modal'
 */
function clearModal(modalId) {
  const modal = Utils.byId(modalId);
  if (modalId === 'reopen-modal') {
    const acceptBtn = modal.querySelector('#reopen-modal-accept');
    modal.querySelector('#end-date').value = null;
    acceptBtn.parentNode.replaceChild(acceptBtn.cloneNode(true), acceptBtn);
  } else if (modalId === 'confirm-modal') {
    const confirmBtn = modal.querySelector('#modal-confirm-btn');
    modal.querySelector('#modal-text').textContent = '';
    confirmBtn.parentNode.replaceChild(confirmBtn.cloneNode(true), confirmBtn);
  } else if(modalId === 'new-group-modal') {
    Utils.byId('group-name').value = '';
    Utils.byId('group-description').value = '';
  }

}
window.onload = function () {
  $('#container').foundation();
  $('.reveal').foundation();
  Utils.extendDefaultObjects();

  fp = Utils.getFlatpickrObject('.flatpickr');
  const openButtons     = document.querySelectorAll('[data-open-survey]');
  const closeButtons    = document.querySelectorAll('[data-close-survey]');

  const removeEvalBtns  = Utils.byClass('remove-evaluation');
  const removeUserBtns  = Utils.byClass('remove-user');

  const changeGroupBtn  = document.querySelector('#change-group-btn');
  const groupDorpdown   = document.querySelector('select[name="group"]');

  // Prevent form submit if button has 'disabled' class
  changeGroupBtn.addEventListener('click', (e)=>{
    if(e.target.classList.contains('disabled')) e.preventDefault();
  });

  // Disable OK button if no group is selected
  groupDorpdown.addEventListener('change', ({target}) => {
    if(!target.value && !changeGroupBtn.classList.contains('disabled')) {
      changeGroupBtn.classList.add('disabled');
    } else {
      changeGroupBtn.classList.remove('disabled');
    }
  });

  removeEvalBtns.addEventListener('click', ({target})=>{
    let evaluation = target.dataset.evaluationId;
    let row = Utils.traverseUp(target, '.profile-row');
    setUpConfirmModal(translator.trans("Are you sure you want to delete this evaluation? All of it's surveys will be unavailable until assigned a new one and all answers will be invalidated"),
      (evaluation)=>{
        new Ajax('POST', {}, `/evaluation/${evaluation}/delete`)
          .send()
          .then((response)=>{
            $('#confirm-modal').foundation('close');
            $(row).fadeOut('fast');
          })
      }, evaluation);
  });

  removeUserBtns.addEventListener('click', ({target}) => {
    setUpConfirmModal(
      translator.trans('Are you sure you want to leave this evaluation? You will no longer be able to answer surveys for this evaluation.'),
      leaveEvaluation,
      target
    );
  });

  $('.reveal').on('closed.zf.reveal', ({target}) => clearModal(target.id));

  openButtons.addEventListener('click', ({target}) => setUpOpenModal(target));

  closeButtons.addEventListener('click', ({target}) => {
    const row = Utils.traverseUp(target, '.profile-row');
    const title = row.querySelector('p').textContent;
    const endDate = moment().format();
    setUpConfirmModal(translator.trans(`Are you sure you want to close the following survey: '%value%'?`, title), closeSurvey, target, endDate);
  });

  Utils.byClass('delete-btn').addEventListener('click', ({target}) => {
    setUpConfirmModal(translator.trans("Are you sure you want to delete survey '%value%'?", target.dataset.title), deleteSurvey, target);
  });

  Utils.byClass('delete-template-btn').addEventListener('click', ({target}) => {
    setUpConfirmModal(translator.trans("Are you sure you want to delete template '%value%'?", target.dataset.title), deleteTemplate, target);
  });

  Utils.byId('create-group-btn').addEventListener('click', (e) => {
    const name = Utils.byId('group-name').value;
    const description = Utils.byId('group-description').value;
    new Ajax('POST', {name, description}, '/usergroup/new')
    .send()
    .then((response) => {
      response = JSON.parse(response);
      if(response.userGroup){
        $('#new-group-modal').foundation('close');
        const option = document.createElement('option');
        option.value = response.userGroup.id;
        option.text = response.userGroup.name;
        groupDorpdown.add(option);
      }
    });
  });

};
