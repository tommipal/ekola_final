import $ from 'jquery';
import moment from 'moment';
import flatpickr from 'flatpickr';
import locales from "flatpickr/dist/l10n/index";

export class Utils {
  constructor() {
    this.timeout;
  }

  /**
   * Shows a message that pops up on the bottom right corner of the screen
   * Used to inform user if errors occur or save was successful etc.
   * @param {String} message Text that shows in the popup
   * @param {String} type must be 'error', 'success', determines the displayed icon in the popup
  */
  static showMessage(message, type) {
    clearTimeout(this.timeout);
    $('#info-message').slideDown('fast');
    let icon = '';

    if(type === 'error') icon = 'fi-x';
    if(type === 'success') icon = 'fi-check';
    $('#info-message i').removeClass().addClass(icon);
    $('#info-message b').text('');
    $('#info-message b').text(message);
    $('#info-message').slideDown('fast', () => {
      this.timeout = setTimeout(() => {
        $('#info-message').slideUp('fast');
			}, 3000);
		});
  }
  static toArray(obj) {
    let array = [];
    // iterate backwards ensuring that length is an UInt32
    for (let i = obj.length >>> 0; i--;) {
      array[i] = obj[i];
    }
    return array;
  }

  /**
   * Remove duplicates from array
   * @param {Array} array Array that will be modified
   * @param {*} key If array consists of objects key is used to compare the two items
   * @returns {Array} New array without duplicates
   */
  static removeDuplicates(array, key = null) {
    if(!array.length) return array;
    if(typeof array[0] === 'object' && !key) {
      console.error('Array of objects needs a key to compare items');
      return array;
    } else if(typeof array[0] === 'object') {
      return array.filter((item, index, self) => {
        return index === self.findIndex((object) => object[key] === item[key]);
      });
    } else {
      return array.filter((item, index, self) => {
        return self.indexOf(item) === index;
      });
    }
  }

  /**
   * Remove an object with given key from array of objects.
   * Example delete object(s) with id === 3: removeObjFromArray(array,'id',3);
   * @param {Array} array  Array of objects
   * @param {String} objectKey  Target key that is to be compared
   * @param {*} value Delete objects with this value
   * @returns {Array} New array without deleted objects
   */
  static removeObjFromArray(array, objectKey, value) {
    array.forEach((obj, i) => {
      if (obj[objectKey] === value) {
        array.splice(i, 1);
      }
    });
    return array;
  }

  static checkTypeOk(element, types) {
    if (typeof types === "string") {
      return typeof element === types;
    }
    for (let t in types) {
      if (types.hasOwnProperty(t)) {
        if (typeof element === types[t]) return true;
      }
    }
    console.error("element did not fulfill type requirement");
    return false;
  }

  static isDOMNode(obj) {
    return !!obj.nodeName;
  }

  static getCsrfToken() {
    let metaTag = document.querySelector('meta[name="_csrf"]');
    return metaTag.dataset.value;
  }

  static getLocale() { return document.documentElement.lang;}

  /**
   * Shorthand for document.getElementById()
   * @param {String} id id of the element
   */
  static byId(id) {
    return document.getElementById(id);
  }

  /**
   * Shorthand for document.getElementsByClassName()
   * @param {String} className class of the element
   */
  static byClass(className) {
    return document.getElementsByClassName(className);
  }

  static checkArgTypes(args, ...types) {
    for (let i = 0; i < types.length; i++) {
      if (typeof args[i] !== types[i] && !(typeof types[i] === 'object' && args[i] instanceof types[i]) && types[i] !== 'enum') throw new Error(`Wrong argument: ${args[i]} must be ${types[i]}`);
      if (types[i] === 'enum') {
        if (!args.propertyIsEnumerable(i)) throw new Error(`Wrong argument: ${args[i]} must be enumerable`);
      }
    }
  }

  static camelCase(string) {
    return string
      .replace(/\s(.)/g, function ($1) {
        return $1.toUpperCase();
      })
      .replace(/-(.)/g, function ($1) {
        return $1.toUpperCase();
      })
      .replace(/_(.)/g, function ($1) {
        return $1.toUpperCase();
      })
      .replace(/\.(.)/g, function ($1) {
        return $1.toUpperCase();
      })
      .replace(/[\s-_.]/g, '')
      .replace(/^(.)/, function ($1) {
        return $1.toLowerCase();
      });
  }
  /**
   * Reload the elements and all it's children, resetting all event listeners.
   * Don't for to RE-SELECT your elements after this, as old references will be invalid.
   * @param {Iterable} elements the target elements to nuke
   * @param {Boolean} reloadHTML whether to replace target's HTML string, effectively causing old element to be invalid
   * @returns {undefined}
   */
  static reloadHTML(elements, reloadHTML) {
    if(!elements.length) elements = [elements]; //assume we have a single element, wrap in array so we can reuse the iteration code below
    if(elements.length === 0) return;
    try {
      for (let target of elements) {
        if (reloadHTML) {
          let htmlString = target.outerHTML;
          htmlString = (' ' + htmlString).slice(1); //deep copy the html value;
          target.parentNode.innerHTML = target.parentNode.innerHTML.replace(target.outerHTML, htmlString); //replace the html
        } else {
          let newEl = target.cloneNode(true);
          target.parentNode.replaceChild(newEl, target);
        }
      }
    } catch(err){
      console.error(`ReloadHTML failed. This is likely due to invalid parameter type - please give it either a single HTMLElement, a NodeList, an Array of HTMLElements or an HTMLCollection. You passed: ${elements}`);
      console.error(err);
    }
  }

  static updateElementsOnEvent(elementsToUpdate, source, obtainValue, eventName, updateFunction) {
    Utils.checkArgTypes([elementsToUpdate, source, obtainValue, eventName],
      'enum', 'object', 'function', 'string');
    source.addEventListener(eventName, updateFunction || function (event) {
        let value = obtainValue(event.target);
        for (let key in elementsToUpdate) {
          if (elementsToUpdate.hasOwnProperty(key)) {
            if (updateFunction) updateFunction(elementsToUpdate[key]);
            else elementsToUpdate[key].innerHTML = value;
          }
        }
      });
  }

  /**
   * Traverses @level amount of steps up the DOM tree from ofTargetNode
   * and removes the last child upon which it stops during traversal.
   * So for example:
   * <div>
   *     <section>
   *         <aside>
   *             <button>Remove level = 1</button> <-- removes aside from section
   *         </aside>
   *     </section>
   *     <section>
   *         <aside>
   *             <button>Remove level = 2</button> <-- removes section from div
   *         </aside>
   *     </section>
   * <div>
   * @param {Node} ofTargetNode the element from which to start traversal
   * @param {Number} level the amount of levels to go up the DOM tree
   * @returns {undefined}
   */
  static removeContainer(ofTargetNode, level) {
    if (!level) level = 1;
    let childToRemove = ofTargetNode;
    let target = childToRemove.parentNode;
    for (let i = 0; i < level; i++) {
      childToRemove = target;
      target = target.parentNode;
    }
    target.removeChild(childToRemove);
  }

  static findSibling(ofElement, siblingSelector, direction){
    let target = ofElement;
    while (target !== null) {
      if (direction > 0) target = target.nextSibling;
      else target = target.previousSibling;

      while (target && target.nodeType === Node.TEXT_NODE) {
        if (direction > 0) target = target.nextSibling;
        else target = target.previousSibling;
      }

      if(target != null && target.matches(siblingSelector)) break;
    }
    return target;
  }

  /**
   * Scrubs @offset from the target element, returning a clone of the node on which it stops
   * For example:
   * <div>
   *     <button>I will be cloned</button> <-- clone target
   *     <section> up 2 levels <-- scrub 1 element up from here
   *         <p> up 1 level
   *             cloneSibling of this with offset -1 and level 2
   *         </p>
   *         <p>
   *             Heya
   *         </p>
   *     </section>
   * </div>
   * @param {HTMLElement} ofTarget the element from which to start traversal
   * @param {Number} offset how many siblings to step over. Positive for going 'down', negative for 'up'.
   * @param {Number} level traverse the dom tree up this amount of 'steps' before scrubbing for target
   * @returns {HTMLElement} a clone of the element at which the pointer has stopped after navigating
   */
  static cloneSibling(ofTarget, offset, level) {
    if (level && typeof level === 'number' && level > 0) {
      for (let i = 0; i < level; i++) {
        ofTarget = ofTarget.parentNode;
      }
    }

    let target = ofTarget;
    while (target !== null && offset !== 0) {
      if (offset > 0) target = target.nextSibling;
      else target = target.previousSibling;
      //skip text nodes - for Firefox, since other browsers don't make nodes for text
      while (target.nodeType === Node.TEXT_NODE) {
        if (offset > 0) target = target.nextSibling;
        else target = target.previousSibling;
      }
      if (offset > 0) offset--;
      else offset++;
    }
    if (target === null) {
      if (offset > 0) target = ofTarget.parentNode.lastChild;
      else target = ofTarget.parentNode.firstChild;
    }
    return target.cloneNode(true);
  }

  static nodeCollectionAddEventListener(event, listener) {
    if (!(this instanceof HTMLCollection) && !(this instanceof NodeList)) throw new TypeError(`NodeCollection function called with invalid 'this' context. Use it only as an instance method.`);
    for (let index in this) {
      if (this.hasOwnProperty(index)) {
        this[index].addEventListener(event, listener);
      }
    }
  }

  static nodeCollectionRemoveEventListener(event, listener) {
    if (!(this instanceof HTMLCollection) && !(this instanceof NodeList)) throw new TypeError(`NodeCollection function called with invalid 'this' context. Use it only as an instance method.`);
    for (let index in this) {
      if (this.hasOwnProperty(index)) {
        this[index].removeEventListener(event, listener);
      }
    }
  }

  /**
   * Goes up the dom tree from @fromElement, matching parent nodes
   * against the provided css selector. Returns first match or null
   * if reached top of DOM tree
   * @param {HTMLElement} fromElement element from which to start traversal
   * @param {String} cssSelector css selector to match
   * @return {HTMLElement} parent that matched or null
   */
  static traverseUp(fromElement, cssSelector) {
    try {
      let el = fromElement;
      while (el && typeof el.matches === 'function' && !el.matches(cssSelector)) {
        el = el.parentNode;
      }
      return el;
    } catch (err){
      console.error(err);
      return null;
    }
  }

  //Returns true if it is a DOM node
  static isNode(o) {
    return (
      typeof Node === "object" ? o instanceof Node :
        o && typeof o === "object" && typeof o.nodeType === "number" && typeof o.nodeName === "string"
    );
  }

  //Returns true if it is a DOM element
  static isElement(o) {
    return (
      typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
        o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName === "string"
    );
  }

  static extendDefaultObjects() {
    //clear array in place without changing object
    Array.prototype.clear = function () {
      this.splice(0, this.length);
    };

    //Be able to add event listeners to every element in a collection
    HTMLCollection.prototype.addEventListener = Utils.nodeCollectionAddEventListener;
    NodeList.prototype.addEventListener = Utils.nodeCollectionAddEventListener;
    HTMLCollection.prototype.removeEventListener = Utils.nodeCollectionRemoveEventListener;
    NodeList.prototype.removeEventListener = Utils.nodeCollectionRemoveEventListener;

    String.prototype.addToIfAbsent = function (string) {
      if (!this.includes(string.trim())) return this.concat(" " + string);
      else return this;
    };
    String.prototype.removeFromIfPresent = function (string) {
      if (this.includes(string.trim())) {
        return this.replace(string, "").replace("  ", " ");
      } else return this;
    };
  }

  /**
   * Add data to table. Reads table headers, and matches them against passed in data keys
   * If data is array, adds all values that it can match, if object - adds one row
   * If table has no headers, adds an arbitrary row with data values
   * @param {HTMLTableElement} table table element to add to
   * @param {Array|Object} rowData data to add
   * @param {Function} displayRow custom function to render the new row from data, should return string for the <tr> element
   * @return {undefined}
   */
  static tableAdd(table, rowData, displayRow) {
    Utils.checkArgTypes([table, rowData], 'object', 'enum');
    if (!Utils.isDOMNode(table)) throw new Error('table must be a DOM element');
    let headers = table.querySelector('thead tr');
    let insertInto = table.querySelector('tbody') || table; //insert into tbody or directly into table if missing
    if (!displayRow) {
      displayRow = function (data, headers) {
        let keys = Object.keys(rowData);
        let row = `<tr ${(rowData.id) ? 'data-id=' + rowData.id : ''}>`;
        keys.forEach(key => {
          if (((headers && headers.includes(key)) || !headers) && key !== 'id')
            row += `<td>${rowData[key]}</td>`;
        });
        row += '</tr>';
        return row;
      }
    }
    let headerValues = [];
    if (headers) {
      let headerFields = headers.querySelectorAll('th');
      headerFields.forEach(field => headerValues.push(field.innerHTML));
    }
    if (!(rowData instanceof Array)) rowData = [rowData];

    let rowsToInsert = [];
    for (let data of rowData) {
      rowsToInsert.push(displayRow(data, headerValues));
    }
    insertInto.insertAdjacentHTML(rowsToInsert.join('\n'), 'beforeend');
  }

  /**
   * Appends a single row to table, cloning it's last row and filling values in order they are passed
   * return null if tbody does not contain any <tr> elements to clone
   * @param {HTMLElement} table table to which to add
   * @param {Object} dataset hash with the new dataset to assign to row or empty, for nothing
   * @param {String|Number} values values to add, in order from left to right
   * @return {Node} the newly added <tr> element
   */
  static tableAppendRow(table, dataset, ...values) {
    let tbody = table.querySelector('tbody');
    let lastRow = tbody.querySelectorAll('tr');
    if(!lastRow.length) return null;
    lastRow = lastRow[lastRow.length - 1];
    let clone = lastRow.cloneNode(true);
    Object.keys(clone.dataset).forEach((key) => delete clone.dataset[key]);
    Object.keys(dataset).forEach(key => clone.dataset[key] = dataset[key]);
    clone.querySelectorAll('td').forEach((td, i) => {
      td.innerHTML = values[i]
    });
    //TODO: use HTML table API
    tbody.insertAdjacentElement('beforeend', clone);
    return clone;
  }

  static toggleClasses(elements, class1, class2) {
    if (Utils.isNode(elements)) elements = [elements];
    else elements = [].concat(Utils.toArray(elements)); //convert to array if single element
    elements.forEach((element) => {
      let classAttr = element.getAttribute('class');
      if (classAttr.includes(class1))
        element.setAttribute('class', classAttr.replace(class1, class2));
      else if (classAttr.includes(class2))
        element.setAttribute('class', classAttr.replace(class2, class1));
      else element.setAttribute('class', classAttr + ' ' + class1);
    });
  }

  /**
   * Get pre-configured flatpickr object
   * @param selector
   */
  static getFlatpickrObject(selector){
    function setDates(dateObjects, dateStr, instance){
      const date = moment(dateObjects[0]);
      let {dataset, value} = instance.input;
      instance.selectedDateObj = date.toDate();
      dataset['datetime'] = date.format('YYYY-MM-DD HH:mm');
      value = dateStr;
    }
    locales[this.getLocale()].firstDayOfWeek = 1;
    locales['fi'].weekAbbreviation = 'Vk';

    return flatpickr(selector, {
      weekNumbers:true,
      time_24hr: true,
      enableTime: true,
      minuteIncrement: 1,
      locale: locales[this.getLocale()],
      onValueUpdate: setDates,
      onReady: setDates,
      onOpen: setDates,
      onClose: setDates
    });
  }

  /**
   * Add column to a table, using the getCell function to obtain the cell html
   * @param {HTMLTableElement} table table element to modify
   * @param {Function} getNewCell that gets a row as input and returns the innerHTML of the new <td> element
   * @param {String} header optional header for the new column
   * @returns {undefined}
   */
  static tableAddColumn(table, getNewCell, header) {
    if (header) table.querySelector('thead tr').insertAdjacentHTML(`<th>${header}</th>`, 'beforeend');
    let rows = table.querySelectorAll('tbody tr');
    rows.forEach(row => row.insertAdjacentHTML(`<td>${getNewCell(row)}</td>`, 'beforeend'));
  }

  static getSelectElementValue(selectElement) {
    return selectElement.options[selectElement.selectedIndex].value;
  }
  static getSelectElementInnerHTML(selectElement) {
    return selectElement.options[selectElement.selectedIndex].innerHTML;
  }
  static getStandardProps(){
    return [
      'createdAt',
      'updatedAt',
      'id'
    ]
  }
}
