import React, { Component } from 'react';
import {Utils}                from 'utils';
import {Translator}           from 'translator';
const translator = new Translator(Utils.getLocale());

/**
 * Loading component; A spinner icon with Loading text above it
 */
export default class Loading extends Component {
  render() {
    return (
      <div className="cell small-12">
        <div className="text-center">
          <h3>{translator.trans('Loading...')}</h3>
          <div className="spinner large"></div>
        </div>
      </div>
    );
  }
}