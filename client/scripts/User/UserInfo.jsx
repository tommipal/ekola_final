import React, { Component } from 'react';
import moment               from 'moment';
import {Translator}         from 'translator'
import { Utils }            from 'utils';

const translator = new Translator(Utils.getLocale());

/**
 * Renders user information modal
 */
export default class UserInfo extends Component {
  render() {
    const userGroups = this.props.data.userGroups.map((userGroup) => userGroup.name);
    const evaluations = this.props.data.evaluations.map((evaluation) => evaluation.name);
    const supervisedEvaluations = this.props.data.supervisedEvaluations.map((evaluation) => evaluation.name);
    return (
      <section id="user-info" className="grid-x grid-padding-x">
        <div className="cell small-12">
          <h4>{translator.trans('User information')}</h4>
        </div>
        <div className="cell small-12 medium-6">
          <b>{translator.trans('Full name')}</b><p>{`${this.props.data.user.givenName} ${this.props.data.user.familyName}`}</p>
        </div>
        <div className="cell small-12 medium-6">
          <b>{translator.trans('Joined')}</b><p>{moment(this.props.data.joined).format('YYYY-MM-DD')}</p>
        </div>
        <div className="cell small-12">
          <b>{translator.trans('Email')}</b><p>{this.props.data.user.email}</p>
        </div>
        <div className="cell small-12 medium-6">
          <b>{translator.trans('Active group')}</b>
          <p>{this.props.data.user.userGroup ? this.props.data.user.userGroup.name : translator.trans('No active group')}</p>
        </div>
        <div className="cell small-12 medium-6">
          <b>{translator.trans('All groups')} {`(${this.props.data.userGroups.length})`}</b>
          <p>{userGroups.length ? userGroups.join(', ') : translator.trans('No groups')}</p>
        </div>
        <div className="cell small-12 medium-6">
          <b>{translator.trans('Evaluations')} {`(${this.props.data.evaluations.length})`}</b>
          <p>{evaluations.length ? evaluations.join(', ') : translator.trans('No evaluations')}</p>
        </div>
        <div className="cell small-12 medium-6">
          <b>{translator.trans('Supervised evaluations')} {`(${this.props.data.supervisedEvaluations.length})`}</b>
          <p>{supervisedEvaluations.length ? supervisedEvaluations.join(', ') : translator.trans('No supervised evaluations')}</p>
        </div>
      </section>
    );
  }
}