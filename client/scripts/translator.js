const translations = require('./translations');

class Translator {
  constructor(lang = 'en') {
    this.lang = lang;
    this.translations = translations;
    this.re = new RegExp("%value%", "g");
  }

  static get supportedLocales() {
    return ['en', 'fi'];
  }

  changeLang(lang) {
    this.lang = lang;
  }

  /**
   * Translates the given string to whatever language has been set when creating the Translator instance
   * If string contains a variable values use %value% where the variables are and give the variables as parameters after the string
   * example: translator.trans("%value% string with %value% variable values", 'Cool', 2) returns "Cool string with 2 variable values"
   * @param {String} key The string that will be translated
   * @param {Number | String} extra  variables that will replace %value%s inside the string. Must be in the order of %value% occurrences in the key string
   * @returns {String} translated string
   */
  trans(key, ...extra) {
    if(!key) return null;
    let translation = this.translations[key];
    if (!translation) {
      console.log(`Translator: Missing value for key "${key}"`);
      return key;
    } else translation = translation[this.lang];

    if (extra && translation.match(this.re)) {
      // Mismatching %values% and extras
      if (extra.length !== translation.match(this.re).length) {
        console.error('Number of %value%:s and given replace values does not match');
        return null;
      }
      // Single variable
      else if (extra && extra.length === 1) {
        if (parseInt(extra) === 1) {
          // Try to find a singular version of the string, if not found use the default string
          translation = this.translations[key][this.lang + "Single"] || this.translations[key][this.lang];
          return translation.replace(this.re, extra);
        }
        return translation.replace(this.re, extra);
      }
      // Multiple variables
      else if (extra && extra.length > 1) {
        if (extra.includes(1) || extra.includes('1')) {
          translation = this.translations[key][this.lang + "Single"] || this.translations[key][this.lang];
        }
        if (extra.length !== translation.match(this.re).length) {
          console.log(extra);
          console.error('Number of %value%:s and given replace values does not match');
          return null;
        } else {
          let i = 0;
          return translation.replace(this.re, () => {
            const extraKey = extra[i];
            i++;
            return extraKey;
          });
        }
      }
    }
    // No variables
    else if (extra.length === 1 && !translation.match(this.re)) {
      // Single variable
      if (parseInt(extra) === 1) {
        // Try to find a singular version of the string, if not found use the default string
        return this.translations[key][this.lang + "Single"] || this.translations[key][this.lang];
      }
      return this.translations[key][this.lang];
    }
    else if (key.match(this.re)) {
      console.error('You must provide a value to replace %value% in your string');
      return null;
    } else {
      return translation;
    }
  }
}

(function (exports) {
  exports.Translator = Translator;
})(typeof exports === 'undefined' ? this['translatorModule'] = {} : exports);