import React, { Component }   from 'react';
import {SurveyContent}        from './Survey/edit/Survey';
import ReactDOM from 'react-dom';

const root = document.getElementById('react-root');
const surveyId = Number(root.dataset['surveyId']);

/**
 * Parent component for the Survey Edit react page
 */
class App extends Component {
  render() {
    return (
      <div className="App">
        <SurveyContent surveyId = {surveyId} />
      </div>
    );
  }
}

import registerServiceWorker from './registerServiceWorker';
ReactDOM.render(<App />, root);
registerServiceWorker();