import React, { Component }   from 'react';
import ReactDOM               from 'react-dom';
import {SurveyTable}          from './Survey/list/SurveyTable';

const root = document.getElementById('react-root');
const filter = root.dataset['filter'];

/**
 * Parent component for the Survey List react pages
 * filter is either active or closed
 */
class App extends Component {
  render() {
    return (
      <div className="App">
        <SurveyTable filter={filter} />
      </div>
    );
  }
}

import registerServiceWorker from './registerServiceWorker';
ReactDOM.render(<App />, root);
registerServiceWorker();
