module.exports = {
  // JS / REACT
  "%value% surveys": {
    "en": "%value% surveys",
    "fi": "%value% kyselyt"
  },
  "Icon": {
    "en": "Icon",
    "fi": "Ikoni"
  },
  "Answer survey": {
    "en": "Answer survey",
    "fi": "Vastaa kyselyyn"
  },
  "Close survey": {
    "en": "Close survey",
    "fi": "Sulje kysely"
  },
  "Edit answers": {
    "en": "Edit answers",
    "fi": "Muokkaa vastauksia"
  },
  "Delete survey": {
    "en": "Delete survey",
    "fi": "Poista kysely"
  },
  "View results": {
    "en": "View results",
    "fi": "Tarkastele tuloksia"
  },
  "Reopen survey": {
    "en": "Reopen survey",
    "fi": "Uudelleenavaa kysely"
  },
  "Edit survey": {
    "en": "Edit survey",
    "fi": "Muokkaa kyselyä"
  },
  "Survey details": {
    "en": "Survey details",
    "fi": "Kyselyn tiedot"
  },
  "Survey": {
    "en": "Survey",
    "fi": "Kysely"
  },
  "Opened": {
    "en": "Opened",
    "fi": "Avattu"
  },
  "Closes": {
    "en": "Closes",
    "fi": "Sulkeutuu"
  },
  "Closed": {
    "en": "Closed",
    "fi": "Sulkeutui"
  },
  "Evaluation": {
    "en": "Evaluation",
    "fi": "Arviointiryhmä"
  },
  "No active surveys": {
    "en": "No active surveys",
    "fi": "Ei aktiivisia kyselyitä"
  },
  "No closed surveys": {
    "en": "No closed surveys",
    "fi": "Ei suljettuja kyselyitä"
  },

  /// Survey info
  "Number of answers": {
    "en": "Number of answers",
    "fi": "Vastausten määrä"
  },
  "Supervisors": {
    "en": "Supervisors",
    "fi": "Valvojat"
  },
  "Number of questions": {
    "en": "Number of questions",
    "fi": "Kysymysten määrä"
  },
  "Participating groups": {
    "en": "Participating groups",
    "fi": "Osallistuvat vastaajaryhmät"
  },
  "No supervisors": {
    "en": "No supervisors",
    "fi": "Ei valvojia"
  },
}