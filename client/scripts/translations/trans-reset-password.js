module.exports = {
    // Send token EJS
    "Reset password": {
      "en": "Reset password",
      "fi": "Uusi salasana"
    },
    "New password will be sent your email address.": {
      "en": "New password will be sent to your email address.",
      "fi": "Uusi salasana lähetetään sinulle sähköpostina."
    },
    "Username or email": {
      "en": "Username or email",
      "fi": "Käyttäjänimi tai sähköposti"
    },
    "Reset": {
      "en": "Reset",
      "fi": "Uusi"
    },
    "Back to login page": {
      "en": "Back to login page",
      "fi": "Takaisin kirjautumiseen"
    },
    // EJS
    "Password reset": {
      "en": "Password reset",
      "fi": "Salasanan palautus"
    },
    "Password reset has been requested for user: %value%. Click the link to go to the password reset page.": {
      "en": "Password reset has been requested for user: %value%. Click the link to go to the password reset page.",
      "fi": "Salasanan palauttamista on pyydetty käyttäjälle: %value%. Seuraa linkkiä palauttaaksesi salasana."
    },
    "Go to eKola home": {
      "en": "Go to eKola home",
      "fi": "Palaa takaisin eKolaan"
    },
}