module.exports = {
  // JS / REACT
  // Edit survey
  "Title": {
    "en": "Title",
    "fi": "Otsikko"
  },
  "Select evaluation": {
    "en": "Select evaluation",
    "fi": "Valitse arviointiryhmä"
  },
  "Start time": {
    "en": "Start time",
    "fi": "Aloitusaika"
  },
  "End time": {
    "en": "End time",
    "fi": "Päättymisaika"
  },
  "Results open to answerers": {
    "en": "Results open to answerers",
    "fi": "Tulokset avoinna vastaajille"
  },
  "Enable sharing": {
    "en": "Enable sharing",
    "fi": "Ota jakaminen käyttöön"
  },
  "%value% Questions selected": {
    "en": "%value% Questions selected",
    "fi": "%value% Kysymystä valittu",
    "enSingle": "%value% Question selected",
    "fiSingle": "%value% Kysymys valittu"
  },
  "Create a link for a group": {
    "en": "Create a link for a group",
    "fi": "Luo linkki vastaajaryhmälle"
  },
  "Enter a valid email address": {
    "en": "Enter a valid email address",
    "fi": "Kirjoita sähköpostiosoite"
  },
  "Email already exists": {
    "en": "Email already exists",
    "fi": "Sähköposti on jo annettu"
  },
  "Emails": {
    "en": "Emails",
    "fi": "Sähköpostit"
  },
  "Create a link for this group": {
    "en": "Create a link for this group",
    "fi": "Luo linkki tälle vastaajaryhmälle"
  },
  "All groups*": {
    "en": "All groups*",
    "fi": "Kaikki vastaajaryhmät*"
  },
  "Are you sure you want to send an email to:": {
    "en": "Are you sure you want to send an email to:",
    "fi": "Oletko varma, että haluat lähettää kutsun näihin sähköposteihin:"
  },
  "Background questions": {
    "en": "Background questions",
    "fi": "Taustakysymykset"
  },
  "Select questions to import": {
    "en": "Select questions to import",
    "fi": "Valitse tuotavat kysymykset"
  },
  "Search questions": {
    "en": "Search questions",
    "fi": "Hae kysymyksiä"
  },
  "Search": {
    "en": "Search",
    "fi": "Hae"
  },
  "Questions": {
    "en": "Questions",
    "fi": "Kysymykset"
  },
  "Criteria": {
    "en": "Criteria",
    "fi": "Kriteerit"
  },
  "Select type": {
    "en": "Select type",
    "fi": "Valitse tyyppi"
  },
  "Multiple choice": {
    "en": "Multiple choice",
    "fi": "Monivalinta"
  },
  "Scale": {
    "en": "Scale",
    "fi": "Asteikko"
  },
  "Value": {
    "en": "Value",
    "fi": "Arvo"
  },
  "Scale options": {
    "en": "Scale options",
    "fi": "Asteikon asetukset"
  },
  "A select type criteria needs at least 2 options!": {
    "en": "A multiple choice criteria needs at least 2 options!",
    "fi": "Monivalintakysymyksellä täytyy olla vähintää 2 vaihtoehtoa!"
  },
  //// Buttons
  "Import": {
    "en": "Import",
    "fi": "Tuo"
  },
  "Import selected": {
    "en": "Import selected",
    "fi": "Tuo valitut"
  },
  "New": {
    "en": "New",
    "fi": "Uusi"
  },
  "Control": {
    "en": "Control",
    "fi": "Hallitse"
  },
  "Control questions": {
    "en": "Control questions",
    "fi": "Hallitse kysymyksiä"
  },
  "Import questions from the question bank": {
    "en": "Import questions from the question bank",
    "fi": "Tuo kysymyksiä kysymyspankista"
  },
  "New question": {
    "en": "New question",
    "fi": "Luo kysymys"
  },
  "New criteria": {
    "en": "New criteria",
    "fi": "Luo kriteeri"
  },
  "Remove link": {
    "en": "Remove link",
    "fi": "Poista linkki"
  },
  "Send email": {
    "en": "Send email",
    "fi": "Lähetä sähköpostilla"
  },
  "Copy to clipboard": {
    "en": "Copy to clipboard",
    "fi": "Kopioi leikepöydälle"
  },
  "Add field": {
    "en": "Add field",
    "fi": "Lisää kenttä"
  },
  "Remove field": {
    "en": "Remove field",
    "fi": "Poista kenttä"
  },
  "Add a new criteria": {
    "en": "Add a new criteria",
    "fi": "Lisää uusi kriteeri"
  },
  "Remove choice": {
    "en": "Remove choice",
    "fi": "Poista vaihtoehto"
  },
  "Save as template": {
    "en": "Save as template",
    "fi": "Tallenna kyselypohjaksi"
  },
  "Add a choice": {
    "en": "Add a choice",
    "fi": "Lisää vaihtoehto"
  },
  //// Messages
  "Link copied!": {
    "en": "Link copied!",
    "fi": "Linkki kopioitu!"
  },
  "Survey link sent to %value% emails!": {
    "en": "Survey link sent to %value% emails!",
    "fi": "Linkki lähetetty %value% sähköpostiin!"
  },
  "Survey saved!": {
    "en": "Survey saved!",
    "fi": "Kysely tallennettu"
  },
  "Saved as a template!": {
    "en": "Saved as a template!",
    "fi": "Tallennettu kyselypohjaksi!"
  },
  "Saving failed. Template with this title already exists.": {
    en: "Saving failed. Template with this title already exists.",
    fi: "Tallentaminen epäonnistui. Samanniminen kyselypohja on jo olemassa."
  },
  "Saving failed. Try again later.": {
    "en": "Saving failed. Try again later.",
    "fi": "Tallennus epäonnistui. Yritä myöhemmin"
  },
  "Error occured when sending emails": {
    "en": "Error occured when sending emails.",
    "fi": "Sähköposteja ei voitu lähettää. Yritä myöhemmin uudelleen."
  },
  "Could not change evaluation": {
    "en": "Could not change evaluation",
    "fi": "Arviointiryhmän vaihto epäonnistui"
  },
  "Could not add link": {
    "en": "Could not add link",
    "fi": "Linkin lisäys epäonnistui"
  },
  "Could not remove link": {
    "en": "Could not remove link",
    "fi": "Linkin poistaminen epäonnistui"
  },
  "Survey contains errors": {
    "en": "Survey contains errors",
    "fi": "Kysely sisältää virheitä"
  },
  //// Errors
  "Empty value": {
    "en": "Empty value",
    "fi": "Tyhjä arvo"
  },
  "Empty title": {
    "en": "Empty title",
    "fi": "Tyhjä otsikko"
  },
  "Empty field": {
    "en": "Empty field",
    "fi": "Tyhjä kenttä"
  },
  "Empty description": {
    "en": "Empty description",
    "fi": "Tyhjä kuvaus"
  },
  "Duplicate field": {
    "en": "Duplicate field ",
    "fi": "Kenttä on jo olemassa"
  },
  "Duplicate title": {
    "en": "Duplicate title ",
    "fi": "Otsikko on jo olemassa"
  },
  "Duplicate description": {
    "en": "Duplicate description",
    "fi": "Vaihtoehto on jo olemassa"
  },
  "Duplicate value": {
    "en": "Duplicate value",
    "fi": "Arvo on jo olemassa"
  },
  "Survey must start before it ends!": {
    "en": "Survey must start before it ends!",
    "fi": "Kyselyn aloitusaika täytyy olla ennen päättymisaikaa!"
  },
  "Question needs at least 1 criteria!": {
    "en": "Question needs at least 1 criteria!",
    "fi": "Kysymyksellä täytyy olla vähintään 1 kriteeri"
  },
  "Max value must be greater than min!": {
    "en": "Max value must be greater than min!",
    "fi": "Maksimi arvon täytyy olla suurempi kuin minimi!"
  },
  "Survey needs at least 1 question!": {
    "en": "Survey needs at least 1 question!",
    "fi": "Kysely vaatii vähintään 1 kysymyksen!"
  },
}