module.exports = {
    // EJS
    "Additional information": {
      "en": "Additional information",
      "fi": "Lisätiedot"
    },
    "Describe why you chose these specific answers": {
      "en": "Describe why you chose these specific answers",
      "fi": "Perustele, miksi valitsit juuri nämä vastausvaihtoehdot",
      "enSingle": "Describe why you chose this specific answer",
      "fiSingle": "Perustele, miksi valitsit juuri tämän vastausvaihtoehdon",
    },
    "Evidence": {
      "en": "Evidence",
      "fi": "Perustelut"
    },
    "Yes": {
      "en": "Yes",
      "fi": "Kyllä"
    },
    "Save": {
      "en": "Save",
      "fi": "Tallenna"
    },
    "Collapse all": {
      "en": "Collapse all",
      "fi": "Piilota kaikki"
    },
    "Answering as a member of: %value%": {
      "en": "Answering as a member of %value%",
      "fi": "Vastataan %value% -vastaajaryhmän jäsenenä"
    },

    // JS
    "Saved automatically": {
      "en": "Saved automatically!",
      "fi": "Tallennettu automaattisesti!"
    },
}