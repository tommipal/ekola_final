module.exports = {
    // EJS
    "Old password": {
      "en": "Old password",
      "fi": "Vanha salasana"
    },
    "New password": {
      "en": "New password",
      "fi": "Uusi salasana"
    },
    "Confirm new password": {
      "en": "Confirm new password",
      "fi": "Vahvista uusi salasana"
    },
    "Back to profile page": {
      "en": "Back to profile page",
      "fi": "Takaisin profiiliin."
    },
}