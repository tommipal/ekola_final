module.exports = {
  // EJS
  "Results": {
    "en": "Results",
    "fi": "Tulokset"
  },
  "Download results": {
    "en": "Download results",
    "fi": "Lataa tulokset"
  },
  "Select group": {
    "en": "Select group",
    "fi": "Valitse vastaajaryhmä"
  },
  "Average": {
    "en": "Average",
    "fi": "Keskiarvo"
  },
  "Question specific": {
    "en": "Question specific",
    "fi": "Kysymyskohtainen"
  },
  "Four way graph": {
    "en": "Four way graph",
    "fi": "Nelisuuntainen kuvaaja"
  },
  "Previous": {
    "en": "Previous",
    "fi": "Edellinen"
  },
  "Previous question": {
    "en": "Previous question",
    "fi": "Edellinen kysymys"
  },
  "Next question": {
    "en": "Next question",
    "fi": "Seuraava kysymys"
  },

  // JS
  "%value% answers": {
    "en": "%value% answers",
    "fi": "%value% vastausta",
    "enSingle": "%value% answer",
    "fiSingle": "%value% vastaus",
  },
  "There are no answers for this survey.": {
    "en": "There are no answers for this survey.",
    "fi": "Tähän kyselyyn ei ole vastauksia."
  },
  "Average:": {
    "en": "Average:",
    "fi": "Keskiarvo:"
  },
  "Value:": {
    "en": "Value:",
    "fi": "Arvo:"
  },
  "Answers:": {
    "en": "Answers:",
    "fi": "Vastauksia:"
  },
  "Description:": {
    "en": "Description:",
    "fi": "Kuvaus:"
  },
  "Criteria:": {
    "en": "Criteria:",
    "fi": "Kriteeri:"
  },
  "Create": {
    "en": "Create",
    "fi": "Luo"
  },
  "Are you sure you want to end this answering session? This will delete your current answers. You can take the survey again with the same link.": {
    "en": "Are you sure you want to end this answering session? This will delete your current answers. You can take the survey again with the same link.",
    "fi": "Oletko varma, että haluat lopettaa vastaamisen? Painamalla kyllä poistat tähän mennessä tehdyt muutokset ja poistut sivulta."
  },
  "This question doesn't have enough criteria to be drawn.": {
    "en": "This question doesn't have enough criteria to be drawn.",
    "fi": "Tällä kysymyksellä ei ole tarpeeksi kriterioita piirtämistä varten."
  },

  // scatterGraph
  "Question": {
    "en": "Question",
    "fi": "Kysymys"
  },

  // fourWayGraph
  "X-axis": {
    "en": "X-axis",
    "fi": "X-akseli"
  },
  "Y-axis": {
    "en": "Y-axis",
    "fi": "Y-akseli"
  },
}