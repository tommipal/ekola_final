module.exports = {
  // EJS
  "Forgot your password?": {
    "en": "Forgot your password?",
    "fi": "Unohditko salasanasi?",
  },
  "Sign up": {
    "en": "Sign up",
    "fi": "Rekisteröidy"
  },
  "This website uses cookies to save your settings. By pressing log in, you consent to the usage of cookies and forfeit any diet you might have been on.": {
    "en": "This website uses cookies to save your settings. By pressing log in, you consent to the usage of cookies and forfeit any diet you might have been on.",
    "fi": "Tämä sivusto käyttää evästeitä asetustesi tallentamiseen. Painamalla \"kirjaudu sisään\" hyväksyt evästeiden käytön."
  },
}