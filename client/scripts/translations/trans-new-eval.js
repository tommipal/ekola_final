module.exports = {
  // EJS
  "Evaluation name": {
    "en": "Evaluation name",
    "fi": "Arviointiryhmän nimi"
  },
  "Groups": {
    "en": "Groups",
    "fi": "Vastaajaryhmät"
  },
  "Create a new group": {
    "en": "Create a new group",
    "fi": "Luo uusi vastaajaryhmä"
  },
  "New group": {
    "en": "New group",
    "fi": "Luo vastaajaryhmä"
  },
}