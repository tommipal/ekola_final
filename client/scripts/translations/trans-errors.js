module.exports = {
  "Username does not exist": {
    "en": "Username does not exist",
    "fi": "Käyttäjätunnusta ei ole olemassa"
  },
  "Incorrect password": {
    "en": "Incorrect password",
    "fi": "Väärä salasana"
  },
  "Password and confirmation do not match!": {
    "en": "Password and confirmation do not match!",
    "fi": "Salasana ja vahvistus eroavat toisistaan!"
  },
  "Username not given!": {
    "en": "Username not given!",
    "fi": "Käyttäjätunnusta ei annettu!"
  },
  "Email not given!": {
    "en": "Email not given!",
    "fi": "Sähköpostia ei annetteu!"
  },
  "First and last name are required!": {
    "en": "First and last name are required!",
    "fi": "Etu- ja sukunimi ovat pakollisia!"
  },
  "No group or invalid group data": {
    "en": "No group or invalid group data",
    "fi": "Ei vastaajaryhmää tai virheelliset ryhmän tiedot"
  },
  "Username is already taken": {
    "en": "Username is already taken",
    "fi": "Käyttäjätunnus on jo käytössä"
  },
  "Email is already taken": {
    "en": "Email is already taken",
    "fi": "Sähköposti on jo käytössä"
  },
  "Group does not exist": {
    "en": "Group does not exist",
    "fi": "Vastaajaryhmää ei ole olemassa"
  },
  "Group name missing": {
    "en": "Group name missing",
    "fi": "Vastajaryhmän nimi puuttuu"
  },
  "Password or confirmation not given": {
    "en": "Password or confirmation not given",
    "fi": "Salasana tai vahvistus puuttuu"
  },
  "Invalid reset token!": {
    "en": "Invalid reset token!",
    "fi": "Virheellinen nollaustunniste!"
  },
  "Username or email does not exist": {
    "en": "Username or email does not exist",
    "fi": "Käyttäjätunnusta tai sähköpostia ei ole olemassa"
  },
  "Error sending email!": {
    "en": "Error sending email!",
    "fi": "Sähköpostia ei voitu lähettää!"
  },
  "Old password incorrect!": {
    "en": "Old password incorrect!",
    "fi": "Vanha salasana on virheellinen!"
  },
  "Evaluation needs a name!": {
    "en": "Evaluation needs a name!",
    "fi": "Arviointiryhmä tarvitsee nimen!"
  },
  "Evaluation needs at least one group": {
    "en": "Evaluation needs at least one group",
    "fi": "Arviointiryhmä tarvitsee vähintään yhden vastaajaryhmän"
  },
  "An error occurred while processing your request": {
    "en": "An error occurred while processing your request",
    "fi": "Virhe tapahtui käsitellessä pyyntöäsi"
  },
  // error-production.ejs
  "Oops, something went wrong": {
    en: "Oops, something went wrong",
    fi: "Oho, jotain meni pieleen"
  },
  "Don't worry. If the problem recurs please contact karhukopla@metropolia.fi": {
    en: "Don't worry. If the problem recurs, please contact karhukopla@metropolia.fi",
    fi: "Ei hätää. Jos ongelma toistuu, ota yhteyttä karhukopla@metropolia.fi"
  },
  // error-surveynotactive.ejs
  "Survey %value% in not open": {
    en: "Survey %value% in not open",
    fi: "Kysely %value% ei ole aktiivinen"
  },
  "Survey closed at %value%": {
    en:"Survey closed at %value%",
    fi: "Kysely sulkeutui %value%"
  },
  "Survey opens at %value%": {
    en: "Survey opens at %value%",
    fi: "Kysely avautuu %value%"
  },
}