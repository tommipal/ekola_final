module.exports = {
	// EJS
	"You are about to answer to survey %value%": {
		en: "You are about to answer to survey %value%",
		fi: "Olet vastaamassa kyselyyn %value%"
	},
	"Before answering, please select the group you are representing": {
		en: "Please select the group you are representing",
		fi: "Ole hyvä ja valitse edustamasi vastaajaryhmä"
	}
}