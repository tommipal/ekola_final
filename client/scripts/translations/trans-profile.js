module.exports = {
  // EJS
  /// about
  "Personal information": {
    "en": "Personal information",
    "fi": "Henkilökohtaiset tiedot"
  },
  "Group description": {
    "en": "Group description",
    "fi": "Ryhmän kuvaus"
  },
  "No active group": {
    en: "No active group",
    fi: "Ei aktiivista ryhmää"
  },
  /// usercontent
  "Manage profile": {
    "en": "Manage profile",
    "fi": "Muokkaa profiilia"
  },
  "Change password": {
    "en": "Change password",
    "fi": "Vaihda salasanaa"
  },
  "Change group": {
    "en": "Change group",
    "fi": "Vaihda ryhmää"
  },
  "My supervised evaluations": {
    "en": "My supervised evaluations",
    "fi": "Valvomani arviointiryhmät"
  },
  "My evaluations": {
    "en": "My evaluations",
    "fi": "Arviointiryhmäni"
  },
  "My surveys": {
    "en": "My surveys",
    "fi": "Kyselyni"
  },
  "Ongoing surveys": {
    "en": "Ongoing surveys",
    "fi": "Meneillään olevat kyselyt"
  },
  "My templates": {
    "en": "My templates",
    "fi": "Kyselypohjani"
  },
  "Leave": {
    "en": "Leave",
    "fi": "Eroa"
  },
  "Leave evaluation": {
    "en": "Leave evaluation",
    "fi": "Eroa arviointiryhmästä"
  },
  "Answer": {
    "en": "Answer",
    "fi": "Vastaa"
  },
  "Continue answering": {
    "en": "Continue answering",
    "fi": "Jatka vastaamista"
  },
  "Edit": {
    "en": "Edit",
    "fi": "Muokkaa"
  },
  "Delete evaluation": {
    "en": "Delete evaluation",
    "fi": "Poista arviointiryhmä"
  },
  "Delete template": {
    "en": "Delete template",
    "fi": "Poista kyselypohja"
  },
  // JS
  "Are you sure you want to delete template '%value%'?": {
    "en": "Are you sure you want to delete template '%value%'?",
    "fi": "Oletko varma, että haluat poistaa kyselypohjan '%value%'?"
  },
  "Are you sure you want to leave this evaluation? You will no longer be able to answer surveys for this evaluation.": {
    "en": "Are you sure you want to leave this evaluation? You will no longer be able to answer surveys for this evaluation.",
    "fi": "Oletko varma, että haluat poistua tästä arviointiryhmästä? Poistumisen jälkeen et enää voi vastata tämän ryhmän kyselyihin.",
  },
  "Are you sure you want to delete this evaluation? All of it's surveys will be unavailable until assigned a new one and all answers will be invalidated": {
    "en": "Are you sure you want to delete this evaluation? All of it's surveys will be unavailable until assigned a new one and all answers will be invalidated",
    "fi": "Haluatko varmasti poistaa tämän arviointiryhmän? Ryhmän kyselyihin ei voi enää vastata ennen kuin niille määritetään uusi arviointirymä.",
  },
  "You are not supervising any evaluations currently": {
    "en": "You are not supervising any evaluations currently",
    "fi": "Sinulla ei ole yhtään valvottavaa arviointiryhmää tällä hetkellä"
  },
  "You are not a participant in any evaluation currently": {
    "en": "You are not a participant in any evaluation currently",
    "fi": "Sinä et ole osana mitään arvointiryhmää tällä hetkellä"
  },
  "You have not created any surveys": {
    "en": "You have not created any surveys",
    "fi": "Sinä et ole luonut yhtään kyselyä"
  },
  "You are not participating in any active surveys currently": {
    "en": "You are not participating in any active surveys currently",
    "fi": "Sinä et ole osallistunut yhteenkään aktiiviseen kyselyyn tällä hetkellä"
  },

}