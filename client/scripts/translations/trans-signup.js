module.exports = {
  // EJS
  "Confirm password": {
    "en": "Confirm password",
    "fi": "Vahvista salasana"
  },
  "Select an existing group to which you want to be assigned.": {
    "en": "Select an existing group to which you want to be assigned.",
    "fi": "Valitse vastaajaryhmä, johon haluat liittyä."
  },
  "No group": {
    "en": "No group",
    "fi": "Ei vastaajaryhmää"
  },
}