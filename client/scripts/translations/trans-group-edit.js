module.exports = {
  // JS / REACT
  "Are you sure you want to remove %value% users?": {
    "en": "Are you sure you want to remove %value% users?",
    "fi": "Oletko varma että haluat poistaa %value% käyttäjää?",
    "enSingle": "Are you sure you want to remove this user?",
    "fiSingle": "Oletko varma että haluat poistaa tämän käyttäjän?"
  },
  "Are you sure you want to give %value% users admin permissions to this group": {
    "en": "Are you sure you want to give %value% users admin permissions to this group",
    "fi": "Oletko varma, että haluat antaa %value% käyttäjälle ylläpito-oikeudet tähän vastaajaryhmään?",
    "enSingle": "Are you sure you want to give this user admin permissions to this group",
    "fiSingle": "Oletko varma, että haluat antaa tälle käyttäjälle ylläpito-oikeudet tähän vastaajaryhmään?",
  },
  "Select an evaluation to assign %value% users to:": {
    "en": "Select an evaluation to assign %value% users to:",
    "fi": "Valitse arviointiryhmä, johon %value% käyttäjää lisätään:",
    "enSingle": "Select an evaluation to assign %value% user to:",
    "fiSingle": "Valitse arviointiryhmä, johon %value% käyttäjä lisätään:"
  },
  "Select an evaluation where %value% users are removed from:": {
    "en": "Select an evaluation where %value% users are removed from:",
    "fi": "Valitse arviointiryhmä, josta %value% käyttäjää poistetaan:",
    "enSingle": "Select an evaluation where this user is removed from:",
    "fiSingle": "Valitse arviointiryhmä, josta käyttäjä poistetaan:"
  },
  "No selection": {
    "en": "No selection",
    "fi": "Ei valintaa"
  },
  "%value% users removed from evaluation '%value%'": {
    "en": "%value% users removed from evaluation '%value%'",
    "fi": "%value% käyttäjää poistettu arviointiryhmästä '%value%'",
    "enSingle": "%value% user removed from evaluation '%value%'",
    "fiSingle": "%value% käyttäjä poistettu arviointiryhmästä '%value%'"
  },
  "%value% users added to evaluation '%value%'": {
    "en": "%value% users added to evaluation '%value%'",
    "fi": "%value% käyttäjää lisätty arviointiryhmään '%value%'",
    "enSingle": "%value% user added to evaluation '%value%'",
    "fiSingle": "%value% käyttäjä lisätty arviointiryhmään '%value%'"
  },
  "%value% users removed from this group": {
    "en": "%value% users removed from this group",
    "fi": "%value% käyttäjää poistettu tästä ryhmästä",
    "enSingle": "User removed from this group",
    "fiSingle": "Käyttäjä poistettu tästä ryhmästä",
  },
  "%value% users promoted to administrators": {
    "en": "%value% users promoted to administrators",
    "fi": "Ylläpito-oikeudet annettu %value% käyttäjälle",
    "enSingle": "User promoted to administrator",
    "fiSingle": "Ylläpito-oikeudet annettu käyttäjälle",
  },
  "Selected users:": {
    "en": "Manage selected users:",
    "fi": "Hallitse valittuja käyttäjiä:"
  },
  "Group users": {
    "en": "Group users",
    "fi": "Vastaajaryhmän käyttäjät"
  },
  "Assign to an evaluation": {
    "en": "Assign to an evaluation",
    "fi": "Lisää arviointiryhmään"
  },
  "Remove from evaluation": {
    "en": "Remove from evaluation",
    "fi": "Poista arviointiryhmästä"
  },
  "Remove from group": {
    "en": "Remove from group",
    "fi": "Poista vastaajaryhmästä"
  },
  "Give admin permissions": {
    "en": "Give admin permissions",
    "fi": "Anna ylläpito-oikeudet"
  },
  "No supervised evaluations": {
    en: "No supervised evaluations",
    fi: "Ei valvottuja arviointiryhmiä"
  },
  "Add users to the group": {
    "en": "Add users to the group",
    "fi": "Lisää käyttäjiä vastaajaryhmään"
  },
  "Type a name of username to search users": {
    "en": "Type a name of username to search users",
    "fi": "Kirjoita nimi tai käyttäjätunnus hakeaksesi käyttäjiä"
  },
  "No results found": {
    "en": "No results found",
    "fi": "Ei tuloksia"
  },
  "%value% users added to the group": {
    "en": "%value% users added to the group",
    "fi": "%value% käyttäjää lisätty vastaajaryhmään",
    "enSingle": "User added to the group",
    "fiSingle": "Käyttäjä lisätty vastaajaryhmään",
  },
}