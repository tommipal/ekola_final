const answer          = require('./trans-answer');
const changepassword  = require('./trans-change-password');
const common          = require('./trans-common');
const evaledit        = require('./trans-eval-edit');
const layout          = require('./trans-layout');
const login           = require('./trans-login');
const neweval         = require('./trans-new-eval');
const newsession      = require('./trans-new-session');
const newsurvey       = require('./trans-new-survey');
const groupedit       = require('./trans-group-edit');
const profile         = require('./trans-profile');
const resetpassword   = require('./trans-reset-password');
const results         = require('./trans-results');
const signup          = require('./trans-signup');
const surveyedit      = require('./trans-survey-edit');
const surveylist      = require('./trans-survey-list');
const errors          = require('./trans-errors');

const files = [answer, changepassword, common, evaledit, layout, login, neweval, newsession, newsurvey, groupedit, profile, resetpassword, results, signup, surveyedit, surveylist, errors];
function compareArrays(arr1, arr2) {
  let dupes = [];
  arr1.forEach((item) => {
    if(arr2.indexOf(item) !== -1) dupes.push(item);
  });
  return dupes;
}
function loopThrough(array) {
  let i = 0;
  const maxItem = array.length;
  let dupes = [];
  while(array[i]) {
    const currentKeys = Object.keys(array[i]);
    for(let ii = 0; ii < array.length; ii++) {
      if(ii !== i) {
        const compareKeys = Object.keys(array[ii]);
        const c = compareArrays(currentKeys, compareKeys);
        if(!!c.length) dupes.push(c);
      }
    }
    i++;
  }
  return dupes;
}
// Check if there are duplicate keys in multiple files
// console.log(loopThrough(files));

const translations = Object.assign({}, answer, changepassword, common, evaledit, layout, login, neweval, newsession, newsurvey, groupedit, profile, resetpassword, results, signup, surveyedit, surveylist, errors);

module.exports = translations;