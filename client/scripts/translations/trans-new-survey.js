module.exports = {
  // EJS
  "Template": {
    "en": "Template",
    "fi": "Kyselypohja"
  },
  "No template": {
    "en": "No Template",
    "fi": "Ei kyselypohjaa"
  },
  "Template description": {
    "en": "Template description",
    "fi": "Kyselypohjan kuvaus"
  },
  "New survey from scratch": {
    "en": "New survey from scratch",
    "fi": "Luo uusi kysely ilman olemassa olevaa kyselypohjaa"
  },
  "Next": {
    "en": "Next",
    "fi": "Seuraava"
  },
}