module.exports = {
  "Edit evaluation": {
    "en": "Edit evaluation",
    "fi": "Muokkaa arviointiryhmää"
  },
  // EJS
  "Add groups": {
    "en": "Add groups",
    "fi": "Lisää ryhmiä"
  },
  "Void sessions": {
    "en": "Void sessions",
    "fi": "Sulje käyttäjän vastausistunnot"
  },
  "Current users": {
    "en": "Current users",
    "fi": "Nykyiset jäsenet"
  },
  "Actions": {
    "en": "Actions",
    "fi": "Toiminnot"
  },
  "User info": {
    "en": "User info",
    "fi": "Tietoja käyttäjästä"
  },
  "Close": {
    "en": "Close",
    "fi": "Sulje"
  },
  "Add": {
    "en": "Add",
    "fi": "Lisää"
  },
  // JS
  "Users removed": {
    "en": "Users removed",
    "fi": "Käyttäjät poistettu"
  },
  "Groups added": {
    "en": "Groups added",
    "fi": "Vastaajaryhmä lisätty"
  },
  "Group removed": {
    "en": "Group removed",
    "fi": "Vastaajaryhmä poistettu"
  },
  "Sessions successfully voided": {
    "en": "Sessions successfully voided",
    "fi": "Istunnot suljettu"
  },
  "Are you sure you want to remove this group and all of its users from this evaluation?": {
    "en": "Are you sure you want to remove this group and all of its users from this evaluation?",
    "fi": "Oletko varma, että haluat poistaa tämän ryhmän ja kaikki ryhmän käyttäjät tästä arviointiryhmästä?"
  },
  "Are you sure you want to remove %value% users from this evaluation?": {
    "en": "Are you sure you want to remove %value% users from this evaluation?",
    "fi": "Oletko varma, että haluat poistaa %value% käyttäjää tästä arviointiarviointiryhmästä",
    "enSingle": "Are you sure you want to remove %value% user from this evaluation",
    "fiSingle": "Oletko varma, että haluat poistaa käyttäjän tästä arviointiarviointiryhmästä",
  },
  "Are you sure you want to void the answering sessions of %value% users?": {
    "en": "Are you sure you want to void the answering sessions of %value% users?",
    "fi": "Oletko varma, että haluat sulkea %value% käyttäjän vastausistunnot",
    "enSingle": "Are you sure you want to void the answering sessions of this user?",
    "fiSingle": "Oletko varma, että haluat sulkea käyttäjän vastausistunnot"
  },
  "Are you sure you want to give %value% users admin permissions to this evaluation?": {
    "en": "Are you sure you want to give %value% users admin permissions to this evaluation?",
    "fi": "Oletko varma, että haluat antaa %value% käyttäjälle ylläpito-oikeudet tähän arviointiryhmään?",
    "enSingle": "Are you sure you want to give this user admin permissions to this evaluation?",
    "fiSingle": "Oletko varma, että haluat antaa tälle käyttäjälle ylläpito-oikeudet tähän arviointiryhmään?",
  },
}