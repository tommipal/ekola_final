// Translations that appear on multiple pages
module.exports = {
  "Something went wrong. Try again later.": {
    "en": "Something went wrong. Try again later.",
    "fi": "Jokin meni pieleen. Yritä hetken kuluttua uudelleen."
  },
  "Fetch failed! Try again later.": {
    "en": "Fetch failed! Try again later.",
    "fi": "Lataus epäonnistui! Yritä hetken kuluttua uudelleen."
  },
  "No data found. Try again later.": {
    "en": "No data found. Try again later.",
    "fi": "Dataa ei löytynyt. Yritä hetken kuluttua uudelleen."
  },
  "Remove": {
    "en": "Remove",
    "fi": "Poista"
  },
  "Saved": {
    "en": "Saved!",
    "fi": "Tallennettu!"
  },
  "Are you sure you want to close the following survey: '%value%'?": {
    "en": "Are you sure you want to close the following survey: '%value%'?",
    "fi": "Oletko varma, että haluat sulkea seuraavan kyselyn: '%value%'?"
  },
  "Are you sure you want to delete survey '%value%'?": {
    "en": "Are you sure you want to delete survey '%value%'?",
    "fi": "Oletko varma, että haluat poistaa kyselyn '%value%'?"
  },
  "I changed my mind": {
    "en": "I changed my mind",
    "fi": "Muutin mieleni"
  },
  "I'm sure": {
    "en": "I'm sure",
    "fi": "Olen varma"
  },
  "Accept": {
    "en": "Accept",
    "fi": "Hyväksy"
  },
  "‹ Previous": {
    "en": "‹ Previous",
    "fi": "‹ Edellinen"
  },
  "Next ›": {
    "en": "Next ›",
    "fi": "Seuraava ›"
  },
  "Select all": {
    "en": "Select all",
    "fi": "Valitse kaikki"
  },
  "Unselect all": {
    "en": "Unselect all",
    "fi": "Poista valinnat"
  },
  "Select": {
    "en": "Select",
    "fi": "Valitse"
  },
  "Unselect": {
    "en": "Unselect",
    "fi": "Poista valinta"
  },
  "Show user information": {
    "en": "Show user information",
    "fi": "Näytä käyttäjätiedot"
  },
  "Description": {
    "en": "Description",
    "fi": "Kuvaus"
  },
  "Choose...": {
    "en": "Choose...",
    "fi": "Valitse..."
  },
  "Cancel": {
    "en": "Cancel",
    "fi": "Peruuta"
  },

  // User info
  "User information": {
    "en": "User information",
    "fi": "Käyttäjän tiedot"
  },
  "Username": {
    "en": "Username",
    "fi": "Käyttäjätunnus"
  },
  "Email": {
    "en": "Email",
    "fi": "Sähköposti"
  },
  "Password": {
    "en": "Password",
    "fi": "Salasana"
  },
  "Evaluations": {
    "en": "Evaluations",
    "fi": "Arviointiryhmät"
  },
  "Joined": {
    "en": "Joined",
    "fi": "Liittynyt"
  },
  "Group": {
    "en": "Group",
    "fi": "Vastaajaryhmä"
  },
  "No groups": {
    "en": "No groups",
    "fi": "Ei vastaajaryhmiä"
  },
  "All groups": {
    "en": "All groups",
    "fi": "Kaikki vastaajaryhmät"
  },
  "Active group": {
    "en": "Active group",
    "fi": "Aktiivinen vastaajaryhmä"
  },
  "Supervised evaluations": {
    "en": "Supervised evaluations",
    "fi": "Valvottavat arviointiryhmät"
  },
  "Role": {
    "en": "Role",
    "fi": "Rooli"
  },
  "Group name": {
    "en": "Group name",
    "fi": "Ryhmän nimi"
  },
  "Full name": {
    en: "Full name",
    fi: "Koko nimi"
  },
  "Given name": {
    en: "First name",
    fi: "Etunimi"
  },
  "Family name": {
    en: "Last name",
    fi: "Sukunimi"
  },
  "No evaluations": {
    "en": "No evaluations",
    "fi": "Ei arviointiryhmiä"
  },
  "Member": {
    en: "Member",
    fi: "Jäsen"
  },
  "Admin": {
    en: "Admin",
    fi: "Ylläpitäjä"
  },
  "Loading...": {
    "en": "Loading...",
    "fi": "Ladataan..."
  },
  "Open": {
    "en": "Open",
    "fi": "Avoinna"
  },
  "Not open": {
    "en": "Not open",
    "fi": "Ei avoinna"
  },
  "Your active group is not participating in this survey": {
    "en": "Your active group is not participating in this survey",
    "fi": "Sinun aktiivinen vastaajaryhmäsi ei ole osana tätä kyselyä"
  },
}