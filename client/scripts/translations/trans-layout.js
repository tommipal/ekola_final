module.exports = {
  // EJS
  "Log in": {
    "en": "Log in",
    "fi": "Kirjaudu sisään"
  },
  "Log out": {
    "en": "Log out",
    "fi": "Kirjaudu ulos"
  },
  "Profile": {
    "en": "Profile",
    "fi": "Profiili"
  },
  "Surveys": {
    "en": "Surveys",
    "fi": "Kyselyt"
  },
  "New survey": {
    "en": "New survey",
    "fi": "Luo kysely"
  },
  "Active surveys": {
    "en": "Active surveys",
    "fi": "Aktiiviset kyselyt"
  },
  "Closed surveys": {
    "en": "Closed surveys",
    "fi": "Suljetut kyselyt"
  },
  "New evaluation": {
    "en": "New evaluation",
    "fi": "Luo arviointiryhmä"
  },
  "Manage group": {
    "en": "Manage group",
    "fi": "Hallinnoi vastaajaryhmää"
  },
  "Internet Explorer is not supported": {
    "en": "Internet Explorer is not supported",
    "fi": "Internet Explorer -selainta ei tueta"
  },
  "Menu": {
    "en": "Menu",
    "fi": "Valikko"
  },
  "Change language": {
    "en": "Suomeksi",
    "fi": "In english"
  },
}