import React, { Component } from 'react';
import {SurveyInfo}         from '../SurveyInfo';
import ReactDOM             from 'react-dom';
import { Utils }            from 'utils';
import moment               from 'moment';
import {Translator}         from 'translator';
const translator = new Translator(Utils.getLocale());

/**
 * Single survey table row
 * Renders differently based on the filter (active/closed) and users permissions
 */
export class SurveyTableRow extends Component {
  constructor(props) {
    super(props);
    this.startDate = moment(props.survey.startDate).utc(true).local().format('YYYY-MM-DD HH:mm');
    this.endDate = moment(props.survey.endDate).utc(true).local().format('YYYY-MM-DD HH:mm');

  }
  // Render closed items if filter is 'closed'
  renderClosedItem() {
    return (
    <tr data-survey={this.props.survey.id}>
      <td>
      {this.props.survey.canEdit &&
        <button className="button secondary" title={translator.trans('Reopen survey')} data-unlock-survey onClick={this.props.clickSurvey}><i className="fi-unlock"></i></button>
      }
      </td>
      <td data-survey-title>{this.props.survey.title}</td>
      <td>{this.startDate}</td>
      <td data-survey-enddate>{this.endDate}</td>
      <td>{this.props.survey.evaluation.name}</td>
      <td>
        <div className="button-group float-right">
        {this.props.survey.canEdit &&
          <a href={`/survey/${this.props.survey.id}/edit`} className="button" title={translator.trans('Edit survey')}><i className="fi-pencil"></i></a>
        }
        {(this.props.survey.canViewResults || this.props.survey.canView) &&
          <a href={`/answer/results/${this.props.survey.id}`} className="button success" title={translator.trans('View results')}><i className="fi-graph-bar"></i></a>
        }
        {this.props.survey.canEdit &&
          <button className="button alert" title={translator.trans('Delete survey')} data-delete-survey onClick={this.props.clickSurvey}><i className="fi-trash"></i></button>
        }
          <button className="button" title={translator.trans('Survey details')} data-user-info onClick={this.props.clickSurvey}><i className="fi-clipboard-notes"></i></button>
        </div>
      </td>
    </tr>
    );
  }

  // Render open items if filter is 'active'
  renderOpenItem() {
    return (<tr data-survey={this.props.survey.id}>
      <td>
      {this.props.survey.canEdit &&
        <button className="button secondary" data-lock-survey title={translator.trans('Close survey')} onClick={this.props.clickSurvey}><i className="fi-lock"></i></button>
      }
      </td>
      <td data-survey-title>{this.props.survey.title}</td>
      <td>{this.startDate}</td>
      <td data-survey-enddate>{this.endDate}</td>
      <td>{this.props.survey.evaluation.name}</td>
      <td>
        <div className="button-group float-right">
          {(!this.props.survey.ongoingSession && this.props.survey.canAnswer) &&
            <a href={`/survey/${this.props.survey.id}/answer`} className="button warning" title={translator.trans('Answer survey')}><i className="fi-pencil"></i></a>
          }
          {(!this.props.survey.ongoingSession && !this.props.survey.canAnswer) &&
            <button className="button warning" title={translator.trans('Your active group is not participating in this survey')} disabled><i className="fi-pencil"></i></button>
          }

          {this.props.survey.ongoingSession && <a href={`/survey/${this.props.survey.id}/answer`} className="button warning" title={translator.trans('Edit answers')}><i className="fi-page-edit"></i></a>}
          <button className="button" title={translator.trans('Survey details')} data-user-info onClick={this.props.clickSurvey}><i className="fi-clipboard-notes"></i></button>
        </div>
      </td>
    </tr>);
  }
  render() {
    const item = this.props.filter === 'active' ? this.renderOpenItem() : this.renderClosedItem();
    return item;
  }
}