import React, { Component }   from 'react';
import { SurveyTableRow }     from './SurveyTableRow';
import { SurveyInfo }         from '../SurveyInfo';
import Loading                from 'Loading';
import { Utils }              from 'utils';
import Flatpickr              from 'react-flatpickr';
import Modal                  from 'react-modal';
import Pagination             from 'react-paginate';
import moment                 from 'moment';
import {Translator}           from 'translator';
import 'flatpickr/dist/themes/airbnb.css';

const translator = new Translator(Utils.getLocale());
Modal.setAppElement('#react-root');

/**
 * Container for the active / closed survey table
 */
export class SurveyTable extends Component {
  constructor() {
    super();
    this.state = {
      surveys:        [],
      selectedId:     '',
      modalType:      '',

      page:           1,
      pageCount:      1,
      pageLimit:      10,

      showModal:      false,
      fetchedResults: false,
      fetchFailed:    false,
    }
    this.flatOptions = {
      weekNumbers:true,
      time_24hr: true,
      inline:true,
      minuteIncrement: 1,
      locale: {
        firstDayOfWeek: 1
      }
    }
  }

  // Fetch paginated surveys from the backend
  fetchData(page) {
    fetch(`/survey/listJSON/${this.props.filter}?page=${page}&pageLimit=${this.state.pageLimit}`, {credentials:'same-origin'})
    .then((response) => {
      if(!response.ok) throw Error('Network request failed');
      return response.json();
    }).then((response) => this.setState({surveys: response.surveys, pageCount: response.pageCount, page, fetchedResults: true}),
    (error)=>this.setState({fetchFailed: true}));
  }

  // Fetch data for the survey list after component has mounted
  componentDidMount() {this.fetchData(this.state.page);}

  // Handle flatpickr changes
  flatpickrChange(dateObjects, dateStr, instance) {
    let {name, dataset, value} = instance.input;
    const date = moment(dateObjects[0]);
    instance.selectedDateObj = date.toDate();
    //set the value of the input field to include timezone offset
    dataset['datetime'] = date.format('YYYY-MM-DD HH:mm');
    value = dateStr;
  }

  // Returns the modal content based on the type of modal that is wanted
  setUpModal() {
    const surveyTitle = this.state.surveys.find((survey)=>survey.id === this.state.selectedId).title;
    switch(this.state.modalType) {
      case 'info':
        return <SurveyInfo surveyId={this.state.selectedId}/>;
      break;
      case 'open':
        return (<div>
          <label>{translator.trans('End time')}
            <Flatpickr data-enable-time
              options={this.flatOptions}
              name="endDate"
              data-datetime={moment().format('YYYY-MM-DD HH:mm')}
              value={moment().format('YYYY-MM-DD HH:mm')}
              onChange={this.flatpickrChange.bind(this)}
            />
          </label>
          <button style={{marginTop: '10px'}} className="button" onClick={this.openSurvey.bind(this)}>{translator.trans('Open survey')}</button>
        </div>);
      break;
      case 'close':
        return (<div className="grid-x grid-padding-x align-center text-center">
          <h4>{translator.trans(`Are you sure you want to close the following survey: '%value%'?`, surveyTitle)}</h4>
          <div className="button-group">
            <button className="button" onClick={this.closeSurvey.bind(this)}>{translator.trans(`I'm sure`)}</button>
            <button className="alert button">{translator.trans('I changed my mind')}</button>
          </div>
        </div>);
      break;
      case 'delete':
        return (<div className="grid-x grid-padding-x align-center text-center">
          <h4>{translator.trans(`Are you sure you want to delete survey '%value%'?`, surveyTitle)}</h4>
          <div className="button-group">
            <button className="button" onClick={this.deleteSurvey.bind(this)}>{translator.trans(`I'm sure`)}</button>
            <button className="alert button">{translator.trans('I changed my mind')}</button>
          </div>
        </div>);
      break;
      default:
        console.error('Incorrect modal type. Must be info, open, close or delete.');
    }
  }

  // Send deletion request to backend
  deleteSurvey({target}) {
    fetch(`/survey/${this.state.selectedId}/delete`, {
      method:'POST',
      body: JSON.stringify({confirm: true}),
      headers: {'Content-Type': 'application/json', 'csrf-token': Utils.getCsrfToken()},
      credentials: 'same-origin',
    }).then((response)=>{
      if(!response.ok) throw Error('Network request failed');
      return response.json();
    }).then((data)=>{
      const surveys = this.state.surveys.filter((survey)=>survey.id !== this.state.selectedId);
      this.setState({surveys, showModal:false});
      Utils.showMessage('Survey deleted', 'success');
    }).catch((error)=>{
      Utils.showMessage('Error deleting survey. Try again later.', 'error');
    });
  }

  // Send survey opening request to backend
  openSurvey({target}) {
    const endDate = target.parentNode.querySelector('input').dataset.datetime;
    // When reopening survey set startDate to current time
    const startDate = moment().format('YYYY-MM-DD HH:mm');
    fetch(`/survey/${this.state.selectedId}/setdates`, {
      method:'POST',
      body: JSON.stringify({endDate, startDate}),
      headers: {'Content-Type': 'application/json', 'csrf-token': Utils.getCsrfToken()},
      credentials: 'same-origin',
    }).then((response) => {
      if(!response.ok) throw Error('Network request failed');
      return response.json();
    }).then((data) => {
      const surveys = this.state.surveys.filter((survey)=>survey.id !== this.state.selectedId);
      Utils.showMessage('Survey reopened', 'success');
      this.setState({surveys, showModal:false});
    }).catch((error) => {
      Utils.showMessage('Error opening survey. Try again later.', 'error');
    });
  }

  // Send survey closing request to backend
  closeSurvey({target}) {
    const datetime = moment().format('YYYY-MM-DD HH:mm');
    fetch(`/survey/${this.state.selectedId}/setdates`, {
      method:'POST',
      body: JSON.stringify({endDate: datetime}),
      headers: {'Content-Type': 'application/json', 'csrf-token': Utils.getCsrfToken()},
      credentials: 'same-origin',
    }).then((response)=>{
      if(!response.ok) throw Error('Network request failed');
      return response.json();
    }).then((data)=>{
      const surveys = this.state.surveys.filter((survey)=>survey.id !== this.state.selectedId);
      this.setState({surveys, showModal:false});
      Utils.showMessage('Survey closed', 'success');
    }).catch((error)=>{
      Utils.showMessage('Error closing survey. Try again later.', 'error');
    });
  }

  // Render the table header row based on the filter (active / closed)
  renderTableHeader() {
      return (
        <thead>
          <tr>
            <th></th>
            <th>{translator.trans('Survey')}</th>
            <th>{translator.trans('Opened')}</th>
            {this.props.filter === 'active' && <th>{translator.trans('Closes')}</th>}
            {this.props.filter === 'closed' && <th>{translator.trans('Closed')}</th>}
            <th>{translator.trans('Evaluation')}</th>
            <th></th>
          </tr>
        </thead>
      );
  }

  // Click events from tables rows are handeled here
  clickSurvey({target}) {
    const selectedId = Number(Utils.traverseUp(target, '[data-survey]').dataset['survey']);
    if(target.dataset.lockSurvey) this.setState({showModal:true, modalType:'close', selectedId});
    else if(target.dataset.unlockSurvey) this.setState({showModal:true, modalType:'open', selectedId});
    else if(target.dataset.deleteSurvey) this.setState({showModal:true, modalType:'delete', selectedId});
    else if(target.dataset.userInfo) this.setState({showModal:true, modalType:'info', selectedId});
  }

  handleCloseModal() {this.setState({showModal:false});}

  handlePageChange({selected}) {
    this.setState({surveys: [], fetchedResults: false, fetchFailed: false});
    this.fetchData(selected + 1);
  }

  render() {
    if(this.state.fetchFailed) return <h4>{translator.trans('Fetch failed! Try again later.')}</h4>;
    if(!this.state.fetchedResults) return <Loading />;
    const modalContent = this.state.showModal ? this.setUpModal() : '';
    let rows;
    if(!this.state.surveys.length) {
      rows = <tr><td></td><td colSpan={5}><b>{translator.trans(`No ${this.props.filter} surveys`)}</b></td></tr>
    }
    else {
      rows = this.state.surveys.map((survey, index)=>{
        return (
        <SurveyTableRow key={`survey-${index}`} index={index}
          survey        = {survey}
          filter        = {this.props.filter}
          clickSurvey   = {this.clickSurvey.bind(this)}
        />);
      })
    }
    return (
      <div>
        <section className="table-container">
          <table id="surveys-table" className="hover display">
            {this.renderTableHeader()}
            <tbody>
              {rows}
            </tbody>
          </table>
          {this.props.children}
        </section>
        <Pagination
          pageCount           = {this.state.pageCount}
          pageRangeDisplayed  = {5}
          marginPagesDisplayed= {1}
          breakLabel          = {<a>...</a>}
          previousLabel       = {translator.trans('‹ Previous')}
          nextLabel           = {translator.trans('Next ›')}
          containerClassName  = {'paginate'}
          disabledClassName   = {'disabled'}
          forcePage           = {this.state.page-1}
          onPageChange        = {this.handlePageChange.bind(this)}
          />
        <Modal
          isOpen={this.state.showModal}
          onRequestClose={this.handleCloseModal.bind(this)}
          className={'react-reveal'}
          overlayClassName={'react-reveal-overlay'}
          >
          <button type="button" className="close-button" aria-label="Close reveal" onClick={this.handleCloseModal.bind(this)}>
            <span aria-hidden="true">×</span>
          </button>
          {modalContent}
        </Modal>
      </div>
    );
  }
}