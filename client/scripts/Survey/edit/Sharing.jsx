import React, { Component }   from 'react';
import {Utils}                from 'utils';
import {CopyToClipboard}      from 'react-copy-to-clipboard';
import {ReactSelectize, SimpleSelect, MultiSelect}      from 'react-selectize';
import {Translator}           from 'translator';

import 'react-selectize/dist/index.min.css';
const translator = new Translator(Utils.getLocale());

// Valid email regex
const REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';

// Container for the survey sharing links
export class Sharing extends Component {
  constructor(){
    super();
    this.state = {
      copied: -1,
      emails: {},
    };
    this.regex = new RegExp('^' + REGEX_EMAIL + '$', 'i');

  }

  // Copy link to clipboard
  copyLink({target}) {
    Utils.showMessage(translator.trans('Link copied!'))
    if(target) this.setState({copied: Number(target.dataset['userGroupId'])});
  }

  // Opens the email confirmation modal
  sendEmail({target}) {
    const userGroupId = Number(target.dataset['userGroupId']);
    if(this.state.emails[userGroupId] && !!this.state.emails[userGroupId].length) {
      const data = {
        userGroupId,
        emails: this.state.emails[userGroupId],
      }
      this.props.showEmailModal(data);
    }
  }

  render() {
    const shares = this.props.links.map((link, index) => {
      const group = this.props.userGroups.find((o) => o.id === Number(link.userGroup));
      if(!group) return;
      return(<div className="share-row" key={`linkRow-${index}`}>
        <label htmlFor={`link-${group.id}`}>{group.name}
          <div className="input-group">
            <div className="input-group-button">
              <button type="button" className="button alert" title={translator.trans('Remove link')} data-user-group-id={group.id} onClick={this.props.removeLink}><i className="fi-x"></i></button>
            </div>
            <input type="text" readOnly
              id={`link-${group.id}`}
              className="input-group-field link-field"
              name="link"
              value={`http://${this.props.host}/survey/answer/${link.key}`}
              onClick={({target}) => {target.setSelectionRange(0, target.value.length)}}
              />
            <div className="input-group-button">
            <CopyToClipboard text={`http://${this.props.host}/survey/answer/${link.key}`}>
              <button type="button" className={'button' + (this.state.copied === group.id ? ' success': '')}
                data-user-group-id={group.id}
                title={translator.trans('Copy to clipboard')}
                onClick={this.copyLink.bind(this)}>
                <i className="fi-page-copy"></i>
              </button>
            </CopyToClipboard>
            </div>
          </div>
        </label>
        <div className="input-group">
          <MultiSelect
            placeholder     = {translator.trans('Emails')}
            transitionEnter = {true}
            delimiters      = {[188]}

            onValuesChange  = {(values) => {
              const emails = Object.assign({}, this.state.emails);
              values = values.map((val)=>val.value);
              emails[group.id] = values;
              this.setState({emails})
            }}

            restoreOnBackspace = {(item)=>item.label}

            valuesFromPaste = {(options, values, pastedText) => {
              return pastedText.split(',')
              .filter((text)=>{
                  const labels = values.map((item)=>item.label)
                  return labels.indexOf(text) === -1;
              })
              .map((text)=>{return {label: text.trim(), value: text.trim()};});
            }}

            createFromSearch = {(options, values, search) => {
              const labels = values.map((value) => value.label)
              if (search.trim().length == 0 || labels.indexOf(search.trim()) !== -1 || !this.regex.test(search.trim()))
                  return null;
              return {label: search.trim(), value: search.trim()};
            }}

            renderNoResultsFound = {(values, search) => {
              const self = this;
              return <div className="no-results-found">
                  {function(){
                      if (search.trim().length === 0 || !self.regex.test(search.trim())) return translator.trans('Enter a valid email address');
                      else if (values.map((item)=>{ return item.label; }).indexOf(search.trim()) !== -1) return translator.trans('Email already exists');
                  }()}
              </div>
            }}
          />
          <div className="input-group-button">
            <button type="button" className="button secondary" data-user-group-id={group.id} title={translator.trans('Send email')} onClick={this.sendEmail.bind(this)}><i className="fi-mail"></i></button>
          </div>
        </div>
      </div>);
    });
    const links = this.props.links.slice();
    const userGroups = this.props.userGroups.map((item, index) => {
      // Only render links that are not found in links
      if(!links.find((link) => Number(link.userGroup) === item.id)) {
        return <option key={index} value={item.id}>{item.name}</option>;
      }
    });
    return (
        <div className="sharing cell small-12 large-6">
          <label htmlFor="share-user-group">{translator.trans('Create a link for a group')}</label>
          <div className="input-group">
            <select id="share-user-group" className="input-group-field" >
              <option value="">{translator.trans('Choose...')}</option>
              {userGroups}
            </select>
            <div className="input-group-button">
              <button type="button" className="button" title={translator.trans('Create a link for this group')} onClick={this.props.addLink}><i className="fi-plus"></i></button>
            </div>
          </div>
          {shares}
        </div>
    );
  }
}