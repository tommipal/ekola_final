import moment from 'moment';

function getSameValues(array, field = null) {
  const same = {};
  switch(field){
    case null:
      for (const [i, item] of array.entries()) {
        if(!same[item] && same[item] !== '') {
          same[item] = [i];
        } else {
          same[item].push(i);
        }
      }
      break;
    default:
      for (const [i, item] of array.entries()) {
        if(!same[item[field]] && same[item[field]] !== '') {
          same[item[field]] = [i];
        } else {
          same[item[field]].push(i);
        }
      };
      break;
    }
    return same;
}

// Validation checks for duplicate values and for empty values
export function validate(errors, arr, id, key, field = null) {
 const same = getSameValues(arr, field);

  for (const i of errors.keys()) {
    switch(field) {
      case null:
        if(same[key].length > 1 && key !== '') {
          errors[id] = `Duplicate field`;
        } else if (key === '') {
            errors[id] = 'Empty field'
        } else {
          errors[id] = '';
        }
      break;
      default:
        if(same[key].length > 1 && key !== '') {
          errors[id][field] = `Duplicate ${field}`;
        } else if (key === '') {
            errors[id][field] = `Empty ${field}`
        } else {
          errors[id][field] = '';
        }
      break;
    }

  }
  return errors;
}

export function getErrors(data) {
  let errors = {
    survey: {title: '', userInfo: [], noQuestions: '', invalidDates:''},
    questions: [],
  };

  // Get errors for surveys basic fields
  if(data.survey.title === '') {
    errors.survey.title = 'Empty title';
  }
  if(moment(data.survey.startDate).unix() > moment(data.survey.endDate).unix()) {
    errors.survey.invalidDates = 'Survey must start before it ends!';
  }

  for(const [index, title] of data.survey.userInfo.entries()) {
    errors.survey.userInfo.push('');
    validate(errors.survey.userInfo, data.survey.userInfo, index, title);
  }

  if(data.questions.length === 0) {
    errors.survey.noQuestions = 'Survey needs at least 1 question!';
  }

  // Get errors for questions
  for(const [questionId, question] of data.questions.entries()) {
    // Add new question to errors
    errors.questions.push({title:'', criteria:[], noCriteria: ''});

    // Validate questions title field
    validate(errors.questions, data.questions, questionId, question.title, 'title');

    if(question.criteria.length === 0) {
      errors.questions[questionId].noCriteria = 'Question needs at least 1 criteria!';
    }

    for(const [criteriaId, criteria] of question.criteria.entries()) {
      // Add new criteria to errors
      errors.questions[questionId].criteria.push({title: '', options: {}, invalidOptions: ''});

      // Validate questions criterias title field
      validate(errors.questions[questionId].criteria, question.criteria, criteriaId, criteria.title, 'title');

      if(criteria.type === 'select') {
        errors.questions[questionId].criteria[criteriaId].options = {choices:[]};

        if(criteria.options.choices.length < 2) {
          errors.questions[questionId].criteria[criteriaId].invalidOptions = 'A select type criteria needs at least 2 options!';
        }

        for(const [choiceId, choice] of criteria.options.choices.entries()) {
          errors.questions[questionId].criteria[criteriaId].options.choices.push({value: '', description: ''});
          // Validate criterias choices value fields
          validate(errors.questions[questionId].criteria[criteriaId].options.choices, criteria.options.choices, choiceId, choice.value, 'value');
          // Validate criterias choices description fields
          validate(errors.questions[questionId].criteria[criteriaId].options.choices, criteria.options.choices, choiceId, choice.description, 'description');
        }
      } else if(criteria.type === 'scale') {
        if(criteria.options.max <= criteria.options.min) errors.questions[questionId].criteria[criteriaId].invalidOptions = 'Max value must be greater than min!'
      }
    }
  }
  return errors;
}

export function checkErrors({survey, questions}) {

  // survey
  if(survey.title) {return true;}
  if(survey.noQuestions) {return true;}
  if(survey.invalidDates) {return true;}
  for(const info of survey.userInfo) {
    if(info) {return true;}
  }

  // questions
  if(questions.length < 1) { return true; }
  for(const question of questions) {
    if(question.title) {return true;}
    if(question.noCriteria) {return true;}

    //criteria
    for(const crit of question.criteria) {
      if(crit.title) {return true;}
      if(crit.invalidOptions) {return true;}
      if(crit.options.choices) {
        for(const choice of crit.options.choices) {
          if(choice.value) {return true;}
          if(choice.description) {return true;}
        }
      }
    }
  }
  return false;
}