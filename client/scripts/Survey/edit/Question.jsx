import React, { Component } from 'react';
import {SortableHandle}     from 'react-sortable-hoc';
import Criteria             from './Criteria';
import {validate}           from './validation';
import {Utils}              from 'utils'
import {Translator}         from 'translator';
import $ from 'jquery';

const translator = new Translator(Utils.getLocale());

// Handle for reordering questions
const Handle = SortableHandle((props)=>{
  return <div className={props.className}><b>:::</b></div>;
})

// Container for a single question and its criteria
export class Question extends Component {
  addCriteria() {
    const criteria = this.props.criteria.concat({
      title: 'New Criteria', description: '', type: 'scale', options: {min:0, max:5}
      // title: '', description: '', type: 'select', options: {choices:[]}
    });
    this.props.criteriaChange(this.props.questionId, criteria);
  }

  deleteCriteria({target}) {
    const parent = Utils.traverseUp(target, '[data-criteria]');
    const targetId = Number(parent.dataset['criteria']);
    const criteria = this.props.criteria.filter((value, index) => {
      return index !== targetId;
    });
    this.props.criteriaChange(this.props.questionId, criteria);
  }

  // Handle basic information changes to the criteria
  handleChange({target}) {
    const parent = Utils.traverseUp(target, '[data-criteria]');
    const targetId = parent.dataset['criteria'];
    const {name, value} = target;
    let criteria = this.props.criteria;
    criteria[targetId][name] = value;
    switch(name) {
      case 'type':
        if (value === 'scale') {
          criteria[targetId].options = {min:0, max:10}
        } else if (value === 'select') {
          criteria[targetId].options = {choices:[]}
        }
        break;
      default:
        break;
    }
    this.props.criteriaChange(this.props.questionId, criteria);
  }

  // Handle min / max value changes of scale type criteria
  handleScaleChange(criteriaId, {target}) {
    const name = target.name; // min or max
    const value = Number(target.value);
    let criteria = this.props.criteria;
    criteria[criteriaId].options[name] = value;
    this.props.criteriaChange(this.props.questionId, criteria);
  }

  // Handle value / description changes of select type criteria
  handleSelectChange(criteriaId, {target}) {
    const parent = Utils.traverseUp(target, '[data-row]');
    const choiceIndex = parent.dataset['row'];
    const {name, value} = target; // value or description
    let criteria = this.props.criteria;
    criteria[criteriaId].options.choices[choiceIndex][name] = value;

    this.props.criteriaChange(this.props.questionId, criteria);
  }

  // Handle adding and deleting select rows
  handleSelectRows(criteriaId, choices) {
    let criteria = this.props.criteria;
    criteria[criteriaId].options.choices = choices;
    this.props.criteriaChange(this.props.questionId, criteria);
  }

  // Collapse / Open the question container when clicked
  collapseQuestion({target}) {
    // Make sure this doesn't trigger when clicking the remove question button
    if(target.type !== 'button') {
      const parent = Utils.traverseUp(target, '.question');
      if($(parent).hasClass('collapsed')) {
        $(parent).removeClass('collapsed');
        $(parent).addClass('is-active');
        $(parent).find('.question-content').slideDown();
        this.props.changeActive(this.props.questionId);
      } else {
        $(parent).addClass('collapsed');
        $(parent).removeClass('is-active');
        $(parent).find('.question-content').slideUp(()=>{
          this.props.changeActive(-1);
        });
      }
    }
  }

  render() {
    const {title, description} = this.props.question;
    const critElements = this.props.criteria.map((crit, index)=>{
      return(
        <Criteria key={`criteria-${index}`} index={index}
          criteriaId      = {index}
          criteria        = {this.props.criteria[index]}
          errors          = {this.props.errors.criteria[index]}
          deleteCriteria  = {this.deleteCriteria.bind(this)}
          inputChange     = {this.handleChange.bind(this)}
          scaleChange     = {this.handleScaleChange.bind(this)}
          selectChange    = {this.handleSelectChange.bind(this)}
          selectRows      = {this.handleSelectRows.bind(this)}
        />
      );
    });

    let classList = ['question'];
    if(this.props.isActive) {
      classList.push('is-active');
    } else {
      classList.push('collapsed');
    }
    if(this.props.errors.title || this.props.errors.noCriteria) {
      classList.push('has-error');
    }

    return(
      <div className={classList.join(' ')} data-question={this.props.questionId}>
        <div onClick={this.collapseQuestion.bind(this)} className="question-title text-center">
          <button type="button" className={`button alert remove-question-btn ${this.props.showControls ? '' : 'hide'}`} onClick={this.props.deleteQuestion}><i className="fi-x"></i></button>
          <span>{title ? title : ''}</span>
          <Handle className={`question-handle ${this.props.showControls ? '' : 'hide'}`}/>
        </div>
        <div className="question-content" style={this.props.isActive ? {} : {display:'none'}}>
          <div className="grid-x grid-padding-x">
            <div className="cell small-12 medium-6">
              <label>{translator.trans('Title')}
                <input
                  type="text"
                  name="title"
                  onChange={this.props.fieldChange}
                  value={title ? title : ''}
                  />
                <p className="error">{translator.trans(this.props.errors.title)}</p>
                </label>
            </div>
            <div className="cell small-12 medium-6">
              <label>{translator.trans('Description')}
              <textarea rows="10" name="description" onChange={this.props.fieldChange} value={description ? description : ''}></textarea>
              </label>
            </div>
          </div>
          <div className="grid-x grid-padding-x">
            <div className="cell small-12">
              <h4>{translator.trans('Criteria')}</h4>
              <button type="button" className="button" title={translator.trans('Add a new criteria')} onClick={this.addCriteria.bind(this)}>
                <i className="fi-plus"></i> {translator.trans('Add')}
              </button>
              <p className="error">{translator.trans(this.props.errors.noCriteria)}</p>
              <div className="question-criteria">
                {critElements}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
