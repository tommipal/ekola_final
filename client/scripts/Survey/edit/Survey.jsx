import React, { Component }   from 'react';
import ReactDOM               from 'react-dom';
import {QuestionBank}         from '../questionbank/QuestionBank';
import {Question}             from './Question';
import {Sharing}              from './Sharing';
import {Utils}                from 'utils';
import Loading                from 'Loading';
import {AdditionalInfo}       from './AdditionalInfo';
import Modal                  from 'react-modal';
import Flatpickr              from 'react-flatpickr';
import locales                from "flatpickr/dist/l10n/index";
import moment                 from 'moment';
import {getErrors, checkErrors}                         from './validation';
import {SortableContainer, SortableElement, arrayMove}  from 'react-sortable-hoc';
import {Translator}           from 'translator';
import 'flatpickr/dist/themes/airbnb.css';

const translator = new Translator(Utils.getLocale());
Modal.setAppElement('#react-root');

// Create a list item that can be dragged in the MyList
const SortableItem = SortableElement((data) => {
  return (
    <Question
      questionId      = {data.questionId}
      question        = {data.question}
      isActive        = {data.activeQuestion}
      changeActive    = {data.changeActive}
      showControls    = {data.showControls}
      criteria        = {data.question.criteria}
      errors          = {data.errors}
      deleteQuestion  = {data.deleteQuestion}
      fieldChange     = {data.fieldChange}
      criteriaChange  = {data.criteriaChange}
    />
  );
});

// Create a sortable list where user can drag list items around
const MyList = SortableContainer(({questions}) => {
  return (
    <div id="question-list">
      {questions}
    </div>
  );
});

/**
 * Container for the survey form and question bank
 */
export class SurveyContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      survey: {
        title:        '',
        evaluation:   '',
        description:  '',
        startDate:    '',
        endDate:      '',
        openResults:  0,
        userInfo:     [],
      },
      host:               '',
      links:              [],
      questionsToDelete:  [],
      questions:          [],
      errors:             [],
      userGroups:         [],
      emailData:          {},
      activeQuestion:     0,
      sharingEnabled:     false,
      showControls:       false,
      hasErrors:          true,

      fetchedResults: false,
      fetchFailed:    false,
      saving:         false,
      savingTemplate: false,
      showModal:      false,
      modalType:      'bank',
    }
  }

  // Get surveys data from the database
  fetchData() {
    return new Promise((resolve, reject) => {
      fetch(`/survey/${this.props.surveyId}/editJSON`, { credentials: 'same-origin' })
      .then((response) => {
        if(!response.ok) throw Error('Network request failed');
        return response.json();
      })
      .then((data) => resolve(data), (error) => reject(error));
    });
  }

  saveSurvey(surveyData) {
    return new Promise((resolve, reject) => {
      fetch(`/survey/${this.props.surveyId}/save`, {
        method: 'POST',
        body: JSON.stringify(surveyData),
        headers: {'Content-Type':'application/json', 'csrf-token': Utils.getCsrfToken()},
        credentials: 'same-origin'
      })
      .then((response) => {
        if(!response.ok) throw Error('Network request failed');
        return response.json();
      })
      .then((data) => resolve(data), (error) => reject(error))
    });
  }

  // Fire fetchData() when page has loaded
  componentDidMount() {
    this.fetchData()
    .then((data) => {
      const state = data;

      state.survey.startDate  = moment(data.survey.startDate).utc(true).local().format('YYYY-MM-DD HH:mm');
      state.survey.endDate    = moment(data.survey.endDate).utc(true).local().format('YYYY-MM-DD HH:mm');
      state.survey.evaluation = data.survey.evaluation.id;
      state.survey.userInfo   = JSON.parse(data.survey.userInfo);
      state.questions         = data.questions.map((q) => {
                                  q.criteria = JSON.parse(q.criteria);
                                  return q;
                                });
      state.evaluations       = data.evaluations;
      state.errors            = getErrors(state);
      state.hasErrors         = checkErrors(state.errors);
      state.sharingEnabled    = !!state.links.length
      state.userGroups        = [{ id: 0, name: translator.trans('All groups*') }].concat(state.userGroups);
      state.fetchedResults    = true;
      this.setState(state)
    },(e) => this.setState({fetchFailed:true}));
  }

  // Add a new question to the form
  addQuestion() {
    const questions = this.state.questions.concat({
      title: 'New Question',
      description: '',
      criteria: []
    });

    const errors = getErrors({survey:this.state.survey, questions: questions});
    const hasErrors = checkErrors(errors);
    const activeQuestion = questions.length - 1;
    this.setState({questions, activeQuestion, errors, hasErrors});
  }

  // Add an additional info row to the form
  addInfoField() {
    const survey = Object.assign({}, this.state.survey);
    survey.userInfo.push('');

    const errors = getErrors({survey: survey, questions: this.state.questions});
    const hasErrors = checkErrors(errors);
    this.setState({survey, errors, hasErrors});
  }

  // Handle evaluation change. Updates list of groups available for link sharing
  changeEvaluation(evaluationId) {
    if(!evaluationId) return;
    this.setState({links: [], sharingEnabled: false});
    fetch(`/userGroup/find?query=userGroupsInEvaluation&evaluationId=${evaluationId}&surveyId=${this.props.surveyId}`, {
      credentials: 'same-origin',
    }).then((response) => {
      if(!response.ok) throw Error('Network request failed');
      return response.json();
    }).then((userGroups) => {
      userGroups = [{ id: 0, name: translator.trans('All groups*') }].concat(userGroups);
      this.setState({userGroups});
    }, (error) => {Utils.showMessage(translator.trans('Could not change evaluation'), 'error');});
  }

  // Creates and adds a new link for survey sharing
  addGroupLink({target}) {
    const userGroup = Number(Utils.byId('share-user-group').value);
    if(userGroup || userGroup === 0) {
      let links = this.state.links.slice();
      fetch(`/survey/${this.props.surveyId}/createLink`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'csrf-token': Utils.getCsrfToken()},
        body: JSON.stringify({userGroup}),
        credentials: 'same-origin',
      }).then((response) => {
        if(!response.ok) throw Error('Network request failed');
        return response.json();
      }).then((data) => {
        links.push(data);
        this.setState({links});
      }, (error) => {Utils.showMessage(translator.trans('Could not add link'), 'error');});
    }
  }

  // Removes the survey sharing link from the page and database
  removeGroupLink({target}) {
    const userGroup = Number(target.dataset['userGroupId']);
    let links = this.state.links.slice();
    fetch(`/survey/${this.props.surveyId}/deleteLink`, {
      method: 'POST',
      headers: {'Content-Type': 'application/json', 'csrf-token': Utils.getCsrfToken()},
      body: JSON.stringify({userGroup, survey: this.props.surveyId}),
      credentials: 'same-origin',
    }).then((response) => {
      if(!response.ok) throw Error('Network request failed');
      return response.json();
    }).then((data) => {
      links = links.filter((link)=>link.userGroup !== userGroup);
      this.setState({links});
    }, (error) => {Utils.showMessage(translator.trans('Could not remove link'), 'error');});
  }

  // Appends selected questions from the question bank to the active survey
  importQuestions(imported) {
    const questions = this.state.questions.concat(imported);
    const errors = getErrors({survey: this.state.survey, questions: questions});
    const hasErrors = checkErrors(errors);
    this.setState({questions, errors, hasErrors, showModal: false});
  }

  // Removes the question from the active survey form
  deleteQuestion({target}) {
    const parent = Utils.traverseUp(target, '[data-question]');
    const targetId = Number(parent.dataset['question']);
    let activeQuestion = this.state.activeQuestion;
    if(targetId < this.state.activeQuestion && this.state.activeQuestion !== -1) activeQuestion = activeQuestion-1
    const questions = this.state.questions.filter((question, index) => {
      return index !== targetId;
    });

    const errors = getErrors({survey:this.state.survey, questions: questions});
    const hasErrors = checkErrors(errors);
    this.setState({questions, errors, hasErrors, activeQuestion});
  }

  // Removes an additional info field from the page
  deleteInfoField(infoIndex) {
    const survey = Object.assign({}, this.state.survey);
    survey.userInfo = survey.userInfo.filter((value, index) => index !== infoIndex);

    const errors = getErrors({survey: survey, questions: this.state.questions});
    const hasErrors = checkErrors(errors);
    this.setState({survey, errors, hasErrors});
  }

  // Send emails to addresses in the email field
  sendEmail() {
    this.setState({showModal:false});
    fetch(`/survey/${this.props.surveyId}/sendInvite`, {
      method: 'POST',
      body: JSON.stringify(this.state.emailData),
      headers: {'Content-Type': 'application/json', 'csrf-token': Utils.getCsrfToken()},
      credentials: 'same-origin'
    })
    .then((response) => {
      if(!response.ok) throw Error('Network request failed');
      return response.json();
    })
    .then((data) => {
      Utils.showMessage(translator.trans('Survey link sent to %value% emails!', this.state.emailData.emails.length), 'success');
    }, (error) => {
      Utils.showMessage(translator.trans('Error occured when sending emails'), 'error');
    })
  }

  handleCloseModal() {this.setState({ showModal: false });}

  // Update basic information about the survey everytime they change
  handleChange({target}) {
    const {name, value} = target;
    const survey = Object.assign({}, this.state.survey);
    if(target.type === 'checkbox') {
      survey[name] = !!target.checked;
    } else if(target.type === 'select-one') {
      if(!value) delete survey[name];
      else survey[name] = value;
      this.changeEvaluation(value);
    } else {
      survey[name] = value;
    }

    const errors = getErrors({survey: survey, questions: this.state.questions});
    const hasErrors = checkErrors(errors);
    this.setState({survey, errors, hasErrors});
  }

  // Handle changes to the end date and start dates
  flatpickrChange(dateObjects, dateStr, instance) {
    const survey = Object.assign({}, this.state.survey);
    let {name, dataset, value} = instance.input;
    const date = moment(dateObjects[0]);
    instance.selectedDateObj = date.toDate();
    dataset['datetime'] = date.format('YYYY-MM-DD HH:mm');
    value = dateStr;
    survey[name] = dataset['datetime'];

    const errors = getErrors({survey: survey, questions: this.state.questions});
    const hasErrors = checkErrors(errors);
    this.setState({survey, errors, hasErrors});
  }

  // Handle generate link checkbox change
  handleGenerateChange({target}) {
    const {checked, disabled} = target;
    if(disabled) return;
    const sharingEnabled = !!checked;
    this.setState({sharingEnabled});
  }

  // Update question title and description everytime they change
  handleQuestionChange({target}) {
    const parent = Utils.traverseUp(target, '[data-question]');
    const questionId = parent.dataset['question'];
    const {name, value} = target;
    let questions = this.state.questions.slice();
    questions[questionId][name] = value;

    const errors = getErrors({survey: this.state.survey, questions: questions});
    const hasErrors = checkErrors(errors);
    this.setState({questions, errors, hasErrors});
  }

  // Update new criteria object to state everytime a field changes in any criteria
  handleCriteriaChange(questionId, criteria) {
    let questions = this.state.questions.slice();
    questions[questionId].criteria = criteria;

    const errors = getErrors({survey: this.state.survey, questions: questions});
    const hasErrors = checkErrors(errors);
    this.setState({questions, errors, hasErrors});
  }

  // Handle changes to the additional info input fields
  infoFieldChange({target}) {
    const index = Utils.traverseUp(target, '[data-info-index]').dataset['infoIndex'];
    const survey = Object.assign({}, this.state.survey);
    survey.userInfo[index] = target.value;

    const errors = getErrors({survey: survey, questions: this.state.questions});
    const hasErrors = checkErrors(errors);
    this.setState({survey, errors, hasErrors})
  }

  changeActiveQuestion(index) {
    this.setState({activeQuestion: index});
  }

  // Show / Hide delete question button and reorder handle
  toggleControls() {
    const showControls = !this.state.showControls
    this.setState({showControls});
  }

  showQuestionBank() {this.setState({modalType:'bank', showModal: true});}

  showEmailModal(data) {
    this.setState({
      emailData: data,
      modalType:'email',
      showModal: true,
    });
  }

  setUpModal() {
    switch(this.state.modalType) {
      case 'bank':
        return <QuestionBank importQuestions={this.importQuestions.bind(this)}/>
      break;
      case 'email':
        return <div className="grid-x grid-padding-x align-center text-center">
          <h4>{translator.trans('Are you sure you want to send an email to:')} {this.state.emailData.emails.join(', ')}</h4>
          <div className="button-group">
            <button type="button" className="button" onClick={this.sendEmail.bind(this)}>{translator.trans(`I'm sure`)}</button>
            <button type="button" className="button alert" onClick={this.handleCloseModal.bind(this)}>{translator.trans('I changed my mind')}</button>
          </div>
        </div>
      break;
      default:
        console.warn('Invalid modal type')
      break;
    }
  }

  // Combine question data and survay data to a single object that can be sent to the database
  getSaveData() {
    const questions = this.state.questions.map((question, index) => {
      delete question.survey;
      question.position = index;
      return question;
    });
    const survey = Object.assign({}, this.state.survey);
    survey.evaluation = Number(survey.evaluation);
    survey.openResults = survey.openResults ? 1 : 0;
    survey.sharingEnabled = this.state.sharingEnabled ? true : false;
    const data = {survey, questions};
    return data;
  }

  handleSubmit(e) {
    this.setState({saving: true});
    const data = this.getSaveData();

    if(this.state.hasErrors) {
      Utils.showMessage(translator.trans('Survey contains errors'), 'error');
    } else {
      this.saveSurvey(data)
      .then((res) => {
        Utils.showMessage(translator.trans('Survey saved!'), 'success');
        if(!this.state.sharingEnabled) this.setState({links: []}); //clear links because those have been deleted from the database.
        this.setState({saving: false});
      }).catch((error)=>{
        Utils.showMessage(translator.trans('Saving failed. Try again later.'), 'error');
        this.setState({saving: false});
      });
    }
  }

  saveAsTemplate(e) {
    this.setState({savingTemplate: true});
    const data = this.getSaveData();

    if(this.state.hasErrors) {
      Utils.showMessage(translator.trans('Survey contains errors'), 'error');
    } else {
      this.saveSurvey(data).then((res) => {
        fetch(`/survey/${this.props.surveyId}/saveAsTemplate`, {
          method: 'POST',
          headers: {'csrf-token': Utils.getCsrfToken()},
          credentials: 'same-origin'
        }).then((response) => {
          this.setState({savingTemplate: false});
          if (response.status === 400) Utils.showMessage(translator.trans('Saving failed. Template with this title already exists.'), 'error');
          else if (!response.status === 200 || !response.ok) throw Error('Network request failed');
          else return response.json()
        }).then((res) => {
            if(res) Utils.showMessage(translator.trans('Saved as a template!'), 'success');
        }).catch((err) => console.error(err));
      }).catch((error)=> console.error(error));
    }
  }

  onSortEnd({oldIndex, newIndex}) {
    let activeQuestion = this.state.activeQuestion

    // Make sure the active question stays the same while moving elements
    if(oldIndex === activeQuestion) activeQuestion = newIndex;
    else if(newIndex === activeQuestion && newIndex < oldIndex) activeQuestion = newIndex + 1;
    else if(newIndex === activeQuestion && newIndex > oldIndex) activeQuestion = newIndex - 1;
    else if(oldIndex < activeQuestion && newIndex > activeQuestion) activeQuestion--;
    else if( oldIndex > activeQuestion && newIndex < activeQuestion ) activeQuestion++;

    const errors = Object.assign({}, this.state.errors);
    errors.questions = arrayMove(errors.questions, oldIndex, newIndex);

    this.setState({
      questions: arrayMove(this.state.questions, oldIndex, newIndex),
      errors: errors,
      activeQuestion: activeQuestion,
    });
  }

  render() {
    if(this.state.fetchFailed) return <h4>{translator.trans('Fetch failed! Try again later.')}</h4>;
    if(!this.state.fetchedResults) return <Loading />;

    // Create question SortableItem elements for each question in state
    const questions = this.state.questions.map((question, index) => {
      return (<SortableItem key={`question-${index}`} index={index}
        questionId      = {index}
        question        = {this.state.questions[index]}
        changeActive    = {this.changeActiveQuestion.bind(this)}
        activeQuestion  = {this.state.activeQuestion === index}
        showControls    = {this.state.showControls}
        errors          = {this.state.errors.questions[index]}
        deleteQuestion  = {this.deleteQuestion.bind(this)}
        fieldChange     = {this.handleQuestionChange.bind(this)}
        criteriaChange  = {this.handleCriteriaChange.bind(this)}
        />
      );
    });

    const evaluations = this.state.evaluations.map((evaluation, index) =>
      (<option value={evaluation.id} key={'evaluation-'+index}>{evaluation.name}</option>)
    );

    locales[Utils.getLocale()].firstDayOfWeek = 1;
    locales['fi'].weekAbbreviation = 'Vk';
    const flatOptions = {
      weekNumbers:true,
      time_24hr: true,
      minuteIncrement: 1,
      locale: locales[Utils.getLocale()]
    }
    return (
      <div>
        <form id="survey-edit"
          className="inputs"
          onSubmit={this.handleSubmit.bind(this)}>
          <h2>{translator.trans('Edit survey')}</h2>
          <div className="grid-x grid-padding-x">
            {/* Title */}
            <div className="cell small-12 medium-9">
              <label>{translator.trans('Title')}
                <input type="text" name="title" value={this.state.survey.title} onChange={this.handleChange.bind(this)}/>
                <p className="error">{translator.trans(this.state.errors.survey.title)}</p>
              </label>
            </div>
            {/* Select evaluation */}
            <div className="cell small-12 medium-3">
              <label>{translator.trans('Select evaluation')}
                <select name="evaluation" value={this.state.survey.evaluation} onChange={this.handleChange.bind(this)}>
                  <option value="" hidden>{translator.trans('Choose...')}</option>
                  {evaluations}
                </select>
              </label>
            </div>
          </div>
          <div className="grid-x grid-padding-x">
            {/* Description */}
            <div className="cell small-12 medium-6">
              <label>{translator.trans('Description')}
                <textarea name="description" rows='4' value={this.state.survey.description} onChange={this.handleChange.bind(this)}></textarea>
              </label>
            </div>
            {/* Start date & End date */}
            <div className="cell small-12 medium-6">
              <div className="grid-x grid-padding-x">
                <div className="cell small-6">
                  <label>{translator.trans('Start time')}
                    <Flatpickr data-enable-time
                      options={flatOptions}
                      name="startDate"
                      data-datetime={this.state.survey.startDate}
                      value={this.state.survey.startDate}
                      onChange={this.flatpickrChange.bind(this)}
                    />
                  </label>
                </div>
                <div className="cell small-6">
                  <label>{translator.trans('End time')}
                    <Flatpickr data-enable-time
                      options={flatOptions}
                      name="endDate"
                      data-datetime={this.state.survey.endDate}
                      value={this.state.survey.endDate}
                      onChange={this.flatpickrChange.bind(this)}
                    />
                  </label>
                </div>
                <div className="cell small-12">
                  <p className="error">{translator.trans(this.state.errors.survey.invalidDates)}</p>
                </div>
              </div>
            </div>
          </div>
          <div className="grid-x grid-padding-x">
            {/* Survey visible */}
            <div className="cell small-12 medium-6">
              <label style={{display: 'inline'}}>{translator.trans('Results open to answerers')}{' '}
                <input type="checkbox" name="openResults" checked={this.state.survey.openResults} value={this.state.survey.openResults} onChange={this.handleChange.bind(this)}/>
              </label>
            </div>
            {/* Enable sharing */}
            <div className="cell small-12 medium-6">
              <label style={{display: 'inline'}}>{translator.trans('Enable sharing')}{' '}
                <input type="checkbox" name="sharingEnabled" checked={this.state.sharingEnabled} value={this.state.sharingEnabled} disabled={!this.state.survey.evaluation} onChange={this.handleGenerateChange.bind(this)}/>
              </label>
            </div>
          </div>
          <div className="grid-x grid-padding-x">
            <AdditionalInfo
              fields          = {this.state.survey.userInfo}
              errors          = {this.state.errors.survey.userInfo}
              deleteInfoField = {this.deleteInfoField.bind(this)}
              addInfoField    = {this.addInfoField.bind(this)}
              infoFieldChange = {this.infoFieldChange.bind(this)}
            />

            {this.state.sharingEnabled &&
            <Sharing
              host            = {this.state.host}
              links           = {this.state.links}
              addLink         = {this.addGroupLink.bind(this)}
              removeLink      = {this.removeGroupLink.bind(this)}
              userGroups      = {this.state.userGroups}
              showEmailModal  = {this.showEmailModal.bind(this)}
            />}
          </div>
          <h3>{translator.trans('Questions')}</h3>
          <div className="button-group">
            <button type="button" className="button secondary" title={translator.trans('Import questions from the question bank')} onClick={this.showQuestionBank.bind(this)}><i className="fi-download"></i> {translator.trans('Import')}</button>
            <button type="button" className="button" title={translator.trans('New question')} onClick={this.addQuestion.bind(this)}><i className="fi-plus"></i> {translator.trans('New')}</button>
            <button type="button" className="button warning" disabled={!this.state.questions.length} title={translator.trans('Control questions')} onClick={this.toggleControls.bind(this)}><i className="fi-widget"></i> {translator.trans('Control')}</button>
          </div>
          <b className="error">{translator.trans(this.state.errors.survey.noQuestions)}</b>
          <MyList
            questions={questions}
            useDragHandle={true}
            lockAxis="y"
            onSortEnd={this.onSortEnd.bind(this)}
          />
          <button type="button" className="large success button" disabled={this.state.hasErrors || this.state.saving || this.state.savingTemplate} onClick={this.handleSubmit.bind(this)}>
            <i className="fi-save"></i> {translator.trans('Save')} {this.state.saving && <div id="spinner" className="spinner"></div>}
          </button>{' '}
          <button type="button" className="large button" disabled={this.state.hasErrors || this.state.saving || this.state.savingTemplate} onClick={this.saveAsTemplate.bind(this)}>
            {translator.trans('Save as template')} {this.state.savingTemplate && <div id="spinner" className="spinner light"></div>}
          </button>
        </form>
        <Modal
          isOpen={this.state.showModal}
          onRequestClose={this.handleCloseModal.bind(this)}
          className={'react-reveal'}
          overlayClassName={'react-reveal-overlay'}
          >
          <button type="button" className="close-button" aria-label="Close reveal" onClick={this.handleCloseModal.bind(this)}>
            <span aria-hidden="true">×</span>
          </button>
          {this.setUpModal()}
        </Modal>
      </div>
    );
  }
}
