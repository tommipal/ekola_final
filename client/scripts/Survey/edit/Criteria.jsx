import React, { Component } from 'react';
import {Utils}              from 'utils';
import {Translator}         from 'translator';
import $ from 'jquery';

const translator = new Translator(Utils.getLocale());

/**
 * Container for scale type criteria
 */
function ScaleCriteria(props) {
  return (
    <div className="grid-x grid-padding-x">
      <div className="cell small-12">
        <fieldset className="fieldset">
          <legend>{translator.trans('Scale options')}</legend>
          <div className="grid-x grid-padding-x">
            <div className="cell small-6">
              <label>Min
                <input type="number" name="min" max={props.options.max-1} onChange={props.scaleChange} value={props.options.min} />
              </label>
            </div>
            <div className="cell small-6">
              <label>Max
                <input type="number" name="max" min={props.options.min+1} onChange={props.scaleChange} value={props.options.max} />
              </label>
            </div>
          </div>
          <p className="error">{translator.trans(props.invalidOptions)}</p>
        </fieldset>
      </div>
    </div>
  );
}

// Single select type row
function SelectRow(props) {
  return(
    <div className="select-row grid-x" data-row={props.rowNum}>
      <div className="cell small-2">
        <label>{translator.trans('Value')}
          <input type="number" name="value" onChange={props.onChange} value={props.choice.value} />
          <p className="error">{translator.trans(props.errors.value)}</p>
        </label>
      </div>
      <label className="cell small-10">{translator.trans('Description')}
        <div className="input-group">
          <input type="text" name="description" onChange={props.onChange} value={props.choice.description}/>
          <button type="button" className="button alert" title={translator.trans('Remove choice')} onClick={props.remove}><i className="fi-x"></i></button>
        </div>
        <p className="error">{translator.trans(props.errors.description)}</p>
      </label>
    </div>
  );
}

/**
 * Container for the select type criteria
 */
class SelectCriteria extends Component {
  addRow() {
    const num = this.props.options.choices.length;
    const choices = this.props.options.choices.concat({value: num, description: ''});
    this.props.selectRows(this.props.id, choices);
  }

  deleteRow({target}) {
    const parent = Utils.traverseUp(target, '[data-row]');
    const rowIndex = Number(parent.dataset['row']);
    const choices = this.props.options.choices.filter((choice, i)=>{
      return i !== rowIndex;
    });
    this.props.selectRows(this.props.id, choices);
  }

  render() {
    const selectRows = this.props.options.choices.map((choice, i) => {
      return (
        <SelectRow key={`select-row-${i}`} index={i}
        rowNum    = {i}
        errors    = {this.props.errors.choices[i]}
        remove    = {this.deleteRow.bind(this)}
        onChange  = {this.props.selectChange}
        choice    = {choice}
        />
      );
    });

    return (
      <div className="inner-item">
        <button type="button" className="button" title={translator.trans('Add a choice')} onClick={this.addRow.bind(this)}><i className="fi-plus"></i> {translator.trans('Add')}</button>
        <p className="error">{translator.trans(this.props.invalidOptions)}</p>
        <div className="select-options">
          {selectRows}
        </div>
      </div>
    );
  }
}

export default class Criteria extends Component {
  // Handles changes in scale type inputs
  scaleChange(e) {this.props.scaleChange(this.props.criteriaId, e);}

  // Handles changes in select type inputs
  selectChange(e) {this.props.selectChange(this.props.criteriaId, e);}

  // Collapse / Open the criteria container when clicked
  collapseCriteria({target}) {
    if(target.type !== 'button') {
      const parent = Utils.traverseUp(target, '.criteria');
      if(!$(parent).hasClass('is-active')) {
        $(parent).addClass('is-active');
        $(parent).find('.criteria-content').slideDown('fast');
      } else {
        $(parent).removeClass('is-active');
        $(parent).find('.criteria-content').slideUp('fast');
      }
    }
  }

  render() {
    // TypeCriteria is either ScaleCriteria or SelectCriteria depending on the selected type
    const TypeCriteria = this.props.criteria.type === 'scale' ? ScaleCriteria : SelectCriteria;

    let classList = [];
    if(this.props.errors.title || this.props.errors.invalidOptions) classList.push('has-error');
    if($(`[data-criteria="${this.props.criteriaId}"`).hasClass('is-active')) classList.push('is-active');
    return (
      <div className={'criteria '+ classList.join(' ')} data-criteria={this.props.criteriaId}>
        <div className="criteria-title text-center" onClick={this.collapseCriteria.bind(this)}>
          <button type="button" className="button remove-criteria-btn" onClick={this.props.deleteCriteria}><i className="fi-x"></i></button>
          <span>{this.props.criteria.title}</span>
        </div>
        <div className="criteria-content" style={{display: 'none'}}>
          <div className="grid-x grid-padding-x">
            <div className="cell small-12 medium-8">
              <label>{translator.trans('Title')}
                <input type="text" name="title" onChange={this.props.inputChange} value={this.props.criteria.title} />
                <p className="error">{translator.trans(this.props.errors.title)}</p>
              </label>
            </div>
            <div className="cell small-12 medium-4">
            <label>{translator.trans('Select type')}
              <select onChange={this.props.inputChange} name="type" value={this.props.criteria.type}>
                <option value="scale">{translator.trans('Scale')}</option>
                <option value="select">{translator.trans('Multiple choice')}</option>
              </select>
            </label>
            </div>
          </div>
          <div className="grid-x grid-padding-x">
            <div className="cell small-12">
                <label>{translator.trans('Description')}
                  <input type="text" name="description" onChange={this.props.inputChange} value={this.props.criteria.description} />
                </label>
            </div>
          </div>
          <TypeCriteria
            id            = {this.props.criteriaId}
            options       = {this.props.criteria.options}
            errors        = {this.props.errors.options}
            invalidOptions= {this.props.errors.invalidOptions}
            scaleChange   = {this.scaleChange.bind(this)}
            selectChange  = {this.selectChange.bind(this)}
            selectRows    = {this.props.selectRows}
            />
        </div>
      </div>
    );
  }

}
