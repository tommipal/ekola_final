import React, { Component } from 'react';
import {Utils}              from 'utils';
import {Translator}         from 'translator';

const translator = new Translator(Utils.getLocale());

/**
 * Container for additional info inputs
 */
export class AdditionalInfo extends Component {

  deleteField({target}) {
    const parent = Utils.traverseUp(target, '[data-info-index]');
    const index = Number(parent.dataset['infoIndex']);
    this.props.deleteInfoField(index);
  }

  render() {
    const fields = this.props.fields.map((value, index)=>{
      return (
        <AdditionalInfoField key={`info-field-${index}`}
          index={index}
          value={value}
          onClick={this.deleteField.bind(this)}
          onChange={this.props.infoFieldChange}
          error={this.props.errors[index]}
        />);
    })
    return (
      <div id="additional-info-container" className="cell small-12 large-6">
        <h3>{translator.trans('Background questions')}</h3>
        <button type="button" className="small button" onClick={this.props.addInfoField}><i className="fi-plus"></i> {translator.trans('Add field')}</button>
        <div className="grid-x">
          <div className="cell small-12">
            {fields}
          </div>
        </div>
      </div>
    );
  }
}

/**
 * Single additional info row
 */
function AdditionalInfoField(props) {
  return (
    <div>
      <div className="input-group" data-info-index={props.index}>
        <input type="text" className="input-group-field" name="infoField" value={props.value} onChange={props.onChange} />
        <button type="button" className="alert button input-group-button" title={translator.trans('Remove field')} onClick={props.onClick}><i className="fi-x"></i></button>
      </div>
      <p className="error">{translator.trans(props.error)}</p>
    </div>
  );
}