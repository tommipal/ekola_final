import React, { Component } from 'react';

/**
 * Single question bank question item
 */
export class QuestionBankItem extends Component {
  handleClick() { this.props.handleClick(this.props.question); }
  render() {
    return (
      <li>
        <input type="checkbox" value={this.props.question.id} readOnly checked={this.props.isSelected} />
        <label title={this.props.question.description} onClick={this.handleClick.bind(this)}>
          {this.props.question.title} [{this.props.question.template.title}]
        </label>
      </li>
    );
  }
}