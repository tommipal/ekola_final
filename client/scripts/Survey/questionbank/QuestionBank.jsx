import React, { Component } from 'react';
import {QuestionBankItem}   from './QuestionBankItem';
import Pagination           from 'react-paginate';
import Loading              from 'Loading';
import {Translator}         from 'translator';
import {Utils}                from 'utils';
const translator = new Translator(Utils.getLocale());

/**
 * Container for the question bank
 */
export class QuestionBank extends Component {
  constructor() {
    super();
    this.state = {
      selected:       [],
      bankQuestions:  [],

      searchParams: '',
      page:         1,
      pageCount:    1,
      pageLimit:    10,

      fetchedResults: false,
      fetchFailed:    false,
    }
  }

  // Fetch questions from server with search parameters and page number
  fetchQuestions(page) {
    let query = `?search=${this.state.searchParams}&page=${page}&pageLimit=${this.state.pageLimit}`;

    fetch(`/question/bank${query}`, {
      credentials: 'same-origin'
    })
    .then((response) => {
      if(!response.ok) throw new Error('Network request failed');
      return response.json();
    })
    .then((response) =>{
      const bankQuestions = response.questions.map((question) => {
        question.criteria = JSON.parse(question.criteria);
        return question;
      });
      this.setState({bankQuestions, fetchedResults: true, pageCount: response.pageCount, page});
    }).catch((err)=>{
      this.setState({fetchFailed: true})
      console.error(err);
    });
  }

  // Fetch questions after component has mounted
  componentDidMount() {this.fetchQuestions(this.state.page);}

  // Send selected questions to the actual survey
  importQuestions() {
    const selected = this.state.selected;
    selected.forEach(question => delete question.id);
    this.props.importQuestions(selected);
  }

  // Container for the question bank items
  bankQuestions() {
    if(this.state.fetchFailed) return <h4>{translator.trans('Fetch failed! Try again later.')}</h4>;
    if(!this.state.fetchedResults) return <Loading />;

    return this.state.bankQuestions.map((question, index)=>{
      const isSelected = !!this.state.selected.filter((selectedQuestion)=> selectedQuestion.id === question.id).length;
      return (
        <QuestionBankItem key={`bank-item-${index}`}
          question    = {question}
          isSelected  = {isSelected}
          handleClick = {this.handleItemClick.bind(this)}
        />);
    });
  }

  // Handle 'select all' click
  selectAll() {
    let selected = this.state.selected.slice();
    // Push all questions that are not already selected to the selected array
    for (const question of this.state.bankQuestions) {
      if(selected.indexOf(question.id) === -1) selected.push(question);
    }
    this.setState({selected});
  }

  clearSelection() {this.setState({selected: []});}

  // Handle click event for each of the question bank questions. Toggles selected state of each question
  handleItemClick(question) {
    let selected = this.state.selected.slice();
    if(!!selected.filter((selectedQuestion)=> selectedQuestion.id === question.id).length) {
      selected = selected.filter((selectedQuestion)=> selectedQuestion.id !== question.id);
    } else {
      selected.push(question);
    }
    this.setState({selected});
  }
  // Handles search inputs changes
  handleChange({target}) {this.setState({searchParams: target.value});}

  // Handle search submit
  handleSubmit(e) {
    e.preventDefault();
    //when submitting search set page to 1
    this.fetchQuestions(1);
  }

  handlePageChange({selected}) {
    this.setState({bankQuestions: [], fetchedResults: false, fetchFailed: false});
    this.fetchQuestions(selected + 1);
  }

  render() {
    const bankQuestions = this.bankQuestions();
    return (
      <div id="question-bank" className="grid-x">
        <h4>{translator.trans('Select questions to import')}</h4>
        <div className="small-12 cell">
          <form id="search-form" onSubmit={this.handleSubmit.bind(this)}>
            <label>{translator.trans('Search questions')}
              <div className="input-group">
                <input className="input-group-field" type="text" name="searchParams" value={this.state.searchParams} onChange={this.handleChange.bind(this)}/>
                <div className="input-group-button">
                  <button type="submit" title={translator.trans('Search')} className="button">
                    <i className="fi-magnifying-glass"></i>
                  </button>
                </div>
              </div>
            </label>
          </form>
        </div>
        <div className="small-12 cell">
          <div className="button-group small">
            <button type="button" className="button" onClick={this.selectAll.bind(this)}>{translator.trans('Select all')}</button>
            <button type="button" className="button alert" onClick={this.clearSelection.bind(this)}>{translator.trans('Unselect all')}</button>
          </div>
        </div>
        <ul id="question-bank-questions" className="small-12 cell">
          {bankQuestions}
        </ul>
        <div className="small-12 cell">
        <Pagination
          pageCount           = {this.state.pageCount}
          pageRangeDisplayed  = {5}
          marginPagesDisplayed= {1}
          forcePage           = {this.state.page-1}
          breakLabel          = {<a>...</a>}
          previousLabel       = {translator.trans('‹ Previous')}
          nextLabel           = {translator.trans('Next ›')}
          containerClassName  = {'paginate'}
          disabledClassName   = {'disabled'}
          onPageChange        = {this.handlePageChange.bind(this)}
          />
        </div>

        <div className="small-12 cell">
          <p>{translator.trans('%value% Questions selected', this.state.selected.length)}</p>
       </div>
        <button type="button" className="button" onClick={this.importQuestions.bind(this)} disabled={!this.state.selected.length}>{translator.trans('Import selected')}</button>
      </div>
    );
  }
}
