import React, {Component} from 'react';
import Loading            from 'Loading';
import {Translator}       from 'translator';
import { Utils }            from 'utils';
const translator = new Translator(Utils.getLocale());

/**
 * Survey info modal
 */
export class SurveyInfo extends Component {
  constructor() {
    super();
    this.state = {
      surveyData:     {},
      fetchedResults: false,
      fetchFailed:    false,
    }
  }

  componentDidMount() {
    this.getInfo();
  }

  // Get surveys data fron the database
  getInfo() {
    fetch(`/survey/${this.props.surveyId}/info`, {credentials: 'same-origin'})
    .then((response) => {
      if (!response.ok) { throw Error("Network request failed");}
      return response.json();
    })
    .then((data) => this.setState({surveyData: data, fetchedResults: true}),
    (e) => this.setState({fetchFailed: true}));
  }

  render() {
    if(this.state.fetchFailed) return <p>{translator.trans('Fetch failed! Try again later.')}</p>;
    if(!this.state.fetchedResults) return <Loading/>;
    return (
      <section className="grid-x grid-padding-x" id="content">

        <div className="cell small-12">
          <h4>{this.state.surveyData.title}</h4>
        </div>
        <div className="cell small-12">
          <b>{translator.trans('Description')}</b><p>{this.state.surveyData.description}</p>
        </div>
        <div className="cell medium-6 small-12">
          <b>{translator.trans('Evaluation')}</b><p>{this.state.surveyData.evaluation.name} ({this.state.surveyData.evaluationUsers.length} members)</p>
        </div>
        <div className="cell medium-6 small-12">
          <b>{translator.trans('Participating groups')}</b>
          <p>{!this.state.surveyData.evaluationGroups.length ? translator.trans('No groups') : this.state.surveyData.evaluationGroups.map((group) => group.name).join(', ')}</p>
        </div>
        <div className="cell medium-6 small-12">
          <b>{translator.trans('Supervisors')}</b>
          <p>{!this.state.surveyData.supervisors.length ? translator.trans('No supervisors') : this.state.surveyData.supervisors.map((supervisor) => `${supervisor.givenName} ${supervisor.familyName}`).join(', ')}</p>
        </div>
        <div className="cell medium-6 small-12">
          <b>{translator.trans('Number of answers')}</b><p>{this.state.surveyData.sessions}</p>
        </div>
        <div className="cell medium-6 small-12">
          <b>{translator.trans('Number of questions')}</b><p>{this.state.surveyData.questions.length}</p>
        </div>
        <div className="cell medium-6 small-12">
          <b>{translator.trans('Results open to answerers')}</b><p>{!!this.state.surveyData.openResults ? translator.trans('Open') : translator.trans('Not open')}</p>
        </div>
      </section>
    );
  }
}
