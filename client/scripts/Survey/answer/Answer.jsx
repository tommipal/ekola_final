/*
 * THIS IS NOT USED CURRENTLY
import React, {Component} from 'react';

// npm install --save whatwg-fetch
import 'whatwg-fetch';

const survey = {
  id: 1,
  title: 'Survey title',
  description: 'Survey description',
  userInfo: ["Your name please?", "Would you like to have some kmeb with your children?"]
};
const questionsToRender = [
  {
    id: 1,
    title: 'Question title',
    description: 'Description for QuestionList',
    criteria: [
      {
        title: 'Criteria title',
        type: 'scale',
        options: {
          min: 0,
          max: 5,
          step: 1
        }
      }, {
        title: 'Criteria 2',
        type: 'select',
        options: {
          choices: [
            {
              value: 1,
              description: '1'
            }, {
              value: 2,
              description: '2'
            }
          ]
        }
      }
    ]
  }, {
    id: 2,
    title: 'Question title 2',
    description: 'Description for question!',
    criteria: [
      {
        title: 'Importance',
        type: 'scale',
        options: {
          min: 0,
          max: 5,
          step: 1
        }
      }, {
        title: 'Urgency',
        type: 'select',
        options: {
          choices: [
            {
              value: 1,
              description: 'not so urgent'
            }, {
              value: 2,
              description: 'very urgent!'
            }, {
              value: 3,
              description: 'pretty damn urgent!!!'
            }
          ]
        }
      }
    ]
  }
];
const existinAnswers = [
  {
    values: {
      scale: {
        value: 4
      }
    },
    question: 1,
    organization: 1,
    session: 1,
    survey: 1,
    criteria: {
      title: 'Criteria title',
      type: 'select',
      description: '123',
      options: {
        max: 5,
        min: 0,
        step: 1
      }
    },
    type: 'scale',
    criteriaTitle: 'Criteria title'
  }, {
    values: {
      select: {
        value: 1,
        description: 'not so urgent'
      }
    },
    question: 2,
    organization: 1,
    session: 1,
    survey: 1,
    criteria: {
      title: 'Urgency',
      type: 'select',
      options: {
        choices: [
          {
            value: 1,
            description: 'not so urgent'
          }, {
            value: 2,
            description: 'very urgent!'
          }, {
            value: 3,
            description: 'pretty damn urgent!!!'
          }
        ]
      }
    },
    type: 'select',
    criteriaTitle: 'Urgency'
  }
];

export class Answer extends React.Component {
  constructor(props) {
    super(props);
    const answers = [];
    const questions = questionsToRender.map((question) => {
      question.criteria.forEach((criteria) => answers.push({
          criteriaTitle: criteria.title,
          question: question.id,
          criteria: criteria,
          type: criteria.type,
          values: criteria.type === 'scale' ? {scale: { value: ''}} : { select: {}}
        }));

      return {id: question.id, title: question.title, description: question.description, criteria: question.criteria, evidence: ''}
    });
    const userInfo = survey.userInfo.map((question) => {
        return {question: question, value: ''}
    });

    if (true) {
      existinAnswers.forEach((existingAnswer) => {
        let foundAt = answers.findIndex((answer) => answer.question === existingAnswer.question && answer.criteriaTitle === existingAnswer.criteriaTitle); // ???
        if (foundAt !== -1)
          answers[foundAt] = existingAnswer;
          // ^ is this safe enough? If the question has been edited, something horrible
          // might happen!? -> check here at the criteria options match (no point in total
          // validation?) ..or should we already at the BE discard all the answers if the
          // modified question no longer supports the existing answers? or both?
        }
      );
    }
    this.state = {
      questions: questions,
      answers: answers,
      userInfo: userInfo
    };
  }

  handleInputChange(e) {
    const questionId = Number(e.target.dataset.question);
    const criteriaTitle = e.target.dataset.criteria;
    const value = Number(e.target.value);

    const answers = this.state.answers;
    let index = answers.findIndex(answer => answer.question === questionId && answer.criteriaTitle === criteriaTitle);
    let answer = answers[index];
    if (answer.type === 'scale')
      answer.values.scale.value = value;
    else
      answer.values.select = {
        value: value,
        description: e.target.dataset.description
      };
    answers[index] = answer;

    this.setState(answers);
  }

  handleEvidenceChange(e) {
    const questionId = Number(e.target.dataset.question);
    const value = e.target.value;
    const questions = this.state.questions;

    const questionIndex = questions.findIndex(question => question.id === questionId);
    questions[questionIndex].evidence = value;

    this.setState(questions);
  }

  handleUserInfoChange(e) {
    const value = e.target.value;
    const question = e.target.dataset.question;
    const userInfo = this.state.userInfo;

    const index = userInfo.findIndex(userInfo => userInfo.question === question);
    userInfo[index].value = value;

    this.setState(userInfo);
  }

  handleSaveAnswers() {
    const answers = this.state.answers.filter((answer) => answer.values[answer.type].value);
    const evidence = this.state.questions.map((question) => {
        return {question: question.id, evidence: question.evidence}
      }).filter((question) => question.evidence);
    const userInfo = this.state.userInfo.filter((answer) => answer.value);

    console.log('\nAnswers');
    console.log(answers);
    console.log('Evi');
    console.log(evidence);
    console.log('UserInfo');
    console.log(userInfo);
  }

  render() {
    const answers = this.state.answers.slice();
    const questions = this.state.questions.map((question, index) => {
      return(
        <Question
          key={question.id}
          index={index}
          question={question}
          handleInputChange={this.handleInputChange.bind(this)}
          handleEvidenceChange={this.handleEvidenceChange.bind(this)}
          answers={answers.filter((answer) => answer.question === question.id)}
        />);
      });
    const userInfo = this.state.userInfo.map((userInfo) => (
        <label key={userInfo.question}>
          {userInfo.question}
          <input
            type="text"
            data-question={userInfo.question}
            value={userInfo.value}
            onChange={this.handleUserInfoChange.bind(this)}></input>
          <br/>
        </label>
      ));

    return (
      <div id='container'>
        <h2>{survey.title}</h2>
        <p>{survey.description}</p>
        <hr/> {userInfo}
        <hr/> {questions}
        <button onClick={this.handleSaveAnswers.bind(this)}>Save</button>
      </div>
    );
  }
}

class Question extends React.Component {
  handleInputChange(e) {
    this.props.handleInputChange(e);
  }
  handleEvidenceChange(e) {
    this.props.handleEvidenceChange(e);
  }

  render() {
    let question = this.props.question;
    let criteria = this.props.question.criteria.map((criteria) => <Criteria
        criteria={criteria}
        question={this.props.question.id}
        answer={this.props.answers.find((answer) => answer.criteriaTitle === criteria.title)}
        key={criteria.title}
        handleInputChange={this.handleInputChange.bind(this)}/>);
    return (
      <div>
        <p>{`${this.props.index + 1}.`} {question.title}</p>
        {criteria}
        <textarea
          data-question={question.id}
          value={question.evidence}
          onChange={this.handleEvidenceChange.bind(this)}></textarea>
      </div>
    )
  }
}

// add separate classes for each criteria or keep this dynamic?
class Criteria extends React.Component {
  handleInputChange(e) {
    this.props.handleInputChange(e);
  }

  render() {
    const criteria = this.props.criteria;
    const answer = this.props.answer;
    let input;

    if (criteria.type === 'scale') {
      input = <input
        type="number"
        min={criteria.options.min}
        max={criteria.options.max}
        data-question={this.props.question}
        data-criteria={criteria.title}
        value={answer.values.scale.value}
        onChange={this.handleInputChange.bind(this)}/>
    } else {
      input = criteria.options.choices.map((choice) => {
          return(
            <label key={choice.value}>
              {choice.description}
              <input
                type="radio"
                data-question={this.props.question}
                data-criteria={criteria.title}
                value={choice.value}
                checked={answer.values.select.value === choice.value && answer.values.select.description === choice.description}
                data-description={choice.description}
                onChange={this.handleInputChange.bind(this)}/><br/>
            </label>)
          })
    }

    return (
      <div>
        <p>{this.props.criteria.title}</p>
        {input}
      </div>
    )
  }
}

*/