const mongodb = require('mongodb');
const ManagedModel = require('./ManagedModel');
const { Logger, Utils } = require('common');

const { MariaDatabase } = require('database');
const tableName = 'usergroups';
const joinEvalTable = 'join_evaluations_usergroups';

class UserGroup extends ManagedModel {
  constructor(data, admin){
    super(data, admin);
    this.deserialize(data);
  }

  get myUsers() {
    const query = new MariaDatabase.SQLQuery(User.DATASTORE);
    query.select(`SELECT DISTINCT ${User.DATASTORE}.* FROM ${User.DATASTORE}`);
    query.join('INNER JOIN', User.DATASTORE, 'id', Permission.DATASTORE, 'user')
      .join('INNER JOIN', Permission.DATASTORE, 'targetId', this.datastore, 'id')
      .where(`${Permission.DATASTORE}.targetModel = 'UserGroup'`)
      .where(`${Permission.DATASTORE}.targetId = ${this.id}`)
      .where(`${Permission.DATASTORE}.mask >= '${Permission.decode('view')}'`)
      .or().where(`${User.DATASTORE}.userGroup = ${this.id}`);

    return User.query(query).then(result => {
      return result[0][0].map(rr => new User(rr[User.DATASTORE]));
    });
  }

  /**
   * Return evaluations connected to this group via evaluation_userGroup join table
   * @returns {Promise<Array<Evaluation>>}
   */
  get myEvaluations(){
    const query = new MariaDatabase.SQLQuery(MariaDatabase.JOIN_EVALUATIONS_USERGROUPS);
    query
      .join('INNER JOIN', MariaDatabase.JOIN_EVALUATIONS_USERGROUPS, 'evaluation_id', Evaluation.DATASTORE, 'id')
      .where(`userGroup_id = ${this.id}`);
    return UserGroup.query(query)
      .then(results=>results[0][0].map(rr=>new Evaluation(rr[Evaluation.DATASTORE])));
  }

  /**
   * Delete permissions from users. Deletes the group if all the permissions are deleted.
   * @param {Number} group
   * @param {Array<Number>} users Users to remove
   */
  static async removeUsers(group, ...users) {
    group = group.id || group;
    users = Utils.flatten(users).map(user => user.id || user);
    if (!group || isNaN(group)) throw new ArgumentError(`Expected a UserGroup object or id. Got ${group}`);
    if (users instanceof Array === false || !users.every(user => !isNaN(user))) throw new ArgumentError(`Expected an array of ids. Got ${users}`);

    for (let user of users) {
      await Permission.delete({ targetId: group, targetModel: 'UserGroup', user: user });
      await User.update({ userGroup: null }, `id = ${user} AND userGroup = ${group}`);
    }
    const remainingPermissions = await new UserGroup(Number(group)).myPermissions;
    if (!remainingPermissions.length) {
      Logger.info(`Deleted last permission to group ${group}. Removing detached group!`);
      await UserGroup.delete({ id: group });
    }
  }

  /**
   * Modifies given objects by attaching users' role to it.
   * @param {Array<User>} users
   */
  async attachRole(...users) {
    users = Utils.flatten(users);
    if (!users.every(user => user.id) || !users.length) throw new Error('Expected an array of user objects!');

    const permissions = await this.myPermissions;
    for (const user of users) {
      const permission = permissions.find(permission => permission.user.id == user.id);
      if (!permission) throw new Error(`User ${user.id} has no permission to group ${this.id}`);
      else if (permission.mask === Permission.decode('view')) user.role = 'Member';
      else if (permission.mask > Permission.decode('view')) user.role = 'Admin';
    }
  }

  deserialize(data){
    super.deserialize(data);
    for(let [name, value] of Utils.iterateObject(data)){
      switch(name){
        case 'description':
        case 'name':
        this[name] = value;
      }
    }
  }

  serialize(id){
    const json = super.serialize(id);
    json.name = this.name;
    if(this.description) json.description = this.description;
    if(id && this.id) json.id = this.id;
    return json;
  }

  static get DB(){ return MariaDatabase.default; }
  static set DB(newdb){ throw new Error(`${Utils.getObjectClassName(this)} does not support switching databases`); }
  static get DATASTORE(){ return tableName; }
  static set DATASTORE(newDatastore){ throw new Error(`${Utils.getObjectClassName(this)} does not support switching datastore`); }
  //allow setting databases per instance, for use in transactions
  set db(newdb) { this._db = newdb; }
  get db() { return this._db && !this._db.dead ? this._db : UserGroup.DB; }
  get datastore(){ return UserGroup.DATASTORE; }
}

exports.model = UserGroup;