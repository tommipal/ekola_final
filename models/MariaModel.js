const Model = require('./Model');
const { MariaDatabase } = require('database');
const { Utils, Logger } = require('common');
const _ = require('lodash');

module.exports =
class MariaModel extends Model {
  constructor(data){
    super(data);
    if(data.id) this.id = data.id;
    if(Number.isInteger(data)) this.id = data;
  }

  static parseWhere(where, params){
    if(!where) return [];
    if(Array.isArray(where) && where.every(item=>Number.isInteger(item))) return [ 'id IN (?)', where ];
    if(Utils.legitObject(where)) return [ where ];
    if(typeof where === 'string') return [ where, ...params ];
    if(Number.isInteger(where)) return [ `id = ${where}` ];
    throw new TypeError('Where parameter was not an array, plain object, string or integer');
  }

  static async initialize(data){
    const existing = await this.find(data);
    if(existing) return existing;
    else return new this(data);
  }

  /**
   *
   * @param {Number|Object} where id, where clause or object
   * @param {String|Number} params
   */
  static async find(where, ...params){
    if(!where) throw new ArgumentError('Can not perform find with no parameters!');
    let data = await this.DB.select(this.DATASTORE, ...MariaModel.parseWhere(where, params));
    if(!data || data.length !== 1) return null;
    return new this(data[0]);
  }

  static async where(where, ...params){
    let data = await this.DB.select(this.DATASTORE, ...MariaModel.parseWhere(where, params));
    return data.map(dd=>new this(dd));
  }

  static async delete(where, ...params){
    let result = await this.DB.delete(this.DATASTORE, ...MariaModel.parseWhere(where, params));
    return result;
  }

  static async count(where, ...params){
    let result = await this.DB.count(this.DATASTORE, ...MariaModel.parseWhere(where, params));
    return result;
  }

  static async insert(...instances){
    instances = Utils.flatten(instances);
    if(!instances.every(item=>item instanceof this)) throw new Error(`Some of the items to be inserted were not instanceof ${Utils.getObjectClassName(this)}`);
    let ids = await this.DB.insert(this.DATASTORE, ...instances);
    for(let i = 0; i<instances.length; i++){
      //TODO: this assumes that Promise.all in MariaDatabase that executes all the inserts will actually put them in the same order as they are passed. Is this always the case?
      instances[i].id = ids[i];
    }
    return instances;
  }

  static async update(data, where, ...params){
    let result;
    if(Array.isArray(data)){
      if(!data.every(item=>Utils.legitObject(item) && item.id)){
        throw new Error('To update multiple records, every update record data must contain the id attribute equal to that of the database row to match against');
      }
      result = await this.DB.updateMultiple(this.DATASTORE, data, (query, item, index)=>query.where(`id = ${item.id}`));
    } else {
      result = await this.DB.update(this.DATASTORE, data, ...MariaModel.parseWhere(where, params));
    }
    return result;
  }

  static async query(sqlquery){
    if(!(sqlquery instanceof MariaDatabase.SQLQuery)) throw new Error('The raw query must be an instance of MariaDatabase.SQLQuery');
    return await this.DB.execute(sqlquery);
  }

  //If you want to run these in a transaction, you can simply allow an override of the database through a parameter
  //then pass the transaction database instance to the method when you need to.
  async save(){
    if(this.id){
      let result = await this.db.update(this.datastore, this.serialize(), `id = ${this.id}`);
    } else {
      //only get the first query result, we don't expect more
      let [id] = await this.db.insert(this.datastore, this.serialize());
      this.id = id;
    }
    return this;
  }

  async delete(){
    if(!this.id){
      Logger.warn('No id, calling delete() is redundant');
      return this;
    }
    let deleted = await this.db.delete(this.datastore, `id = ${this.id}`);
    if(deleted !== 1) Logger.error(`Something went wrong during delete - expected affected rows to be 1, but got ${deleted.affectedRows}`);
    return this;
  }

  async get(){
    let rows;
    //TODO: implementation of this method might vary quite severely.
    //This is just an example of how it *could* be done - either by id or by combination of all available attributes
    if(!this.id){
      const json = Utils.filter(this.serialize(), 'createdAt', 'updatedAt');
      rows = await this.db.select(this.datastore, json);
      if(rows.length > 1) {
        Logger.error(`Tried to loose match a record ${this}, but the database returned more than 1 result. Ignoring.`)
        return false;
      }
    } else {
      rows = await this.db.select(this.datastore, 'id = ?', this.id);
    }
    if(rows.length === 0) {
      Logger.error(`Did not find record ${this} in the database.`);
      return false;
    } else {
      this.deserialize(rows[0]);
    }
    return true;
  }

  get isNew(){
    return !this.id;
  }
}