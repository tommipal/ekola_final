const mongodb = require('mongodb');
const MariaModel = require('./MariaModel');
const { Logger, Utils } = require('common');
const bcrypt = require('bcryptjs');
const _ = require('lodash');

const { MariaDatabase } = require('database');
const tableName = 'users';

class User extends MariaModel {
  constructor(data) {
    super(data);
    this.deserialize(data);
  }

  get myPermissions() {
    return Permission.where('user = ?', this.id).then(permissions=>{
      permissions.forEach(p => p.user = this);
      this.permissions = permissions;
      return this.permissions;
    });
  }

  get myUserGroup() {
    if(!this.userGroup) return null;
    return UserGroup.find(this.userGroup);
  }

  get myGroups() {
    const query = new MariaDatabase.SQLQuery(UserGroup.DATASTORE);
    query.select(`SELECT * FROM ${UserGroup.DATASTORE}`);
    query.join('INNER JOIN', UserGroup.DATASTORE, 'id', Permission.DATASTORE, 'targetId');
    query.where(`${Permission.DATASTORE}.targetModel = 'UserGroup' AND ${Permission.DATASTORE}.mask >= ${Permission.decode('view')}`)
      .where(`${Permission.DATASTORE}.user = ${this.id}`);

    return UserGroup.query(query).then(records => records[0][0].map(rr => new UserGroup(rr[UserGroup.DATASTORE])));
  }

  get mySessions() {
    return SurveySession.where(`user = ${this.id}`);
  }

  /**
   * Return evaluations based on user's groups
   * @returns {Promise<Array<Evaluation>>}
   */
  get myEvaluations() {
    const ignoredEvaluations = new MariaDatabase.SQLQuery(Permission.DATASTORE);
    ignoredEvaluations.select(`SELECT ${Permission.DATASTORE}.targetId FROM ${Permission.DATASTORE}`);
    ignoredEvaluations.where(`${Permission.DATASTORE}.targetModel = 'Evaluation' AND ${Permission.DATASTORE}.user = ${this.id} AND ${Permission.DATASTORE}.mask = 0`);

    const query = new MariaDatabase.SQLQuery(User.DATASTORE);
    query.select(`SELECT DISTINCT ${Evaluation.DATASTORE}.* FROM ${Permission.DATASTORE}`);
    query.join('INNER JOIN', Permission.DATASTORE, 'targetId', MariaDatabase.JOIN_EVALUATIONS_USERGROUPS, 'usergroup_id')
      .join('INNER JOIN', MariaDatabase.JOIN_EVALUATIONS_USERGROUPS, 'evaluation_id', Evaluation.DATASTORE, 'id')
      .where(`${Permission.DATASTORE}.targetModel = 'UserGroup'`)
      .where(`${Permission.DATASTORE}.mask >= ${Permission.decode('view')}`)
      .where(`${Permission.DATASTORE}.user = ${this.id}`)
      .where(`${Evaluation.DATASTORE}.id NOT IN (${ignoredEvaluations.toString()})`)

    return User.query(query).then(results => results[0][0].map(rr => new Evaluation(rr[Evaluation.DATASTORE])));
  }

  /**
   * Return evaluations user has (edit) permissions with
   * @returns {Promise<Array<Evaluation>>}
   */
  get mySupervisedEvaluations() {
    const query = new MariaDatabase.SQLQuery(Evaluation.DATASTORE);
    query.join('INNER JOIN', Evaluation.DATASTORE, 'id', Permission.DATASTORE, 'targetId')
      .where(`${Permission.DATASTORE}.targetModel = "Evaluation"`)
      .where(`${Permission.DATASTORE}.mask >= ${Permission.decode('view', 'edit')}`)
      .where(`${Permission.DATASTORE}.user = ${this.id}`);

    return Evaluation.query(query).then(records => records[0][0].map(rr => new Evaluation(rr[Evaluation.DATASTORE])));
  }

  /**
   * @returns {Promise<Array<SurveyTemplate>>}
   */
  get myTemplates() {
    const query = new MariaDatabase.SQLQuery(SurveyTemplate.DATASTORE);
    query.join('INNER JOIN', SurveyTemplate.DATASTORE, 'id', Permission.DATASTORE, 'targetId')
    .where(`${Permission.DATASTORE}.targetModel = 'SurveyTemplate'`)
    .where(`${Permission.DATASTORE}.user = ${this.id}`)
    .where(`${Permission.DATASTORE}.mask = ${Permission.ADMIN}`);

    return SurveyTemplate.query(query).then(records => records[0][0].map(rr => new SurveyTemplate(rr[SurveyTemplate.DATASTORE]))
    );
  }

  /**
   * Obtains a query that filters out all users that the current user can not see
   * meaning all that are not in userGroups this user can view, and those that are
   * not participating in the same evaluations. The query can then be further filtered
   * by calling .where method
   *
   * Note that this is intended EXCLUSIVELY for search purposes among users, because
   * it for example will have collisions between userGroups and evaluations, and probably
   * has over 9000 other issues. Use this to only filter user records, no other
   */
  get peers() {
    const query = new MariaDatabase.SQLQuery(User.DATASTORE);
    query
      .join('LEFT JOIN', User.DATASTORE, 'id', Permission.DATASTORE, 'user')
      .join('LEFT JOIN', Permission.DATASTORE, 'targetId', UserGroup.DATASTORE, 'id')
      .join('LEFT JOIN', User.DATASTORE, 'id', MariaDatabase.JOIN_EVALUATIONS_USERS, 'user_id')
      .join('LEFT JOIN', MariaDatabase.JOIN_EVALUATIONS_USERS, 'evaluation_id', Evaluation.DATASTORE, 'id')
      .where(`(
      (${Permission.DATASTORE}.mask >= 4 AND ${Permission.DATASTORE}.targetModel IN ('UserGroup', 'Evaluation'))
      OR
      (${User.DATASTORE}.userGroup = ${this.userGroup})
      )`);
    return query;
  }

  /**
   * modifies the given objects and attaches permissions to the objects as canEdit/canView/canDelete
   * @param {Array<ManagedModel>} objects
   */
  async attachPermissions(...objects) {
    objects = Utils.flatten(objects);
    if(!objects || objects instanceof Array === false) throw new Error(`Expected objects to be an instance of array, got: ${objects}`);
    await this.myPermissions;
    for(const object of objects) {
      const permission = this.permissions.find(permission => permission.targetId === object.id && permission.targetModel === Utils.getObjectClassName(object));
      if(permission) {
        object.canView = permission.permissions.view;
        object.canEdit = permission.permissions.edit;
        object.canDelete = permission.permissions.delete;
      }
    }
  }

  //get session for a survey
  async getSession(survey) {
    survey = survey.id || survey;
    return await SurveySession.find(`user = ${this.id} AND survey = ${survey}`);
  }

  static async match(username, password) {
    let user = await User.find('? IN (username, email)', username);
    if (!user) throw new AuthorizationError('Username does not exist');
    let ok = await bcrypt.compare(password, user.password);
    if (!ok) throw new AuthorizationError('Incorrect password');
    return user;
  }

  //I put this to separate method because it needs to create potentially two records at once,
  //so better to use transaction here. Note that this is the only case where both user AND managed record
  //have to be created "at the same time", so it warrants a custom approach
  //in all other cases, at least the user would have already existed when creating managed records
  /**
   * Creates a new user, generating the password, assigning/creating userGroup, generating permissions, filling in paperwork, taking fingerprints, checking criminal record...
   * @param {Object} data user data containing username, password, confirmation, email and whatever other required parameters for a new user
   * @param {Object|Number} userGroup userGroup id or data to create a new one. Note that data.userGroup will always be overridden by this value
   * @param {Boolean} [implicitAssignToGroup=undefined] whether to search for existing userGroup using given non-id parameters and if one is found, assign user to it.
   * If true, it will be equivalent to passing an id to the second argument and no permissions will be generated for this user.
   * @returns {User} the user that was just created
   */
  static async create(data, userGroup, implicitAssignToGroup) {
    if (data.confirmation !== data.password) throw new Error('Password and confirmation do not match!');
    if (!data.username) throw new Error('Username not given!');
    if (!data.email) throw new Error('Email not given!');
    if (!data.givenName || !data.familyName) throw new Error(`First and last name are required!`);
    if (!userGroup) throw new Error('No group or invalid group data');
    const user = new User(data);
    const result = await User.DB.transaction(async function (db, connection) {
      user.db = db;
      const duplicates = await User.where('username LIKE ?', user.username);
      if (duplicates.length > 0) throw new Error(`Username is already taken`);
      const duplicateEmail = await User.where('email LIKE ?', user.email);
      if (duplicateEmail.length) throw new Error(`Email is already taken`);
      //hash password
      user.password = await bcrypt.hash(data.password, 10);
      user.passwordResetToken = null;
      if(!isNaN(userGroup)) userGroup = Number(userGroup);
      if (Number.isInteger(userGroup) || userGroup.id) {
        const existingUserGroup = await UserGroup.find(userGroup.id || userGroup);
        if(!existingUserGroup) throw new Error('Group does not exist');
        //existing userGroup
        user.userGroup = userGroup.id || userGroup;
        await user.save();
        await new Permission().to(user).for(existingUserGroup).allow(1).save(db);
      } else {
        //create userGroup
        if (!userGroup.name) throw new Error('Group name missing');
        //insert directly to database, bypassing validation since we haven't created user yet
        let group = new UserGroup(userGroup, user);
        group.db = db;
        if(implicitAssignToGroup) await group.get();
        if(group.id) {
          user.userGroup = group.id;
          await user.save();
          await group.allow(user, 'view').save();
        } else {
          const [groupId] = await db.insert(UserGroup.DATASTORE, group.serialize());
          user.userGroup = group.id = groupId;
          await user.save();
          await new Permission().to(user).for(group).allow(Permission.ADMIN).save(db);
        }
      }
      user.db = null; //transaction db will be dead, so unset it here at the end
      return user;
    });
    return result;
  }

  static async hashpwd(pwd) {
    return await bcrypt.hash(pwd, 10);
  }

  /**
   * Returns users of the given group
   * @param {Number} page
   * @param {Number} pageLimit
   * @param {Number} userGroup
   * @returns {Object} {users, pageCount}
   */
  static async paginationSearch(page, pageLimit, userGroup) {
    if(!userGroup) throw new Error('UserGroup is required parameter!');

    const offset = (page -1) * pageLimit;
    const query = new MariaDatabase.SQLQuery(User.DATASTORE);
    query.join('INNER JOIN', User.DATASTORE, 'id', Permission.DATASTORE, 'user')
      .where(`${Permission.DATASTORE}.targetId = ${userGroup} AND ${Permission.DATASTORE}.targetModel = 'UserGroup'`)
      .where(`${Permission.DATASTORE}.mask >= ${Permission.decode('view')}`);
    const countQuery = _.cloneDeep(query);
    query.modifiers(`LIMIT ${pageLimit} OFFSET ${offset}`);
    const [[records]] = await User.query(query);
    const users = records.map(row => new User(row[User.DATASTORE]));

    countQuery.count();
    let [[[recordCount]]] = await User.query(countQuery);
    recordCount = recordCount[''] ? recordCount[''].count : recordCount.count;

    return {users, pageCount: Math.ceil(recordCount / pageLimit)};
  }

  deserialize(data) {
    super.deserialize(data);
    for (let [name, value] of Utils.iterateObject(data)) {
      switch (name) {
        case 'username':
        case 'password':
        case 'givenName':
        case 'familyName':
        case 'id':
        case 'userGroup':
        case 'email':
        case 'passwordResetToken':
          this[name] = value;
          break;
      }
    }
  }

  serialize(id) {
    const json = super.serialize(id);
    json.username = this.username;
    json.password = this.password;
    json.givenName = this.givenName;
    json.familyName = this.familyName;
    json.email = this.email;
    json.passwordResetToken = this.passwordResetToken;
    if (this.userGroup) json.userGroup = this.userGroup;
    if (id && this.id) json.id = this.id;
    return json;
  }

  toJSON(){
    return {
      id: this.id,
      username : this.username,
      givenName: this.givenName,
      familyName: this.familyName,
      email: this.email,
      userGroup: this.userGroup,

      role: this.role
    }
  }


  static get DB() { return MariaDatabase.default; }
  static set DB(newdb) { throw new Error(`${Utils.getObjectClassName(this)} does not support switching databases`); }
  static get DATASTORE() { return tableName; }
  static set DATASTORE(newDatastore) { throw new Error(`${Utils.getObjectClassName(this)} does not support switching datastore`); }
  //allow setting databases per instance, for use in transactions
  set db(newdb) { this._db = newdb; }
  get db() { return this._db && !this._db.dead ? this._db : User.DB; }
  get datastore() { return User.DATASTORE; }
}

exports.model = User;