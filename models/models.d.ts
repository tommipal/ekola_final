import * as _User from './User';
import * as _Answer from './Answer';
import * as _Evaluation from './Evaluation';
import * as _Link from './Link';
import * as _UserGroup from './UserGroup';
import * as _Permission from './Permission';
import * as _Question from './Question';
import * as _QuestionBank from './QuestionBank';
import * as _Survey from './Survey';
import * as _SurveySession from './SurveySession';
import * as _SurveyTemplate from './SurveyTemplate';

declare global {
  const User : typeof _User.model;
  const Answer : typeof _Answer.model;
  const Evaluation : typeof _Evaluation.model;
  const Link : typeof _Link.model;
  const UserGroup : typeof _UserGroup.model;
  const Permission : typeof _Permission.model;
  const Question : typeof _Question.model;
  const QuestionBank : typeof _QuestionBank.model;
  const Survey : typeof _Survey.model;
  const SurveySession : typeof _SurveySession.model;
  const SurveyTemplate : typeof _SurveyTemplate.model;
}