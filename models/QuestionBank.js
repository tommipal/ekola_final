const mongodb = require('mongodb')
const MariaModel = require('./MariaModel')
const { Logger, Utils } = require('common');
const _ = require('lodash');

const { MariaDatabase } = require('database');
const tableName = 'questionbank';

class QuestionBank extends MariaModel {
  constructor(data){
    super(data);
    this.deserialize(data);
  }

  get myTemplate(){
    return SurveyTemplate.find(this.template);
  }

  deserialize(data){
    super.deserialize(data);
    for(let [name, val] of Utils.iterateObject(data)){
      switch(name){
        case 'criteria':
          if (Utils.containsValidJSON(val)) val = JSON.parse(val);
          if (val.constructor === [].constructor) this[name] = val;
          break;
        case 'title':
        case 'description':
        case 'position':
        case 'tags':
        case 'template':
        this[name] = val;
        break;
      }
    }
  }

  /**
   * Return every question bank question if no search paramter is provided
   * @param {Number} pageLimit
   * @param {Number} page
   * @param {String} searchParam optional
   * @returns {Object} {questions, pageCount}
   */
  static async paginationSearch(pageLimit, page, searchParam) {
    const offset = pageLimit * (page - 1);

    const query = new MariaDatabase.SQLQuery(QuestionBank.DATASTORE);
    if (searchParam) query.join('INNER JOIN', QuestionBank.DATASTORE, 'template', SurveyTemplate.DATASTORE, 'id')
      .where(`${QuestionBank.DATASTORE}.title LIKE '%${searchParam}%'`)
      .or().where(`${SurveyTemplate.DATASTORE}.title LIKE '%${searchParam}%'`)
      .or().where(`${QuestionBank.DATASTORE}.tags LIKE '%${searchParam}%'`)
    else query.where();
    const countQuery = _.cloneDeep(query);
    query.modifiers(`LIMIT ${pageLimit} OFFSET ${offset}`);
    const [[result]] = await QuestionBank.query(query);
    const questions = result.map(rr => new QuestionBank(rr[QuestionBank.DATASTORE] ? rr[QuestionBank.DATASTORE] : rr));
    for (const question of questions) question.template = await question.myTemplate;

    countQuery.count();
    let [[[recordCount]]] = await QuestionBank.query(countQuery);
    recordCount = recordCount[''] ? recordCount[''].count : recordCount.count;
    const pageCount = Math.ceil(recordCount / pageLimit) ? Math.ceil(recordCount / pageLimit) : 1;
    return {questions, pageCount};
  }

  serialize(id, forSending){
    const json = super.serialize(id);
    json.title = this.title;
    json.criteria = JSON.stringify(this.criteria);
    json.template = this.template;
    if(this.description) json.description = this.description;
    if(!isNaN(this.position)) json.position = this.position;
    if(this.tags) json.tags = this.tags;

    if(forSending) {
      json.criteria = this.criteria;
    }

    return json;
  }

  static get DB(){ return MariaDatabase.default; }
  static set DB(newdb){ throw new Error(`${Utils.getObjectClassName(this)} does not support switching databases`); }
  static get DATASTORE(){ return tableName; }
  static set DATASTORE(newDatastore){ throw new Error(`${Utils.getObjectClassName(this)} does not support switching datastore`); }
  set db(newdb) { this._db = newdb; }
  get db(){ return this._db && !this._db.dead ? this._db : QuestionBank.DB; }
  get datastore(){ return QuestionBank.DATASTORE; }
}

exports.model = QuestionBank;