const MariaModel = require('./MariaModel');
const { MariaDatabase } = require('database');
const { Utils, Logger, ValidationError } = require('common');

/**
 * Extending models from this should take care of permission generation
 */
module.exports =
class ManagedModel extends MariaModel {
  constructor(data){
    super(data);
    this.permissions = [];
  }

  get myPermissions(){
    if(!this.id) throw new Error(`Unsaved object can't return permissions!`)
    return Permission.where(`targetId = ${this.id} AND targetModel = '${Utils.getObjectClassName(this)}'`)
      //add them all to this model's permissions
      .then(pp=>Utils.arrayMerge(this.permissions, pp, (p1, p2)=>(p1.id === p2.id || p1.user === p2.user)))
      .then(()=>this.permissions.forEach(pp=>pp.target = this))
      .then(()=>this.permissions);
  }
  /**
   * @param {number} mask level of permissions user has.
   * @returns {Promise<Array<User>>}
   */
  async findSupervisors(mask = Permission.decode('view'), comparator = '>=') {
    if (!this.id) throw new Error(`Unsaved object can't return permissions!`);
    const query = new MariaDatabase.SQLQuery(User.DATASTORE);
    query.join('INNER JOIN', User.DATASTORE, 'id', Permission.DATASTORE, 'user')
      .join('INNER JOIN', Permission.DATASTORE, 'targetId', this.datastore, 'id')
      .where(`${Permission.DATASTORE}.targetModel = '${Utils.getObjectClassName(this)}'`)
      .where(`${Permission.DATASTORE}.targetId = ${this.id}`)
      .where(`${Permission.DATASTORE}.mask ${comparator} ${mask}`);

    return User.query(query).then(result => {
      return result[0][0].map(rr => new User(rr[User.DATASTORE]));
    });
  }

  /**
   * Allow a user one or more rights
   * @param {User} user the instance of the user record to whom these rights will be granted
   * @param {Array<string|Number>} rights to allow either as strings or direct bitmask values
   * @returns {ManagedModel} this instance of chaining
   */
  allow(user, ...rights){
    if(!(user instanceof User) && !user.id) throw new Error('User must be an instanceof user or have an id')
    if(!(user instanceof User)) user = new User(user);
    this.permissions.push(new Permission().to(user).for(this).allow(...rights))
    return this;
  }

  //await ManagedModel.find(id).deny(ctx.session.user. 'delete').save();
  deny(user, ...rights){
    if(!(user instanceof User) && !user.id) throw new Error('User must be an instanceof user or have an id')
    if(!(user instanceof User)) user = new User(user);
    this.permissions.push(new Permission().to(user).for(this).deny(...rights))
    return this;
  }

  /**
   * Mark this instance as locked (no permissions for anyone)
   * @returns {ManagedModel} this instance for chaining
   */
  lock(){
    this.locked = true;
    return this;
  }

  /**
   * Constructs a query for the given ManagedModel that matches any rows for which the specified
   * user has any sort of permission.
   * @param {User} user the user who's permissions will be searched
   * @returns {MariaDatabase.SQLQuery} the query to match permitted records
   */
  static permitted(user){
    const query = new MariaDatabase.SQLQuery(this.DATASTORE);
    query.select('SELECT DISTINCT * FROM '+this.DATASTORE);
    query.join('LEFT JOIN', this.DATASTORE, 'id', Permission.DATASTORE, 'targetId');
    query.where(`${Permission.DATASTORE}.user = ${user.id}`)
    query.where(`${Permission.DATASTORE}.targetModel = '${Utils.getObjectClassName(this)}'`);
    query.where(`${Permission.DATASTORE}.mask >= '${Permission.decode('view')}'`);
    return query;
  }

  static async generatePermissions(db, ...managedModels){
    if(!db.connection) Logger.warn('ManagedModel.generatePermissions should be run with a transaction database instance, otherwise you risk creating shadow records.');
    if(!managedModels.every(mm=>mm.id)) throw new Error('Every managed model instance needs to have been saved in a transaction before generating permission');
    if(!managedModels.every(mm=>mm.permissions.every(pp=>pp.user.id))) throw new Error('Some managed model permissions did not have user IDs set on them, make sure to call .allow or .deny at least once for new managed records');
    if(!managedModels.every(mm=>mm.permissions.every(pp=>pp.target.id))) throw new Error('Some managed model permissions did not have target ID set. The reference might be broken, please inspect the permission instance');
    return await Promise.all(managedModels.map(mm=>mm.permissions.map(pp=>pp.save(db))));
  }

  static async insert(...instances){
    if(!instances.every(mm=>mm instanceof ManagedModel)) throw new Error('Every instance must be a ManagedModel');
    const result = await this.DB.transaction(async function(db, connection){
      let ids = await db.insert(this.DATASTORE, ...instances.map(ii=>ii.serialize()));
      for(let i = 0; i<instances.length; i++){
        instances[i].id = ids[i];
      }

      let permissions = await Promise.all(instances.filter(ii=>!ii.locked).map(ii=>ii.myPermissions));
      permissions = Utils.flatten(permissions);
      let savedPermissions = await Promise.all(permissions.map(pp=>pp.save(db)));
      return instances;
    });
    if(result.length !== instances.lenght) Logger.error(`Unexpected bulk insert resultset length for ManagedModel - expected ${instances.length} got ${result.length}, check records similar to \n ${JSON.stringify(instances)}`);
    return result;
  }

  async save(){
    const result = await this.db.transaction(async (db, connection) => {
      //save the actual model
      if(this.id){
        let result = await db.update(this.datastore, this.serialize(), `id = ${this.id}`);
      } else {
        if(this.permissions.length === 0 && !this.locked) throw new ValidationError('Permissions must be set before inserting a new ManagedRecord');
        //only get the first query result, we don't expect more
        let [id] = await db.insert(this.datastore, this.serialize());
        this.id = id;
      }
      await Promise.all(this.permissions.map(pp=>pp.save(db)));
      return this;
    });
    return result;
  }

  async delete(){
    const result = await super.delete();
    await this.db.delete(Permission.DATASTORE, `targetModel = '${Utils.getObjectClassName(this)}' AND targetId = ${this.id}`);
    return result;
  }

  static async delete(where, ...params){
    const rows = await super.where(where, ...params);
    if(rows.length>0){
      await super.delete(where, ...params);
      await Permission.delete(`targetModel = '${Utils.getObjectClassName(this)}' AND targetId IN (${rows.map(rr=>rr.id)})`);
    }
    return rows;
  }
}