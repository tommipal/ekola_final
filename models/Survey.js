const mongodb = require('mongodb')
const ManagedModel = require('./ManagedModel')
const { Logger, Utils, ValidationError } = require('common');
const moment = require('moment');
const _ = require('lodash');

const { MariaDatabase } = require('database');
const tableName = 'surveys';

class Survey extends ManagedModel {
  constructor(data) {
    super(data);
    this.questions = [];
    this.links = [];
    this.userInfo = [];
    this.deserialize(data);
  }

  get myEvaluation() {
    if(!this.evaluation) return null;
    return Evaluation.find(this.evaluation);
  }

  get myQuestions() {
    return Question.where(`survey = ${this.id}`).then(questions => {
      Utils.sortArrayOfObjects(questions, 'position');
      questions.forEach(question => question.survey = this);
      this.questions = questions;
      return this.questions;
    });
  }

  get myLinks() {
    return Link.where({ survey: this.id });
  }

  get mySessions() {
    return SurveySession.where(`survey = ${this.id}`);
  }

  //TODO: Potentially huge dataset, maybe limit somehow?
  get myAnswers() {
    return Answer.where({ survey: this.id });
  }

  get open() {
    const now = moment().utc();
    const endDate = moment(this.endDate).utc(true);
    const startDate = moment(this.startDate).utc(true);
    return now.isAfter(startDate) && now.isBefore(endDate);
  }

  set open(newEndDate) {
    if (!(newEndDate instanceof moment)) throw new ValidationError('Dates must be instances of moment');
    const now = moment();
    if (endDate.isBefore(now)) throw new ValidationError(`Can't open survey - ${endDate} is before ${now}`);
    this.endDate = newEndDate.toDate();
    this.startDate = now.toDate();
  }

  //get the entire survey as a json with all fields populated
  //TODO: add this kind of function to every model so we can do nested populates if necessary
  //(e.g. questions.map(qq=>if(qq.getPopulatedJson) qq.getPopulatedJson()))
  async getPopulatedJson(questions, links, sessions, evaluation) {
    const json = this.serialize(true);
    if (questions) json.questions = await this.myQuestions;
    if (links) json.links = await this.myLinks;
    if (sessions) json.sessions = await this.mySessions;
    if (evaluation && this.evaluation) json.evaluation = await this.myEvaluation;
    return json;
  }

  /**
   * Save the survey together with all of the questions and evaluation currently set via instance.questions = [...] setter.
   * @param {Boolean} strict whether to delete all questions not set in this model (i.e. match the current in-memory state of the instance with the database, including all questions)
   * @returns {Survey} this object after saving. The id is guaranteed to be set if the save was succssful.
   */
  async save(strict) {
    let result = await super.save();
    //save questions if any have been assigned
    if (this.questions.length > 0) {
      for (let question of this.questions) {
        if(typeof question.survey === 'object') question.survey = question.survey.id;
        if(this.id){
          if(question.id && question.survey !== this.id) throw new Error(`Tried to save an existing question that was already attached to a different survey! ${JSON.stringify(question)}`);
          question.survey = this.id;
        }
        question.db = this.db;
      }
      await Promise.all(this.questions.map(qq => qq.save()));
      if (strict) {
        await this.db.delete('questions', `id NOT IN (?) AND survey = ?`, this.questions.map(qq => qq.id), [this.id]);
      }
    }
    if (this.template && this.questions.length === 0) {
      const template = await SurveyTemplate.find(this.template);
      if (template) {
        const questions = (await template.myQuestions).map(qq => qq.serialize());
        questions.forEach(qq => {
          qq.survey = this.id
          delete qq.template;
        });
        const qids = await this.db.insert(Question.DATASTORE, questions);
        questions.forEach((qq, index) => qq.id = qids[index]);
        this.questions = questions.map(qdata => new Question(qdata));

        if(template.userInfo) {
          this.userInfo = template.userInfo;
          await super.save();
        }
      }
    }
    //TODO: save links here too?
    await this.get(); //fill this particular record on every save
    return result;
  }

  /**
   *
   * TODO add search and ordering!
   * @param {Number} page
   * @param {Number} pageLimit
   * @param {String} filter
   * @param {Array<Evaluation>} evaluations
   * @returns {Object} {surveys, pageCount}
   */
  static async paginationSearch(page, pageLimit, filter, evaluations = []) {
    if(evaluations instanceof Array === false) throw new Error(`Expected an array of evaluations. Got ${evaluations}`);
    if(!page || !pageLimit) throw new Error(`page and pageLimit are required parameters!`);
    const query = new MariaDatabase.SQLQuery(Survey.DATASTORE);
    switch(filter){
      case 'active':
      query.where(`CAST('${moment.utc().format('YYYY-MM-DD HH:mm:ss')}' AS DATETIME) BETWEEN surveys.startDate AND surveys.endDate`)
      break;
      case 'closed':
      query.where(`CAST('${moment.utc().format('YYYY-MM-DD HH:mm:ss')}' AS DATETIME) NOT BETWEEN surveys.startDate AND surveys.endDate`);
      break;
      default:
      throw new Error(`Unsupported filter: ${ctx.params.filter}`);
    }

    if(evaluations.length) query.where(`${Survey.DATASTORE}.evaluation IN (${evaluations.map(evaluation => evaluation.id)})`);

    const countQuery = _.cloneDeep(query);
    countQuery.count((`SELECT COUNT(DISTINCT ${Survey.DATASTORE}.id) AS count FROM ${Survey.DATASTORE}`));
    let [[[recordCount]]] = await Survey.query(countQuery);
    recordCount = recordCount[''] ? recordCount[''].count : recordCount.count;

    query.select(`SELECT DISTINCT ${Survey.DATASTORE}.* FROM ${Survey.DATASTORE}`);
    const offset = (page - 1) * pageLimit;
    query.modifiers(`LIMIT ${pageLimit} OFFSET ${offset}`);
    let [[records]] = await Survey.query(query);
    const surveys = records.map(row => new Survey(row));

    return {surveys, pageCount: Math.ceil(recordCount / pageLimit)};
  }

  deserialize(data) {
    super.deserialize(data);
    for (let [name, val] of Utils.iterateObject(data)) {
      switch (name) {
        case 'questions':
          if (val.length && val.length > 0) {
            for (let i = 0; i < val.length; i++) {
              if (val[i] instanceof Question) this.questions.push(val[i]);
              else {
                val[i].survey = this.id || this;
                this.questions.push(new Question(val[i]));
              }
            }
          } else {
            Logger.warn(`Empty questions for survey, skipping!`);
          }
          break;
        case 'userInfo':
          if (Utils.containsValidJSON(val)) val = JSON.parse(val);
          if (val.constructor === [].constructor) this[name] = val;
          break;
        case 'template':
        case 'title':
        case 'description':
        case 'evaluation':
        case 'openResults':
        this[name] = val;
        break;
        case 'startDate':
        case 'endDate':
          this[name] = val ? moment(val) : moment().utc();
          break;
      }
    }
    if (this.isNew && (!this.title || !this.evaluation)) {
      throw new ValidationError('New survey missing title field and/or evaluation assignment', this, data);
    }
  }

  serialize(id, forSending) {
    const json = super.serialize(id);
    if (this.id && id) json.id = this.id;
    json.title = this.title;
    if (this.evaluation) json.evaluation = this.evaluation;
    if (this.startDate) {
      if (this.startDate instanceof moment) {
        json.startDate = this.startDate.format('YYYY-MM-DD HH:mm:ss');
      } else if (typeof this.startDate === 'string') {
        json.startDate = moment(this.startDate).format('YYYY-MM-DD HH:mm:ss');
      }
    }
    if (this.endDate) {
      if (this.endDate instanceof moment) {
        json.endDate = this.endDate.format('YYYY-MM-DD HH:mm:ss');
      } else if (typeof this.endDate === 'string') {
        json.endDate = moment(this.endDate).format('YYYY-MM-DD HH:mm:ss');
      }
    }
    json.openResults = this.openResults || 0;
    if (this.description) json.description = this.description;
    if (this.userInfo) json.userInfo = JSON.stringify(this.userInfo);
    if(this.canEdit) json.canEdit = this.canEdit;

    if(forSending) {
      if(this.questions) json.questions = this.questions;
    }
    if(this.canView) json.canView = this.canView;//what to do with these?
    if(this.canDelete) json.canDelete = this.canDelete;
    if(this.canAnswer) json.canAnswer = this.canAnswer;
    if(this.isOpen) json.isOpen = this.isOpen;
    if(this.canViewResults) json.canViewResults = this.canViewResults;
    if(this.ongoingSession) json.ongoingSession = this.ongoingSession;

    return json;
  }

  static get DB() { return MariaDatabase.default; }
  static set DB(newdb) { throw new Error(`${Utils.getObjectClassName(this)} does not support switching databases`); }
  static get DATASTORE() { return tableName; }
  static set DATASTORE(newDatastore) { throw new Error(`${Utils.getObjectClassName(this)} does not support switching datastore`); }
  set db(newdb) { this._db = newdb; }
  get db() { return this._db && !this._db.dead ? this._db : Survey.DB; }
  get datastore() { return Survey.DATASTORE; }
}

exports.model = Survey;