const mongodb = require('mongodb')
const MariaModel = require('./MariaModel')
const { Logger, Utils } = require('common');

const { MariaDatabase } = require('database');
const tableName = 'questions';

class Question extends MariaModel {
  constructor(data){
    super(data);
    this.deserialize(data);
  }

  get mySurvey(){
    return Survey.find(this.survey);
  }

  deserialize(data){
    super.deserialize(data);
    for(let [name, val] of Utils.iterateObject(data)){
      switch(name){
        case 'criteria':
          if (Utils.containsValidJSON(val)) val = JSON.parse(val);
          if (val.constructor === [].constructor) this[name] = val;
          break;
        case 'title':
        case 'description':
        case 'tags':
        case 'survey':
        case 'position':
        this[name] = val;
        break;
      }
    }
  }

  async save(){
    if(this.survey instanceof Survey) this.db = this.survey.db;
    // await this.get();
    return await super.save();
  }

  async get(){
    if(this.id) return await super.get();
    else {
      const [me] = await this.db.select(Question.DATASTORE, `survey = ${this.survey.id || this.survey} AND title = ?`, this.title);
      if(me) this.id = me.id;
      return this;
    }
  }

  serialize(id){
    const json = super.serialize(id);
    json.criteria = JSON.stringify(this.criteria);
    json.survey = this.survey;
    json.title = this.title;
    if(this.description) json.description = this.description;
    if(this.tags) json.tags = this.tags;
    if(!isNaN(this.position)) json.position = this.position;
    return json;
  }

  static get DB(){ return MariaDatabase.default; }
  static set DB(newdb){ throw new Error(`${Utils.getObjectClassName(this)} does not support switching databases`); }
  static get DATASTORE(){ return tableName; }
  static set DATASTORE(newDatastore){ throw new Error(`${Utils.getObjectClassName(this)} does not support switching datastore`); }
  set db(newdb) { this._db = newdb; }
  get db(){ return this._db && !this._db.dead ? this._db : Question.DB; }
  get datastore(){ return Question.DATASTORE; }
}

exports.model = Question;