const mongodb = require('mongodb')
const MongoModel = require('./MongoModel')
const { Logger, Utils } = require('common');

const {MongoDatabase} = require('database');
const collectionName = 'links';

class Link extends MongoModel {
  constructor(data){
    super(data);
    this.deserialize(data);
  }

  get mySurvey(){
    return Survey.find(this.survey);
  }

  get myUserGroup(){
    return UserGroup.find(this.userGroup);
  }

  async save(){
    try {
      await this.get();
    } finally{
      return await super.save();
    }
  }

  static get DB(){ return MongoDatabase.default; }
  static set DB(newdb){ throw new Error(`${Utils.getObjectClassName(this)} does not support switching databases`); }
  static get DATASTORE(){ return collectionName; }
  static set DATASTORE(newDatastore){ throw new Error(`${Utils.getObjectClassName(this)} does not support switching datastore`); }
  get db(){ return Link.DB; }
  get datastore(){ return Link.DATASTORE; }
}

exports.model = Link;