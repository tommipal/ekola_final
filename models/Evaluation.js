const mongodb = require('mongodb')
const ManagedModel = require('./ManagedModel')
const { Logger, Utils } = require('common');
const { MariaDatabase } = require('database');
const tableName = 'evaluations';

class Evaluation extends ManagedModel {
  constructor(data, owner){
    super(data, owner);
    this.deserialize(data);
  }

  /**
   * Returns users that belong to a group that is part of this evaluation.
   * Ignore users that have negative permission to this evaluation.
   * @returns {Array<User>}
   */
  get myUsers(){
    const groupsQuery = new MariaDatabase.SQLQuery(MariaDatabase.JOIN_EVALUATIONS_USERGROUPS);
    groupsQuery.select(`SELECT usergroup_id FROM ${MariaDatabase.JOIN_EVALUATIONS_USERGROUPS}`);
    groupsQuery.where(`evaluation_id = ${this.id}`);

    const userQuery = new MariaDatabase.SQLQuery(Permission.DATASTORE)
    userQuery.select(`SELECT ${Permission.DATASTORE}.user FROM ${Permission.DATASTORE}`);
    userQuery.where(`${Permission.DATASTORE}.targetModel = 'Evaluation' AND ${Permission.DATASTORE}.targetId = ${this.id} AND ${Permission.DATASTORE}.mask = 0`)

    const query = new MariaDatabase.SQLQuery(User.DATASTORE);
    query.select(`SELECT DISTINCT ${User.DATASTORE}.* FROM ${User.DATASTORE}`);
    query.join('INNER JOIN', User.DATASTORE, 'id', Permission.DATASTORE, 'user')
      .where(`${Permission.DATASTORE}.targetModel = 'UserGroup' AND targetId IN (${groupsQuery.toString()})`)
      .where(`${Permission.DATASTORE}.mask >= ${Permission.decode('view')}`)
      .where(`${User.DATASTORE}.id NOT IN (${userQuery.toString()})`);

      return User.query(query).then(results => {
      this.users = results[0][0].map(rr => new User(rr[User.DATASTORE]));
      return this.users;
    });
  }

  get myUserGroups() {
    const query = new MariaDatabase.SQLQuery(MariaDatabase.JOIN_EVALUATIONS_USERGROUPS);
    query.join('INNER JOIN', MariaDatabase.JOIN_EVALUATIONS_USERGROUPS, 'userGroup_id', UserGroup.DATASTORE, 'id');
    query.where({ 'evaluation_id': this.id });
    return UserGroup.query(query).then(results => {
      this.userGroups = results[0][0].map(rr=>new UserGroup(rr[UserGroup.DATASTORE]));
      return this.userGroups;
    });
  }

  /**
   * NOTE This method is technically only used to reverse the effect that leaving an evalution has.
   * Adds view permission to evaluation for the given users, but will not override any higher existing permissions.
   * @param {Array<User>} users user objects or ids.
   * @param {Number} mask
   */
  async addUsers(users, mask = Permission.decode('view')) {
    users = users.map(user => parseInt(user.id || user));
    if(!users.every(user => typeof user === 'number')) throw new Error(`Expected to have an array of ids. Got: ${users}`);

    const permissions = await this.myPermissions;
    users = users.filter(user => !permissions.find(permission => permission.user.id === user && permission.mask >= mask));
    if(users.length < 1) return;
    users = users.map(user => new User(user));
    for(const user of users) this.allow(user, mask);

    await this.save();
  }

  /**
   * Set permission with mask 0 to given users.
   * If given evaluation has no valid permissions left after the operation, delete it.
   * @param {number} evaluation
   * @param {Array<User>} users user objects or ids.
   */
  static async removeUsers(evaluation, ...users) {
    const id = evaluation.id || evaluation;
    users = Utils.flatten(users).map(user=>user.id || user);
    if (!users.length) return;
    evaluation = await Evaluation.find(id);
    if (!evaluation) return Logger.warn(`Tried to remove users from evaluation ${id} that doesn't exist!`);

    for(const user of users) evaluation.allow(new User(Number(user)), 0);
    await evaluation.save();

    const permissions = (await evaluation.myPermissions).filter(permission => permission.mask > 0);
    if(!permissions.length) {
      Logger.info(`Last permission to evaluation ${evaluation.id} was abolished, deleting detached evaluation..`);
      await evaluation.delete();
    }
  }
  async removeUsers(users) {
    if (!this.id) throw new Error(`Can't remove users from an evaluation without id`);
    await Evaluation.removeUsers(this.id, users);
  }

  async addUserGroups(...userGroups) {
    userGroups = Utils.flatten(userGroups).map(userGroup => parseInt(userGroup.id || userGroup));
    let existing = await this.myUserGroups;
    let UserGroupsToAdd = [];
    userGroups.forEach(userGroup => {
      if (existing.findIndex(existingUserGroup => existingUserGroup.id === userGroup) === -1) UserGroupsToAdd.push(userGroup);
    });
    Logger.info(`Adding ${UserGroupsToAdd.length} userGroups to evaluation ${this.id}`);
    await Promise.all(UserGroupsToAdd.map(userGroup => this.db.insert(MariaDatabase.JOIN_EVALUATIONS_USERGROUPS, { userGroup_id: userGroup, evaluation_id: this.id })));
  }

  static async removeUserGroups(evaluation, ...userGroups) {
    evaluation = evaluation.id || evaluation;
    userGroups = Utils.flatten(userGroups).map(userGroup => userGroup.id || userGroup);
    if (userGroups.length < 1) throw new Error(`Expected an array of ids or userGroup objects, but got nothing`);
    await Evaluation.DB.delete(MariaDatabase.JOIN_EVALUATIONS_USERGROUPS, `evaluation_id = ${evaluation} AND userGroup_id IN(${userGroups.join(',')})`);
  }
  async removeUserGroups(...userGroups) {
    if (!this.id) throw new Error(`Can't remove userGroups from an evaluation without id`);
    await Evaluation.removeUserGroups(this.id, userGroups);
  }

  /**
   * Close surveySessions from the given evaluation and users.
   * @param {Number} evaluation
   * @param {Array<Number>} users
   */
  static async voidUsers(evaluation, users) {
    if (users.length < 1) throw new Error(`Expected an array of users!`);
    evaluation = evaluation.id || evaluation;
    users = users.map(user => user.id || user);

    let query = new MariaDatabase.SQLQuery(SurveySession.DATASTORE);
    query.update()
      .join('INNER JOIN', SurveySession.DATASTORE, 'survey', Survey.DATASTORE, 'id')
      .set(`${SurveySession.DATASTORE}.open = 0`)
      .where(`${Survey.DATASTORE}.evaluation = ${evaluation} AND ${SurveySession.DATASTORE}.user IN (${users.join(',')})`)
      .where(`${SurveySession.DATASTORE}.open = 1`);
    let [[result]] = await SurveySession.query(query);

    return Logger.info(`Closed ${result.affectedRows} survey sessions for ${users.length} users of evaluation ${evaluation}`);
  }

  deserialize(data){
    super.deserialize(data);
    for(let [name, val] of Utils.iterateObject(data)){
      switch(name){
        case 'name':
        this[name] = val;
        break;
      }
    }
  }

  serialize(id){
    const json = super.serialize(id);
    json.name = this.name;

    return json;
  }

  static get DB(){ return MariaDatabase.default; }
  static set DB(newdb){ throw new Error(`${Utils.getObjectClassName(this)} does not support switching databases`); }
  static get DATASTORE(){ return tableName; }
  static set DATASTORE(newDatastore){ throw new Error(`${Utils.getObjectClassName(this)} does not support switching datastore`); }
  set db(newdb){ this._db = newdb; }
  get db(){ return this._db && !this._db.dead ? this._db : Evaluation.DB; }
  get datastore(){ return Evaluation.DATASTORE; }
}

exports.model = Evaluation;