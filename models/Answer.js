const mongodb = require('mongodb')
const MongoModel = require('./MongoModel')
const { Logger, Utils } = require('common');
const exceljs = require('exceljs');
const _ = require('lodash');

const {MongoDatabase} = require('database');
const collectionName = 'answers';
const Translator = require('../client/scripts/translator').Translator;

class Answer extends MongoModel {
  constructor(data){
    super(data);
    this.deserialize(data);
  }

  /**
   * @returns {Number}
   */
  get myValue() {
    return this.values[this.type].value;
  }

  get mySession(){

  }

  static get DB(){ return MongoDatabase.default; }
  static set DB(newdb){ throw new Error(`${Utils.getObjectClassName(this)} does not support switching databases`); }
  static get DATASTORE(){ return collectionName; }
  static set DATASTORE(newDatastore){ throw new Error(`${Utils.getObjectClassName(this)} does not support switching datastore`); }
  get db(){ return Answer.DB; }
  get datastore(){ return Answer.DATASTORE; }

  /**
   * Handle nested documents here for now
   */
  serialize() {
    let data = super.serialize();
    for (let [name, val] of Object.entries(this)) {
      switch (name) {
        case 'values':
        case 'criteria':
          data[name] = val;
          break;
      }
    }
    return data;
  }

  /**
   * Saves the answers
   * @param {number} surveySession survey session id
   * @param {number} currentUser id of the user to whom these answers must be attributed
   * @param {Array} answers answer data to save
   * @returns {Object} the result of a mongodb upsert for each of the answer records
   */
  static async save(surveySession, currentUser, ...answers) {
    answers = Utils.flatten(answers); //use the Utils flatten, it's only one level, I'd rather we don't flatten deeply nested arrays here
    if(answers.length < 1) return;
    let session = await SurveySession.find({id: surveySession});
    if(!session) throw new Error(`Could not find session with id ${surveySession}`);
    if(!session.open) throw new Error(`Could not save answers to a closed session ${surveySession}`);
    if(session.user && currentUser != session.user) throw new Error(`User ${currentUser} is not the owner of session ${surveySession}`);

    let questions = await Question.where({survey: session.survey});

    answers.forEach(answer=>{
      answer.values[answer.type].value = Number(answer.values[answer.type].value);
      answer.session = session.id;
      answer.userGroup = session.userGroup;
      answer.survey = session.survey;
      let question = questions.find(question => question.id == answer.question);
      if(!question) throw new Error(`Could not find matching question from survey ${session.survey} for answer: ${JSON.stringify(answer)}`);
      let criteria = question.criteria.find(criteria => criteria.title == answer.criteriaTitle);
      if(!criteria) throw new Error(`Could not find matching criteria from question ${session.survey} for answer: ${JSON.stringify(answer)}`);

      //is there any point in throwing here? should the validation live here?
      // - it can. No real reason yet to make it separate
      switch(answer.type) {
        case 'select':
          const choice = answer.criteria.options.choices.find(choice => choice.value == answer.values.select.value &&
            choice.description == answer.values.select.description);
          if(!choice) throw new Error(`Answer's value doesn't match any of the available choices: ${JSON.stringify(answer)}`);
          break;
        case 'scale':
          if(answer.values.scale.value > answer.criteria.options.max ||
            answer.values.scale.value < answer.criteria.options.min) throw new Error(`Answer's value doesn't match the boundaries of its criteria: ${JSON.stringify(answer)}`);
          break;
        default:
            throw new Error(`Found an answer with an unsupported criteria type: ${JSON.stringify(answer)}`);
        }
    });

    answers = answers.map(answer=>new Answer(answer)); //do we want to just accept answer data here? Or maybe create instances in the controller, so that this function only accepts them?
    const db = await Answer.DB.connect();
    const collection = db.collection(collectionName);
    answers = answers.map(answer => answer.serialize());
    let results = await Promise.all(answers.map(json=>collection.updateOne(
      json._id ? {_id: json._id} : { session: session.id, question: json.question, criteriaTitle: json.criteriaTitle},
      { $set: json },
      { upsert: true }
    )));
    return results;
  }

  /**
   * Obtains average values from all answers to given survey
   * @param {number} survey survey id
   * @returns {Array} Array, where each element are average answers to it's corresponding survey's question
   */
  static async getAverageValues(survey) {
    survey = survey.id || survey;
    let results = await Answer.aggregate([
      { '$match': { 'survey': survey } },
      { '$group': {
        _id: { question: '$question', criteriaTitle: '$criteriaTitle', type: '$type', userGroup: '$userGroup' },
        select: { $avg: '$values.select.value' },
        scale: { $avg: '$values.scale.value' },
        criteria: { $addToSet: '$criteria' }
      }},
      { $sort: { _id: 1 } }
    ]);

    results = results.map(result => {
      let answer = {
        criteriaTitle: result._id.criteriaTitle,
        question: result._id.question,
        type: result._id.type,
        userGroup: result._id.userGroup,
        //Try to use the latest version of the criteria this answer (~combination of answers) corresponds to.
        criteria: _.last(result.criteria)
      }
      switch(answer.type) {
        case 'scale':
          answer.values = { scale: { value: result.scale } };
          break;
        case 'select':
          answer.values = { select: { value: result.select } };
          break;
      }
      return answer;
    });
    return results;
  }

  /**
   * Exports answers to an excel sheet
   * @param {number} surveyId survey id the answers of which to export
   * @param {string} fileName the filename of exported file
   * @returns {String} the filename
   */
  static async excel(survey, fileName = 'results.xlsx', locale = 'en') {
    const surveyId = survey.id || survey;
    const translator = new Translator(locale);
    survey = await Survey.find({id: surveyId});
    if(!survey) throw new Error(`Requested survey with id ${surveyId} doesn't exist!`);
    let questions = await Question.where({survey: surveyId}); //TODO add sorting?
    let answers = await Answer.where({survey: surveyId});
    let sessions = await SurveySession.where({survey: surveyId});
    sessions = sessions.filter(session => _.some(answers, answer => answer.session == session.id));

    for(const question of questions) {
      if(Utils.containsValidJSON(question.criteria)) question.criteria = JSON.parse(question.criteria);
      if(question.criteria.constructor !== [].constructor) throw new Error(`Question's criteria is expected to be an array. Found ${question.criteria}`);
    }
    for(const session of sessions) {
      if(Utils.containsValidJSON(session.evidence)) session.evidence = JSON.parse(session.evidence);
      if(session.evidence.constructor !== [].constructor) throw new Error(`Session's evidence is expected to be an array. Found ${session.evidence}`);
      if(Utils.containsValidJSON(session.userInfo)) session.userInfo = JSON.parse(session.userInfo);
      if(session.userInfo.constructor !== [].constructor) throw new Error(`Session's userInfo is expected to be an array. Found ${session.userInfo}`);

      session.userGroupName = (await session.myUserGroup).name;
    }

    const workbook = new exceljs.Workbook();
    const worksheet = workbook.addWorksheet();

    //Create columns for the worksheet
    let columns = [{ header: translator.trans('Group'), key: 'userGroup' }];
    if (survey.userInfo) columns.push(survey.userInfo.map(question => { return { header: question, key: `${question}-userInfo` } }));
    questions.forEach(question=>{
      let column = question.criteria.map(criteria => { return { header: `${question.title} - ${criteria.title}`, key: `${question.id}-${criteria.title}` } });
      column.push({ header: `${question.title} - ${translator.trans('Evidence')}`, key: `${question.id}-evidence` });
      columns.push(column);
    });

    columns = _.flatten(columns);
    worksheet.columns = columns;

    //Fill the worksheet by iterating sessions
    sessions.forEach(session=>{
      const row = {};
      columns.forEach(column => {
        const questionId = column.key.split('-')[0];
        const criteriaTitle = column.key.split('-')[1];

        //Column for userGroup
        if(column.key == 'userGroup') {
          row[column.key] = session.userGroupName;
          return;
        }
        //Column for evidence
        if(column.key.includes('-evidence')) {
          if(!session.evidence) return;
          const evidence = session.evidence.find(evidence => evidence.question == questionId);
          row[column.key] = evidence ? evidence.value : '';
          return;
        }
        //Column for userInfo
        if(column.key.includes('-userInfo')) {
          if(!session.userInfo) return;
          const info = session.userInfo.find(info => info.question == questionId);
          row[column.key] = info ? info.value : '';
          return;
        }
        //Column for scale/select value
        let answersBySession = answers.filter(answer => answer.session == session.id);
        const answer = answersBySession.find(answer => answer.question == questionId && answer.criteriaTitle == criteriaTitle);
        if(answer) row[column.key] = answer.criteria.type == 'select' ? answer.values['select'].description : answer.values['scale'].value;
      });
      worksheet.addRow(row);
    });
    //Style
    worksheet.getRow(1).font = { bold: true };
    worksheet.getRow(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };

    await workbook.xlsx.writeFile(fileName);
    return fileName;
  }
}

exports.model = Answer;