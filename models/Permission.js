const Model = require('./Model');
const mongodb = require('mongodb')
const MariaModel = require('./MariaModel')
const ManagedModel = require('./ManagedModel');
const { Logger, Utils } = require('common');

const { MariaDatabase } = require('database');
const tableName = 'permissions';
//DO NOT EVER CHANGE THIS ORDER OR REMOVE ANY ELEMENTS!!!!!
//add new permissions to the end. If you do have to refactor permission model you
//must update the masks in the database FIRST!
//each of these corresponds to a bit starting from the left
//e.g.
//0                 0                 0       1     1   = Number.parseInt('11', 2) = 3
//addUsersToGroups  surveyViewResults delete  edit  view
const PERMISSIONS = [
  'view',
  'edit',
  'delete',
  'surveyViewResults',
  'addUsersToGroups'
];

class Permission extends MariaModel {
  constructor(data) {
    super({});
    if(data) this.deserialize(data)
  }

  get permissions(){
    let aFromMask = [].fill(false, 0, PERMISSIONS.length);
    let nShifted = this.mask;
    let i = 0;
    while(nShifted){
      aFromMask[i] = (Boolean(nShifted & 1));
      nShifted >>>= 1;
      i++;
    }
    let map = {};
    PERMISSIONS.forEach((p,i)=>map[p] = aFromMask[i] || false);
    return map;
  }

  set permissions(mask){
    if(Utils.legitObject(mask)){
      for(let key of Object.keys(mask)){
        let index = PERMISSIONS.findIndex((val)=>val === key);
        if(index === -1) throw new Error(`${key} is not one of ${PERMISSIONS.join(', ')}`);
        if(mask[key]) this.mask |= 1 << index;
        else this.mask &= ~(1 << index);
      }
    } else if(typeof mask === 'number'){
      if(mask >> PERMISSIONS.length !== 0) throw new Error(`${mask} is out of range - only the first ${PERMISSIONS.length} bits can be set.`);
      this.mask |= mask;
    } else if(typeof mask === 'string'){
      this.mask |= Permission.decode(mask);
    } else {
      throw new Error(`Invalid mask: ${mask}`);
    }
  }

  //edit, view, delete
  static get ADMIN() { return 7; }

  deserialize(data) {
    if(data.targetModel && data.targetId && data.user){
      if(!global[data.targetModel] || !(global[data.targetModel].prototype instanceof ManagedModel)) throw new ArgumentError(`${data.targetModel} was not a ManagedModel or wasn't a registered model at all.`);
      this.target = new global[data.targetModel](data.targetId);
      this.user = new User(data.user);
      if(data.mask || data.mask === 0) this.mask = data.mask
      if(data.id) this.id = data.id;
      this.targetId = data.targetId;
      this.userId = data.user;
      this.targetModel = data.targetModel;
    }
  }

  serialize() {
    const json = super.serialize(true);
    json.user = this.userId || this.user.id;
    json.targetId = this.targetId || this.target.id;
    json.targetModel = this.targetModel || Utils.getObjectClassName(this.target);
    json.mask = this.mask;
    return json;
  }

  for(instance){
    if(!(instance instanceof ManagedModel)) throw new ValidationError(`${instance} was not a managed model`)
    this.target = instance;
    return this;
  }

  allow(...permissions){
    for(let permission of permissions){
      if(typeof permission !== 'number' && !PERMISSIONS.some(p=>p===permission)) throw new Error(`Can not add permission: not one of ${PERMISSIONS.join(', ')}`);
      this.permissions = permission;
    }
    return this;
  }

  deny(...permissions){
    for(let permission of permissions){
      this.permissions = { [permission]: false };
    }
    return this;
  }

  to(user){
    if(!Number.isInteger(user) && !(user instanceof User)) throw new ValidationError('User id not found!');
    this.user = user instanceof User ? user : new User(user);
    return this;
  }

  async save(db){
    if(!db) db = this.db;
    if(this.id){
      Permission.verify(this, true);
      let result = await db.update(this.datastore, this.serialize(), `id = ${this.id}`);
    } else {
      Permission.verify(this, false);
      const json = this.serialize(); //serialize for target id extraction, but don't match by mask(obviously)
      const existing = await Permission.find({targetId: json.targetId, targetModel: json.targetModel, user: json.user});
      if(existing){
         this.id = existing.id;
         return await this.save(db); //recurse
      }
      Logger.verbose(`Adding new permission to user with id ${json.user} for a ${json.targetModel} instance with id ${json.targetId} and the following rights: \n${JSON.stringify(this.permissions, 2)}`);
      let [id] = await db.insert(this.datastore, this.serialize());
      this.id = id;
    }
    return this;
  }

  async delete(db){
    if(!db) db = this.db;
    if(!this.id){
      return this;
    }
    let result = await db.delete(this.datastore, `id = ${this.id}`);
    if(result !== 1) Logger.error(`Something went wrong during delete - expected affected rows to be 1, but got ${result}`);
    return this;
  }

  async get(db){
    if(!db) db = this.db;
    let rows;
    if(!this.id){
      rows = await db.select(this.datastore, Utils.filter(this.serialize(), 'mask'));
      if(rows.length > 1) {
        Logger.error(`Tried to loose match a record ${this}, but the database returned more than 1 result. Ignoring.`)
        return this;
      }
    } else {
      rows = await db.select(this.datastore, 'id = ?', this.id);
    }
    if(rows.length === 0) {
      Logger.error(`Did not find record ${this} in the database.`);
    } else {
      this.deserialize(rows[0]);
    }
    return this;
  }

  static verify(instance, update){
    const json = instance.serialize(update);
    if(!json.user || !json.targetId || !json.targetModel) throw new ValidationError(`Permission ${instance} must have user target id and target model`);
    if(!global[json.targetModel] || !(global[json.targetModel].prototype instanceof ManagedModel)) throw new ValidationError(`${json.targetModel} was not a ManagedModel or not detected at global scope`);
  }

  /**
   * Returns the mask that has only the given permissions set to true
   * @param {String[]} permissions the string permissions to decode
   * @returns {Number} the given permission's representation as a bitwise mask.
   */
  static decode(...permissions){
    if(permissions.every(permission => typeof permission !== 'string')) throw new TypeError('Permission.decode only accepts strings');
    let mask = 0;
    for(let permission of permissions){
      const index = PERMISSIONS.findIndex((val)=>val===permission);
      if(index === -1) throw new Error(`${permission} is not one of ${PERMISSIONS.join(', ')}`);
      mask |= 1 << index;
    }
    return mask;
  }

  static filterMyPermissions(permissions, target, mask = null) {
    return permissions.filter((permission) => {
      if(!mask) return (permission.targetModel === target);
      return (permission.targetModel === target && permission.mask === mask);
    });
  }

  static get DB() { return MariaDatabase.default; }
  static set DB(newdb) { throw new Error(`${Utils.getObjectClassName(this)} does not support switching databases`); }
  static get DATASTORE() { return tableName; }
  static set DATASTORE(newDatastore) { throw new Error(`${Utils.getObjectClassName(this)} does not support switching datastore`); }
  set db(newdb) { return this._db = newdb }
  get db() { return this._db && !this._db.dead ? this._db : Permission.DB; }
  get datastore() { return Permission.DATASTORE; }
}

exports.model = Permission;