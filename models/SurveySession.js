const mongodb = require('mongodb')
const MariaModel = require('./MariaModel')
const { Logger, Utils } = require('common');

const { MariaDatabase } = require('database');
const tableName = 'surveysessions';

class SurveySession extends MariaModel {
  constructor(data){
    super(data);
    this.userInfo = [];
    this.evidence = [];
    this.deserialize(data);
  }

  get myUser(){
    return User.find({id: this.user.id || this.user}).then(user=>{
      this.user = user;
      return this.user;
    });
  }

  get mySurvey(){
    return Survey.find({id: this.survey.id || this.survey}).then(survey=>{
      this.survey = survey;
      return this.survey;
    });
  }

  get myUserGroup(){
    return UserGroup.find({id: this.userGroup.id || this.userGroup}).then((group) => {
      this.userGroup = group;
      return this.userGroup;
    });
  }

  deserialize(data){
    super.deserialize(data);
    for(let [name, val] of Utils.iterateObject(data)){
      switch(name){
        case 'evidence':
        case 'userInfo':
          if (Utils.containsValidJSON(val)) val = JSON.parse(val);
          if (val.constructor === [].constructor) this[name] = val;
          break;
        case 'open':
        case 'survey':
        case 'user':
        case 'userGroup':
        this[name] = val;
        break;
      }
    }
  }

  serialize(id, forSending){
    const json = super.serialize(id);
    json.open = this.open || false;
    json.survey = this.survey;
    json.userGroup = this.userGroup;
    if(this.user) json.user = this.user;
    json.userInfo = JSON.stringify(this.userInfo);
    json.evidence = JSON.stringify(this.evidence);

    if(forSending) {
      json.userInfo = this.userInfo;
      json.evidence = this.evidence;
    }

    return json;
  }

  static get DB(){ return MariaDatabase.default; }
  static set DB(newdb){ throw new Error(`${Utils.getObjectClassName(this)} does not support switching databases`); }
  static get DATASTORE(){ return tableName; }
  static set DATASTORE(newDatastore){ throw new Error(`${Utils.getObjectClassName(this)} does not support switching datastore`); }
  set db(newdb) { this._db = newdb; }
  get db(){ return this._db && !this._db.dead ? this._db : SurveySession.DB; }
  get datastore(){ return SurveySession.DATASTORE; }
}

exports.model = SurveySession;