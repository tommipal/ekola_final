const mongodb = require('mongodb')
const MariaModel = require('./MariaModel')
const ManagedModel = require('./ManagedModel');
const { Logger, Utils } = require('common');

const { MariaDatabase } = require('database');
const tableName = 'surveytemplates';

class SurveyTemplate extends ManagedModel {
  constructor(data){
    super(data);
    this.deserialize(data);
  }

  get myQuestions(){
    return QuestionBank.where(`template = ${this.id}`);
  }

  static async initialize(data){
    const criteria = {};
    for(let [key, val] of Utils.iterateObject(data)){
      switch(key){
        case 'title':
        case 'id':
        criteria[key] = val;
        break;
      }
    }
    const record = new SurveyTemplate(criteria);
    await record.get()
    record.deserialize(data);
    return record;
  }

  deserialize(data){
    super.deserialize(data);
    for(let [name, val] of Utils.iterateObject(data)){
      switch(name){
        case 'userInfo':
          if (typeof val === 'object') this[name] = val;
          else if (Utils.containsValidJSON(val)) this[name] = JSON.parse(val);
          break;
        case 'title':
        case 'description':
        this[name] = val;
        break
      }
    }
  }

  serialize(id){
    const json = super.serialize(id);
    json.title = this.title;
    if(this.description) json.description = this.description || 'No description';
    if(this.userInfo) json.userInfo = JSON.stringify(this.userInfo);
    return json;
  }

  static get DB(){ return MariaDatabase.default; }
  static set DB(newdb){ throw new Error(`${Utils.getObjectClassName(this)} does not support switching databases`); }
  static get DATASTORE(){ return tableName; }
  static set DATASTORE(newDatastore){ throw new Error(`${Utils.getObjectClassName(this)} does not support switching datastore`); }
  get db(){ return SurveyTemplate.DB; }
  get datastore(){ return SurveyTemplate.DATASTORE; }
}

exports.model = SurveyTemplate;