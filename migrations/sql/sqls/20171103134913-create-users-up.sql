CREATE TABLE `users` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`updatedAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`username` VARCHAR(255) NOT NULL,
	`password` VARCHAR(255) NOT NULL,
	`givenName` VARCHAR(255) NOT NULL,
	`familyName` VARCHAR(255) NOT NULL,
	`userGroup` INT(11) UNSIGNED NULL DEFAULT NULL,
	`email` VARCHAR(255) NOT NULL,
	`passwordResetToken` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `email` (`email`),
	UNIQUE INDEX `username` (`username`),
	INDEX `fk_users_usergroup` (`userGroup`),
	CONSTRAINT `fk_users_usergroup`
		FOREIGN KEY (`userGroup`) REFERENCES `usergroups` (`id`)
			ON UPDATE CASCADE
			ON DELETE SET NULL
)
CHARACTER SET 'utf8'
COLLATE 'utf8_general_ci'
ENGINE=InnoDB;