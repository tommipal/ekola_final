CREATE TABLE `surveysessions` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`updatedAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`open` TINYINT(1) NULL DEFAULT '0',
	`survey` INT(11) UNSIGNED NOT NULL,
	`user` INT(11) UNSIGNED NULL DEFAULT NULL,
	`userInfo` LONGTEXT NULL,
	`evidence` LONGTEXT NULL,
	/* this needs to be separate field for userless sessions or users that answer survey from different usergroups */
	`userGroup` INT(11) UNSIGNED NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `fk_surveysessions_user` (`user`),
	INDEX `fk_surveysessions_survey` (`survey`),
	INDEX `fk_surveysessions_usergroup` (`userGroup`),
	CONSTRAINT `fk_surveysessions_usergroup`
		FOREIGN KEY (`userGroup`) REFERENCES `usergroups` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `fk_surveysessions_survey`
		FOREIGN KEY (`survey`) REFERENCES `surveys` (`id`)
			ON UPDATE CASCADE
			ON DELETE CASCADE,
	CONSTRAINT `fk_surveysessions_user`
		FOREIGN KEY (`user`) REFERENCES `users` (`id`)
			ON UPDATE CASCADE
			ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;