CREATE TABLE `questionbank` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`updatedAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`title` VARCHAR(255) NOT NULL,
	`description` MEDIUMTEXT NULL,
	`position` INT(11) NULL DEFAULT NULL,
	`criteria` LONGTEXT NOT NULL,
	`tags` TEXT NULL,
	`template` INT(11) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `fk_questionbank_template` (`template`),
	CONSTRAINT `fk_questionbank_template`
		FOREIGN KEY (`template`) REFERENCES `surveytemplates` (`id`)
			ON UPDATE CASCADE
			ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
