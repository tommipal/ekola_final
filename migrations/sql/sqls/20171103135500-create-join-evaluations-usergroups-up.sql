CREATE TABLE `join_evaluations_usergroups` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`usergroup_id` INT(11) UNSIGNED NOT NULL,
	`evaluation_id` INT(11) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `id` (`id`),
  CONSTRAINT `fk_join_evaluations_usergroups_usergroup_id`
    FOREIGN KEY (`usergroup_id`) REFERENCES `usergroups` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT `fk_join_evaluations_usergroups_evaluation_id`
    FOREIGN KEY (`evaluation_id`) REFERENCES `evaluations` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;