CREATE TABLE `permissions` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`updatedAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`targetId` INT(11) UNSIGNED NOT NULL,
	`targetModel` VARCHAR(255) NOT NULL,
	`user` INT(11) UNSIGNED NOT NULL,
	`mask` INT(11) UNSIGNED NOT NULL DEFAULT 3,
	PRIMARY KEY (`id`),
	INDEX `fk_permissions_user` (`user`),
	/* prevent duplicate for the same user-target pair - the record mask should change instead */
	UNIQUE INDEX `comp_targetId_targetModel_user` (`targetId`, `targetModel`, `user`),
	CONSTRAINT `fk_permissions_user` 
		FOREIGN KEY (`user`) REFERENCES `users` (`id`) 
			ON UPDATE CASCADE 
			ON DELETE CASCADE	
)
CHARACTER SET 'utf8'
COLLATE 'utf8_general_ci'
ENGINE=InnoDB;