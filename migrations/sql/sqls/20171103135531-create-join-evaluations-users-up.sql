CREATE TABLE `join_evaluations_users` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) UNSIGNED NOT NULL,
	`evaluation_id` INT(11) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `id` (`id`),
  CONSTRAINT `fk_join_users_evaluations_evaluation_id` 
    FOREIGN KEY (`evaluation_id`) REFERENCES `evaluations` (`id`)
      ON DELETE CASCADE 
      ON UPDATE CASCADE,
  CONSTRAINT `fk_join_users_evaluations_user_id` 
    FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
      ON DELETE CASCADE 
      ON UPDATE CASCADE
)
COLLATE='latin2_general_ci'
ENGINE=InnoDB;