const Router = require('koa-router');
const Koa = require('koa');
const { Utils, Logger } = require('common');
const controllers = Utils.requireNamespace('controllers', 'controller');
/**
 * One interesting idea is to create controller instances for every user session
 * In this case controllers must export a function that takes ctx and next, like middleware,
 * which checks for session/token hash in ctx and if found, either retrieves or creates an
 * instance for it, that is kept between requests. That is, however, against the stateless
 * conventions of SPAs, so not implemented here, though architecture supports it.
 * 
 * If that is done, the the class hierarchy would actually make more sense. Right now
 * it is nothing more than an ogranizational mechanism.
 */

 //Probably better to go with a separate router per controller approach - better fits the loose coupling architecture
 //otherwise pass the router to the function instead
exports.load = function(app){
  if(!(app instanceof Koa)) throw new Error('Please provide your app reference to controllers so they can bootstrap their routes');
  for (let [name, controller] of Utils.iterateObject(controllers)){    
    Logger.info(`Loading ${name} with ${controller.router.stack.length} routes`);
    //TODO: check that there are no route collisions. How to get a list of all bound routes from the router instance?
    app.use(controller.router.routes());
    app.use(controller.router.allowedMethods());
  }
}
/*
exports.load = function(router){
  if(!(router instanceof Router)) throw new TypeError('Router must be a koa router');    
  
  Logger.info('Loading common routes');
  Controller.routes(router);
  Logger.info('Loading api routes');
  ApiController.routes(router);

  for (let [name, controller] of Utils.iterateObject(controllers)){  
    Logger.info(`Loading ${name}`);
    controller.routes(router);
  }
  return router;
}*/