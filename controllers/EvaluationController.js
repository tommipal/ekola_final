const Controller = require('./Controller');
const { MariaDatabase, MongoDatabase } = require('database');
const { Authenticator } = require('middleware');
const Router = require('koa-router');
const { Utils } = require('common');
const Translator = require('../client/scripts/translator').Translator;

const router = new Router();

class EvaluationController extends Controller {

  static async initNew(ctx, next) {
    const translator = new Translator(ctx.session.locale || 'en');
    return ctx.view('evaluation/new-evaluation.ejs', {user:ctx.session.user, title: translator.trans('New evaluation')});
  }
  static async new(ctx, next) {
    if(!ctx.params.name) throw new Error('Evaluation needs a name!');
    const userGroups = ctx.params.groups.split(',');
    if(!userGroups.length || !userGroups[0]) throw new Error('Evaluation needs at least one group');
    const user = await User.find(ctx.session.user.id);
    const evaluation = await new Evaluation({ name: ctx.params.name }).allow(user, Permission.ADMIN).save();
    await evaluation.addUserGroups(userGroups);
    return ctx.redirect(`/evaluation/${evaluation.id}/edit`);
  }

  /**
   * return evaluation and all of it's userGroups as well as all the users who have edit rights to it
   */
  static async edit(ctx, next){
    const translator = new Translator(ctx.session.locale || 'en');
    const evaluation = await Evaluation.find({id: ctx.params.id});
    await evaluation.myUserGroups;
    await evaluation.myPermissions;

    const users = await evaluation.myUsers;
    for (const user of users) user.userGroup = await user.myUserGroup;

    return ctx.view('evaluation/edit-evaluation.ejs', { route: 'edit', evaluation, users, userGroups: evaluation.userGroups, title: translator.trans('Edit evaluation') });
  }

  static async save(ctx, next){
    const usersToAdd = ctx.params.users.add                 || [];
    const usersToRemove = ctx.params.users.remove           || [];
    const usersToVoid = ctx.params.users.void               || [];
    const usersToAdmin = ctx.params.users.admin             || [];
    const userGroupsToAdd = ctx.params.userGroups.add       || [];
    const userGroupsToRemove = ctx.params.userGroups.remove || [];

    const evaluation = await Evaluation.find({id: ctx.params.id});

    if(usersToAdd.length > 0)     await evaluation.addUsers(usersToAdd, Permission.decode('view'));
    if(usersToRemove.length > 0)  await evaluation.removeUsers(usersToRemove);
    if(usersToVoid.length > 0)    await Evaluation.voidUsers(evaluation.id, usersToVoid);
    if(usersToAdmin.length > 0)   for (let user of usersToAdmin) evaluation.allow(new User(Number(user)), Permission.ADMIN);

    if(userGroupsToAdd.length > 0) {
      await evaluation.addUserGroups(userGroupsToAdd);
      for(const id of userGroupsToAdd) {
        const supervisors = (await new UserGroup(parseInt(id)).findSupervisors(Permission.ADMIN)).filter(user => user.id !== ctx.session.user.id);
        evaluation.addUsers(supervisors.map(user => user.id), Permission.ADMIN);
      }
    }
    if(userGroupsToRemove.length > 0) {
      await evaluation.myUserGroups;

      for(const id of userGroupsToRemove) {
        const supervisors = (await new UserGroup(parseInt(id)).findSupervisors(Permission.ADMIN)).filter(user => user.id !== ctx.session.user.id);
        for(const user of supervisors) {
          const usersGroups = await user.myGroups;
          //If user has no other groups in the evaluation, delete their permission
          if(!usersGroups.some((ug) => evaluation.userGroups.some((eg) => eg.id == ug.id && ug.id != id))){
            await Permission.delete({targetId: evaluation.id, targetModel: 'Evaluation', user: user.id });
          }
        }
      }
      await evaluation.removeUserGroups(userGroupsToRemove);
    }

    if(ctx.params.name) {
      evaluation.name = ctx.params.name;
    }
    await evaluation.save();

    const users = await evaluation.myUsers;
    for (const user of users) user.userGroup = await user.myUserGroup;
    return ctx.json({ evaluation, users });
  }

  static async removeUsers(ctx, next) {
    const users = ctx.params.users;
    if(users.length > 0) await Evaluation.removeUsers(ctx.params.id, users);
    return ctx.json({ route: 'removeUsers' });
  }
  static async leave(ctx, next) {
    const user = ctx.session.user;
    await Evaluation.removeUsers(ctx.params.id, user);
    return ctx.json({ route: 'leaveEvaluation' });
  }

  static async removeUserGroup(ctx, next){
    const userGroups = ctx.params.userGroups;
    if(userGroups.length > 0) await Evaluation.removeUserGroups(ctx.params.id, userGroups);
    return ctx.json({route: 'removeUserGroups'});
  }

  static async userGroups(ctx, next){
    const evaluation = await Evaluation.find({id: ctx.params.id});
    if(!evaluation) throw new Error(`No evaluation with id ${ctx.params.id}`);
    switch(ctx.params.filter){
      case 'participating':
        const participating = await evaluation.myUserGroups;
        Utils.sortArrayOfObjects(participating, 'name');
        return ctx.json({userGroups: participating});
      case 'notparticipating':
        const groupsQuery = UserGroup.permitted(new User(ctx.session.user.id));
        groupsQuery.join('INNER JOIN', UserGroup.DATASTORE, 'id', MariaDatabase.JOIN_EVALUATIONS_USERGROUPS, 'userGroup_id')
        groupsQuery.join('OUTER JOIN', MariaDatabase.JOIN_EVALUATIONS_USERGROUPS, 'evaluation_id', Evaluation.DATASTORE, 'id')
        const [data] = await UserGroup.query(groupsQuery);
        const notparticipating = data.map(dd=>new UserGroup(dd));
        Utils.sortArrayOfObjects(notparticipating, 'name');
        return ctx.json({route: 'notParticipating', userGroups: notparticipating});
      default:
        throw new Error('Unknown filter, please specify a filter when listing userGroups of this evaluation. Allowed values are: /participating and /notparticipating');
    }
  }

  // static async list(ctx, next){
  //   ctx.json({route: 'list'});
  // }
  static async delete(ctx, next) {
    const evaluation = await Evaluation.find({id: ctx.params.id});
    const groups = await evaluation.myUserGroups;
    for (const group of groups) {
      await Link.delete({userGroup: group.id});
    }
    if(!evaluation) throw new Error(`Requested evaluation with id ${ctx.params.id} doesn't exist!`);
    await evaluation.delete();

    return ctx.json(evaluation);
  }

  static get router(){
    if(router.stack>0) return router;
    router.prefix('/evaluation');

    router.use(Authenticator.login);

    router.post('newevaluation',      '/new',                                             this.new);
    router.post('saveevaluation',     '/:id/save',              Authenticator.canEdit,    this.save);
    router.post('removeusers',        '/:id/removeUsers',       Authenticator.canEdit,    this.removeUsers);
    router.post('removeusers',        '/:id/leave',                                       this.leave);
    router.post('removeuserGroup',    '/:id/removeUserGroup',   Authenticator.canEdit,    this.removeUserGroup);
    router.post('deleteevaluation',   '/:id/delete',            Authenticator.canDelete,  this.delete);
    router.get('listgroups',          '/:id/userGroups/:filter',                          this.userGroups);
    // router.get('listevaluations',     '/list',                                            this.list);
    //views
    router.get('newevaluation',      '/new',                                              this.initNew);
    router.get('editevaluation',      '/:id/edit',              Authenticator.canEdit,    this.edit);

    return router;
  }
}

exports.controller = EvaluationController;