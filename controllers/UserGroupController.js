const Controller = require('./Controller');
const { MariaDatabase, MongoDatabase } = require('database');
const { Authenticator } = require('middleware');
const Router = require('koa-router');
const { Utils } = require('common');
const Translator = require('../client/scripts/translator').Translator;

const router = new Router();

class UserGroupController extends Controller {

  //return userGroup's info
  static async edit(ctx, next){
    const translator = new Translator(ctx.session.locale || 'en');
    const page = ctx.params.page || 1;
    const pageLimit = ctx.params.pageLimit || 10;

    const userGroup = await UserGroup.find({id: ctx.params.id});
    const {users, pageCount} = await User.paginationSearch(page, pageLimit, userGroup.id);
    let evaluations = await userGroup.myEvaluations;
    await new User(ctx.session.user).attachPermissions(evaluations, userGroup);
    evaluations = evaluations.filter(evaluation => evaluation.canEdit);
    await userGroup.attachRole(users);

    if(ctx.wantsJSON()) {
      return ctx.json({userGroup, users, evaluations, pageCount, canEdit: userGroup.canEdit});
    }
    return ctx.view('userGroup/edit-group.ejs', {userGroup, users, evaluations, title: translator.trans('Manage group')});
  }

  static async new(ctx, next){
    const params = ctx.params;
    if(!params.name) throw new Error('UserGroups must have a name!');
    const userGroup = new UserGroup(params);
    userGroup.allow(new User(ctx.session.user), Permission.ADMIN);
    await userGroup.save();
    return ctx.json({userGroup});
  }

  /**
   * save changes to userGroup and add permissions to given users so that they will be allowed to change to this userGroup if they weren't before
   */
  static async save(ctx, next){
    let params = ctx.params;
    if(params.name && params.description) await UserGroup.update({name: params.name, description: params.description}, `id = ?`, ctx.params.id);
    let usersToAdd = ctx.params.users.add       || [];
    let usersToAdmin = ctx.params.users.admin   || [];
    let usersToRemove = ctx.params.users.remove || [];
    if(usersToAdd.length > 0 || usersToRemove.length > 0 || usersToAdmin.length > 0) {
      const userGroup = await UserGroup.find({ id: ctx.params.id });
      if(!!usersToAdd.length) for (let user of usersToAdd) userGroup.allow(new User(user), 'view');
      if(!!usersToAdmin.length) for (let user of usersToAdmin) userGroup.allow(new User(user), Permission.ADMIN);
      await userGroup.save();

      if(!!usersToRemove.length) {
        await UserGroup.removeUsers(userGroup, usersToRemove);
        ctx.session.user = await User.find(ctx.session.user.id);
        if(ctx.session.user.userGroup !== userGroup.id) return ctx.json({redirect: '/'});
      }
    }
    return ctx.json({route: 'save'});
  }

  static async search(ctx, next){
    let search = ctx.params.search || ctx.request.search;
    if (!search) return ctx.json({ users: await User.where('userGroup = ?', ctx.params.id) });
    search = `%${search.slice(1)}%`;
    return ctx.json({users: await User.where('(username LIKE ? OR email LIKE ?) AND userGroup = ?', search, search, ctx.params.id)});
  }

  static async delete(ctx, next){
    let deleted = await UserGroup.delete({id: ctx.params.id});
    return ctx.json({route: 'delete userGroup', deleted});
  }

  static async find(ctx, next) {
    if(!ctx.params.query || ctx.params.query === 'all') {
      const groups = await UserGroup.where();
      Utils.sortArrayOfObjects(groups, 'name');
      return ctx.json(groups);
    }

    switch(ctx.params.query) {
      case 'userGroupsNotInEvaluation':
        const userGroupsNotInEval = await UserGroup.where(`id NOT IN (SELECT userGroup_id FROM ${MariaDatabase.JOIN_EVALUATIONS_USERGROUPS} WHERE evaluation_id = ${ctx.params.evaluation})`);
        Utils.sortArrayOfObjects(userGroupsNotInEval, 'name');
        return ctx.json(userGroupsNotInEval);
      break;
      case 'userGroupsInEvaluation':
        const evaluationId = Number(ctx.params.evaluationId);
        const surveyId = Number(ctx.params.surveyId);

        const evaluation = new Evaluation(evaluationId);
        const userGroupsInEval = await evaluation.myUserGroups;

        await Survey.update({evaluation: evaluationId}, `id = ${surveyId}`);
        await Link.delete({survey: surveyId});

        Utils.sortArrayOfObjects(userGroupsInEval, 'name');
        return ctx.json(userGroupsInEval);
      break;
      default:
        throw new Error(`Unrecognized query '${ctx.params.query}'`);
    }
  }

  static get router(){
    if(router.stack.lenth>0) return router;
    router.prefix('/userGroup');

    router.use(Authenticator.login);

    router.post('deletegroup',     '/:id/delete', Authenticator.canDelete,this.delete);
    router.post('savegroup',       '/:id/save',   Authenticator.canEdit,  this.save);
    router.post('createnewgroup',  '/new',                                this.new);
    router.get('searchgroupusers', '/:id/search', Authenticator.canEdit,  this.search);
    router.get('findgroups',       '/find',                               this.find);
    //views
    router.get('editgroup',        '/:id/edit',   Authenticator.canView,  this.edit);

    return router;
  }
}

exports.controller = UserGroupController;