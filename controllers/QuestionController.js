const Controller = require('./Controller');
const { MariaDatabase, MongoDatabase } = require('database');
const { Authenticator, Responder } = require('middleware');
const Router = require('koa-router');

const router = new Router();

class QuestionController extends Controller {
  static async questionBankQuestions(ctx, next) {
    const pageLimit = ctx.params.pageLimit || 5;
    const page = ctx.params.page || 1;
    const searchParam = ctx.params.search;
    const {questions, pageCount} = await QuestionBank.paginationSearch(pageLimit, page, searchParam);

    ctx.json({questions, pageCount});
  }

  static get router() {
    if (router.stack.length > 0) return router;
    router.prefix('/question');

    router.use(Authenticator.login);

    router.get('questionbankquestions', '/bank', this.questionBankQuestions);
    return router;
  }
}

exports.controller = QuestionController;