const Controller = require('./Controller');
const { MariaDatabase, MongoDatabase } = require('database');
const { Authenticator, Responder } = require('middleware');
const Router = require('koa-router');
const { Utils, Logger, Email, config } = require('common');
const Translator = require('../client/scripts/translator').Translator;
const bcrypt = require('bcryptjs');
const moment = require('moment');
const fs = require('fs');
const ejs = require('ejs');
const path = require('path');

const router = new Router();

class UserController extends Controller {

  static async signup(ctx, next) {
    const params = ctx.params;
    // determine if creating a new userGroup or selecting an existing one
    const userGroup = params.userGroup.id ? params.userGroup.id : params.userGroup;
    try {
      let user = await User.create(params, userGroup);
      return ctx.redirect('/');
    } catch (error) {
      const translator = new Translator(ctx.session.locale || 'en');
      ctx.session.flash = error.message;
      const userGroups = await UserGroup.where();
      return ctx.view('/user/signup.ejs', { userGroups, title: translator.trans('Sign up'), user: params });
    }
  }

  static async signupView(ctx, next){
    const translator = new Translator(ctx.session.locale || 'en');
    const userGroups = await UserGroup.where();
    Utils.sortArrayOfObjects(userGroups, 'name');
    return ctx.view('/user/signup.ejs', {userGroups, title: translator.trans('Sign up')});
  }

  static async login(ctx, next) {
    //if we use jwt, this should return a signed token.
    const params = ctx.params;
    if (!params.username || !params.password) throw new RequestError('Username or password not given');
    try {
      let user = await User.match(params.username, params.password);
      ctx.session.user = user;
      if (!ctx.session.locale) ctx.session.locale = 'en';
      ctx.session.save();
      return ctx.redirect('/');
    } catch (error) {
      const translator = new Translator(ctx.session.locale || 'en');
      ctx.session.flash = error.message;
      return ctx.view('user/login.ejs', { title: translator.trans('Log in'), username: params.username });
    }
  }

  static async loginView(ctx, next) {
    const translator = new Translator(ctx.session.locale || 'en');
    if(ctx.session.user) {
      return ctx.redirect('/user/profile');
    }
    return ctx.view('user/login.ejs', {title: translator.trans('Log in')});
  }

  static async logout(ctx, next){
    ctx.session = null;
    return ctx.redirect('/');
  }

  static async sendToken(ctx, next){
    const params = ctx.params;
    try {
      if(!params.username) throw new Error('Username not given!');
      const user = await User.find('? IN (username, email)', params.username);
      if(!user) throw new Error(`Username or email does not exist`);
      user.passwordResetToken = Utils.randomString(40);
      await user.save();
      try {
        const emailFile = fs.readFileSync(path.join(config.appRoot, 'views', 'email', 'password-reset.ejs'),'utf-8');
        const emailHtml = ejs.render(emailFile, {host: ctx.host, token: user.passwordResetToken, username: params.username, translator: new Translator(ctx.session.locale || 'en') });
        await Email.sendEmail([user.email], 'Password reset token', null, emailHtml);
      } catch (err){
        Logger.error(err);
        Logger.warn('Mail failed, resetting token!');
        user.passwordResetToken = null;
        await user.save();
        throw new Error('Error sending email!');
      }
      return ctx.redirect('/');
    } catch (error) {
      const translator = new Translator(ctx.session.locale || 'en');
      ctx.session.flash = error.message;
      return ctx.view('user/sendresettoken.ejs', {username: params.username, title: translator.trans('Reset password')})
    }
  }

  static async sendTokenView(ctx, next){
    const translator = new Translator(ctx.session.locale || 'en');
    return ctx.view('user/sendresettoken.ejs', {title: translator.trans('Reset password')})
  }

  static async resetPassword(ctx,next){
    const params = ctx.params;
    try {
      if(!params.token) throw new Error('No reset token!');
      if(!params.username) throw new Error('No username provided!');
      if(!params.password || !params.confirmation) throw new Error('Password or confirmation not given');
      if(params.password !== params.confirmation) throw new Error('Password and confirmation do not match!');
      const user = await User.find('? IN (username)', params.username);
      if(!user) throw new Error('Username does not exist');
      const token = params.token.replace(new RegExp(`[^${Utils.RANDOM_STRING_CHAR_POOL}]`), ''); //sanitize - remove all characters not in used during generation
      if(user.passwordResetToken !== token) throw new Error('Invalid reset token!');
      user.passwordResetToken = null;
      user.password = await bcrypt.hash(params.password, 10);
      await user.save();
      // ctx.flash('Password has been reset!');
      return ctx.redirect('/');
    } catch (error) {
      const translator = new Translator(ctx.session.locale || 'en');
      ctx.session.flash = error.message;
      return ctx.view('user/resetpassword.ejs', {token: params.token, username: params.username, title: translator.trans('Reset password')});
    }
  }

  static async resetPasswordView(ctx, next){
    const translator = new Translator(ctx.session.locale || 'en');
    const params = ctx.params;
    if(!params.token) throw new Error('No reset token!');
    return ctx.view('user/resetpassword.ejs', {token: params.token, title: translator.trans('Reset password')});
  }

  static async changePassword(ctx, next){
    const params = ctx.params;
    try {
      if(params.password !== params.confirmation) throw new Error('Password and confirmation do not match!');
      const user = await User.match(ctx.session.user.username, params.oldpassword);
      if(!user) throw new Error('Old password incorrect!');
      user.password = await User.hashpwd(params.password);
      await user.save();
      return ctx.redirect('/');
    } catch(error) {
      const translator = new Translator(ctx.session.locale || 'en');
      ctx.session.flash = error.message;
      return ctx.view('user/changepassword.ejs', {title: translator.trans('Change password')});
    }
  }

  static async changePasswordView(ctx, next){
    const translator = new Translator(ctx.session.locale || 'en');
    return ctx.view('user/changepassword.ejs', {title: translator.trans('Change password')});
  }

  static async changeUserGroup(ctx, next){
    const user = await User.find(ctx.session.user.id);
    const group = await UserGroup.find(ctx.params.group);
    if(!group) throw new Error(`No userGroup by that id found (${ctx.params.group})`);
    user.userGroup = group.id;
    await user.save();
    ctx.session.user = user.serialize(true);
    ctx.session.save();
    ctx.redirect('/');
  }

  static async profileView(ctx, next){
    const translator = new Translator(ctx.session.locale || 'en');
    const user = await User.find(ctx.session.user.id);
    const group = await user.myUserGroup;
    const groups = await user.myGroups;
    Utils.sortArrayOfObjects(groups, 'name');
    const supervisedEvaluations = await user.mySupervisedEvaluations;
    const evaluations = await user.myEvaluations
    const permissions = await user.myPermissions;
    const sessions = await user.mySessions;
    await Promise.all(sessions.map(session => session.mySurvey));
    let supervisedSurveys = [];
    let permittedUserGroups = [];

    const surveyPermissions = await Permission.filterMyPermissions(permissions, 'Survey', 7).map((perm)=>perm.targetId);
    if(surveyPermissions.length > 0) supervisedSurveys = await Survey.where(`id IN (${surveyPermissions})`);

    const userGroupPermissions = await Permission.filterMyPermissions(permissions, 'UserGroup').map((perm)=>perm.targetId);
    if(userGroupPermissions.length > 0) permittedUserGroups = await UserGroup.where(`id IN (${userGroupPermissions})`);
    const templates = await user.myTemplates;
    for (const survey of supervisedSurveys) {
      survey.ongoingSession = sessions.some((session) => session.survey.id === survey.id && session.open);
      survey.evaluation = await survey.myEvaluation;
      if(survey.evaluation) {
        survey.evaluation.userGroups = await survey.evaluation.myUserGroups;
        survey.canAnswer = survey.evaluation.userGroups.some((g) => g.id === user.userGroup) && evaluations.some((e) => e.id === survey.evaluation.id);
      } else {
        survey.canAnswer = false;
      }
    }

    const json = {
      user,
      sessions,
      evaluations,
      supervisedSurveys,
      supervisedEvaluations,
      permittedUserGroups,
      userGroup: group ? group.serialize(true) : null,
      groups,
      templates,
      moment,
      title: translator.trans('Profile'),
    }
    return ctx.view('/user/profile.ejs', json);
  }

  static async search(ctx, next){
    const search = ctx.request.search;
    //we can only search among users where we have rights towards their userGroup
    const user = await User.find(ctx.session.user.id);
    if(!search){
      return ctx.json(
        await User.query(user.peers)
          .then(peers=>peers.map(pp=>new User(pp[User.DATASTORE])))
      );
    } else {
      const peers = user.peers;
      //yes, my eyes bleed too. If you have an idea of how to make it look nice without making it too clever, please do!
      if(search.startsWith('!')){
        const keyword = `%${search.substring(1)}%`;
        peers.where(`${User.DATASTORE}.username NOT LIKE ? OR ${User.DATASTORE}.email NOT LIKE ? OR ${UserGroup.DATASTORE}.name NOT LIKE ? OR ${Evaluation.DATASTORE}.name NOT LIKE ?`)
             .values(...Array.from(Array(4), () => keyword));
      } else {
        const keyword = `%${search.substring(1)}%`;
        peers.where(`${User.DATASTORE}.username LIKE ? OR ${User.DATASTORE}.email LIKE ? OR ${UserGroup.DATASTORE}.name LIKE ? OR ${Evaluation.DATASTORE}.name LIKE ?`)
             .values(...Array.from(Array(4), () => keyword));
      }
      const users = await User.query(peers).then(data=>data[0].map(dd => new User(dd[User.DATASTORE])));
      return ctx.json(users);
    }
  }

  static async find(ctx, next) {
    if(!ctx.params.query || ctx.params.query === 'all') return ctx.json(await User.where());

    switch(ctx.params.query) {
      case 'usersNotInGroup':
        const users = await User.where(`id NOT IN (SELECT user FROM ${Permission.DATASTORE} WHERE targetModel = 'UserGroup' AND targetId = ${ctx.params.userGroup})`);
        return ctx.json(users);
      break;
      default:
        throw new Error(`Unrecognized query '${ctx.params.query}'`);
    }
  }

  static async info(ctx, next){
    const id = ctx.params.id;
    const user = await User.find(id);
    if(!user) return ctx.json({ error: `User with id ${id} not found, perkele!` });
    const group = await user.myUserGroup;
    const evaluations = await user.myEvaluations;
    const userGroups = await user.myGroups;
    const supervisedEvaluations = await user.mySupervisedEvaluations;
    const json = {
      user: {
        id: user.id,
        username: user.username,
        givenName: user.givenName,
        familyName: user.familyName,
        email: user.email,
        userGroup: group ? group.serialize(true) : null
      },
      userGroups: userGroups,
      evaluations: evaluations,
      supervisedEvaluations: supervisedEvaluations,
      //any other info goes here
      joined: user.createdAt
    }
    return ctx.json(json);
  }

  static async changeLocale(ctx, next) {
    const locale = ctx.params.locale;
    if(!Translator.supportedLocales.includes(locale)) throw new Error(`'${locale}' is not a supported locale!`);

    ctx.session.locale = locale;

    ctx.redirect('back', '/');
  }

  /**
   * Saves changes to user's personal information.
   */
  static async save(ctx, next) {
    const user = await User.find(ctx.session.user.id);
    user.email = ctx.params.email;
    user.givenName = ctx.params.givenName;
    user.familyName = ctx.params.familyName;
    try {
      await user.save();
      ctx.session.user = user;
    } catch (error) {
      ctx.session.flash = error.message;
    }
    ctx.redirect('/');
  }

  static get router(){
    if(router.stack.length > 0) return router;
    router.prefix('/user');

    router.post('login',              '/login',                                      this.login);
    router.post('signup',             '/signup',                                     this.signup);
    router.post('changeusergroup',    '/changeUserGroup',       Authenticator.login, this.changeUserGroup);
    router.post('changepassword',     '/change-password',       Authenticator.login, this.changePassword);
    router.post('sendresettoken',     '/password/reset/send',                        this.sendToken);
    router.post('resetpassword',      '/password/reset/:token',                      this.resetPassword);
    router.get('logout',              '/logout',                Authenticator.login, this.logout);
    router.get('search',              '/search',                Authenticator.login, this.search); //what kind of protection to put on this one?
    router.get('find',                '/find',                  Authenticator.login, this.find); //what kind of protection to put on this one?
    router.get('showuserinfo',        '/:id/info',                                   this.info);
    router.get('changelocalization',  '/locale/change/:locale',                      this.changeLocale);
    router.post('savepersonalinfo',   '/save',                  Authenticator.login, this.save);
    //Views
    router.get('profileview',         '/profile',               Authenticator.login, this.profileView);
    router.get('loginview',           '/login',                                      this.loginView);
    router.get('signupview',          '/signup',                                     this.signupView);
    router.get('changepasswordview',  '/password/change',       Authenticator.login, this.changePasswordView);
    router.get('sendtokenview',       '/password/reset/send',                        this.sendTokenView);
    router.get('resetpasswordview',   '/password/reset/:token',                      this.resetPasswordView);
    return router;
  }
}

exports.controller = UserController;