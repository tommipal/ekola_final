const Controller = require('./Controller');
const { MariaDatabase, MongoDatabase } = require('database');
const { Authenticator } = require('middleware');
const Router = require('koa-router');
const send = require('koa-send');
const _ = require('lodash');
const moment = require('moment');
const Translator = require('../client/scripts/translator').Translator;


const router = new Router();

class AnswerController extends Controller {

  // Saves given answers of this survey session
  static async save(ctx, next){
    let session = ctx.params.sessionId;
    const currentUser = ctx.session.user ? ctx.session.user.id : null;
    const answers = ctx.params.answers;
    await Answer.save(session, currentUser, answers);
    await SurveySession.update({userInfo: ctx.params.userInfo, evidence:ctx.params.evidence}, `id = ${session}`);
    return ctx.json({success: true});
  }

  // deletes session and answers for a session without a user.
  // deletes session if it holds no answers for a session with a user.
  static async cancel(ctx, next){
    let target = '/';
    let session = await SurveySession.find(ctx.params.session);

    if (!session) Logger.info({route: `cancel (didn't find session with id ${ctx.params.sessionId}`});
    else if(!session.user && ctx.session.surveySession === session.id) {
      await Answer.delete({session: session.id});
      await session.delete();
    }
    else if(ctx.session.user && session.user === ctx.session.user.id) {
      if(await Answer.count({session: session.id}) === 0) await session.delete();
      const referrer = ctx.params.referrer;
      if(referrer && referrer.includes('/survey/list')) target = referrer;
    }
    ctx.json({target});
  }

  // Returns results of a survey
  static async results(ctx, next){
    const translator = new Translator(ctx.session.locale || 'en');

    const surveyId = parseInt(ctx.params.surveyId);
    let userGroups = await Answer.aggregate([
      { $match: { 'survey': surveyId } },
      { $group: { _id: { userGroup: '$userGroup' } } }
    ]);
    userGroups = userGroups.map(userGroup => userGroup._id.userGroup);
    if(userGroups.length > 0) userGroups = await UserGroup.where(`id IN (${userGroups.join(',')})`);
    const survey = await Survey.find(surveyId);
    if(!survey) throw new Error(`Requested survey with id ${ctx.params.id} doesn't exist`) //let authenticator take care of this
    return ctx.view('survey/results.ejs', {userGroups, questions:await survey.myQuestions, survey, title: translator.trans('Results')});
  }

  // Exports results of a survey to excel file
  static async excel(ctx, next){
    const surveyId = parseInt(ctx.params.surveyId);
    const survey  = await Survey.find(surveyId);
    const fileName = 'results.xlsx';
    await Answer.excel(surveyId, fileName, ctx.session.locale);
    ctx.attachment(`${survey.title}-results-${moment().format('YYYY-MM-DD')}.xlsx`);

    return await send(ctx, `./${fileName}`);
  }

  // Returns answers of a survey session
  static async answers(ctx, next){
    const sessionId = ctx.params.sessionId;
    const session = await SurveySession.find(sessionId);
    const answers = await Answer.where({session: parseInt(sessionId)});
    const survey = await session.mySurvey;
    await survey.myQuestions;
    return ctx.json({session: session, answers: answers, survey: survey, questions: survey.questions });
  }

  // Returns average answers of an userGroup
  static async averages(ctx, next){
    const surveyId = parseInt(ctx.params.surveyId);

    const averageAnswers = await Answer.getAverageValues(surveyId);
    const answers = await Answer.where({survey: surveyId});
    let sessions = await SurveySession.where({survey: surveyId});
    sessions = sessions.filter(session => _.some(answers, answer => answer.session == session.id));
    const survey = await Survey.find(surveyId);

    ctx.json({averageAnswers, answers, sessions, questions: await survey.myQuestions });
  }

  static get router(){
    if(router.stack.length>0) return router;
    //all routes start with /answer
    router.prefix('/answer');

    router.get('cancelanswering',    '/cancel',                                            this.cancel);
    router.all( 'viewsessionanswers','/:sessionId',        Authenticator.canAccessAnswers, this.answers);
    router.post('saveanswers',       '/save/:sessionId',   Authenticator.canSaveAnswers,   this.save);

    //protect all routes behind login
    router.use(Authenticator.login);
    router.get( 'viewresults',       '/results/:surveyId',  Authenticator.canViewResults,   this.results);
    router.get( 'exporttoexcel',     '/excel/:surveyId',    Authenticator.canViewResults,   this.excel);
    router.get( 'viewaverages',      '/averages/:surveyId', Authenticator.canViewResults,   this.averages)

    return router;
  }
}

exports.controller = AnswerController;