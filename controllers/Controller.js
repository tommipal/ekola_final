class Controller {
  constructor(){
    throw new Error('Controllers are static in this implementation and can\'t be instantiated');
  }
  static routes(){
    throw new Error('Not implemented');
  }
}

module.exports = Controller;