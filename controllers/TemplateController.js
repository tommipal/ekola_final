const Controller = require('./Controller');
const { MariaDatabase, MongoDatabase } = require('database');
const { Authenticator } = require('middleware');
const Router = require('koa-router');
const { Utils, Logger, Email, config } = require('common');

const router = new Router();

class TemplateController extends Controller {

	static async delete(ctx, next) {
    const template = await SurveyTemplate.find(ctx.params.id);
    if(!template) throw new Error(`Requested template with id ${ctx.params.id} doesn't exist!`);
    const deleted = await template.delete();

    return ctx.json(deleted);
  }

	static get router() {
		if (router.stack.length > 0) return router;
		router.prefix('/surveytemplate');

		router.use(Authenticator.login);

		router.post('deletetemplate', '/:id/delete', Authenticator.canDelete, this.delete);

		return router;
	}
}

exports.controller = TemplateController;