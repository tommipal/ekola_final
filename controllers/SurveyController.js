const Controller = require('./Controller');
const { MariaDatabase, MongoDatabase } = require('database');
const { Authenticator } = require('middleware');
const Router = require('koa-router');
const moment = require('moment');
const { Utils, Logger, Email, config } = require('common');
const fs = require('fs');
const ejs = require('ejs');
const path = require('path');
const Translator = require('../client/scripts/translator').Translator;

const router = new Router();

class SurveyController extends Controller {

  //gets the survey and returns it for editing
  static async editView(ctx, next) {
    const translator = new Translator(ctx.session.locale || 'en');
    const params = ctx.params;
    const survey = await Survey.find(params.id);
    if(!survey) throw new Error('Could not find survey with id '+params.id);
    // const json = await survey.getPopulatedJson(true, true, false, true);
    //return ctx.json(json);
    return ctx.view('survey/edit-survey.ejs', {id: params.id, title: translator.trans('Edit survey')});
  }

  static async edit(ctx, next) {
    const params = ctx.params;
    const user = new User(ctx.session.user);
    const survey = await Survey.find(params.id);
    if(!survey) throw new Error('Could not find survey with id '+params.id);
    const surveyEval = await survey.myEvaluation;
    const surveyInfo = await survey.getPopulatedJson(true, true, false, true);
    const userGroups = surveyEval ? await surveyEval.myUserGroups : [];
    Utils.sortArrayOfObjects(userGroups, 'name');
    const evaluations = Utils.arrayMerge(await user.mySupervisedEvaluations, await user.myEvaluations, (evaluation1, evaluation2) => evaluation1.id === evaluation2.id);
    Utils.sortArrayOfObjects(evaluations, 'name');
    const json = {};

    json.survey = {
      title: surveyInfo.title,
      evaluation: surveyInfo.evaluation || {},
      description: surveyInfo.description,
      startDate: surveyInfo.startDate,
      endDate: surveyInfo.endDate,
      openResults: !!surveyInfo.openResults,
      userInfo: surveyInfo.userInfo,
    }
    json.questions = surveyInfo.questions;
    json.links = surveyInfo.links;
    json.host = ctx.host;
    json.userGroups = userGroups;
    json.evaluations = evaluations;
    return ctx.json(json);
  }

  //saves the survey and all corresponding data
  //how do we want it to look here?
  static async save(ctx, next) {

    const surveyData = ctx.params.survey;
    surveyData.questions = ctx.params.questions;
    surveyData.id = ctx.params.id;
    const survey = new Survey(surveyData);
    survey.startDate = survey.startDate.utc().format('YYYY-MM-DD HH:mm');//TODO where to place this? should only be converted to utc ONCE!
    survey.endDate = survey.endDate.utc().format('YYYY-MM-DD HH:mm');
    await survey.save(true); //true = strict, meaning it will delete all questions that were not present in the request
    const json = survey.serialize(true);
    json.questions = survey.questions;
    if(!surveyData.sharingEnabled) await Link.delete({survey: survey.id});

    return ctx.json(json);
  }

  static async setdates(ctx, next) {
    const survey = await Survey.find(ctx.params.id);
    if(!survey) throw new Error('Survey not found ¯\\_(ツ)_/¯');
    if(!ctx.params.startDate && !ctx.params.endDate) throw new Error('Start date or end date must be provided');
    const now = moment().utc();
    if(ctx.params.startDate){
      const startDate = moment(ctx.params.startDate).utc();
      if(startDate.isBefore(now)) {
        Logger.info(`Provided start date ${startDate} is before now: ${now}`);
        survey.startDate = now.format('YYYY-MM-DD HH:mm');
      } else survey.startDate = startDate.format('YYYY-MM-DD HH:mm');
    }
    if(ctx.params.endDate){
      const endDate = moment(ctx.params.endDate).utc().format('YYYY-MM-DD HH:mm');
      survey.endDate = endDate;
    }
    await survey.save();
    return ctx.json(survey);
  }

  static async delete(ctx, next) {
    const survey = await Survey.find(ctx.params.id);
    if(!survey) throw new Error('Survey not found ¯\\_(ツ)_/¯');
    if(ctx.params.confirm) {
      const questions = await survey.myQuestions;
      await survey.delete();
      survey.questions = questions; //return with questions
      return ctx.json({ message: 'Deleted succesfully', survey: survey });
    } else {
      const questionCount = await Question.count(`survey = ${survey.id}`);
      //show something like "U sure you wanna delete survey with title "Best Anime Fights 2017 (Legit)" containing 10 questions????!!!!"
      return ctx.json({survey: survey, questionCount: questionCount});
    }
  }

  static async saveAsTemplate(ctx, next) {
    const survey = await Survey.find(ctx.params.id);
    const template = await SurveyTemplate.initialize(survey.serialize());
    template.allow(ctx.session.user, Permission.ADMIN);
    await template.save();
    const surveyQuestions = (await survey.myQuestions).map(question => {
      delete question.id;
      return new QuestionBank(question);
    });
    let questions = await QuestionBank.where(`template = ${template.id} AND title IN (?)`, surveyQuestions.map(qq=>qq.title));
    questions = Utils.arrayMerge(questions, surveyQuestions, (q1, q2)=>q1.title === q2.title);
    questions.forEach(qq=>qq.template = template.id);
    await Promise.all(questions.map(qq=>qq.save()));
    await QuestionBank.delete(`template = ${template.id} AND id NOT IN (${questions.map(question => question.id)})`);
    template.questions = questions;

    return ctx.json(template);
  }

  static async sendInvite(ctx, next) {
    const survey = await Survey.find(ctx.params.id);
    if(!survey) throw new Error('Survey not found ¯\\_(ツ)_/¯');
    const links = await survey.myLinks;
    const link = links.find(ll=>ll.userGroup === ctx.params.userGroupId);
    if(!link) throw new Error(`Link not found!`);
    const emailFile = fs.readFileSync(path.join(config.appRoot, 'views', 'email', 'invite.ejs'),'utf-8');
    let emailHtml = ejs.render(emailFile, {host: ctx.host, link: link.key, fullName: ctx.session.user.givenName + ' ' + ctx.session.user.familyName});
    const result = await Email.sendEmail(ctx.params.emails, `Invitation to participate in survey ${survey.title}`, null, emailHtml);
    return ctx.json({message: 'Email sent!', result: result});
  }

  static async listView(ctx, next) {
    const translator = new Translator(ctx.session.locale || 'en');
    const filter = ctx.params.filter;
    if(filter !== 'active' && filter !== 'closed') throw new Error(`You can only list 'active' and 'closed' surveys`);
    const title = filter === 'active' ? translator.trans(`Active surveys`) : translator.trans(`Closed surveys`);
    return ctx.view('survey/list.ejs', {filter, title});
  }
  static async list(ctx, next) {
    const me = await User.find(ctx.session.user.id);
    const evaluations = await me.myEvaluations;
    if(evaluations < 1) return ctx.json({surveys: [], pageCount:1});
    const permittedSurveys = Survey.permitted(me);
    if(Utils.isEmpty(ctx.params)){
      //return all surveys I can see, meaning
      //those I have view permission for and that are part of the evaluation I am in
      if(permittedSurveys.length > 0) permittedSurveys.or().where('evaluation IN (?)').values(...evaluations.map(ee=>ee.id));
      const [[surveyRecords]] = Survey.query(permittedSurveys);
      return ctx.json(surveyRecords.map(rr=>new Survey(rr.surveys)));
    } else {
      const filter = ctx.params.filter;
      const page = ctx.params.page || 1;
      const pageLimit = ctx.params.pageLimit || 15;

      const {surveys, pageCount} = await Survey.paginationSearch(page, pageLimit, filter, evaluations);
      if(surveys.length < 1) return ctx.json({surveys: [], pageCount:1});
      await me.attachPermissions(surveys);
      const sessions = await SurveySession.where(`user = ${ctx.session.user.id} AND open = 1 AND survey IN (${surveys.map(survey=>survey.id).join(',')})`);
      for (const survey of surveys) {
        survey.ongoingSession = !!sessions.find(session => session.survey === survey.id && session.open);
        survey.evaluation = await survey.myEvaluation;
        survey.evaluation.userGroups = await survey.evaluation.myUserGroups;
        survey.isOpen = !!filter === 'active';
        //listClosed: allow user to see results if he has answered to the survey and survey has open results
        survey.canViewResults = !!(survey.openResults && sessions.find(session => session.survey === survey.id));
        survey.canAnswer = !!survey.evaluation.userGroups.find((group) => group.id === me.userGroup);
      }

      return ctx.json({surveys, pageCount});
    }
  }

  static async answer(ctx, next) {
    const translator = new Translator(ctx.session.locale || 'en');
    if(ctx.params.link) {
      const link = await Link.find({key:ctx.params.link});
      let session;
      if(ctx.session.surveySession && ctx.session.link == link.id) session = await SurveySession.find({id: ctx.session.surveySession, open: 1});
      if(!session && link.userGroup === 0) {
        return ctx.redirect(`/survey/session/${link.key}`);
      } else if (!session) {
        session = await new SurveySession({ survey: link.survey, userGroup: link.userGroup, open:1 }).save();
        ctx.session.surveySession = session.id;
        ctx.session.link = link.id;
      }
      await session.mySurvey;
      await session.myUserGroup;
      await session.survey.myQuestions;
      return ctx.view('survey/answer.ejs', {session, quick:true, title: translator.trans('Answer survey')});
    }

    const surveyId = ctx.params.id;
    const user = await User.find(ctx.session.user.id);
    const query = { survey: surveyId, user: user.id, open: 1 };
    let session = await SurveySession.find(query);
    if(!session) {
      if(!user.userGroup) throw new Error(`User ${user.id} has no active group!`);
      query.userGroup = user.userGroup;
      session = await new SurveySession(query).save();
    }
    await session.mySurvey;
    await session.survey.myQuestions;
    await session.myUserGroup;
    return ctx.view('survey/answer.ejs', { session, quick:false, title: translator.trans('Answer survey') });
  }

   /**
   * Action for handling links that have no dedicated group.
   * Renders a view that allows user to select a group from survey's evaluation. Redirects back to answer action upon submit.
   */
  static async createSession(ctx, next) {
    const link = await Link.find({ key: ctx.params.link });
    const survey = await link.mySurvey;
    const groups = await new Evaluation(survey.evaluation).myUserGroups;

    if (ctx.params.group && groups.some(group => group.id == ctx.params.group)) {
      if(ctx.session.surveySession && ctx.session.link == link.id) {
        const duplicate = await SurveySession.find({id: ctx.session.surveySession, open: 1});
        if(duplicate) return ctx.redirect(`/survey/answer/${link.key}`);
      }
      const session = await new SurveySession({ survey: link.survey, userGroup: Number(ctx.params.group), open: 1 }).save();
      ctx.session.surveySession = session.id;
      ctx.session.link = link.id;
      return ctx.redirect(`/survey/answer/${link.key}`);
    }
    return ctx.view('survey/session.ejs', { groups, link, survey });
  }

  static async newView(ctx, next) {
    const translator = new Translator(ctx.session.locale || 'en');
    const user = await User.find(ctx.session.user.id);
    const evaluations = Utils.arrayMerge(await user.mySupervisedEvaluations, await user.myEvaluations, (evaluation1, evaluation2) => evaluation1.id === evaluation2.id);
    Utils.sortArrayOfObjects(evaluations, 'name');
    const templates = await SurveyTemplate.where();
    return ctx.view('survey/new-survey.ejs', { user, evaluations, templates, title: translator.trans('New survey')});
  }
  static async new(ctx, next) {
    const params = ctx.params;
    const survey = await Survey.DB.transaction(async function(db, connection){
      if(Utils.legitObject(params.evaluation)){
        let evaluation = new Evaluation(params.evaluation).allow(ctx.session.user, Permission.ADMIN);
        evaluation.db = db;
        await evaluation.get();
        await evaluation.save();
        params.evaluation = evaluation.id;
      }
      let survey = new Survey(params).allow(new User(ctx.session.user), Permission.ADMIN);
      survey.db = db;
      return await survey.save();
    });
    return ctx.redirect(`/survey/${survey.id}/edit`);
  }

  static async info(ctx, next) {
    const params = ctx.params;
    const survey = await Survey.find(params.id);
    if(survey === null) throw new NotFoundError(`No survey found with id ${params.id}`);
    const json = survey.serialize();
    const evaluation = await survey.myEvaluation;
    const users = await evaluation.myUsers;
    const groups = await evaluation.myUserGroups;
    Utils.sortArrayOfObjects(groups, 'name');
    json.evaluation = evaluation;
    json.evaluationUsers = users;
    json.evaluationGroups = groups;
    json.supervisors = await evaluation.findSupervisors();
    json.questions = await survey.myQuestions;
    json.sessions = (await Answer.aggregate([
      { $match: { 'survey': Number(params.id) } },
      { $group: { _id: '$session' } }
    ])).length;

    return ctx.json(json);
  }

  static async createLink(ctx, next) {
    let json = {
      userGroup: parseInt(ctx.params.userGroup),
      survey: parseInt(ctx.params.id)
    }
    let link = await Link.find({ survey: json.survey, userGroup: json.userGroup });
    if (link) {
      throw new Error(`Tried to create a duplicate link for survey ${json.survey} and userGroup ${json.userGroup}`);
    } else {
      const survey = await Survey.find(json.survey);
      const groups = await new Evaluation(survey.evaluation).myUserGroups;
      if (json.userGroup !== 0 && !groups.some(group => group.id === json.userGroup)) throw new Error(`Tried to create link for group ${json.userGroup} which is not part of evaluation ${survey.evaluation}`);
      while (true) {
        json.key = Utils.randomString(Utils.random(15, 30));
        const duplicate = await Link.find({ key: json.key });
        if (!duplicate) break;
      }
      link = new Link(json);
      await link.save();
    }
    json = link.serialize();
    json.host = ctx.host;
    return ctx.json(json);
  }

  static async deleteLink(ctx, next) {
    const link = await Link.find({survey: Number(ctx.params.id), userGroup: Number(ctx.params.userGroup)});

    if(!link) throw new Error(`Requested link of survey ${ctx.params.id} doesn't exist!`);
    await link.delete();

    ctx.json(link);
  }

  static get router() {
    if (router.stack.length > 0) return router;
    router.prefix('/survey');
    //add this one before login, since people can answer it without loggin in
    router.get('/:id/answer',    Authenticator.canAnswerSurvey, this.answer);
    router.get('/answer/:link',  Authenticator.canAnswerSurvey, this.answer);
    router.all('/session/:link', Authenticator.canAnswerSurvey, this.createSession);

    router.use(Authenticator.login);

    router.post('createlink',           '/:id/createLink',     Authenticator.canEdit,           this.createLink);
    router.post('deletelink',           '/:id/deleteLink',     Authenticator.canEdit,           this.deleteLink);
    router.post('savesurvey',           '/:id/save',           Authenticator.canEdit,           this.save);
    router.post('setsurveydates',       '/:id/setdates',       Authenticator.canEdit,           this.setdates);
    router.post('deletesurvey',         '/:id/delete',         Authenticator.canDelete,         this.delete);
    router.post('saveastemplate',       '/:id/saveAsTemplate', Authenticator.canSaveAsTemplate, this.saveAsTemplate);
    router.post('sendinvite',           '/:id/sendInvite',     Authenticator.canAnswerSurvey,   this.sendInvite);
    router.post('createnewsurvey',      '/new',                                                 this.new);
    router.get('editsurvey',            '/:id/editJSON',       Authenticator.canEdit,           this.edit);
    router.get('listsurveys',           '/listJSON',                                            this.list);
    router.get('listsurveys',           '/listJSON/:filter',                                    this.list);
    router.get('viewsurveyinfo',        '/:id/info',                                            this.info);
    //views
    router.get('newsurveyview',         '/new',                                                 this.newView);
    router.get('editsurveyview',        '/:id/edit',           Authenticator.canEdit,           this.editView);
    router.get('listsurveysview',       '/list/:filter',                                        this.listView);

    return router;
  }
}

exports.controller = SurveyController;