const { expect } = require('chai');
const { Utils, Logger } = require('common');
const { User, Answer } = require('models');
const { MongoDatabase, MariaDatabase } = require('database');

const db = new MongoDatabase(encodeURI(`mongodb://${process.env['MONGO_TEST_USER']}:${process.env['MONGO_TEST_PASS']}@${process.env['MONGO_TEST_HOST']}/testdb`));
const mdb = new MariaDatabase(encodeURI(`mysql://${process.env['MARIA_TEST_USER']}:${process.env['MARIA_TEST_PASS']}@${process.env['MARIA_TEST_HOST']}:3306/test`));
//Change database to the test one
User.DB = db;
Answer.DB = db;
///*
describe('User model:', async function() {
  Logger.info(`Testing user model. Database ${User.DB.url} and collection ${User.DATASTORE}`);
  it('tests whether connection works', async function() {
    await User.where({username: 'nopestitynopes'}); //this will create the collection implicitly
    const count = await User.DB.connect().then(db=>db.collection(User.DATASTORE).count());
    expect(count).to.equal(0);
  });
});

describe('Answer:', function() {
  Logger.info(`Testing Answer model. Database ${Answer.DB.url} and collection ${Answer.DATASTORE}`);
  it('tests whether connection works', async function() {    
    const count = await Answeromment.count();
    expect(count).to.equal(0);
  });
});

after(async function(){  
  const connection = await mdb.connect();
  const [tables] = await connection.query('SHOW TABLES');  
  const truncates = [];
  let query;
  for (let tableData of tables) {
    for (let tableName of Object.values(tableData)) {
      Logger.info('Cleanup: Truncating ' + tableName);
      query = `TRUNCATE ${tableName};`;
      if(tableName.startsWith('join')) truncates.splice(0,0,query);
      else truncates.push(query);
    }
  }
  truncates.splice(0,0,'SET FOREIGN_KEY_CHECKS = 0;');
  truncates.push('SET FOREIGN_KEY_CHECKS = 1;');
  await connection.query(truncates.join('\n'));
  connection.release();
});
//*/