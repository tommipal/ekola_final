const bcrypt = require('bcryptjs');
const txtgen = require('txtgen');
const moment = require('moment');
const { Utils, Logger } = require('common');
const ManagedModel = require('../../models/ManagedModel');

const givenNames = ['Matti', 'Maija', 'Ville', 'Seppo', 'Sepi', 'Henri', 'Aleksi', 'Markku'];
const familyNames = ['Suomalainen', 'Valo', 'Meri', 'Monninen', 'Aalto', 'Maalari', 'Mansikkala', 'Innala'];

const { MariaDatabase, MongoDatabase } = require('database');
const maria = MariaDatabase.default;
const mongo = MongoDatabase.default;

class EntityGenerator {
  constructor() {
    throw new Error(`keeping it abstract`);
  }

	/**
	 * @param {JSON} params
	 * @returns {Answer}
	 */
  static async generateAnswer(params = {}) {
    let criteria = params.criteria;
    if (!criteria) throw new Error('Criteria is required to generate an answer!');

    let json = {
      question: params.question,
      session: params.session,
      userGroup: params.group,
      survey: params.survey,

      criteria: criteria,
      criteriaTitle: criteria.title,
      type: criteria.type
    }
    switch (json.type) {
      case 'scale':
        json.values = { scale: { value: Utils.random(criteria.options.min, criteria.options.max) } };
        break;
      case 'select':
        let option = Utils.selectRandom(criteria.options.choices);
        json.values = { select: { value: option.value, description: option.description } };
        break;
      default:
        throw new Error(`Found an unsupported criteria type: ${json.type}`);
    }

    let answer = new Answer(json);
    if (!params.noSave) await answer.save();

    return answer;
  }

	/**
	 * survey* / group* / session*
	 * @param {Object} params
	 * @param {Array<Question>} questions
	 */
  static async generateAnswersForQuestions(params = {}, questions = []) {
    if (!params.survey || !params.group || !params.session) throw new Error('survey, group and session are required');
    if (!questions.every(question => question.id && question.criteria)) throw new Error('Received invalid questions.');
    questions = params.survey.questions || questions;
    if (questions instanceof Array === false || questions.length === 0) throw new Error(`Expected an array of questions`);
    const chanceToGenerateAnswersForQuestion = params.chanceToGenerateAnswersForQuestion || 1;
    const chanceToGenerateAnswersForCriteria = params.chanceToGenerateAnswersForCriteria || 1;

    let answers = [];
    for (let question of questions) {
      if (!Utils.randomBoolean(chanceToGenerateAnswersForQuestion)) continue;
      for (let criteria of question.criteria) {
        if (!Utils.randomBoolean(chanceToGenerateAnswersForCriteria)) continue;
        const answer = await EntityGenerator.generateAnswer({
          criteria: criteria,
          question: question.id,
          survey: params.survey.id || params.survey,
          group: params.group.id || params.group,
          session: params.session.id || params.session,
          noSave: true
        });
        answers.push(answer);
      }
    }
    if (!params.noSave) await Promise.all(answers.map(answer => answer.save()));

    return answers;
  }

	/**
	 * name / allow / save
	 * @param {Object} params
	 */
  static async generateEvaluation(params = {}) {
    const json = {
      name: params.name || this.randomString()
    }

    while (true) {
      let duplicate = await Evaluation.find({ name: json.name });
      if (!duplicate) break;
      json.name += this.randomString();
    }
    let evaluation = new Evaluation(json);

    await this.setPermissions(evaluation, params.allow, params.mask);
    if (!params.noSave) await evaluation.save();
    if (params.groups && params.groups.length) {
      if (params.noSave) throw new Error('Evaluation must be saved to add groups to it');
      await evaluation.addUserGroups(params.groups);
    }

    return evaluation;
  }

	/**
	 * name / adress / allow / save
	 * @param {Object} params
	 */
  static async generateGroup(params = {}) {
    const json = {
      name: params.name || this.randomString(),
      description: params.description || this.randomString()
    }
    while (true) {
      let duplicate = await UserGroup.find({ name: json.name });
      if (!duplicate) break;
      json.name += this.randomString();
    }
    let group = new UserGroup(json);
    await this.setPermissions(group, params.allow, params.mask);
    if (!params.noSave) await group.save();

    return group;
  }

  /**
   * count / type
   * @param {Object} params
   * @returns {Array{Object}} Array of criteria
   */
  static generateCriteria(params = {}) {
    let count = params.count || Utils.random(1, 3);
    const criteria = [];

    const chanceToGenerateScaleCriteria = 0.8;

    for (let i = 0; i < count; i++) {
      const object = {
        title: params.title || txtgen.sentence(),
        description: txtgen.paragraph(),
        options: {},
        type: params.type || Utils.randomBoolean(chanceToGenerateScaleCriteria) ? 'scale' : 'select'
      }

      while (true) {
        if (criteria.some(criteria => criteria.title === object.title)) object.title += this.randomString();
        else break;
      }
      if (object.type === 'scale') {
        object.options = { min: 0, max: 5 };
      } else {
        object.options.choices = [];
        for (let ii = 0; ii < Utils.random(2, 4); ii++) object.options.choices.push({ value: ii, description: this.randomString() });
      }
      criteria.push(object);
    }

    return criteria;
  }

	/**
	 * title / description / survey / tags / index / criteria
	 * @param {Object} params
	 */
  static async generateQuestion(params = {}) {
    let criteria;
    if (!params.criteria || !isNaN(params.criteria)) {
      criteria = EntityGenerator.generateCriteria({ count: params.criteria || 3 });
    }
    else if (params.criteria instanceof Array) criteria = params.criteria;
    else throw new Error('Expected a criteria count or an array of criteria');

    const json = {
      title: params.title || txtgen.sentence(),
      description: params.description || txtgen.paragraph(),
      tags: params.tags || this.randomString(),
      survey: params.survey,
      position: params.position,
      criteria: JSON.stringify(criteria)
    }
    let question = new Question(json);
    if (!params.noSave) {
      if (!question.survey) Logger.warn('Questions should not be saved without a survey');
      await question.save();
    }
    return question;
  }
	/**
	 * title / evaluation / description / startDate / endDate / openResults / userInfo
	 * @param {Object} params
   * @param {Boolean} open determines if the survey is open or not
	 */
  static async generateSurvey(params = {}, open = true) {
    let startDate = moment();
    let endDate = startDate;

    if (open) {
      endDate = moment(endDate).add(Utils.random(1 * 1440, 3 * 1440), 'minutes');
    }

    const json = {
      title: params.title || this.randomString(),
      evaluation: params.evaluation || (await EntityGenerator.generateEvaluation({ save: true, allow: true })).id,
      description: params.description || txtgen.sentence(),
      startDate: params.startDate || startDate.utc().format('YYYY-MM-DD HH:mm'),
      endDate: params.endDate || endDate.utc().format('YYYY-MM-DD HH:mm'),
      openResults: params.openResults || Utils.randomBoolean() ? 1 : 0,
      userInfo: params.userInfo || []
    };
    let survey = new Survey(json);

    if (params.questions && params.questions instanceof Array) survey.questions = params.questions;
    else if (params.questions && !isNaN(params.questions)) {
      const questions = [];
      for (let i = 0; i < params.questions; i++) questions.push(await EntityGenerator.generateQuestion({ noSave: true }));
      survey.questions = questions;
    }

    await this.setPermissions(survey, params.allow, params.mask);
    if (!params.noSave) await survey.save();

    return survey;
  }

	/**
	 * title / description / userInfo
	 */
  static async generateTemplate(params = {}) {
    const json = {
      title: params.title || this.randomString(),
      description: params.description || txtgen.sentence(),
      userInfo: []
    }
    const template = new SurveyTemplate(json);
    await this.setPermissions(template, params.allow, params.mask);
    if (!params.noSave) await template.save();

    return template;
  }

	/**
	 * username / password / group* / email / passwordResetToken / save
	 * @param {Object} params
	 */
  static async generateUser(params = {}) {
    let group;
    if (params.group) group = params.group.id || params.group;
    const json = {};
    json.username = params.username || this.randomString()
    json.givenName = params.givenName || Utils.selectRandom(givenNames);
    json.familyName = params.familyName || Utils.selectRandom(familyNames);
    json.password = await bcrypt.hash(params.password ? params.password : 'test', 10);
    json.userGroup = group || null;
    json.email = params.email || `${this.randomString()}@${this.randomString()}.com`
    json.passwordResetToken = params.passwordResetToken || null;
    while (true) {
      let duplicate = await User.find({ username: json.username });
      if (!duplicate) break;
      json.username += this.randomString();
    }
    while (true) {
      let duplicate = await User.find({ email: json.email });
      if (!duplicate) break;
      json.email = this.randomString() + json.email;
    }
    const user = new User(json);
    if (!params.noSave) await user.save();

    return user;
  }
	/**
	 * group / survey
	 * @param {Object} params
	 */
  static async generateLink(params = {}) {
    const json = {
      userGroup: params.group,
      survey: params.survey
    }
    while (true) {
      json.key = Utils.randomString(Utils.random(15, 30));
      const duplicate = await Link.find({ key: json.key });
      if (!duplicate) break;
    }
    let link = new Link(json);
    if (params.save) await link.save();

    return link;
  }
	/**
	 * user / group / survey / open / userInfo
	 * @param {Object} params
	 */
  static async generateSurveySession(params = {}) {
    let user;
    if (params.user) user = params.user.id || params.user
    const json = {
      user: user || null,
      userGroup: params.group.id || params.group,
      survey: params.survey.id || params.survey,
      open: params.open || 1,
      userInfo: params.userInfo || []
    }
    const session = new SurveySession(json);
    if (!params.noSave) await session.save();

    return session;
  }

	/**
	 * user / target / mask
	 * @param {Object} params
	 */
  static async generatePermission(params = {}) {
    const json = {
      target: params.target,
      user: params.user,
      mask: params.mask || Permission.ADMIN
    }
    const permission = new Permission({});
    permission.target = json.target;
    permission.user = json.user;
    permission.mask = json.mask;
    if (!params.noSave) await permission.save();

    return permission;
  }

  /**
   * @param {ManagedModel} object target object
   * @param {User|Array<User>} allow user or array of users that are granted permission to the target object
   * @param {Number} mask mask for the permission
   */
  static async setPermissions(object, allow, mask) {
    if (object instanceof ManagedModel === false) throw new Error(`Expected an instance of ManagedModel. Got ${object}`);

    mask = (mask || mask === 0) ? mask : Permission.ADMIN;
    if (!allow) {
      object.lock();
    }
    else if (allow instanceof Array && allow.every(user => user instanceof User)) {
      for (const user of allow) object.allow(user, mask);
    }
    else {
      let user = allow instanceof User ? allow : await EntityGenerator.generateUser({ save: true });
      object.allow(user, mask);
    }
  }

  static randomString() {
    const randomText = txtgen.paragraph().replace(/[^\w\s]/g, '').toLowerCase().split(/\s/g);
    return Utils.selectRandom(randomText);
  }
  /**
  * Simple function to clear the database after tests.
  */
  static async cleanDatabase() {
    if (process.env.NODE_ENV === 'production') throw new Error('This function will clean up the database and should NEVER be run in production environment!');

    const collections = ['links', 'answers'];
    const tables = [User.DATASTORE, UserGroup.DATASTORE, Evaluation.DATASTORE, Survey.DATASTORE, Question.DATASTORE, SurveySession.DATASTORE, Permission.DATASTORE, SurveyTemplate.DATASTORE, QuestionBank.DATASTORE, MariaDatabase.JOIN_EVALUATIONS_USERGROUPS];

    Logger.warn('Cleaning up mongoDB!');
    const mongoConnection = await mongo.connect();
    Logger.info(`Dropping mongoDB collections`);
    for (const collection of collections) {
      Logger.info(`Dropping collection '${collection}'`);
      try {
        await mongoConnection.collection(collection).drop();
      } catch (error) { }
    }
    Logger.info(`Recreating mongoDB collections`);
    for (const collection of collections) {
      Logger.info(`Creating collection '${collection}'`);
      await mongoConnection.createCollection(collection);
    }

    Logger.warn('Cleaning up mariadb!');
    const connection = await maria.connect();
    Logger.info('Disabling foreign key checks');
    await connection.query('SET FOREIGN_KEY_CHECKS = 0;');
    for (const table of tables) {
      Logger.info(`Truncating table '${table}'`);
      await connection.query(`TRUNCATE TABLE ${table};`);
    }
    Logger.info('Enabling foreign key checks');
    await connection.query('SET FOREIGN_KEY_CHECKS = 1;');
    connection.release();
    // mongoConnection.close();
    Logger.info('Cleanup done!');
  }


}

module.exports = EntityGenerator;