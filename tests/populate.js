const { expect } = require('chai');
const { Utils, Logger } = require('common');
const { MongoDatabase, MariaDatabase } = require('database');
const EntityGenerator = require('./generate/EntityGenerator');
const txtgen = require('txtgen');
const moment = require('moment');

const maria = MariaDatabase.default;
const mongo = MongoDatabase.default;

const userLimit = 10;
const groupLimit = 3;
const evaluationLimit = 4;
const surveyLimit = 10;
const maxGroupsInEvaluation = 5;
const minGroupsInEvaluation = 1;
/**
 * Generate userless sessions to bump up the answer count (if desired).
 */
const numberOfAdditionalSessionsToCreate = 25;

//sessions
const chanceToAddEvidenceForQuestion = 0.6;
//surveys
const chanceForSurveyToBeOpen = 0.5;
const chanceToGenerateLinkForSurvey = 0.3;
//answers
const chanceToGenerateAnswersForSurvey = 0.75;
const chanceToGenerateAnswersForUser = 0.75;
//evaluations
const chanceToAddUserToEvaluation = 0.3;

require('models');

//TODO add select type criteria to the question bank questions.
//TODO generate userInfo questions for surveys(?).

before(async function(){
  this.timeout(10000);
  await EntityGenerator.cleanDatabase();
  Logger.warn('This script will populate the currently set database with test values. \nIt will not drop the database and it will not update values, but it is still NOT recommended to run this in production. \nBeginning in 5 seconds, press any key to cancel.')
  process.stdin.setRawMode(true);
  process.stdin.resume();
  process.stdin.on('data', process.exit.bind(process, 0));
  await Utils.delay(5000);
  process.stdin.removeAllListeners('data');
  Logger.info(`Beginning data population on ${maria.url}\nAll generated users have password 'test'`);
  //spawn connections
  const connection = await maria.connect();
  Logger.info('Disabling foreign key checks');
  await connection.query('SET FOREIGN_KEY_CHECKS = 0;');
});

describe('Populates the database with some test values for development:', async function() {
  it('Should populate users and userGroups', async function() {
    const users = await User.where();
    if(users.length > userLimit) {
      Logger.info(`More than ${userLimit} users detected, skipping user generation.`);
      return;
    }
    const groups = [];
    for(let i = 0; i < groupLimit; i++) groups.push({name: `Control group ${i+1}`, description: 'Test'});

    const password = 'test';
    for(let i = 0; i < userLimit; i++){
      const unique = await EntityGenerator.generateUser({noSave:true});

      const dna = txtgen.sentence().replace(/[^\w\s]/g, '').toLowerCase().split(/\s/g);
      const username = `user${i+1}`;
      const givenName = Utils.selectRandom(['Matti', 'Maija', 'Ville', 'Seppo', 'Sepi', 'Henri', 'Aleksi', 'Markku']);
      const familyName = Utils.selectRandom(['Suomalainen', 'Valo', 'Meri', 'Monninen', 'Aalto', 'Maalari', 'Mansikkala', 'Innala']);
      const email = unique.email;
      Logger.info(`Adding user ${username}; ${email}`);
      try {
        const user = await User.create({ username, email, password, confirmation: password, givenName, familyName }, Utils.selectRandom(groups, false), true);
      } catch(e){
        //ignored
      }
    }
    Logger.info(`Done. ${userLimit} users added.`);
  });

  it('Should create evaluations', async function() {
    const evals = await Evaluation.where();
    if(evals.length > evaluationLimit) {
      Logger.info(`More than ${evaluationLimit} evaluations detected, skipping evaluations`);
      return;
    }
    const users = await User.where();
    for(let i = 0; i < evaluationLimit; i++){
      const name = `Evaluation ${i+1}`;
      Logger.info(`Adding evaluation ${name}`);
      await new Evaluation({name}).allow(Utils.selectRandom(users), Permission.ADMIN).save(); //?
    }
  });

  it('Should create survey template(s)', async function() {
    const QAEMP = require('./json/QAEMP');
    const title = 'QAEMP Survey';
    const description = 'Official Quality Assurance and Enhancement MarketPlace for higher education institutions following KOLA specifications.';
    const template = new SurveyTemplate({ title, description });
    await template.get();
    if(template.id) {
      Logger.info('QAEMP survey template already exists, skipping');
      return;
    }
    Logger.info('Adding QAEMP survey template');
    const users = await User.where();
    await template.allow(Utils.selectRandom(users), Permission.ADMIN).save();
    for(let question of QAEMP){
      let qb = new QuestionBank(question);
      qb.template = template.id;
      Logger.info(`Adding question ${qb.title}`);
      await qb.save();
    }
    Logger.info('QAEMP populated');
  });

  it(`Should add groups to evaluations`, async function() {
    const evaluations = await Evaluation.where();

    for(const evaluation of evaluations) {
      const groups = await UserGroup.where();
      const selected = [];
      for (let i = 0; i < Utils.random(minGroupsInEvaluation, maxGroupsInEvaluation); i++){
        if(groups.length)selected.push(Utils.selectRandom(groups, true));
      }
      await evaluation.addUserGroups(selected);
    }
  });

  it('Should create survey(s) from template(s) and assign them to evaluations', async function(){
    const minimumNumberOfQuestions = 1;
    const maximumNumberOfQuestions = 5;

    const surveys = await Survey.where();
    if(surveys.length > surveyLimit) {
      Logger.info(`More than ${surveyLimit} surveys detected, skipping surveys`);
      return;
    }
    const evaluations = await Evaluation.where();

    for(let i = 0; i < surveyLimit; i++) {
      let startDate;
      if (Utils.randomBoolean(chanceForSurveyToBeOpen)) startDate = Utils.randomDatePast(0, 3);
      else startDate = Utils.randomDateFuture(0,3);

      const questionBankQuestions = await QuestionBank.where();
      const evaluation = Utils.selectRandom(evaluations);
      const users = await evaluation.myUsers;

      let questions = [];
      for(let q = 0; q < Utils.random(minimumNumberOfQuestions, maximumNumberOfQuestions); q++) questions.push(Utils.selectRandom(questionBankQuestions, true));
      questions = questions.map(question => {
        delete question.id;
        return new Question(question);
      });

      await EntityGenerator.generateSurvey({
        title: `Survey ${i}`,
        questions: questions,
        startDate: startDate.utc().format('YYYY-MM-DD HH:mm'),
        endDate: moment(startDate).add(Utils.random(3*1440,30*1440), 'minutes').utc().format('YYYY-MM-DD HH:mm'),
        evaluation: evaluation.id,
        allow: Utils.selectRandom(users)
      });
    }
  });

  it('Should add users to evaluations', async function () {
    const evaluations = await Evaluation.where();
    for (const evaluation of evaluations) {
      const users = await evaluation.myUsers;
      const usersToAdd = [];
      for (const user of users) if (Utils.randomBoolean(chanceToAddUserToEvaluation)) usersToAdd.push(user);
      Logger.info(`Adding ${usersToAdd.length} users (${usersToAdd.map(user=>user.id).join(',')}) to evaluation ${evaluation.id}`);
      await evaluation.addUsers(usersToAdd, Permission.ADMIN);
    }
  });

  it(`Should generate answers`, async function () {
    const surveys = await Survey.where();
    for (const survey of surveys) {
      const answers = await Answer.where({survey: survey.id});
      if(answers.length) {
        Logger.info(`There are already answers for survey ${survey.id}. Skipping.`);
        continue;
      }
      if(!Utils.randomBoolean(chanceToGenerateAnswersForSurvey)) continue;

      const evaluation = await survey.myEvaluation;
      const allowedGroups = await evaluation.myUserGroups;
      const users = await evaluation.myUsers;
      for (const user of users) {
        if(!Utils.randomBoolean(chanceToGenerateAnswersForUser)) continue;
        const groups = await user.myGroups;
        const commonGroups = [];
        for (const allowedGroup of allowedGroups) {
          let match = groups.find(group => group.id === allowedGroup.id);
          if (match) commonGroups.push(match);
        }
        let session = await SurveySession.find({user: user.id, survey:survey.id});
        if(session) continue;

        session = await EntityGenerator.generateSurveySession({
          user: user.id,
          group: Utils.selectRandom(commonGroups).id,
          survey: survey.id,
        });
        await EntityGenerator.generateAnswersForQuestions({
          survey: survey.id,
          group: session.userGroup,
          session: session.id,
          save: true,
          chanceToGenerateAnswersForQuestion: 0.9,
          chanceToGenerateAnswersForCriteria: 0.8,
        }, await survey.myQuestions)
      }
    }
  });

  it(`Should generate userless sessions and answers`, async function() {
    const existing = await SurveySession.where(`${SurveySession.DATASTORE}.user IS NULL`);
    if(existing.length > numberOfAdditionalSessionsToCreate) return Logger.info(`More than ${numberOfAdditionalSessionsToCreate} userless sessions detected. Skipping creation of userless sessions`);

    const answers = await Answer.where();
    const surveys = (await Survey.where()).filter(survey => answers.some(answer => answer.survey === survey.id));
    if(!surveys.length) return Logger.info(`Couldn't find any surveys with answers. Skipping creation of userless sessions.`);

    Logger.info(`Adding ${numberOfAdditionalSessionsToCreate} userless sessions to ${surveys.length} surveys that already have some answers.`);
    for(let i = 0; i < numberOfAdditionalSessionsToCreate; i++) {
      const survey = Utils.selectRandom(surveys);
      const evaluation = await survey.myEvaluation;
      const groups = await evaluation.myUserGroups;

      const session = await EntityGenerator.generateSurveySession({
        survey: survey.id,
        group: Utils.selectRandom(groups).id,
      });
      await EntityGenerator.generateAnswersForQuestions({
        survey: survey.id,
        group: session.userGroup,
        session: session.id,
        chanceToGenerateAnswersForQuestion: 0.9,
        chanceToGenerateAnswersForCriteria: 0.85,
      }, await survey.myQuestions)
    }
  });

  it(`Should generate evidence for survey sessions`, async function() {
    const sessions = await SurveySession.where();
    Logger.info(`Adding evidence to ${sessions.length} survey sessions`);
    for (const session of sessions) {
      session.evidence = [];
      const questions = await Question.where({survey: session.survey});
      for (const question of questions) {
        if(!Utils.randomBoolean(chanceToAddEvidenceForQuestion)) continue;
        session.evidence.push({
          question: question.id,
          value: txtgen.sentence()
        });
      }
      await session.save();
    }
  });

  it(`Should generate links for surveys`, async function() {
    const surveys = await Survey.where();
    for (const survey of surveys) {
      const links = await survey.myLinks;
      if(links.length) {
        Logger.info(`Survey ${survey.id} already has links. Skipping`);
        return;
      }
      const evaluation = await survey.myEvaluation;
      const allowedGroups = await evaluation.myUserGroups;
      for(const group of allowedGroups) {
        if(!Utils.randomBoolean(chanceToGenerateLinkForSurvey)) continue;
        await EntityGenerator.generateLink({group: group.id, survey:survey.id });
      }
    }

  });
});

after(async function(){
  const connection = await maria.connect();
  Logger.info('Enabling foreign key checks');
  await connection.query('SET FOREIGN_KEY_CHECKS = 1;');
  Logger.info('Population done');
  connection.release();
});