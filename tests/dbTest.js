const { expect } = require('chai');
const { Utils, Logger } = require('common');
const { MariaDatabase } = require('database');

const db = new MariaDatabase(encodeURI(`mysql://${process.env['MARIA_TEST_USER']}:${process.env['MARIA_TEST_PASS']}@${process.env['MARIA_TEST_HOST']}:3306/test`));

describe('MariaDatabase tests', async function () {
  it('Tests that connection works', async function () {
    const connection = await db.connect();
    connection.release();
  });  
});