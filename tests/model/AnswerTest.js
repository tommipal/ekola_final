const { expect, assert } = require('chai');
const { Utils } = require('common');
require('models');
const { MariaDatabase } = require('database');

const EntityGenerator = require('../generate/EntityGenerator');

if (process.env.NODE_ENV === 'production') throw new Error('These tests will manipulate data and should NEVER be run in production environment!');

before(async function () {
  this.timeout(5000);
  await EntityGenerator.cleanDatabase();
});

describe('Answer Model', async function () {
  let user, group, evaluation, questions, session, survey;
  before(async function () {
    user = await EntityGenerator.generateUser({ save: true });
    group = await EntityGenerator.generateGroup({ allow: user });
  });
  beforeEach(async function () {
    evaluation = await EntityGenerator.generateEvaluation({ allow: user });
    survey = await EntityGenerator.generateSurvey({ evaluation: evaluation.id, questions: 1, allow: user });
    session = await EntityGenerator.generateSurveySession({ user: user.id, survey: survey.id, group: group.id });
  });

  describe('save', async function () {
    it('save should save given answers', async function () {
      const expected = await EntityGenerator.generateAnswersForQuestions({ survey, session, group, noSave: true });
      assert.isTrue(expected.every(answer => !answer.id));

      await Answer.save(session.id, user.id, expected);

      const actual = await Answer.where({ survey: survey.id });
      expect(actual).to.have.lengthOf(expected.length);
      for (const answer of expected) {
        const match = actual.find(actual => actual.question === answer.question && actual.criteriaTitle === answer.criteriaTitle);
        assert.isNotNull(match);
        delete match.id;
        assert.deepEqual(answer, match);
      }
    });
    it('save should update answers with id', async function () {
      const answers = await EntityGenerator.generateAnswersForQuestions({ survey, session, group, noSave: true });
      await Promise.all(answers.map(answer => answer.save()));

      const expected = await Answer.where({ survey: survey.id });
      for (const answer of expected) {
        const randomized = await EntityGenerator.generateAnswer({ criteria: answer.criteria });
        answer.values = randomized.values;
      };
      await Answer.save(session.id, user.id, expected);

      const actual = await Answer.where({ survey: survey.id });
      expect(actual).to.have.lengthOf(expected.length);
      for (const answer of expected) {
        const match = actual.find(actual => actual.id === answer.id);
        assert.isNotNull(match);
        assert.deepEqual(answer, match);
      }
    });
    it('save should update answer if there exists one for the same criteria in case an id is not provided', async function () {
      const answers = await EntityGenerator.generateAnswersForQuestions({ survey, session, group });

      const expected = await EntityGenerator.generateAnswersForQuestions({ survey, session, group, noSave: true });
      await Answer.save(session.id, user.id, expected);

      const actual = await Answer.where({ survey: survey.id });
      expect(actual).to.have.lengthOf(expected.length);
      for (const answer of expected) {
        const match = actual.find(actual => actual.question === answer.question && actual.criteriaTitle === answer.criteriaTitle);
        assert.isNotNull(match);
        delete match.id;
        assert.deepEqual(answer, match);
      }
    });
    it(`save should validate answers and not save any of the answers if the validation doesn't pass`, async function () {
      //make sure there is at least one scale criteria in the questions
      survey.questions[0].criteria.push(EntityGenerator.generateCriteria({ type: 'scale' })[0]);

      const answers = await EntityGenerator.generateAnswersForQuestions({ survey, session, group, noSave: true });
      const invalid = answers.find(answer => answer.type === 'scale');
      invalid.values.scale.value = (invalid.criteria.options.max + 10);
      let error;
      try {
        await Answer.save(session.id, user.id, answers);
      } catch (err) {
        error = err;
      }
      assert.isNotNull(error);
      const actual = await Answer.where({ survey: survey.id });
      expect(actual).to.have.lengthOf(0);
    });
  });

  describe('getAverageValues', async function () {
    it(`getAverageValues should return objects representing the average answers`, async function () {
      let answers = await EntityGenerator.generateAnswersForQuestions({ survey, session, group });
      const expectedLength = answers.length;

      let averages = await Answer.getAverageValues(survey);
      expect(averages).to.have.lengthOf(expectedLength);
      //there should be a matching object for each of the original answers.
      for (const averageAnswer of averages) {
        assert.notInstanceOf(averageAnswer, Answer);
        let answer = answers.find(answer => answer.criteriaTitle === averageAnswer.criteriaTitle && answer.question === averageAnswer.question);
        assert.isNotNull(answer);
        assert.equal(answer.myValue, averageAnswer.values[averageAnswer.type].value);
      }

      for (let i = 0; i < 3; i++) {
        let session = await EntityGenerator.generateSurveySession({ user: await EntityGenerator.generateUser({ save: true }), survey, group });
        answers = answers.concat(await EntityGenerator.generateAnswersForQuestions({ survey, session, group }));
      }

      averages = await Answer.getAverageValues(survey);
      expect(averages).to.have.lengthOf(expectedLength);

      for (const averageAnswer of averages) {
        const matching = answers.filter(answer => answer.question === averageAnswer.question && answer.criteriaTitle === averageAnswer.criteriaTitle);
        const count = matching.length;
        let expectedValue = 0;
        while (matching.length) expectedValue += matching.pop().myValue;
        expectedValue = expectedValue / count;
        assert.equal(averageAnswer.values[averageAnswer.type].value, expectedValue);
      }
    });
  });

  describe('excel', async function () {
    // Think of some was to properly test this.
    it(`excel should not throw(?)`, async function () {
      let answers = await EntityGenerator.generateAnswersForQuestions({ survey, session, group });
      let err;
      try {
        await Answer.excel(survey);
      } catch (error) {
        err = error;
      }
      assert.isUndefined(err);
    });
  });
});