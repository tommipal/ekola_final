const { expect, assert } = require('chai');
const { Utils } = require('common');
require('models');
const { MariaDatabase } = require('database');
const SQLQuery = MariaDatabase.SQLQuery;

const EntityGenerator = require('../generate/EntityGenerator');

if (process.env.NODE_ENV === 'production') throw new Error('These tests will manipulate data and should NEVER be run in production environment!');

//TODO
//which methods rewrite the starting statement etc. (+comment the methods!)
//modifiers, prepare, count, update, combining query methods

before(async function () {
  this.timeout(5000);
  await EntityGenerator.cleanDatabase();
});

describe('SQLQuery', async function () {
  /**
   * Target
   * @type {SQLQuery}
   */
  let query;
  let user;
  before(async function () {
    user = await EntityGenerator.generateUser();
  });
  beforeEach(async function () {
    query = new MariaDatabase.SQLQuery(User.DATASTORE);
  });

  describe('constructor', async function () {
    it(`constructor should default to select all statement`, async function () {
      const expected = `SELECT * FROM ${User.DATASTORE}`;

      assert.equal(query.toString().replace(/\r?\n|\r/g, ' '), expected);
    });
  });

  describe('select', async function () {
    it(`select should create a select statement for a query`, async function () {
      const expected = `SELECT id FROM ${User.DATASTORE}`;
      query.select(expected);

      assert.equal(query.toString().replace(/\r?\n|\r/g, ' '), expected);
    });
  });

  describe('where', async function () {
    it(`where should support integers`, async function () {
      const expected = `SELECT * FROM ${User.DATASTORE} WHERE( id = 5 )`;
      query.where(5);

      assert.equal(query.toString().replace(/\r?\n|\r/g, ' '), expected);
    });
    it(`where should support interger like strings`, async function () {
      const expected = `SELECT * FROM ${User.DATASTORE} WHERE( id = 5 )`;
      query.where('5');

      assert.equal(query.toString().replace(/\r?\n|\r/g, ' '), expected);
    });
    it(`where should support objects`, async function () {
      const expected = `SELECT * FROM ${User.DATASTORE} WHERE( id = ? AND username = ? )`;
      query.where({ id: 5, username: 'test' });

      assert.equal(query.toString().replace(/\r?\n|\r/g, ' '), expected);
      assert.equal(query.params[0], 5);
      assert.equal(query.params[1], 'test');
    });
    it(`where should support arrays of objects`, async function () {
      const expected = `SELECT * FROM ${User.DATASTORE} WHERE( (id = ?) OR (username = ?) )`;
      query.where([{ id: 5 }, { username: 'test' }]);

      assert.equal(query.toString().replace(/\r?\n|\r/g, ' '), expected);
      assert.equal(query.params[0], 5);
      assert.equal(query.params[1], 'test');
    });
    it(`where should stack where statements with the current join word`, async function () {
      const expected = `SELECT * FROM users WHERE( id = 5 ) AND( username = ? )`;
      query.where(5);
      query.where({ username: 'test' });

      assert.equal(query.toString().replace(/\r?\n|\r/g, ' '), expected);
      assert.equal(query.params[0], 'test');
    });
  });

  describe('insert', async function () {
    it(`insert should create insert statement for a query`, async function () {
      const expected = `INSERT INTO ${User.DATASTORE} SET`;
      query.insert(`SET`);

      assert.equal(query.toString().replace(/\r?\n|\r/g, ' '), expected);
    });
  });

  describe('values', async function () {
    it(`values shoud define values used in an insert query`, async function () {
      const expected = `INSERT INTO users SET username = ?, password = ?, givenName = ?, familyName = ?, email = ?, passwordResetToken = ?`;
      let user = await EntityGenerator.generateUser({ save: false });

      query.insert(`SET`);
      query.values(user.serialize());

      assert.equal(query.toString().replace(/\r?\n|\r/g, ' '), expected);
      assert.lengthOf(query.params, 6);
    });
  });

  describe('join', async function () {
    it(`join should create a join statement for a query`, async function () {
      const expected = `SELECT * FROM ${User.DATASTORE} INNER JOIN ${Permission.DATASTORE} ON permissions.user = users.id`
      query.join('INNER JOIN', User.DATASTORE, 'id', Permission.DATASTORE, 'user');

      assert.equal(query.toString().replace(/\r?\n|\r/g, ' '), expected);
    });
  });

  describe('delete', async function () {
    it(`delete should create a delete statement for a query`, async function () {
      const expected = `DELETE FROM users`;
      query.delete();

      assert.equal(query.toString().replace(/\r?\n|\r/g, ' '), expected);
    });
    it(`delete should throw if query already had a starting statement`, async function () {
      query.join('INNER JOIN', User.DATASTORE, 'id', Permission.DATASTORE, 'user');
      let err;
      try {
        query.delete()
      } catch (error) {
        err = error;
      }
      assert.isDefined(err);
      assert.equal(err.message, `Did not start a delete query with a delete() call`);
    });
  });

  describe('count', async function () {
    it(`count should create select count statement for a query`, async function () {
      const expected = `SELECT COUNT(*) as count FROM users`;
      query.count();

      assert.equal(query.toString().replace(/\r?\n|\r/g, ' '), expected);
    });
    it(`count should override current start statement if there is one`, async function () {
      const expected = `SELECT COUNT(*) as count FROM users`;
      query.update(`SET username = 'test'`);
      assert.equal(query._start[0], `UPDATE ${User.DATASTORE}`);
      query.count();
      assert.equal(query._start[0], expected);
    });
  });

  describe('prepare', async function () {
    it(`prepare should prepare the query for execution`, async function () {
      const expected = `SELECT id FROM ${User.DATASTORE} WHERE( username = ? )`;
      query.select(`SELECT id FROM ${User.DATASTORE}`)
        .where({ username: 'test' });

      const [queryObject, params] = query.prepare();
      assert.equal(queryObject.sql.toString().replace(/\r?\n|\r/g, ' '), expected)
      assert.lengthOf(params, 1);
    });
  });

  describe('Combining query methods', async function () {
    it(`should create valid queries`, async function () {
      const expected = `SELECT id FROM ${User.DATASTORE} WHERE( id > 5 ) OR( username = ? ) AND( passwordResetToken IS NULL ) ORDER BY id ASC`;
      query.select(`SELECT id FROM ${User.DATASTORE}`)
        .where(`id > 5`)
        .or()
        .where({ username: 'test' })
        .and()
        .where('passwordResetToken IS NULL')
        .modifiers('ORDER BY id ASC');

      assert.equal(query.toString().replace(/\r?\n|\r/g, ' '), expected);
      assert.lengthOf(query.params, 1);
    });
  });

});