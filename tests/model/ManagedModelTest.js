const { expect, assert } = require('chai');
const { Utils } = require('common');
require('models');

const EntityGenerator = require('../generate/EntityGenerator');
const ManagedModel = require('../../models/ManagedModel');

if (process.env.NODE_ENV === 'production') throw new Error('These tests will manipulate data and should NEVER be run in production environment!');

//TODO deny, findSupervisors, insert, permitted

before(async function () {
  this.timeout(5000);
  await EntityGenerator.cleanDatabase();
});

describe('ManagedModel:', async function () {
  /**
   * Target
   * @type {Evaluation}
   */
  let evaluation;
  let user;
  before(async function () {
    user = await EntityGenerator.generateUser();
  });
  beforeEach(async function () {
    evaluation = new Evaluation({ name: EntityGenerator.randomString() });
  });

  describe('save', async function () {
    it('save should throw if the object has no locked status or permission objects attached to it', async function () {
      assert.instanceOf(evaluation, ManagedModel);
      let error;
      try {
        await evaluation.save();
      } catch (err) {
        error = err;
      }
      assert.isDefined(error);
      assert.equal(error.message, 'Permissions must be set before inserting a new ManagedRecord'); //compare message/ error type
      assert.isUndefined(evaluation.id);
    });
    it(`save should pass if target has locked status, but no permissions`, async function () {
      evaluation.lock();
      assert.lengthOf(evaluation.permissions, 0);
      let error;
      try {
        await evaluation.save();
      } catch (err) {
        error = err;
      }
      assert.isUndefined(error);
    });
    it('save should save both the entity and related permissions', async function () {
      assert.isUndefined(evaluation.id);
      let permission = new Permission().to(user).for(evaluation).allow(Permission.ADMIN);
      assert.isUndefined(permission.id);
      evaluation.permissions.push(permission);
      await evaluation.save();

      assert.isDefined(evaluation.id);
      permission = evaluation.permissions[0];
      assert.isDefined(permission.id);
    });
  });

  describe('allow', async function () {
    it('allow should create a valid permission object with given permissions', async function () {
      assert.instanceOf(evaluation.permissions, Array);
      assert.lengthOf(evaluation.permissions, 0);

      evaluation.allow(user, Permission.ADMIN);

      assert.lengthOf(evaluation.permissions, 1)
      const permission = evaluation.permissions[0];
      assert.instanceOf(permission, Permission);
      assert.equal(permission.mask, Permission.ADMIN);
    });
    it(`allow should attach user and target objects to the created permission`, async function () {
      evaluation.allow(user, Permission.ADMIN);
      const permission = evaluation.permissions[0];

      assert.instanceOf(permission.user, User);
      assert.equal(permission.user.id, user.id);
      assert.instanceOf(permission.target, Evaluation);
      assert.equal(permission.target.id, evaluation.id);
    });
    it(`allow should support string based representations of permission mask`, async function () {
      const expected = Permission.decode('edit', 'delete');
      evaluation.allow(user, 'edit', 'delete');
      const actual = evaluation.permissions[0].mask;
      assert.equal(expected, actual);
    });
  });

  describe('myPermissions', async function () {
    it('myPermissions should return all related permissions as valid Permission objects', async function () {
      evaluation.permissions.push(new Permission().to(user).for(evaluation).allow(Permission.ADMIN));
      await evaluation.lock().save()
      const expected = await new Permission().to(user).for(evaluation).allow(Permission.ADMIN).save();

      const actual = await evaluation.myPermissions;
      assert.instanceOf(actual, Array);
      assert.lengthOf(actual, 1);
      const permission = actual[0];
      assert.instanceOf(permission, Permission);
      assert.equal(permission.user.id, user.id);
      assert.equal(permission.target.id, evaluation.id);
    });
  });

  describe('delete', async function () {
    it(`delete should also remove related permissions`, async function () {
      evaluation = await EntityGenerator.generateEvaluation({ allow: true });
      const query = { targetModel: 'Evaluation', targetId: evaluation.id };
      let permission = await Permission.find(query);
      assert.isNotNull(permission);

      await evaluation.delete();
      permission = await Permission.find(query);
      assert.isNull(permission);
    });
  });
});
