const { expect, assert } = require('chai');
const { Utils } = require('common');
require('models');

const EntityGenerator = require('../generate/EntityGenerator');
const { MariaDatabase } = require('database');
if (process.env.NODE_ENV === 'production') throw new Error('These tests will manipulate data and should NEVER be run in production environment!');


describe('User Model', async function () {
  this.timeout(5000);
	/**
	 * Target
	 */
  let user
  let survey, evaluation, group;
  before(async function () {
    user = await EntityGenerator.generateUser({});
    evaluation = await EntityGenerator.generateEvaluation({ allow: user, });
    survey = await EntityGenerator.generateSurvey({ evaluation: evaluation.id, allow: user, });
    group = await EntityGenerator.generateGroup({ allow: user, });
  });
  beforeEach(async function () {
    user = await EntityGenerator.generateUser({ group: group.id, });
    await group.allow(user, Permission.decode('view')).save();
  });

  describe('myPermissions', async function () {
    it('myPermissions should return all related permissions as valid Permission objects', async function () {
      user = await EntityGenerator.generateUser();
      let permissions = await user.myPermissions;
      assert.instanceOf(permissions, Array);
      expect(permissions).to.have.lengthOf(0);

      await survey.allow(user, Permission.ADMIN).save();
      permissions = await user.myPermissions;

      expect(permissions).to.have.lengthOf(1);
      const permission = permissions[0];
      assert.instanceOf(permission, Permission);
      assert.instanceOf(permission.user, User);
      assert.equal(user.id, permission.user.id);
      assert.instanceOf(permission.target, Survey);
      assert.equal(survey.id, permission.target.id);
      assert.equal(permission.mask, Permission.ADMIN);
    });
  });

  describe('myUserGroup', async function () {
    it(`myUserGroups should return user's current group`, async function () {
      expect(user.userGroup).to.be.a('number');
      let group = await user.myUserGroup;
      expect(group).to.be.an.instanceOf(UserGroup);
      expect(user.userGroup).to.be.a('number');
      assert.equal(group.id, user.userGroup);
    });
  });

  describe('myGroups', async function () {
    it(`myGroups should return all groups that user has at least view permission with`, async function () {
      user = await EntityGenerator.generateUser();
      let actual = await user.myGroups;
      assert.instanceOf(actual, Array);
      expect(actual).to.have.lengthOf(0);

      const unexpected = [
        await EntityGenerator.generateGroup({ allow: true }),
        await EntityGenerator.generateGroup({ allow: user, mask: 0 })
      ];
      const expected = [
        await EntityGenerator.generateGroup({ allow: user, mask: Permission.decode('view') }),
        await EntityGenerator.generateGroup({ allow: user, mask: Permission.decode('edit') }),
        await EntityGenerator.generateGroup({ allow: user, mask: Permission.ADMIN })
      ];

      actual = await user.myGroups;
      expect(actual).to.have.lengthOf(expected.length);
      for (const group of actual) {
        assert.instanceOf(group, UserGroup);
        assert.isTrue(expected.some(expectedGroup => expectedGroup.id === group.id));
      }
    });
  });

  describe('mySessions', async function () {
    it(`mySessions should return user's surveySessions`, async function () {
      let surveySessions = await user.mySessions;
      assert.instanceOf(surveySessions, Array);
      expect(surveySessions).to.have.lengthOf(0);

      const expected = await EntityGenerator.generateSurveySession({ user: user.id, group: user.userGroup, survey: survey.id });
      surveySessions = await user.mySessions;
      expect(surveySessions).to.have.lengthOf(1);
      const actual = surveySessions[0];

      assert.equal(actual.id, expected.id);
      assert.instanceOf(actual, SurveySession);
    });
  });

  describe('myEvaluations', async function () {
    it(`myEvaluations should return evaluations that user's groups are a part of`, async function () {
      let evaluations = await user.myEvaluations;
      assert.instanceOf(evaluations, Array);
      expect(evaluations).to.have.lengthOf(0);

      const unexpected = await EntityGenerator.generateEvaluation({ allow: user });
      await Evaluation.DB.insert(MariaDatabase.JOIN_EVALUATIONS_USERGROUPS, { userGroup_id: group.id, evaluation_id: evaluation.id }); // await evaluation.addUserGroups(group);
      evaluations = await user.myEvaluations;

      expect(evaluations).to.have.lengthOf(1);
      const actual = evaluations[0];
      assert.instanceOf(actual, Evaluation);
      assert.equal(actual.id, evaluation.id);
    });
  });

  describe('mySupervisedEvaluations', async function () {
    it(`mySupervisedEvaluations should return evaluations that users have edit permission with`, async function () {
      let evaluations = await user.mySupervisedEvaluations;
      assert.instanceOf(evaluations, Array);
      expect(evaluations).to.have.lengthOf(0);

      const unexpected = await evaluation.allow(user, Permission.decode('view')).save();
      const expected = await EntityGenerator.generateEvaluation({ allow: user }); // results in ADMIN permission
      await evaluation.addUserGroups(group);

      evaluations = await user.mySupervisedEvaluations;
      expect(evaluations).to.have.lengthOf(1);
      const actual = evaluations[0];
      assert.instanceOf(actual, Evaluation);
      assert.isTrue(actual.id === expected.id);
    });
  });

  describe('myTemplates', async function () {
    it(`myTemplates should return templates that user has admin permission with`, async function () {
      const expected = await EntityGenerator.generateTemplate({ allow: user });
      const unexpected = await EntityGenerator.generateTemplate({ allow: true });

      const actual = await user.myTemplates;

      assert.instanceOf(actual, Array);
      expect(actual).to.have.lengthOf(1);
      assert.instanceOf(actual[0], SurveyTemplate);
      assert.equal(actual[0].id, expected.id);
    });
  });

  describe('attachPermissions', async function () {
    it(`attachPermissions should attach permissions to managed model objects`, async function () {
      const objects = [
        await EntityGenerator.generateSurvey({ allow: user, mask: Permission.decode('view') }),
        await EntityGenerator.generateSurvey({ allow: user, mask: Permission.ADMIN }),
        await EntityGenerator.generateSurvey({ allow: true })
      ];
      await user.attachPermissions(objects);
      for (const survey of objects) assert.instanceOf(survey, Survey);

      let survey = objects[0];
      expect(survey).to.have.property('canView');
      assert.isTrue(survey.canView);
      expect(survey).to.have.property('canEdit');
      assert.isFalse(survey.canEdit);
      expect(survey).to.have.property('canDelete');
      assert.isFalse(survey.canDelete);

      survey = objects[1];
      expect(survey).to.have.property('canView');
      assert.isTrue(survey.canView);
      expect(survey).to.have.property('canEdit');
      assert.isTrue(survey.canEdit);
      expect(survey).to.have.property('canDelete');
      assert.isTrue(survey.canDelete);

      survey = objects[2];
      expect(survey).to.not.have.property('canView');
      expect(survey).to.not.have.property('canEdit');
      expect(survey).to.not.have.property('canDelete');
    });
  });

  describe('match', async function () {
    it(`match should throw if provided with an invalid password`, async function () {
      const invalidPassword = '123';
      let error;
      try {
        await User.match(user.username, invalidPassword);
      } catch (err) {
        error = err;
      }
      assert.isDefined(error);
      assert.equal(error.message, 'Incorrect password');
    });
    it(`match should throw if provided with an invalid username`, async function () {
      const unpersisted = await EntityGenerator.generateUser({ noSave: true });
      const password = 'test';
      let error;
      try {
        await User.match(unpersisted.username, password);
      } catch (err) {
        error = err;
      }
      assert.isDefined(error);
      assert.equal(error.message, 'Username does not exist');
    });
    it(`match should return user if provided username and password match a stored object`, async function () {
      const password = 'test';
      const actual = await User.match(user.username, password);
      assert.isNotNull(actual);
      assert.instanceOf(actual, User);
      assert.equal(actual.id, user.id);
    });
  });

  describe('create', async function () {
    it(`create should create a new user and add permissions to an existing group if given one`, async function () {
      const expected = await EntityGenerator.generateUser({ noSave: true });
      const password = 'test';
      const data = {
        password: password,
        confirmation: password,
        email: expected.email,
        username: expected.username,
        familyName: expected.familyName,
        givenName: expected.givenName
      };

      const actual = await User.create(data, group.id);

      assert.instanceOf(actual, User);
      expect(actual).to.have.property('id');
      assert.equal(expected.username, actual.username);
      assert.equal(expected.email, actual.email);
      assert.equal(group.id, actual.userGroup);

      const match = await User.match(expected.username, password);
      assert.instanceOf(match, User);

      const permissions = await Permission.where({ user: actual.id, targetModel: 'UserGroup', targetId: group.id });
      expect(permissions).to.have.lengthOf(1);
      assert.equal(permissions[0].mask, Permission.decode('view'));
    });
    it(`create should create a new group if provided with more than an id`, async function () {
      const expected = await EntityGenerator.generateUser({ noSave: true });
      const password = 'test'
      const data = {
        password: password,
        confirmation: password,
        email: expected.email,
        username: expected.username,
        familyName: expected.familyName,
        givenName: expected.givenName
      };

      let groupData = await EntityGenerator.generateGroup({ noSave: true });
      groupData = { name: groupData, description: groupData.description };

      const actual = await User.create(data, groupData);
      assert.instanceOf(actual, User);
      expect(actual).to.have.property('id');
      expect(actual).to.have.property('userGroup');
      assert.equal(expected.username, actual.username);
      assert.equal(expected.email, actual.email);

      const match = await User.match(expected.username, password);
      assert.instanceOf(match, User);

      let group = await UserGroup.find({ id: actual.userGroup });
      assert.instanceOf(group, UserGroup);

      const permissions = await Permission.where({ user: actual.id, targetModel: 'UserGroup', targetId: actual.userGroup });
      expect(permissions).to.have.lengthOf(1);
      assert.equal(permissions[0].mask, Permission.ADMIN);
    });
  });

  describe('paginationSearch', async function () {
    it(`paginationSearch should return page count and users of the provided group limited by the page limit`, async function () {
      const group = await EntityGenerator.generateGroup({ allow: user });
      const expected = [user];
      for (let i = 0; i < 5; i++) expected.push(await EntityGenerator.generateUser({}));
      for (const user of expected) group.allow(user, Permission.decode('view'));
      await group.save();

      let { users, pageCount } = await User.paginationSearch(1, 10, group.id);
      assert.equal(pageCount, 1);
      expect(users).to.have.lengthOf(expected.length);
      for (const user of users) {
        assert.isTrue(expected.some(expectedUser => expectedUser.id === user.id));
      }

      for (let i = 0; i < expected.length; i++) {
        const page = i + 1;
        let { users, pageCount } = await User.paginationSearch(page, 1, group.id);
        expect(users).to.have.lengthOf(1);
        const user = users[0];
        assert.instanceOf(user, User);
        assert.equal(user.id, expected[i].id);
        assert.equal(pageCount, expected.length);
      }
    });
  });
});