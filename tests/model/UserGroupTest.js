const { expect, assert } = require('chai');
const { Utils } = require('common');
require('models');

const EntityGenerator = require('../generate/EntityGenerator');
const { MariaDatabase } = require('database');
if (process.env.NODE_ENV === 'production') throw new Error('These tests will manipulate data and should NEVER be run in production environment!');

before(async function () {
  await EntityGenerator.cleanDatabase();
});

describe('UserGroup Model', async function () {
  /**
   * target
   */
  let group;
  let user, evaluation;
  before(async function () {
    user = await EntityGenerator.generateUser();
    evaluation = await EntityGenerator.generateEvaluation({ allow: user });
  });
  beforeEach(async function () {
    group = await EntityGenerator.generateGroup({ allow: user });
  });

  describe('myUsers', async function () {
    it('myUsers should return all users who have at least view permission to the group', async function () {
      const expected = [user];
      let actual = await group.myUsers;
      assert.instanceOf(actual, Array);
      expect(actual).to.have.lengthOf(expected.length);

      for (let i = 0; i < 2; i++) {
        const user = await EntityGenerator.generateUser();
        group.allow(user, Permission.decode('view'));
        expected.push(user);
      }
      await group.save();

      actual = await group.myUsers;

      expect(actual).to.have.lengthOf(expected.length);
      assert.isTrue(actual.every(user => user instanceof User));
      for (const user of actual) assert.isTrue(expected.some(expectedUser => expectedUser.id === user.id));
    });
  });

  describe('myEvaluations', async function () {
    it(`myEvaluations should return empty array if group is not connected to any evaluations`, async function () {
      const actual = await group.myEvaluations;
      assert.isTrue(actual instanceof Array);
      expect(actual).to.have.lengthOf(0);
    });
    it('myEvaluations should return evaluations connected to the group via join table', async function () {
      await Evaluation.DB.insert(MariaDatabase.JOIN_EVALUATIONS_USERGROUPS, { usergroup_id: group.id, evaluation_id: evaluation.id });
      const actual = await group.myEvaluations;
      assert.isTrue(actual instanceof Array);
      expect(actual).to.have.lengthOf(1);
      expect(actual[0]).to.be.an.instanceof(Evaluation);
      assert.equal(evaluation.id, actual[0].id);
    });
  });

  describe('removeUsers', async function () {
    it(`removeUsers should delete users' permission to the group`, async function () {
      let permissions = await Permission.where({ targetModel: 'UserGroup', targetId: group.id });
      expect(permissions).to.have.lengthOf(1);
      assert.equal(permissions[0].userId, user.id);
      await UserGroup.removeUsers(group, user);

      permissions = await Permission.where({ targetModel: 'UserGroup', targetId: group.id });
      expect(permissions).to.have.lengthOf(0);
    });
    it(`removeUsers should delete the group if all the permissions are removed`, async function () {
      const user2 = await EntityGenerator.generateUser();
      await group.allow(user2, Permission.ADMIN).save();
      await UserGroup.removeUsers(group, user);

      group = await UserGroup.find({ id: group.id });
      assert.isNotNull(group);

      await UserGroup.removeUsers(group, user2);
      group = await UserGroup.find({ id: group.id });
      assert.isNull(group);
    });
  });

  describe('attachRole', async function () {
    it(`attachRole should attach role information to the given user objects`, async function () {
      const user2 = await EntityGenerator.generateUser();
      const user3 = await EntityGenerator.generateUser();
      group.allow(user2, Permission.decode('view'));
      group.allow(user3, Permission.decode('view', 'edit'));
      await group.save();

      const users = [user, user2, user3]
      await group.attachRole(users);
      assert.isTrue(users.every(user => user.role));
      assert.equal(user.role, 'Admin');
      assert.equal(user2.role, 'Member');
      assert.equal(user3.role, 'Admin');
    });
  });
});