const { expect, assert } = require('chai');
const { Utils } = require('common');
require('models');
const { MariaDatabase } = require('database');

const EntityGenerator = require('../generate/EntityGenerator');

if (process.env.NODE_ENV === 'production') throw new Error('These tests will manipulate data and should NEVER be run in production environment!');

before(async function () {
  this.timeout(5000);
  await EntityGenerator.cleanDatabase();
});

describe('Evaluation Model', async function () {
  /**
   * Target
   * @type {Evaluation}
   */
  let evaluation;
  let user, users, groups;
  before(async function () {
    user = await EntityGenerator.generateUser();
    users = [user];
    for (let i = 0; i < 3; i++) users.push(await EntityGenerator.generateUser());
    groups = [];
    for (let i = 0; i < 3; i++) groups.push(await EntityGenerator.generateGroup({ allow: user }));
  });
  beforeEach(async function () {
    evaluation = await EntityGenerator.generateEvaluation({ allow: user });
  });

  describe('myUsers', async function () {
    it(`myUsers should return users that belong to any of the evaluation's groups`, async function () {
      let actual = await evaluation.myUsers;
      assert.isTrue(actual instanceof Array);
      assert.lengthOf(actual, 0);

      const unexpected = await EntityGenerator.generateUser();
      const expected = [user];
      for (let i = 0; i < 2; i++) expected.push(await EntityGenerator.generateUser());
      const group = await EntityGenerator.generateGroup({ allow: expected });
      await Evaluation.DB.insert(MariaDatabase.JOIN_EVALUATIONS_USERGROUPS, { userGroup_id: group.id, evaluation_id: evaluation.id });

      actual = await evaluation.myUsers;
      assert.lengthOf(actual, expected.length);
      assert.isTrue(actual.every(user => user instanceof User));
      for (const user of actual) assert.isTrue(expected.some(expectedUser => expectedUser.id === user.id));
    });
    it(`myUsers should ignore users with zero permission to the group(?)`, async function () {
      const expected = user;
      const unexpected = await EntityGenerator.generateUser();
      const group = await EntityGenerator.generateGroup({ allow: expected });
      await group.allow(unexpected, 0).save();

      await Evaluation.DB.insert(MariaDatabase.JOIN_EVALUATIONS_USERGROUPS, { userGroup_id: group.id, evaluation_id: evaluation.id });
      const actual = await evaluation.myUsers;
      assert.lengthOf(actual, 1);
      assert.equal(actual[0].id, expected.id);
    });
    it(`myUsers should ignore users with zero permission to the evaluation`, async function () {
      const expected = user;
      const unexpected = await EntityGenerator.generateUser();
      const group = await EntityGenerator.generateGroup({ allow: [expected, unexpected] });
      await evaluation.allow(unexpected, 0).save();

      await Evaluation.DB.insert(MariaDatabase.JOIN_EVALUATIONS_USERGROUPS, { userGroup_id: group.id, evaluation_id: evaluation.id });
      const actual = await evaluation.myUsers;
      assert.lengthOf(actual, 1);
      assert.equal(actual[0].id, expected.id);
    });
  });

  describe('addUsers', async function () {
    it('addUsers should add permission for users with the given mask', async function () {
      const expectedMask = Permission.decode('view');
      const user = await EntityGenerator.generateUser();
      let permissions = await user.myPermissions;
      assert.lengthOf(permissions, 0);

      await evaluation.addUsers([user], expectedMask);

      permissions = await user.myPermissions;
      assert.lengthOf(permissions, 1);
      assert.equal(permissions[0].targetId, evaluation.id);
      assert.equal(permissions[0].mask, expectedMask);
    });
    it('addUsers should not override any higher existing permissions', async function () {
      const expectedMask = Permission.ADMIN;
      const user = await EntityGenerator.generateUser();

      await evaluation.addUsers([user], expectedMask);
      await evaluation.addUsers([user], Permission.decode('view'));
      const permissions = await user.myPermissions;

      assert.lengthOf(permissions, 1);
      assert.equal(permissions[0].targetId, evaluation.id);
      assert.equal(permissions[0].mask, expectedMask);
    });
  });

  describe('removeUsers', async function () {
    it('removeUsers should create permission with 0 mask to given users', async function () {
      const expectedMask = 0;
      const user = await EntityGenerator.generateUser();

      await evaluation.removeUsers([user]);
      const permissions = await user.myPermissions;

      assert.lengthOf(permissions, 1);
      assert.equal(permissions[0].targetId, evaluation.id);
      assert.equal(permissions[0].mask, expectedMask);
    });
  });

  describe('myUserGroups', async function () {
    it('myUserGroups should return all the related groups', async function () {
      const expected = await EntityGenerator.generateGroup({ allow: true });
      let groups = await evaluation.myUserGroups;
      assert.isTrue(groups instanceof Array);
      assert.equal(groups.length, 0);

      await evaluation.db.insert(MariaDatabase.JOIN_EVALUATIONS_USERGROUPS, { usergroup_id: expected.id, evaluation_id: evaluation.id });
      groups = await evaluation.myUserGroups;
      assert.equal(groups.length, 1);
      const actual = groups[0];
      assert.isTrue(actual instanceof UserGroup);
      assert.equal(actual.id, expected.id);
    });
  });

  describe('addGroups', async function () {
    it('addGroups should add groups to evaluation', async function () {
      const expectedCount = groups.length;
      let actual = await evaluation.myUserGroups;
      assert.lengthOf(actual, 0);

      await evaluation.addUserGroups(groups);
      actual = await evaluation.myUserGroups;

      assert.lengthOf(actual, expectedCount);
      groups.forEach(expected => assert.isTrue(actual.some(actual => actual.id === expected.id)));
    });
    it('addGroups should not add the same group multiple times', async function () {
      const expectedCount = groups.length;
      await evaluation.addUserGroups(groups);
      await evaluation.addUserGroups(groups);

      actual = await evaluation.myUserGroups;
      assert.lengthOf(actual, expectedCount);
      groups.forEach(expected => assert.isTrue(actual.some(actual => actual.id === expected.id)));
    });
  });

  describe('removeGroups', async function () {
    it('removeGroups should remove specified groups', async function () {
      let groups = [];
      for (let i = 0; i < 3; i++) groups.push(await EntityGenerator.generateGroup({ allow: true }));

      for (const group of groups) await evaluation.db.insert(MariaDatabase.JOIN_EVALUATIONS_USERGROUPS, { usergroup_id: group.id, evaluation_id: evaluation.id });

      const unexpected = groups.splice(0, 1);
      const expected = groups;
      await Evaluation.removeUserGroups(evaluation.id, unexpected);
      const actual = await evaluation.myUserGroups;

      assert.lengthOf(actual, expected.length);
      for (const group of actual) assert.isTrue(expected.some(expected => expected.id === group.id));
    });
  });
});
