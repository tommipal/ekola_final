const { expect, assert } = require('chai');
const { Utils } = require('common');
require('models');

const EntityGenerator = require('../generate/EntityGenerator');
const MariaModel = require('../../models/MariaModel');

if (process.env.NODE_ENV === 'production') throw new Error('These tests will manipulate data and should NEVER be run in production environment!');

before(async function () {
  this.timeout(5000);
  await EntityGenerator.cleanDatabase();
});

describe('MariaModel:', async function () {
  let user;
  before(async function () {
  });
  beforeEach(async function () {
  });

  describe('save', async function () {
    it('save should save the object and update the id field of the object', async function () {
      user = await EntityGenerator.generateUser({ noSave: true });

      expect(user).to.be.an.instanceof(MariaModel)
      expect(user).not.to.have.property('id');
      await user.save();
      expect(user).to.have.property('id');
      assert.isNumber(user.id);
    });
    it('save should save changes to an existing entity', async function () {
      user = await EntityGenerator.generateUser();

      const expectedUsername = (await EntityGenerator.generateUser({ noSave: true })).username;
      user.username = expectedUsername;
      await user.save();
      const actual = await User.find({ id: user.id });

      assert.equal(actual.username, expectedUsername);
    });
  });

  describe('initialize', async function () {
    it(`initialize should retrieve an existing entity matching the given data if one exists`, async function () {
      const expected = await EntityGenerator.generateUser();
      const actual = await User.initialize({ email: expected.email, username: expected.username });
      assert.instanceOf(actual, User);
      assert.equal(actual.id, expected.id);
      assert.equal(actual.username, expected.username);
    });
    it(`initialize should return new object if the provided data doesn't match any existing object`, async function () {
      const expected = await EntityGenerator.generateUser({ noSave: true });
      const actual = await User.initialize({ email: expected.email, username: expected.username });
      assert.isUndefined(actual.id);
      assert.equal(actual.email, expected.email);
      assert.equal(actual.username, expected.username);
    });
  });

  describe('constructor', async function () {
    it(`constructor should always extract id from the given data if it contains one`, async function () {
      const expected = 5;
      const data = { id: expected }
      let user = new User(data);
      expect(user).to.have.property('id');
      assert.equal(user.id, expected);

      user = new User(expected);
      expect(user).to.have.property('id');
      assert.equal(user.id, expected);
    });
  });

  describe('parseWhere', async function () {
    it(`parseWhere should make the query based on id if provided only with a number`, async function () {
      const expected = 'id = 1';
      const [query, params] = User.parseWhere(1);
      assert.equal(query, expected);
      assert.isUndefined(params);
    });
    it(`parseWhere should make query based on ids if provided an array with numbers`, async function () {
      const expected = 'id IN (?)';
      const expectedParams = [1, 2, 3];
      const [query, params] = User.parseWhere(expectedParams);
      assert.equal(query, expected);
      assert.deepEqual(params, expectedParams);
    });
    it(`parseWhere should return a plain object if given one`, async function () {
      const expected = { id: 1 };
      const [query, params] = User.parseWhere(expected);
      assert.deepEqual(query, expected);
      assert.isUndefined(params);
    });
    it(`parseWhere should return custom query if provided with a string`, async function () {
      const expected = 'familyName = ?'; //should work also without the separate params
      const expectedParams = [1]
      const [query, params] = User.parseWhere(expected, expectedParams);
      assert.equal(query, expected);
      assert.equal(params, expectedParams);
    });
    it(`parseWhere should throw if provided query-params pair doesn't match any of the supported ones`, async function () {
      const user = await EntityGenerator.generateUser();
      let err;
      try {
        User.parseWhere(user);
      } catch (error) {
        err = error;
      }
      assert.isDefined(err);
      assert.equal(err.message, 'Where parameter was not an array, plain object, string or integer');
    });
    it(`parseWhere should return empty array if no parameters is provided`, async function () {
      const expected = [];
      const actual = User.parseWhere();
      assert.deepEqual(actual, expected)
    });
  });

  describe('get', async function () {
    it(`get should populate the record if it has an id`, async function () {
      const expected = await EntityGenerator.generateUser();
      const actual = new User({ id: expected.id });

      const match = await actual.get();
      assert.isTrue(match);
      assert.deepEqual(Utils.filter(actual.serialize(), 'userGroup'),
        Utils.filter(expected.serialize(), 'createdAt', 'updatedAt', 'userGroup'));
    });
    it(`get should should try to loose match the record by its other properties if it has no id`, async function () {
      const expected = await EntityGenerator.generateUser();
      const actual = new User({ email: expected.email, username: expected.username });

      const match = await actual.get();
      assert.isTrue(match)
      assert.deepEqual(Utils.filter(actual.serialize(), 'userGroup'),
        Utils.filter(expected.serialize(), 'createdAt', 'updatedAt', 'userGroup'));
    });
    it(`get should not populate the record if loose matching provides zero matches`, async function () {
      const actual = new User({ id: -1 });
      const match = await actual.get();
      assert.isFalse(match);
      assert.equal(actual.id, -1);
    });
    it(`get should not populate the record if loose matching provides more than one match`, async function () {
      const unexpected = await EntityGenerator.generateUser();
      await EntityGenerator.generateUser({ familyName: unexpected.familyName });

      const actual = new User({ familyName: unexpected.familyName });
      const match = await actual.get();
      assert.isFalse(match);
      expect(actual).to.not.have.property('id');
    });
  });

  describe('find', async function () {
    it(`find should return a record matching the given query`, async function () {
      const expected = await EntityGenerator.generateUser({});
      const actual = await User.find(expected.id);
      assert.instanceOf(actual, User);
      assert.equal(actual.id, expected.id);
    });
    it(`find should return null if no record match the provided query`, async function () {
      const actual = await User.find({ id: -1 });
      assert.isNull(actual);
    });
    it(`find should return null if more than one record match provided query`, async function () {
      await EntityGenerator.generateUser({ familyName: 'duplicate' });
      await EntityGenerator.generateUser({ familyName: 'duplicate' });

      const actual = await User.find({ familyName: 'dublidate' });
      assert.isNull(actual);
    });
  });

  describe('where', async function () {
    it(`where should return all records matching the provided query`, async function () {
      const group = await EntityGenerator.generateGroup({ save: true, allow: true });
      const expected = [
        await EntityGenerator.generateUser({ group: group, save: true }),
        await EntityGenerator.generateUser({ group: group, save: true })
      ];

      const actual = await User.where(`userGroup = ?`, group.id);
      expect(actual).to.have.lengthOf(expected.length);
      assert.isTrue(actual.every(user => user instanceof User));
      assert.deepEqual(actual.map(user => user.id), expected.map(user => user.id));
    });
    it(`where should return empty array if provided query doesn't match any records`, async function () {
      const expected = [];
      const actual = await User.where({ id: -1 });
      assert.deepEqual(actual, expected);
    });
  });
  //TODO delete, count, insert, update, query, delete
});