const { expect, assert } = require('chai');
const { Utils, Logger } = require('common');
Logger.remove(Logger.transports.Console);
const moment = require('moment');
require('models');

const EntityGenerator = require('../generate/EntityGenerator');
const MariaModel = require('../../models/MariaModel');

if (process.env.NODE_ENV === 'production') throw new Error('These tests will manipulate data and should NEVER be run in production environment!');

before(async function(){
  this.timeout(5000);
  await EntityGenerator.cleanDatabase();
});

describe('Survey Model', async function () {
  /**
   * Target
   * @type {Survey}
  */
  let survey;
  let user, evaluation, group;
  before(async function () {
    user = await EntityGenerator.generateUser();
    group = await EntityGenerator.generateGroup({ allow: user });
    evaluation = await EntityGenerator.generateEvaluation({ allow: user, groups: [group.id] });
  });
  beforeEach(async function () {
    survey = await EntityGenerator.generateSurvey({ evaluation: evaluation.id, allow: user });
  });

  describe('myEvaluation', async function () {
    it('myEvaluation should return related evaluation', async function () {
      let expected = await Evaluation.find({ id: survey.evaluation });
      let actual = await survey.myEvaluation;
      assert.isTrue(actual instanceof Evaluation);
      assert.equal(expected.id, actual.id);
    });
  });
  describe('myQuestions', async function () {
    it('myQuestions should return related questions', async function () {
      let question = await EntityGenerator.generateQuestion({ survey: survey.id });
      await question.save();

      let expected = await Question.where({ survey: survey.id });
      let actual = await survey.myQuestions;
      assert.equal(expected.length, actual.length);
      actual.forEach(question => {
        assert.isTrue(question instanceof Question);
        assert.isTrue(expected.some(expectedQuestion => question.id === expectedQuestion.id));
      });
    });
  });
  describe('myLinks', async function () {
    it('myLinks should return related links', async function () {
      await EntityGenerator.generateLink({ survey: survey.id, group: group.id });
      let expected = await Link.where({ survey: survey.id });
      let actual = await survey.myLinks;
      assert.equal(expected.length, actual.length);
      actual.forEach(actual => {
        assert.isTrue(actual instanceof Link);
        assert.isTrue(expected.some(expected => actual.id === expected.id));
      });
    });
  });
  describe('mySessions', async function () {
    it('mySessions should return related sessions', async function () {
      await EntityGenerator.generateSurveySession({
        user: user.id,
        survey: survey.id,
        group: group.id,
      });
      let expected = await SurveySession.where({ survey: survey.id });
      let actual = await survey.mySessions;
      assert.isTrue(expect.length > 0);
      assert.equal(expected.length, actual.length);
      actual.forEach(actual => {
        assert.isTrue(actual instanceof SurveySession);
        assert.isTrue(expected.some(expected => actual.id === expected.id));
      });
    });
  });
  describe('myAnswers', async function () {
    it('myAnswers should return related answers', async function () {
      const question = await EntityGenerator.generateQuestion({ survey: survey.id });
      const session = await EntityGenerator.generateSurveySession({
        user: user.id,
        group: group.id,
        survey: survey.id,
      });
      await EntityGenerator.generateAnswer({
        survey: survey.id,
        group: group.id,
        question: question.id,
        session: session.id,
        criteria: question.criteria[0],
      });

      const expected = await Answer.where({ survey: survey.id });
      const actual = await survey.myAnswers;

      assert.equal(expected.length, actual.length);
      actual.forEach((actual) => {
        assert.isTrue(actual instanceof Answer);
        assert.isTrue(expected.some((expected) => actual.id === expected.id))
      });
    });
  });
  describe('save', async function () {
    it('save should also save related questions', async function () {
      const expected = [];
      for (let i = 0; i < 3; i++) expected.push(await EntityGenerator.generateQuestion({ title: `Question-${i}`, survey: survey.id, noSave: true }));
      survey.questions = expected;
      await survey.save();
      const questions = await Question.where({ survey: survey.id });
      assert.equal(questions.length, expected.length);
      questions.forEach(question => {
        expect(question).to.have.property('id');
        assert.equal(survey.id, question.survey);
        assert.isTrue(expected.some(expectedQuestion => question.title === expectedQuestion.title));
      });
    });
    it('save should also save changes to related questions', async function () {
      const expected = 'updated question';
      await EntityGenerator.generateQuestion({ survey: survey.id });
      await survey.myQuestions;
      expect(survey.questions).to.have.lengthOf(1);
      survey.questions[0].title = expected;
      await survey.save(); //why the get in question.save ?
      let actual = await Question.where({ survey: survey.id });
      expect(actual).to.have.lengthOf(1);
      assert.equal(expected, actual[0].title);
    });
    it('save(strict) should save the given questions and remove other ones', async function () {
      const question1 = await EntityGenerator.generateQuestion({ title: 'Question 1', survey: survey.id, noSave: true });
      const question2 = await EntityGenerator.generateQuestion({ title: 'Question 2', survey: survey.id, noSave: true });

      survey.questions = [question1, question2];
      await survey.save(true);
      const firstSave = await Question.where({ survey: survey.id });

      let deletedQuestion = await Question.where({ title: 'Question 2', survey: survey.id });
      assert.isTrue(deletedQuestion[0] instanceof Question);

      survey.questions = [question1];
      await survey.save(true);
      const secondSave = await Question.where({ survey: survey.id });

      deletedQuestion = await Question.where({ title: 'Question 2', survey: survey.id });

      assert.notEqual(firstSave.length, secondSave.length);
      assert.isEmpty(deletedQuestion);
    });
  });
  describe('paginationSearch', async function () {
    it('paginationSearch should return page count and surveys of the provided filter limited by the page limit', async function () {
      const newgroup = await EntityGenerator.generateGroup({ allow: user });
      const newevaluation = await EntityGenerator.generateEvaluation({ allow: user, groups: [newgroup.id] });

      const pageLimit = 2;
      let surveyCount = 5;
      let expectedPageCount = Math.ceil(surveyCount / pageLimit);
      const evaluations = [newevaluation];

      let expectedClosed = [];
      for (let i = 0; i < surveyCount; i++) {
        expectedClosed.push(await EntityGenerator.generateSurvey({ evaluation: newevaluation.id, allow: user }, false));
      }
      const actualClosed = await Survey.paginationSearch(1, pageLimit, 'closed', evaluations);

      expect(actualClosed.surveys).to.be.lengthOf(pageLimit);
      assert.equal(expectedPageCount, actualClosed.pageCount);
      actualClosed.surveys.forEach((closed) => {
        assert.instanceOf(closed, Survey);
        assert.isTrue(expectedClosed.some((expected) => closed.id === expected.id));
        assert.isFalse(closed.endDate.isAfter(closed.startDate));
      });
      let expectedActive = [];
      for (let i = 0; i < surveyCount; i++) {
        expectedActive.push(await EntityGenerator.generateSurvey({ evaluation: newevaluation.id, allow: user }, true));
      }
      const actualActive = await Survey.paginationSearch(1, pageLimit, 'active', evaluations);

      expect(actualActive.surveys).to.be.lengthOf(pageLimit);
      assert.equal(expectedPageCount, actualActive.pageCount);
      actualActive.surveys.forEach((active) => {
        assert.instanceOf(active, Survey);
        assert.isTrue(expectedActive.some((expected) => active.id === expected.id));
        assert.isTrue(active.endDate.isAfter(active.startDate));
      });
    });
  });
});